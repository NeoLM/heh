import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';

const devisData = new Mongo.Collection('devisData')

var devisSchema = new SimpleSchema({
    userId:{
        type:String,
        label:'id commercial',
    },
    clientId:{
        type:String,
        label:'id client'
    },
    path:{
        type:String,
        label:"chemin d'accès du devis",
        optional:true
    },
    status:{
        type:String,
        label:'Status du devis',
        allowedValues:['créé', 'sauvegardé', 'envoyé', 'sms', 'signé', 'refusé']
    },
    code:{
      type:Number,
      label:'Code sms de vérification',
      optional:true
    },
    data:{
        type:Object,
        label:'Données insérées dans le devis',
        blackbox:true
    },
    addedDate: {
        type: Date,
        label: "Creation date",
        autoValue: function () {
          if (this.isInsert) {
            return new Date();
          } else {
            this.unset();
          }
        }
      },
      modifiedDate: {
        type: Date,
        label: "Modification date",
        autoValue: function () {
          if (this.isUpdate) {
            return new Date();
          } else {
            this.unset();
          }
        },
        optional: true
      },
})

devisData.attachSchema(devisSchema);

export {
    devisData
}