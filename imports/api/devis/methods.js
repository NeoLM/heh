import { Meteor } from 'meteor/meteor'
import { devisData } from './devis'
import fs from 'fs';
import path from 'path';
import PdfLib from 'pdf-lib';
const { PDFDocument, StandardFonts } = PdfLib;
import { moment } from 'meteor/momentjs:moment';
import '../clients/methods'
import '../needs/methods'
import '../simulation/methods'
import { Email } from 'meteor/email'
import '../sms/methods'
require('intl')
Intl.NumberFormat = IntlPolyfill.NumberFormat;

import { paneauxPhotovoltaiques,
    ballonsThermodynamiques,
    pompeAirEau,
    paneauxPhotovoltaiquesPrix,
    batteriePhotovolataique,
    isolation
} from '../dbprice'


Meteor.methods({

    generateAndSendDevis(clientId){
        const user = Meteor.user();
        if(user.profile.active){
            const data = devis.getDataToFillDevis(clientId)
            return devis.fillDevis(data).then((pdfBytes)=>{
                const devisId = devis.saveDevis(data,clientId,user._id)
                console.log(devisId)
                devis.writeDevis(devisId,pdfBytes)
                devis.sendDevis(devisId)
                return devisId
            })
        } else {
            throw new Meteor.Error('Not allowed');
        }
    },
    getDevis(devisId){
        const {path, status} = devis.getDevis(devisId)
        if(!!path){
            const blob = fs.readFileSync(path);
            return [blob, status]
        } else {
            throw new Meteor.Error('No devis found');
        }
    },
    sendVerficationSMSDevis(devisId){
        const {clientId} = devis.getDevis(devisId)
        const {phone, name} = clients.getClient(clientId)
        const formatedPhone = phone.replace('0', '+33')
        const code = Math.floor(Math.random() * 90000) + 10000
        const message = `Bonjour ${name}, voici votre code de vérification pour la signature : ${code}`
        sms.sendVerfication(formatedPhone, message)
        devis.addVerifCode(devisId, code)
        return true
    },
    verifySmsDevis(devisId, codeSms){
        const {code, status, modifiedDate} = devis.getDevis(devisId)
        const startTime = new Date(modifiedDate)
        const actualTime = new Date()
        const delay = 2*60*1000
        if(actualTime - startTime < delay){
            if(codeSms == code.toString() && status == "sms"){
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    },
    signAndSendDevis(devisId, signature){
        const {data} = devis.getDevis(devisId)
        if(!!data){
            data.signature = signature
            return devis.fillDevis(data).then((pdfBytes)=>{
                console.log(devisId)
                devis.writeDevis(devisId,pdfBytes,true)
                devis.sendDevis(devisId,true)
                return devisId
            })
        } else {
            throw new Meteor.Error('Not allowed');
        }
    }
})

// @describ : Server API for devis manipulation
devis = (function () {

    var api = {};

    // @describ : creating a devis document
    // @Params : json object with all required fields
    // @result : new id of the document created
    api.newDevis = (data) => {
      try {
        var id = devisData.insert({...data});
        return id;
      } catch (e) {
        throw new Meteor.Error('Error new devis',e.message);
      }
    };

    // @describ : getting a devis document
    // @Params : id of the document 
    // @result : content of the document fetched
    api.getDevis = (devisId) => {
      try {
        return devisData.findOne({ _id:devisId });
      } catch (e) {
        throw new Meteor.Error('Error get devis',e.message);
      }
    };

    // @describ : getting a devis document
    // @Params : id of the document 
    // @result : content of the document fetched
    api.getDevisByClientId = (clientId) => {
        try {
          return devisData.find({ clientId:clientId }, {sort: {modifiedDate: -1}}).fetch()[0];
        } catch (e) {
          throw new Meteor.Error('Error get devis',e.message);
        }
      };

    // @describ : deleting a devis document
    // @Params : id of the devis document(string)
    // @result : id of the document deleted
    api.deleteDevis = (devisId) => {
        try {
          return devisData.remove({ _id:devisId });
        } catch (e) {
          throw new Meteor.Error('Error delete devis');
        }
    };

    // @describ : add verification code
    // @Params : id of the devis document(string)
    // @result : id of the document 
    api.addVerifCode = (devisId,code) => {
        try {
          return devisData.update({ _id:devisId }, {$set:{code:code, status:'sms'}});
        } catch (e) {
          throw new Meteor.Error('Error add verification code devis');
        }
    };

    api.getDataToFillDevis = (clientId) =>{
        const data = {}

        data.number = 'DE0000'
        data.customer ='0000'
        data.date = moment().format('DD/MM/YYYY')
        data.expireDate = moment().format('DD/MM/YYYY')

        const clientData = clients.getClient(clientId)
        data.name = `${clientData.name} ${clientData.lastname}`
        data.address1 = clientData.adress
        data.address2 = `${clientData.zipCode} ${clientData.city} France`
        const needData = needs.getNeedByClientId(clientId)
        const simuData = simulations.getSimulationByClientId(clientId)
        data.items = []

        const code1 = {
            code1HT:0,
            code1TVA:5.5,
            code1TTC:0
        }
        const code2 = {
            code2HT:0,
            code2TVA:10,
            code2TTC:0
        }
        const code3 = {
            code3HT:0,
            code3TVA:20,
            code3TTC:0
        }

        const addTvaItem = (newItem) => {
            switch(newItem.tauxTva){
                case 5.5:
                    code1.code1HT += newItem.totalHT
                    break
                case 10:
                    code2.code2HT += newItem.totalHT
                    break
                case 20:
                    code3.code3HT += newItem.totalHT
                    break
            }
        }

        const toFixedNumber = (num, digits, base) =>{
            var pow = Math.pow(base||10, digits);
            return Math.round(num*pow) / pow;
        }

        const makeItemsIsolation = ()=>{
            const keyBase = ['floorRoof', 'crawlingRoof', 'insideWall', 'outsideWall', 'lowFloor']
            const description ={
                floorRoof:'de combles aux sols',
                crawlingRoof:'de combles rampants',
                insideWall:`de murs par l'intérieur`,
                outsideWall:`de murs par l'extérieur`,
                lowFlor:'de plancher bas'
            }

            keyBase.forEach((key,index)=>{
                let sufix = 'Measure'
                if(index < 2){
                    sufix = 'Space'
                }
                if(needData[key+sufix] > 0 && needData[key+'Price'] > 0){
                    const newItem = {}
                    newItem.ref = 'ITEISOLFRANCE'
                    newItem.description = `BAR EN 102 Mise en place d'une isolation ${description[key]}, Polystirene Expansé, marque ISOLFRANCE, référence ISOLITEX GRIS 31 épaisseur 120 mm, résistance 3,85 m²,K/W  ACERMI 15/171/1016 prix au m²`
                    newItem.tauxTva = 5.5
                    newItem.quantity = `${needData[key+sufix]} m2`
                    newItem.totalTTC = needData[key+'Price']
                    newItem.totalHT = toFixedNumber(needData[key+'Price'] / (1 + (newItem.tauxTva/100)),2,10)
                    newItem.prixTTC = toFixedNumber(newItem.totalTTC / needData[key+sufix],2,10)
                    newItem.prixHT = toFixedNumber(newItem.prixTTC / (1 + (newItem.tauxTva/100)) ,2,10)
                    addTvaItem(newItem)
                    data.items.push(newItem)
                }
            })
        }

        const makeItemPhotovoltaicPanels = (photovoltaicPanels, panelsNumber, batteriePanels, panelsPrice)=>{
            if(photovoltaicPanels){
                const newItem = {...paneauxPhotovoltaiques}
                const tauxTva = panelsNumber > 10 ? 20 : 10
                newItem.quantity = panelsNumber.toString()
                newItem.totalTTC = panelsPrice
                newItem.prixTTC = toFixedNumber(panelsPrice / panelsNumber,2,10)
                newItem.totalHT = toFixedNumber(panelsPrice / (1 + (tauxTva/100)),2,10)
                newItem.prixHT = toFixedNumber(panelsPrice / (1 + (tauxTva/100)) / panelsNumber,2,10)
                newItem.tauxTva = tauxTva
                addTvaItem(newItem)
               
                data.items.push(newItem)
                if(!!batteriePanels){
                    let batteriePhoto = {}
                    batteriePhotovolataique.forEach((item)=>{
                        if(item.type === batteriePanels){
                            batteriePhoto = {...item}
                        }
                    })
                    const newItem = {}
                    newItem.quantity = '1'
                    newItem.totalTTC = batteriePhoto.prixTTC
                    newItem.totalHT = batteriePhoto.prixHT
                    newItem.prixTTC = newItem.totalTTC
                    newItem.prixHT = newItem.totalHT
                    newItem.description = 'Option batterie ' + batteriePanels
                    newItem.ref = batteriePhoto.ref
                    newItem.tauxTva = 20
                    addTvaItem(newItem)
    
                    data.items.push(newItem)
                }
            }
            
        }
        const makeItems = (value) => {
            if(value.length>0){
                value.forEach((val)=>{
                    const newItem = {...val}
                    newItem.totalTTC = toFixedNumber(newItem.quantity * newItem.prixTTC,2,10)
                    newItem.totalHT = toFixedNumber(newItem.quantity * newItem.prixHT,2,10)
                    newItem.quantity = newItem.quantity.toString()
                    addTvaItem(newItem)
                    data.items.push(newItem)
                })
            }
        }

        const computeHelp = () =>{
            if(simuData?.aides){
                const { aides } = simuData
                let help = 0
                const baseItem = {
                    ref:'',
                    quantity:'1',
                    prixHT:'',
                    prixTTC:'',
                    totalHT:'',
                }
                if(simuData?.aides.region && simuData.aides.regionReception == 2){
                    const regionItem = {...baseItem}
                    regionItem.totalTTC = - simuData.aides.region
                    regionItem.description = 'Aide régionnale'
                    help += - simuData.aides.region
                    data.items.push(regionItem)
                }
                if(simuData?.aides.ecobonus && simuData.aides.ecobonusReception == 2){
                    const ecoItem = {...baseItem}
                    ecoItem.totalTTC = - simuData.aides.ecobonus
                    ecoItem.description = 'Ecobonus'
                    help += - simuData.aides.ecobonus
                    data.items.push(ecoItem)
                }
                if(simuData?.aides.mpr && simuData.aides.mprReception == 2){
                    const mprItem = {...baseItem}
                    mprItem.totalTTC = - simuData.aides.mpr
                    mprItem.ref = 'MaPrimeRevnov'
                    // mprItem.description = ` MaPrimeRevnov - Cette offre est cumulable avec l'aide MaPrimeRénov' d'un montant de ${ simuData.aides.mpr }€ qui vous sera versée en une fois, par virement bancaire de l'État (sous quatre mois maximum) sous réserve de la non-utilisation de votre plafond de dépense. Source : https://maprimerenov.gouv.fr/`
                    mprItem.description = `MaPrimeRevnov`
                    help += - simuData.aides.mpr
                    data.items.push(mprItem)
                    data['mpr'] = simuData.aides.mpr
                }
                if(simuData?.aides.cee.total && simuData.aides.ceeReception == 2){
                    const ceeItem = {...baseItem}
                    ceeItem.totalTTC = - simuData.aides.cee.total
                    ceeItem.ref = 'Prime CEE'
                    // ceeItem.description = `Prime CEE - Les travaux ou prestations objet du présent document donneront lieu à une contribution financière de PREMIUM ENERGY dans le cadre de son rôle incitatif, directement ou via son (ses) mandataire(s), sous réserve de l’engagement de fournir exclusivement à PREMIUM ENERGY les documents nécessaires à la valorisation des opérations au titre du dispositif des Certificats d’Economies d’Energie et sous réserve de la validation de l'éligibilité du dossier par PREMIUM ENERGY puis par l’autorité administrative compétente. Cette offre comprend la prime versée par PREMIUM ENERGY ou son mandataire au titre du dispositif des Certificats d'Economies d'Energie d'un montant de ${simuData.aides.cee}€. Le client accepte que PREMIUM ENERGY collecte et traite ses données à caractère personnel pour les besoins du dépôt d’un dossier CEE sur le registre EMMY conformément aux dispositions réglementaires en vigueur et qu’il communique ces données à PREMIUM ENERGY à des fins de contrôle de la conformité des opérations réalisées chez le client. PREMIUM ENERGY s’engage à respecter la réglementation française et européenne relative à la protection des données à caractère personnel. Prime CEE : ${simuData.aides.cee}`
                    ceeItem.description = `Prime CEE`
                    help += - simuData.aides.cee.total
                    data.items.push(ceeItem)
                    data['cee'] = simuData.aides.cee.total
                    console.log(simuData.aides,'simu aides')
                    data['ceeDelegataire'] = simuData.aides.delegataire
                }
                if(help !== 0){
                    data.help = toFixedNumber(help,2,10)
                }
            }
        }

        makeItemsIsolation()

        makeItemPhotovoltaicPanels(needData.photovoltaicPanels, needData.panelsNumber, needData.batteriePanels, needData.panelsPrice)

        makeItems(needData.airToAirHeatPump)

        makeItems(needData.airWaterHeatPump)

        makeItems(needData.thermodynamicHeater)

        makeItems(needData.accessories)
        
        code1.code1HT = toFixedNumber(code1.code1HT,2,10)
        code2.code2HT = toFixedNumber(code2.code2HT,2,10)
        code3.code3HT = toFixedNumber(code3.code3HT,2,10)


        code1.code1TTC = toFixedNumber(code1.code1HT * code1.code1TVA / 100,2,10)

        code2.code2TTC = toFixedNumber(code2.code2HT * code2.code2TVA / 100,2,10)

        code3.code3TTC = toFixedNumber(code3.code3HT * code3.code3TVA / 100,2,10)

        for (const property in code1) {
            data[property] = code1[property]
        }
        for (const property in code2) {
            data[property] = code2[property]
        }
        for (const property in code3) {
            data[property] = code3[property]
        }

        data.totalHT = toFixedNumber(Number(data.code1HT) + Number(data.code2HT) + Number(data.code3HT),2,10)

        data.netHT = data.totalHT

        data.totalTVA = toFixedNumber(Number(data.code1TTC) + Number(data.code2TTC) + Number(data.code3TTC),2,10)

        data.totalTTC = toFixedNumber(data.totalHT + data.totalTVA,2,10)

        computeHelp()

        data.toPay = toFixedNumber(!!data?.help ? data.totalTTC + data.help : data.totalTTC,2,10)

        data.remise = ''
        
        return data
    }
 
    api.fillDevis = async (data) => {
        const existingPdfBytes = fs.readFileSync('/home/ec2-user/files/models/devis.pdf')
        let model = await PDFDocument.load(existingPdfBytes)
        const numFormater = new Intl.NumberFormat('fr-FR',{minimumFractionDigits: 2, maximumFractionDigits: 2})
        const doc = await PDFDocument.create()
        const font = await doc.embedFont(StandardFonts.TimesRoman);
        const boldFont = await doc.embedFont(StandardFonts.TimesRomanBold);
      
        const items = data.items;
        let space = 0;
        items.map(item => {
            console.log(item)
          space += Math.trunc(item.description.length / 50) + 2
        })
        space = space * 16 - 180 // 200px for the last page
        const pagesNumber = space < 0 ? 0 : Math.trunc(space / 460) + 1
      
        Array.from({ length: pagesNumber }).map(async (i) => {
          const [ firstPage ] = await doc.copyPages(model, [0]);
          doc.addPage(firstPage);
        })
        const [ secondPage ] = await doc.copyPages(model, [1]);
        doc.addPage(secondPage);
      
        const pages = doc.getPages()
        let index = -1;
        let y = 0;
      
        const newPage = async () => {
          index += 1;
          console.log(index)
          y = 500;
          const lastPage = (index === pages.length - 1)
          pages[index].drawText(data.number, { font: boldFont, x: 130, y: lastPage ? 665 : 669.5, size: 10 })
          pages[index].drawText(data.date, { font, x: 115, y: lastPage ? 644.5 : 648.5, size: 10 })
          pages[index].drawText(data.expireDate, { font, x: 120, y: lastPage ? 587 : 591, size: 10 })
          pages[index].drawText(':   ' + data.customer, { font, x: 115, y: lastPage ? 571 : 575, size: 10 })
          pages[index].drawText(data.name, { font: boldFont, x: 270, y: 650, size: 10 })
          pages[index].drawText(data.address1, { font: boldFont, x: 270, y: 630, size: 10 })
          pages[index].drawText(data.address2, { font: boldFont, x: 270, y: 610, size: 10 })
          pages[index].drawText(`Page ${index + 1}/${pages.length}`, { font, x: 290, y: 5, size: 8 })
          if (lastPage) {
            if(!!data.signature){
                const image = await doc.embedPng(data.signature)
                const dims = image.scale(0.5);
                const widthRatio = 80 / dims.width;
                const heightRatio = 35 / dims.height;
                const ratio = widthRatio > heightRatio ? heightRatio : widthRatio;
                const width = dims.width * ratio;
                const height = dims.height * ratio;
                pages[index].drawImage(image, { x: 60, y: 35, width, height })
                pages[index].drawText(moment().format('DD/MM/YYYY'), { x: 47, y: 19, font, size: 10 })
            }  
            pages[index].drawText(numFormater.format(data.totalHT), { x: 560 - font.widthOfTextAtSize(numFormater.format(data.totalHT),10), y: 198 , font, size: 10 })
            pages[index].drawText(numFormater.format(data.remise), { x: 560 - font.widthOfTextAtSize(numFormater.format(data.remise),10), y: 178 , font, size: 10 })
            pages[index].drawText(numFormater.format(data.netHT), { font: boldFont, x: 560 - boldFont.widthOfTextAtSize(numFormater.format(data.toPay),10), y: 157, size: 10 })
            pages[index].drawText(numFormater.format(data.totalTVA), { x: 560 - font.widthOfTextAtSize(numFormater.format(data.totalTVA),10), y: 135 , font, size: 10 })
            pages[index].drawText(numFormater.format(data.totalTTC), { x: 560 - font.widthOfTextAtSize(numFormater.format(data.totalTTC),10), y: 115 , font, size: 10 })
            data?.help && pages[index].drawText(numFormater.format(data.help), { font, x:  560 - font.widthOfTextAtSize(numFormater.format(data.help),10), y: 95, size: 10 })
            pages[index].drawText(numFormater.format(data.toPay), { font: boldFont, x: 560 - boldFont.widthOfTextAtSize(numFormater.format(data.toPay),10), y: 75, size: 10 })
            pages[index].drawText(numFormater.format(data.code1HT), { font, x: 80, y: 182, size: 10 })
            pages[index].drawText(numFormater.format(data.code2HT), { font, x: 80, y: 164, size: 10 })
            pages[index].drawText(numFormater.format(data.code3HT), { font, x: 80, y: 147, size: 10 })
            pages[index].drawText(numFormater.format(data.code1TVA), { font, x: 140, y: 182, size: 10 })
            pages[index].drawText(numFormater.format(data.code2TVA), { font, x: 140, y: 164, size: 10 })
            pages[index].drawText(numFormater.format(data.code3TVA), { font, x: 140, y: 147, size: 10 })
            pages[index].drawText(numFormater.format(data.code1TTC), { font, x: 200, y: 182, size: 10 })
            pages[index].drawText(numFormater.format(data.code2TTC), { font, x: 200, y: 164, size: 10 })
            pages[index].drawText(numFormater.format(data.code3TTC), { font, x: 200, y: 147, size: 10 })
          }
        }
        for (const { ref, description, quantity, prixHT, prixTTC, totalHT, totalTTC } of data.items) {
          if (y < 50) {
            await newPage();
          }
          pages[index].drawText(ref, { font, x: 25, y, size: 6 })
          pages[index].drawText(quantity, { font, x: 290, y, size: 10 })
          pages[index].drawText(!!prixHT ? numFormater.format(prixHT) : '', { font, x: 375 - font.widthOfTextAtSize(numFormater.format(prixHT), 10), y, size: 10 })
          pages[index].drawText(!!prixTTC ? numFormater.format(prixTTC) : '', { font, x: 440 - font.widthOfTextAtSize(numFormater.format(prixTTC), 10), y, size: 10 })
          pages[index].drawText(!!totalHT ? numFormater.format(totalHT) : '', { font, x: 500 - font.widthOfTextAtSize(numFormater.format(totalHT), 10), y, size: 10 })
          pages[index].drawText(numFormater.format(totalTTC), { font, x: 564 - font.widthOfTextAtSize(numFormater.format(totalTTC), 10), y, size: 10 })
      
          // largeur max de 200
          let words = description.split(' ');
          let x = 80;
          while (words.length > 0) {
            const [ w ] = words;
            const width = font.widthOfTextAtSize(w + ' ', 10);
            if (x + width >= 280) {
              y -= 16;
              x = 80;
              if (y < 60) {
                await newPage();
              }
            }
            pages[index].drawText(w, { font, x, y, size: 10 })
            words.splice(0, 1);
            x += width;
          }
          y -= 32
        }
        if(index + 2 === pages.length){
            await newPage();
        }
        console.log(data.cee, data.ceeDelegataire)
        if(!!data.cee || !!data.mpr){
            const lastPage = doc.addPage()
            const { width, height } = lastPage.getSize()
            if(!!data.cee){
                lastPage.drawText('Termes et conditions',{font:boldFont,x:25, y:height - 50, size:12})
                lastPage.drawText(`Les travaux ou prestations objet du présent document donneront lieu à une contribution financière de ${data.ceeDelegataire} dans le
cadre de son rôle incitatif, directement ou via son (ses) mandataire(s), sous réserve de l’engagement de fournir exclusivement à
${data.ceeDelegataire} les documents nécessaires à la valorisation des opérations au titre du dispositif des Certificats d’Economies
d’Energie et sous réserve de la validation de l'éligibilité du dossier par ${data.ceeDelegataire} puis par l’autorité administrative
compétente. Cette offre comprend la prime versée par ${data.ceeDelegataire} ou son mandataire au titre du dispositif des Certificats 
d'Economies d'Energie d'un montant de ${numFormater.format(data.cee)}€. 
Le client accepte que ${data.ceeDelegataire} collecte et traite ses données à caractère personnel pour les besoins du dépôt d’un dossier
CEE sur le registre EMMY conformément aux dispositions réglementaires en vigueur et qu’il communique ces données à ${data.ceeDelegataire}
à des fins de contrôle de la conformité des opérations réalisées chez le client. ${data.ceeDelegataire} s’engage à respecter la 
réglementation française et européenne relative à la protection des données à caractère personnel.`,
                {font:font,x:25, y:height-70, size:10})
            }
            if(!!data.mpr){
                let posY = 0
                if(!!data.cee){
                    posY = 280
                }
                lastPage.drawText(`AIDE MaPrimeRenov'`,{font:boldFont,x:25, y:height - 50 - posY, size:12})
                lastPage.drawText(`Cette offre est cumulable avec l'aide MaPrimeRénov' d'un montant de ${ data.mpr }€ qui vous sera versée en une fois, par virement
bancaire de l'État (sous quatre mois maximum) sous réserve de la non-utilisation de votre plafond de dépense. 
Source : https://maprimerenov.gouv.fr/`,
                {font:font,x:25, y:height - 70 - posY, size:10})
            }
        }

        const pdfBytes = await doc.save()
        return pdfBytes


    }
    
    api.saveDevis = (data,clientId,userId) => {
        const insertData = {
            userId:userId,
            clientId:clientId,
            data:data,
            status:'créé',
            path:''
        }
        const id = devis.newDevis(insertData)
        return id
    }

    api.writeDevis = (devisId,pdfBytes,signed=false) => {
        const path = `/home/ec2-user/files/generated/devis/${devisId}${signed ? '-signé':''}.pdf`
        fs.writeFileSync(path, pdfBytes);
        const status = signed ? 'signé' : 'sauvegardé'
        devisData.update({_id:devisId},{$set:{path:path, status:status}})
    }

    api.sendDevis = (devisId,signed=false) =>{ 
        console.log(devisId)
        const { path, clientId, _id } = devis.getDevis(devisId)
        const { name, lastname, email } = clients.getClient(clientId)
        const user = Meteor.user()
        const emailOptions = {}
        const htmlArray = [
            `<h3>Bonjour ${name},</h3>`,
            `<div>Vous trouverez ci-joint un exemplaire du devis ${signed ? 'signé' : ''} établis avec ${user.profile.name} ${user.profile.lastname}.</div><br>`,
            `<div>De plus, vous avez la possibilité de le consulter ${signed ? 'et de le signer en ligne' : ''} à tout moment en cliquant sur le bouton ci dessous :</div><br>`,
            `<div style='width:100%;display:flex'>`,
            `<a href='${Meteor.absoluteUrl()}documents/devis/${_id}' style='margin:auto'><button style='border:1px solid #F7E13B;background:#F7E13B; border-radius:37px; height:25px'>Accédez à votre devis</button></a>`,
            `</div><br>`,
            `Cordialement,<br> le service commercial heh<br> `
        ]
        let emailHTML = ''
        htmlArray.forEach((line)=>{
            emailHTML += line
        })
        emailOptions.to = `${name} ${lastname} <${email}>`
        emailOptions.from = `${user.profile.name} ${user.profile.lastname} <no-reply@insideall.fr>`
        emailOptions.subject = `Votre devis HEH du ${moment().format('DD/MM/YYYY')}`
        emailOptions.html = emailHTML
        emailOptions.attachments = [
            {   
                filename: `devis${signed ? '-signé' : ''}.pdf`,
                path:path
            }
        ]

        Email.send(emailOptions)

        if(!signed){
            devisData.update({_id:devisId},{$set:{status:'envoyé'}})
        }
        
    }


    return api;
})();