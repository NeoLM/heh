import { Meteor } from 'meteor/meteor'
import { clientsData } from './clients'



Meteor.methods({

  saveClient(data, clientId){
    const user = Meteor.user();
    this.unblock();    
    if(user.profile.active){
      const newClientData = {...data}

      newClientData['professions'] = [newClientData.profession1,newClientData.profession2]
      newClientData['houseHoldIncome'] = [Number(newClientData.houseHoldIncome1), Number(newClientData.houseHoldIncome2)]
      newClientData['birthDate'] = [newClientData.birthDate1, newClientData.birthDate2]
      newClientData['lastYearHouseholdIncome'] = Number(newClientData.lastYearHouseholdIncome)
      newClientData['residentsNumber'] = Number(newClientData.residentsNumber)

      delete newClientData.profession1
      delete newClientData.profession2

      delete newClientData.houseHoldIncome1
      delete newClientData.houseHoldIncome2

      delete newClientData.birthDate1
      delete newClientData.birthDate2
      if(!!clientId){
        clientsData.update({_id:clientId},{$set:{...newClientData,userId:user._id}})
        return clientId
      }else{
        let id = clients.newClient({...newClientData,userId:user._id})
        return id
      }
    } else {
      throw new Meteor.Error('Not allowed');
    }
  }
})

// @describ : Server API for clients manipulation
clients = (function () {

    var api = {};

    // @describ : creating a clients document
    // @Params : json object with all required fields
    // @result : new id of the document created
    api.newClient = (data) => {
      try {
        var id = clientsData.insert({...data});
        return id;
      } catch (e) {
        throw new Meteor.Error('Error new clients',e.message);
      }
    };

    // @describ : getting a clients document
    // @Params : id of the document 
    // @result : content of the document fetched
    api.getClient = (clientId) => {
      try {
        return clientsData.findOne({ _id:clientId });
      } catch (e) {
        throw new Meteor.Error('Error get clients',e.message);
      }
    };

    // @describ : deleting a clients document
    // @Params : id of the clients document(string)
    // @result : id of the document deleted
    api.deleteClient = (clientId) => {
        try {
          return clientsData.remove({ _id:clientId });
        } catch (e) {
          throw new Meteor.Error('Error delete clients');
        }
    };

    return api;
})();
