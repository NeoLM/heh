import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';

const clientsData = new Mongo.Collection('clientsData')

var clientsSchema = new SimpleSchema({
    userId:{
        type:String,
        label:'id commercial',
    },
    civilite:{
        type:String,
        label:'civilité',
        allowedValues:['Mr','Mme']
    },
    name:{
        type:String,
        label:'Prénom',
        max:"100"
    },
    lastname:{
        type:String,
        label:'Nom',
        max:"100"
    },
    email:{
        type:String,
        label:'Email',
        max:"100"
    },
    phone:{
        type:String,
        label:'Téléphone',
        max:"100"
    },
    adress:{
        type:String,
        label:'Numéro et nom de la voie',
        max:"200"
    },
    zipCode:{
        type:String,
        label:'Code postal',
        max:'7'
    },
    city:{
        type:String,
        label:'Ville',
        max:"100"
    },
    familySituation:{
        type:String,
        label:'Situation familiale',
        allowedValues:['Mariés','Concubinage','Célibataire']
    },
    residentsNumber:{
        type:Number,
        label:"Nombre d'habitants dans le foyer"
    },
    professions:{
        type:Array,
        label:'Professions des conjoints',
    },
    'professions.$':String,
    birthDate:{
        type:Array,
        label:'Date de naissance des conjoints',
    },
    'birthDate.$':String,
    lastYearHouseholdIncome:{
        type:Number,
        label:'Revenu fiscal du foyer année N-1'
    },
    houseHoldIncome:{
        type:Array,
        label:'Revenus fiscaux des conjoints'
    },
    'houseHoldIncome.$':Number,
    addedDate: {
        type: Date,
        label: "Creation date",
        autoValue: function () {
          if (this.isInsert) {
            return new Date();
          } else {
            this.unset();
          }
        }
      },
      modifiedDate: {
        type: Date,
        label: "Modification date",
        autoValue: function () {
          if (this.isUpdate) {
            return new Date();
          } else {
            this.unset();
          }
        },
        optional: true
      },
})

clientsData.attachSchema(clientsSchema);

export {
    clientsData
}
