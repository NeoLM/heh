import fs from 'fs';
import path from 'path';
import PDF from 'pdfkit';
import PdfLib from 'pdf-lib';
const { degrees, PDFDocument, rgb, StandardFonts } = PdfLib;

import mandat from './mandat.js';
import bdc from './bdc.js';



const pdf = (function(){
    var api = {}

    api.addSignature = (type, data, output, signature) => {
        const existingPdfBytes = fs.readFileSync(path.join(path.resolve(), 'assets/documents', type + '.pdf'))
      
        const pdfDoc = await PDFDocument.load(existingPdfBytes)
        const timesRomanFont = await pdfDoc.embedFont(StandardFonts.TimesRoman)
        const pages = pdfDoc.getPages()
      
        let model;
        switch (type) {
          case 'mandat':
            model = mandat;
            break
          case 'bdc':
            model = bdc;
            break
          default:
            console.error('Document type not known:', type);
            return
        }
      
        await Promise.all(model.map(async (pageData, i) => {
          const page = pages[i];
          for (const [k, v] of Object.entries(pageData)) {
            if (!data[k]) continue
            switch (v.type || '') {
              case 'text':
                page.drawText(data[k], { font: timesRomanFont, ...v });
                break
              case 'image':
                const image = await pdfDoc.embedPng(data[k])
                const dims = image.scale(0.5);
                const widthRatio = v.width / dims.width;
                const heightRatio = v.height / dims.height;
                const ratio = widthRatio > heightRatio ? heightRatio : widthRatio;
                const width = dims.width * ratio;
                const height = dims.height * ratio;
                page.drawImage(image, { ...v, width, height })
                break
              case 'checkbox':
                if (data[k]) {
                  page.drawText('X', { font: timesRomanFont, size: 14, ...v });
                }
                break
              default:
                console.log('Unknown type', v.type)
            }
          }
        }))
      
        const pdfBytes = await pdfDoc.save()
        fs.writeFileSync(`${path.resolve()}/files/${output}.pdf`, pdfBytes)
      }
      
      
})()