import PdfLib from 'pdf-lib';
const { rgb } = PdfLib;


export default {
    cdp:[
        {
            name:{ type: 'text', x: 160, y: 615, size: 12 },
            address1:{ type: 'text', x: 250, y: 595, size: 12 },
            address2:{ type: 'text', x: 50, y: 575, size: 12 },
            addressSameBox: { type: 'checkbox', x: 50, y: 470 },
            addressOtherBox: { type: 'checkbox', x: 345, y: 470 },
            addressOther1:{ type: 'text', x: 340, y: 450, size: 12 },
            addressOther2:{ type: 'text', x: 340, y: 430, size: 12 },
            addressOther3:{ type: 'text', x: 340, y: 410, size: 12 },
            signature: { type: 'image', x: 400, y: 120, height: 40, width: 200 },
        },
        {},
        {},
        {
            name:{ type: 'text', x: 160, y: 650, size: 12 },
            address1:{ type: 'text', x: 100, y: 620, size: 12 },
            address2:{ type: 'text', x: 60, y: 600, size: 12 },
            tel:{ type: 'text', x: 200, y: 570, size: 12 },
            email:{ type: 'text', x: 145, y: 530, size: 12 },
            edfYes:{ type: 'checkbox', x: 230, y: 490 },
            edfNo:{ type: 'checkbox', x: 300, y: 490 },
            revenuFiscal:{ type: 'text', x: 370, y: 460, size: 12 },
            personnesFoyer:{ type: 'text', x: 490, y: 440, size: 12 },
            chaudiereGaz: { type: 'checkbox', x: 60, y: 370 },
            chaudiereFioul: { type: 'checkbox', x: 110, y: 370 },
            chaudiereCharbon: { type: 'checkbox', x: 190, y: 370 },
            modelePACRO:{ type: 'text', x: 490, y: 340, size: 12 },
            modelePACRR:{ type: 'text', x: 490, y: 320, size: 12 },
            modeleChauffe:{ type: 'text', x: 510, y: 300, size: 12 },
            surfaceHabitation:{ type: 'text', x: 180, y: 200, size: 12 },
            surfaceChauffe:{ type: 'text', x: 260, y: 180, size: 12 },
        },
    ]
}