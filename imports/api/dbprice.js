const paneauxPhotovoltaiques = {
    ref:'SOLUXCTE330',
    modele:'Soluxtec 330',
    description:"Panneaux photovoltaique Soluxtec 330 watts crètes, puissance max. (WP) 330 VOC:39.4041.58 tolèrance de puissance : 0-4.99 Wp dimensions : '1665x1005x35 mm poids 18.5 kg (+ou-3%) performance électriques sous conditions NMOT : Pmpp:244 Voc 38.48 Isc 8.07 Vmpp 30.75 Impp 7.91 20 ans de grantie produit, 25 ans de garantie de performance linéaire certifications : IEC61215/EN61730/LVD2014/35/EU/EMC2014/30/EU INMETRO",
    puissance:330
}

const paneauxPhotovoltaiquesPrix = [
    {
        nombre:9,
        puissance:2970,
        prixTTC:12000,
        tauxTva:10
    },
    {
        nombre:10,
        puissance:3300,
        prixTTC:14000,
        tauxTva:10
    },
    {
        nombre:12,
        puissance:3960,
        prixTTC:16000,
        tauxTva:20
    },
    {
        nombre:14,
        puissance:4620,
        prixTTC:18000,
        tauxTva:20
    },
    {
        nombre:16,
        puissance:5280,
        prixTTC:20000,
        tauxTva:20
    },
    {
        nombre:18,
        puissance:5940,
        prixTTC:22000,
        tauxTva:20
    },
    {
        nombre:20,
        puissance:6600,
        prixTTC:23000,
        tauxTva:20
    },
    {
        nombre:22,
        puissance:7260,
        prixTTC:24000,
        tauxTva:20
    },
    {
        nombre:24,
        puissance:7920,
        prixTTC:25000,
        tauxTva:20
    },
    {
        nombre:26,
        puissance:8580,
        prixTTC:27000,
        tauxTva:20
    },
]

const batteriePhotovolataique = [
    {
        type:'Sans Batterie',
        ref:'SANSBATTERIE'
    },
    {
        type:'Monophasé',
        prixHT:2083.33,
        prixTTC:2500,
        ref:'OPTBATMONO'
    },
    {
        type:'Triphasé',
        prixHT:2500,
        prixTTC:3000,
        ref:'OPTBATTRI'
    }
]

const abrisJardin = [
    {
        nombresPaneaux:[1,10],
        prixTTC:1800,
    },
    {
        nombresPaneaux:[11,20],
        prixTTC:2500,
    },
    {
        nombresPaneaux:[21,27],
        prixTTC:3000,
    }
]

const pompeAirEau = [
    {
        ref:'PACROMT11',
        modèle:'Atlantic - Alféa',
        puissance:10.80,
        description:"Fourniture et pose d'une Pompe à chaleur air eau splittée de marque  Atlantic modele Alféa puissance thermique nominale 10,80 kW avec appoint électrique de 6 kW monophasé Efficacité énergétique saisonnière (35°C/55°C) 151/112 % Classe énergétique  (35°/55°) A++/A+ Groupe extérieur Fujitsu Fluide frigorigène HFC R410A, avec thermostat modulant d'ambiance radio navylink garantie compresseur 5 ans accessoires necessaires à l'installation compris",
        type:'Monophasé',
        prixHT:13270.14,
        prixTTC:14000,
        tauxTva:5.5
    },
    {
        ref:'PACROMT14',
        modèle:'Atlantic - Alféa',
        puissance:13.50,
        description:"Fourniture et pose d'une Pompe à chaleur air eau splittée de marque  Atlantic modele Alféa puissance thermique nominale 13,50 kW avec appoint électrique de 6 kW monophasé Efficacité énergétique saisonnière (35°C/55°C) 148/113 % Classe énergétique  (35°/55°) A++/A+ Groupe extérieur Fujitsu Fluide frigorigène HFC R410A, avec thermostat modulant d'ambiance radio navylink garantie compresseur 5 ans accessoires necessaires à l'installation compris",
        type:'Monophasé',
        prixHT:14218.01,
        prixTTC:15000,
        tauxTva:5.5
    },
    {
        ref:'PACROMT16',
        modèle:'Atlantic - Alféa',
        puissance:16,
        description:"Fourniture et pose d'une Pompe à chaleur air eau splittée de marque  Atlantic modele Alféa puissance thermique nominale 16 kW avec appoint électrique de 6 kW monophasé Efficacité énergétique saisonnière (35°C/55°C) 163/125 % Classe énergétique  (35°/55°) A++/A++ Groupe extérieur Fujitsu Fluide frigorigène HFC R410A, avec thermostat modulant d'ambiance radio navylink garantie compresseur 5 ans accessoires necessaires à l'installation compris",
        type:'Monophasé',
        prixHT:15165.88,
        prixTTC:16000,
        tauxTva:5.5
    },
    {
        ref:'PACROMT11TRI',
        modèle:'Atlantic - Alféa',
        puissance:10.80,
        description:"Fourniture et pose d'une Pompe à chaleur air eau splittée de marque  Atlantic modele Alféa puissance thermique nominale 10,80 kW avec appoint électrique de 9 kW triphasé Efficacité énergétique saisonnière (35°C/55°C) 154/112 % Classe énergétique  (35°/55°) A++/A+ Groupe extérieur Fujitsu Fluide frigorigène HFC R410A, avec thermostat modulant d'ambiance radio navylink garantie compresseur 5 ans accessoires necessaires à l'installation compris",
        type:'Triphasé',
        prixHT:14218.01,
        prixTTC:15000,
        tauxTva:5.5
    },
    {
        ref:'PACROMT14TRI',
        modèle:'Atlantic - Alféa',
        puissance:13.50,
        description:"Fourniture et pose d'une Pompe à chaleur air eau splittée de marque  Atlantic modele Alféa puissance thermique nominale 13,50 kW avec appoint électrique de 9 kW triphasé Efficacité énergétique saisonnière (35°C/55°C) 150/117 % Classe énergétique  (35°/55°) A++/A+ Groupe extérieur Fujitsu Fluide frigorigène HFC R410A, avec thermostat modulant d'ambiance radio navylink garantie compresseur 5 ans accessoires necessaires à l'installation compris",
        type:'Triphasé',
        prixHT:15165.88,
        prixTTC:16000,
        tauxTva:5.5
    },
    {
        ref:'PACROMT16TRI',
        modèle:'Atlantic - Alféa',
        puissance:16,
        description:"Fourniture et pose d'une Pompe à chaleur air eau splittée de marque  Atlantic modele Alféa puissance thermique nominale 16 kW avec appoint électrique de 9 kW triphasé Efficacité énergétique saisonnière (35°C/55°C) 149/117 % Classe énergétique  (35°/55°) A++/A+ Groupe extérieur Fujitsu Fluide frigorigène HFC R410A, avec thermostat modulant d'ambiance radio navylink garantie compresseur 5 ans accessoires necessaires à l'installation compris",
        type:'Triphasé',
        prixHT:16113.74,
        prixTTC:17000,
        tauxTva:5.5
    },
    {
        ref:'PACROHT11',
        modèle:'Hitatchi - Yutaki S80',
        puissance:11,
        description:"Pompe à chaleur air eau splittée Hitachi Yutaki S80 haute température (80°) puissance thermique nominale 11 Kw monophasé 230 volts Efficacité énergétique saisonnière (35°C/55°C) 189%/144% Classe énergétique (35°C/55°C) A+++/A+++ Groupe extérieur R410A compresseur Scroll avec télécommande programmable et thermostat  d'ambiance radio  garantie compresseur 5 ans accessoires necessaires à l'installation compris",
        type:'Monophasé',
        prixHT:14218.01,
        prixTTC:15000,
        tauxTva:5.5
    },
    {
        ref:'PACROHT14',
        modèle:'Hitatchi - Yutaki S80',
        puissance:14,
        description:"Pompe à chaleur air eau splittée Hitachi Yutaki S80 haute température (80°) puissance thermique nominale 14 Kw monophasé 230 volts Efficacité énergétique saisonnière (35°C/55°C) 176%/133% Classe énergétique (35°C/55°C) A+++/A++ Groupe extérieur R410A compresseur Scroll avec télécommande programmable et thermostat  d'ambiance radio  garantie compresseur 5 ans accessoires necessaires à l'installation compris",
        type:'Monophasé',
        prixHT:15165.88,
        prixTTC:16000,
        tauxTva:5.5
    },
    {
        ref:'PACROHT16',
        modèle:'Hitatchi - Yutaki S80',
        puissance:16,
        description:"Pompe à chaleur air eau splittée Hitachi Yutaki S80 haute température (80°) puissance thermique nominale 16 Kw monophasé 230 volts Efficacité énergétique saisonnière (35°C/55°C) 154%/128% Classe énergétique (35°C/55°C) A++/A++ Groupe extérieur R410A compresseur Scroll avec télécommande programmable et thermostat  d'ambiance radio  garantie compresseur 5 ans accessoires necessaires à l'installation compris",
        type:'Monophasé',
        prixHT:16113.74,
        prixTTC:17000,
        tauxTva:5.5
    },
    {
        ref:'PACROHT11TRI',
        modèle:'Hitatchi - Yutaki S80',
        puissance:11,
        description:"Pompe à chaleur air eau splittée Hitachi Yutaki S80 haute température (80°) puissance thermique nominale 11 Kw triphasé 400 volts Efficacité énergétique saisonnière (35°C/55°C) 185%/142% Classe énergétique (35°C/55°C) A+++/A++ Groupe extérieur R410A compresseur Scroll avec télécommande programmable et thermostat  d'ambiance radio  garantie compresseur 5 ans accessoires necessaires à l'installation compris",
        type:'Triphasé',
        prixHT:15165.88,
        prixTTC:16000,
        tauxTva:5.5
    },
    {
        ref:'PACROHT14TRI',
        modèle:'Hitatchi - Yutaki S80',
        puissance:14,
        description:"Pompe à chaleur air eau splittée Hitachi Yutaki S80 haute température (80°) puissance thermique nominale 14 Kw triphasé 400 volts Efficacité énergétique saisonnière (35°C/55°C) 173%/131% Classe énergétique (35°C/55°C) A++/A++ Groupe extérieur R410A compresseur Scroll avec télécommande programmable et thermostat  d'ambiance radio  garantie compresseur 5 ans accessoires necessaires à l'installation compris",
        type:'Triphasé',
        prixHT:16113.74,
        prixTTC:17000,
        tauxTva:5.5
    },
    {
        ref:'PACROHT16TRI',
        modèle:'Hitatchi - Yutaki S80',
        puissance:16,
        description:"Pompe à chaleur air eau splittée Hitachi Yutaki S80 haute température (80°) puissance thermique nominale 16 Kw triphasé 400 volts Efficacité énergétique saisonnière (35°C/55°C) 152%/127% Classe énergétique (35°C/55°C) A++/A++ Groupe extérieur R410A compresseur Scroll avec télécommande programmable et thermostat  d'ambiance radio  garantie compresseur 5 ans accessoires necessaires à l'installation compris",
        type:'Triphasé',
        prixHT:17061.61,
        prixTTC:18000,
        tauxTva:5.5
    },
]

const pompeAirAir = [
    {
        ref:'PACRRLGR32',
        modèle:'LG',
        description:"Pompe à chaleur air air LG",
    },
    {
        ref:'PACRRATR32',
        modèle:'ATLANTIC',
        description:"Pompe à chaleur air air Atlantic",
    },
    {
        ref:'PACRRTR32',
        modèle:'TOSHIBA',
        description:"Pompe à chaleur air air Toshiba",
    }
]

const pompeAirAirPrix = {
    basePrix:12900,
    baseNombre:2,
    seuil:5,
    augmentation:[1000,1500],
    tva:20
}

const ballonsThermodynamiques = [
    {
        ref:'BTHECS200M',
        modèle:'Thermor Aeromax 5',
        puissance:2450,
        contenance:200,
        description:"Chauffe eau Themodynamique Thermor Aeromax 5 capacité 200 litres puissance totale absorbée 2450 watts puissance résistance stéatite 1800 watts COP à 7°C 3,18 (profil L) rendement énergétique 133%  classe énergétique A+ fluide frigorigène R513a garantie 2 ans compresseur 5 ans cuve pièces instalation raccordement et mise en service incluse ",
        prixHT:3317.54,
        prixTTC:3500,
        tauxTva:5.5
    },
    {
        ref:'BTHECS250M',
        modèle:'Thermor Aeromax 5',
        puissance:2450,
        contenance:250,
        description:"Chauffe eau Themodynamique Thermor Aeromax 5 capacité 250 litres puissance totale absorbée 2450 watts puissance résistance stéatite 1800 watts COP à 7°C 3,15 (profil L) rendement énergétique 131%  classe énergétique A+ fluide frigorigène R513a garantie 2 ans compresseur 5 ans cuve pièces instalation raccordement et mise en service incluse ",
        prixHT:3791.47,
        prixTTC:4000,
        tauxTva:5.5
    }
]

const isolation = [
    {
        ref:'ITEISOLFRANCE',
        description:"BAR EN 102 Mise en place d'une isolation de murs par l'extérieur, Polystirene Expansé, marque ISOLFRANCE, référence ISOLITEX GRIS 31 épaisseur 120 mm, résistance 3,85 m²,K/W  ACERMI 15/171/1016 prix au m² ",
        prixHT:156.40,
        prixTTC:165,
        tauxTva:5.5
    },
    {
        ref:'ISOSUP101',
        description:"BAR EN 101 Mise en place d'une isolation en combles perdues, par soufflage de laine de verre, marque SUPERGLASS, référence SUPERWHITE LOFT, épaisseur avant tassement 320 mm résistyance 7,0 m²,K/W ACERMI N°18/D/037/1313, Nomes EN 14064-1:2010 le m²",
        prixHT:156.40,
        prixTTC:165,
        tauxTva:5.5
    },
    {
        ref:'ISOPBISOL103',
        description:"BAR EN 103 Mise en place d'une isolation sur plafond de cave ou sous sol, produit polystirene expansé, marque ISOLFRANCE Référence ISOLDALLE GRIS 32, épaisseur 120 mm, résistance 3,75 m²,K/W ACERMI 18/171/1279 Marquage CE le m²",
        prixHT:52.13,
        prixTTC:55,
        tauxTva:5.5
    },
    {
        ref:'ISORAMP101',
        description:"BAR EN 101 Mise en place d'une isolation en combles perdues, pose sur rampant en  laine de verre, marque SUPERGLASS, référence SUPERWHITE LOFT, épaisseur avant tassement 320 mm résistyance 7,0 m²,K/W ACERMI N°18/D/037/1313, Nomes EN 14064-1:2010 le m²",
        prixHT:52.13,
        prixTTC:55,
        tauxTva:5.5
    },
]

const mappingZipCodeToDegree = {
    15:[67,57,54,88],
    12:[55,52,70,25,90],
    10:[08,51,10,89,58,21,71,42,69,39,01,74,73,05,38],
    9:[52,59,80],
    8:[63,03,87,23,19,15,46,04,48],
    7:[76,60,02,95,78,91,77,50,14,51,27,45,53,72,41,18,36,37,86,79,49],
    6:[84,07,26,12,46],
    5:[92,93,94,75,35,44,85,17,16,33,24,40,47,64,32,82,65,31,81,09,66,11,34,30,13],
    4:[50,22,56,29],
    2:[20,06]
}

export{
    paneauxPhotovoltaiques,
    ballonsThermodynamiques,
    pompeAirEau,
    pompeAirAir,
    pompeAirAirPrix,
    abrisJardin,
    batteriePhotovolataique,
    paneauxPhotovoltaiquesPrix,
    isolation,
    mappingZipCodeToDegree
}