import { clientsData } from '../clients/clients'
import '../clients/methods'

import { housingsData } from '../housings/housings'
import '../housings/methods'

import { needsData } from '../needs/needs'
import '../needs/methods'

import { simulationsData } from '../simulation/simulation'
import '../simulation/methods'

import { devisData } from '../devis/devis'
import '../devis/methods'

import { orderFormsData } from '../orderForms/orderForms'
import '../orderForms/methods'

import { bdcsData } from '../bdc/bdc'
import '../bdc/methods'

import { moment } from 'meteor/momentjs:moment';


Meteor.methods({

    getClientFormData(clientId){
        const user = Meteor.user();
        this.unblock();    
        if(user.profile.active){
            const methodsList = {
                clientData(){
                    const clientsVal = clients.getClient(clientId)
                    if(!!clientsVal){
                        const formatedClientsVal = {...clientsVal}
                        formatedClientsVal['profession1'] = clientsVal['professions'][0]
                        formatedClientsVal['profession2'] = clientsVal['professions'][1]
                        formatedClientsVal['birthDate1'] = clientsVal['birthDate'][0]
                        formatedClientsVal['birthDate2'] = clientsVal['birthDate'][1]
                        formatedClientsVal['houseHoldIncome1'] = clientsVal['houseHoldIncome'][0]
                        formatedClientsVal['houseHoldIncome2'] = clientsVal['houseHoldIncome'][1]

                        delete formatedClientsVal['professions']
                        delete formatedClientsVal['birthDate']
                        delete formatedClientsVal['houseHoldIncome']

                        return formatedClientsVal
                    }else{
                        return false
                    }
                },
                housingData(){
                    const housingVal = housings.getHousingByClientId(clientId)
                    if(!!housingVal){
                        housingVal.insulationCoeff = housingVal.insulationCoeff.toString()
                        return housingVal
                    }else{
                        return false
                    }
                },
                needData(){
                    const needsVal = needs.getNeedByClientId(clientId)
                    if(!!needsVal){
                        return needsVal
                    }else{
                        return false
                    }
                },
                simulationData(){
                    const simulationsVal = simulations.getSimulationByClientId(clientId)
                    if(!!simulationsVal){
                        return simulationsVal
                    }else{
                        return false
                    }
                },
                devisData(){
                    const devisVal = devis.getDevisByClientId(clientId)
                    if(!!devisVal){
                        return devisVal
                    }else{
                        return false
                    }
                },
                orderData(){
                    const orderVal = orderForms.getOrderFormByClientId(clientId)
                    if(!!orderVal){
                        const flatenOrderVal = utils.SimpleFlattenObj(orderVal,false)
                        return flatenOrderVal
                    }else{
                        return false
                    }
                },
                bdcData(){
                    const bdcVal = bdcs.getBdcByClientId(clientId)
                    if(!!bdcVal){
                        const flatenBdcVal = utils.SimpleFlattenObj(bdcVal,false)
                        return flatenBdcVal
                    }else{
                        return false
                    }
                }
            }
            let clientFormData = {}
            for (const [key, method] of Object.entries(methodsList)){
                const valueFetched = method()
                if(!!valueFetched){
                    clientFormData[key] = valueFetched
                } else{
                    break
                }
            }

            return clientFormData
            
        } else {
            throw new Meteor.Error('Not allowed');
        }
    },

    getClientPageData(searchedValue){
        const query ={
            $or:[
                { name: { $regex:`.*${searchedValue}.*`, $options:'i' } },
                { lastname: { $regex:`.*${searchedValue}.*`, $options:'i' } },
            ]
        }
        const clientsVal = clientsData.find(query,{sort:{addedDate:-1}})

        const clientPageData = clientsVal.map((client)=>{
            const devisFetchedData = devis.getDevisByClientId(client._id)
            const bdcFetchedData = bdcs.getBdcByClientId(client._id)
            const clientData = {
                id:client._id,
                name:`${client.name} ${client.lastname}`,
                date:`${!!client.modifiedDate ? moment(client.modifiedDate).format('DD/MM/YYYY') : moment(client.addedDate).format('DD-MM-YYYY')}`,
                amount:!!devisFetchedData && devisFetchedData.data.toPay,
                devisId: !!devisFetchedData && devisFetchedData._id,
                bdcId: !!bdcFetchedData && bdcFetchedData._id
            }
            return clientData
        })
        
        return clientPageData
    }
})

utils = (function(){
    var api = {}

    api.SimpleFlattenObj = (oldObject, keepParrent) => {
        const newObject = {};
      
        flattenHelper(oldObject, newObject,'', keepParrent);
      
        return newObject;
      
        function flattenHelper(currentObject, newObject, previousKeyName, keepParrent) {
          for (let key in currentObject) {
            let value = currentObject[key];
      
            if (value.constructor !== Object) {
              if (previousKeyName == null || previousKeyName == '') {
                newObject[key] = value;
              } else {
                if (key == null || key == '') {
                  newObject[previousKeyName] = value;
                } else if (!keepParrent){
                    newObject[key] = value;
                }else{
                  newObject[previousKeyName + '.' + key] = value;
                }
              }
            } else {
              if (previousKeyName == null || previousKeyName == '') {
                flattenHelper(value, newObject, key);
              } else {
                flattenHelper(value, newObject, previousKeyName + '.' + key);
              }
            }
          }
        }
    }

    api.toFixedNumber = (num, digits, base) =>{
        var pow = Math.pow(base||10, digits);
        return Math.round(num*pow) / pow;
    }

    return api
})()