import { Meteor } from 'meteor/meteor'
import { bdcsData } from './bdc'
import fs from 'fs';
import path from 'path';
import PdfLib from 'pdf-lib';
const { PDFDocument, StandardFonts } = PdfLib;
import { moment } from 'meteor/momentjs:moment';
import '../clients/methods'
import '../needs/methods'
import '../orderForms/methods'
import { Email } from 'meteor/email'
import bdcModel from '../pdf/bdc'
require('intl')
Intl.NumberFormat = IntlPolyfill.NumberFormat;

import { paneauxPhotovoltaiques,
    ballonsThermodynamiques,
    pompeAirEau,
    paneauxPhotovoltaiquesPrix,
    batteriePhotovolataique,
    isolation
} from '../dbprice'


Meteor.methods({

    generateAndSendBdc(clientId){
        const user = Meteor.user();
        if(user.profile.active){
            const data = bdcs.getDataToFillBdc(clientId,user._id)
            return bdcs.fillBdc(data).then((pdfBytes)=>{
                const bdcId = bdcs.saveBdc(data,clientId,user._id)
                console.log(bdcId)
                bdcs.writeBdc(bdcId,pdfBytes)
                bdcs.sendBdc(bdcId)
                return bdcId
            })
        } else {
            throw new Meteor.Error('Not allowed');
        }
    },
    getBdc(bdcId){
        console.log(bdcId)
        const {path, status} = bdcs.getBdc(bdcId)
        if(!!path){
            const blob = fs.readFileSync(path);
            return [blob, status]
        } else {
            throw new Meteor.Error('No bdc found');
        }
    },
    sendVerficationSMSBdc(bdcId){
        const {clientId} = bdcs.getBdc(bdcId)
        const {phone, name} = clients.getClient(clientId)
        const formatedPhone = phone.replace('0', '+33')
        const code = Math.floor(Math.random() * 90000) + 10000
        const message = `Bonjour ${name}, voici votre code de vérification pour la signature : ${code}`
        sms.sendVerfication(formatedPhone, message)
        devis.addVerifCode(bdcId, code)
        return true
    },
    verifySmsBdc(bdcId, codeSms){
        const {code, status, modifiedDate} = bdcs.getBdc(bdcId)
        const startTime = new Date(modifiedDate)
        const actualTime = new Date()
        const delay = 2*60*1000
        if(actualTime - startTime < delay){
            if(codeSms == code.toString() && status == "sms"){
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    },
    signAndSendBdc(bdcId, signature){
        const bdcData = bdcs.getBdc(bdcId)
        if(!!bdcData){
            const data = {}
            data.signature = signature
            data.path = bdcData.path
            return bdcs.fillBdc(data, true).then((pdfBytes)=>{
                console.log(bdcId)
                bdcs.writeBdc(bdcId,pdfBytes,true)
                bdcs.sendBdc(bdcId,true)
                return bdcId
            })
        } else {
            throw new Meteor.Error('Not allowed');
        }
    }
})

// @describ : Server API for bdc manipulation
bdcs = (function () {

    var api = {};

    // @describ : creating a bdc document
    // @Params : json object with all required fields
    // @result : new id of the document created
    api.newBdc = (data) => {
      try {
        var id = bdcsData.insert({...data});
        return id;
      } catch (e) {
        throw new Meteor.Error('Error new bdc',e.message);
      }
    };

    // @describ : getting a bdc document
    // @Params : id of the document 
    // @result : content of the document fetched
    api.getBdc = (bdcId) => {
      try {
        return bdcsData.findOne({ _id:bdcId });
      } catch (e) {
        throw new Meteor.Error('Error get bdc',e.message);
      }
    };

    // @describ : getting a bdc document
    // @Params : id of the document 
    // @result : content of the document fetched
    api.getBdcByClientId = (clientId) => {
        try {
          return bdcsData.findOne({ clientId:clientId });
        } catch (e) {
          throw new Meteor.Error('Error get bdc',e.message);
        }
      };

    // @describ : deleting a bdc document
    // @Params : id of the bdc document(string)
    // @result : id of the document deleted
    api.deleteBdc = (bdcId) => {
        try {
          return devisData.remove({ _id:bdcId });
        } catch (e) {
          throw new Meteor.Error('Error delete bdc');
        }
    };

    api.saveBdc = (data,clientId,userId) => {
        const insertData = {
            userId:userId,
            clientId:clientId,
            data:data,
            status:'créé',
            path:''
        }
        try{
            const id = bdcs.newBdc(insertData)
            return id
        }catch (e) {
            throw new Meteor.Error('Error save bdc',e.message);
        }
    }

    api.getDataToFillBdc = (clientId,userId) =>{
        const data = {}

        data.number = 'DE0000'
        data.customer ='0000'

        const clientData = clients.getClient(clientId)
        const userData = Meteor.users.findOne({_id:userId})
        data.conseiller = `${userData.profile.name} ${userData.profile.lastname}`
        data.conseillerTel = userData.profile.phone

        data.signatureConseiller = userData.profile.signature

        data.soussigne = `${clientData.lastname} ${clientData.name}`
        data.faitA = 'Maisons-Alfort'
        data.leJour = moment().format('D')
        data.leMois = moment().format('M')
        data.leAnnee = moment().format('YYYY')

        data.lastname = clientData.lastname
        data.firstname = clientData.name
        data.addressStreet = clientData.adress
        data.addressZipcode = clientData.zipCode
        data.addressCity = clientData.city
        data.mobile = clientData.phone
        data.email = clientData.email

        const needData = needs.getNeedByClientId(clientId)

        const numFormater = new Intl.NumberFormat('fr-FR',{minimumFractionDigits: 2, maximumFractionDigits: 2})

        const toFixedNumber = (num, digits, base) =>{
            var pow = Math.pow(base||10, digits);
            return Math.round(num*pow) / pow;
        }

        const makeIsolationData = () => {
            if(needData?.floorRoofSpace || needData?.crawlingRoofSpace || needData?.insideWallMeasure || needData?.outsideWallMeasure || needData?.lowFloorMeasure){
                let surface = 0
                let totalHT = 0
                let totalTTC = 0
                if(needData?.floorRoofSpace){
                    data.isolationCombles = true
                    surface += needData.floorRoofSpace
                    totalHT += toFixedNumber(needData.floorRoofPrice / (1 + (5.5/100)),2,10)
                    totalTTC += toFixedNumber(needData.floorRoofPrice,2,10)
                }
                if(needData?.crawlingRoofSpace){
                    data.isolationCombles = true
                    surface += needData.crawlingRoofSpace
                    totalHT += toFixedNumber(needData.crawlingRoofPrice / (1 + (5.5/100)),2,10)
                    totalTTC += toFixedNumber(needData.crawlingRoofPrice,2,10)
                }
                if(needData?.insideWallMeasure){
                    data.isolationMurs = true
                    surface += needData.insideWallMeasure
                    totalHT += toFixedNumber(needData.insideWallPrice / (1 + (5.5/100)),2,10)
                    totalTTC += toFixedNumber(needData.insideWallPrice,2,10)
                }
                if(needData?.outsideWallMeasure){
                    data.isolationMurs = true
                    surface += needData.outsideWallMeasure
                    totalHT += toFixedNumber(needData.outsideWallPrice / (1 + (5.5/100)),2,10)
                    totalTTC += toFixedNumber(needData.outsideWallPrice,2,10)
                }
                if(needData?.lowFloorMeasure){
                    data.isolationPlancher = true
                    surface += needData.lowFloorMeasure
                    totalHT += toFixedNumber(needData.lowFloorPrice / (1 + (5.5/100)),2,10)
                    totalTTC += toFixedNumber(needData.lowFloorPrice,2,10)
                }
                data.isolationSurface = surface
                data.isolationDescription = 'test'
                data.isolationAmountHt = totalHT
                data.isolationAmountTtc = totalTTC
            }
        }

        const makePanelsData = () => {
            if(!!needData.photovoltaicPanels){
                data.panneauxAuto = true
                data.panneauxRevente = true
                data.panneauxMarque = paneauxPhotovoltaiques.modele
                data.panneauxNombre = needData.panelsNumber
                data.panneauxPuissance = paneauxPhotovoltaiques.puissance
                data.panneauxPuissanceTotale = paneauxPhotovoltaiques.puissance * needData.panelsNumber
                const tauxTva = needData.panelsNumber > 10 ? 20 : 10
                data.panneauxAmountHt = toFixedNumber(needData.panelsPrice / (1 + (tauxTva/100)),2,10)
                data.panneauxAmountTtc = needData.panelsPrice


                if(!!needData.batteriePanels){
                    data.panneauxBatterie = true
                    let batteriePhoto = {}
                    batteriePhotovolataique.forEach((item)=>{
                        if(item.type === needData.batteriePanels){
                            batteriePhoto = {...item}
                        }
                    })
                    data.panneauxAmountTtc += batteriePhoto.prixTTC
                    data.panneauxAmountHt += batteriePhoto.prixHT
                }
            }

        }

        const makeThermodynamiqueData = () => {
            if(!!needData.thermodynamicHeater && needData.thermodynamicHeater.length > 0){
                data.chauffeContenance = 0
                data.chauffeMarque = needData.thermodynamicHeater[0].modèle
                data.chauffeAmountHt = 0
                data.chauffeAmountTtc = 0
                needData.thermodynamicHeater.forEach((product)=>{
                    data.chauffeContenance += product.quantity * product.contenance
                    data.chauffeAmountHt += product.quantity * product.prixHT
                    data.chauffeAmountTtc += product.quantity * product.prixTTC
                })
            }
        }

        const makePACData = () => {
            const isPACAirAir = !!needData.airToAirHeatPump && needData.airToAirHeatPump.length > 0
            const isPACAirEau = !!needData.airWaterHeatPump && needData.airWaterHeatPump.length > 0

            if(isPACAirEau || isPACAirAir){
                data.eauType = ''
                data.eauMarque = 'Hitachi ou Atlantic'
                data.eauPuissance = 0
                data.eauAmountHt = 0
                data.eauAmountTtc = 0
                if(isPACAirEau){
                    needData.airWaterHeatPump.forEach((product)=>{
                        data.eauAmountHt += product.quantity * product.prixHT
                        data.eauAmountTtc += product.quantity * product.prixTTC
                        data.eauPuissance += product.quantity * product.puissance
                        data.type = product.type
                    })
                }
                if(isPACAirAir){
                    data.airNumber = needData.airToAirHeatPump.length
                    data.airMarque = 'LG ou ATLANTIC ou Toshiba'
                    data.airMurale = false
                    data.airConsole = false
                    data.airExterieurNumber = needData.airToAirHeatPump.length
                    data.airExterieurPuissance = 0
                    data.airAmountHt = 0
                    data.airAmountTtc = 0
                    needData.airToAirHeatPump.forEach((product)=>{
                        if(product.type.toLowerCase() == 'murales'){
                            data.airMurale = true
                        }
                        if(product.type.toLowerCase() == 'console'){
                            data.airConsole = true
                        }
                        data.airExterieurPuissance += product.quantity * product.puissance
                        data.airAmountHt += product.quantity * product.prixHT
                        data.airAmountTtc += product.quantity * product.prixTTC
                    })
                }
            }
        }

        const orderFormData = orderForms.getOrderFormByClientId(clientId)

        const makeOrderData = () => {
            data.deliverDelay = orderFormData.deliveryDelay
            if(orderFormData?.adress){
                data.deliverAddress = `${orderFormData.adress}, ${orderFormData.zipCode} ${orderFormData.city}`
            }
            if(orderFormData.paiementTerms == 'Au comptant'){
                data.paymentComptantAcompte = numFormater.format(orderFormData.cashTerm.depositAmount)
                data.paymentComptantSolde = numFormater.format(orderFormData.cashTerm.balance)
            } else if (orderFormData.paiementTerms == 'Avec financement'){
                data.paymentFinanceOrg = orderFormData.fundingTerm.financialOrganization
                data.paymentFinanceMontant = numFormater.format(orderFormData.fundingTerm.amount)
                data.paymentFinanceMensualite = numFormater.format(orderFormData.fundingTerm.monthlyPayment)
                data.paymentFinanceDuree = orderFormData.fundingTerm.duration
                data.paymentFinanceTaeg = orderFormData.fundingTerm.taeg
                data.paymentFinanceReport = orderFormData.fundingTerm.postponement
                data.paymentFinanceTaux = orderFormData.fundingTerm.lendingRate
                data.paymentTotalAmount = numFormater.format(orderFormData.fundingTerm.totalCost)
                data.paymentApportPerso = numFormater.format(orderFormData.fundingTerm.financialContribution)
            }
            orderFormData.agreement === 1 ? data.option1 = true : data.option2 = true


        }

        const makeAcessoriesData = ()=>{ 
            const isAccessories = !!needData.accessories && needData.accessories.length > 0
            const methodList = {
                'Isolation':(product)=>{
                    data.isolationAmountHt += product.prixHT
                    data.isolationAmountTtc += product.prixTTC
                },
                'Panneaux photo':(product)=>{
                    data.panneauxAmountTtc += product.prixTTC
                    data.panneauxAmountHt += product.prixHT
                },
                'Chauffe eau':(product)=>{
                    data.chauffeAmountHt += product.prixHT
                    data.chauffeAmountTtc += product.prixTTC
                },
                'Radiateurs':(product)=>{
                    data.radiaAmountHt = product.prixHT
                    data.radiaAmountTtc = product.prixTTC
                    data.radiaNumber = product.quantity
                    data.radiaType = product.type
                    data.radiaMarque = product.modèle
                },
                'Pompe Air-Air':(product)=>{
                    data.chauffeAmountHt += product.prixHT
                    data.chauffeAmountTtc += product.prixTTC
                },
                'Pompe Air-eau':(product)=>{
                    data.airAmountHt += product.prixHT
                    data.airAmountTtc += product.prixTTC
                },
            }
            const selectMethod = (product)=>{
                return methodList[product.categorie](product)
            }
            if(isAccessories){
                needData.accessories.forEach((product)=>{
                    selectMethod(product)
                })
            }
        }
        
        const makeTableData = () =>{
            let tableContent = []
            if(!!data.isolationPlancher || !!data.isolationMurs || !!data.isolationCombles){
                const row = {}
                row.qty = data.isolationSurface + 'm2'
                row.descr = 'Pack Isolation'
                row.tauxTva = 5.5
                row.ht = data.isolationAmountHt
                row.ttc = data.isolationAmountTtc

                tableContent.push(row)
            }
            if(!!data.panneauxAuto){
                const row = {}
                row.qty = data.panneauxNombre
                row.descr = 'Pack Panneaux Photovoltaïque'
                row.tauxTva = needData.panelsNumber > 10 ? 20 : 10
                row.ht = data.panneauxAmountHt
                row.ttc = data.panneauxAmountTtc

                tableContent.push(row)
            }
            if(!!data.chauffeAmountHt){
                const row = {}
                row.qty = 1
                row.descr = 'Pack Chauffe Eau'
                row.tauxTva = 5.5
                row.ht = data.chauffeAmountHt
                row.ttc = data.chauffeAmountTtc

                tableContent.push(row)
            }
            if(!!data.radiaAmountHt){
                const row = {}
                row.qty = 1
                row.descr = 'Pack Radiateurs Électriques à inertie'
                console.log(data.radiaAmountTtc , data.radiaAmount)
                row.tauxTva = toFixedNumber((((data.radiaAmountTtc / data.radiaAmount) - 1 )*100),2,10)
                row.ht = data.radiaAmountHt
                row.ttc = data.radiaAmountTtc

                tableContent.push(row)
            }
            if(!!data.airAmountHt){
                const row = {}
                row.qty = 1
                row.descr = 'Pack Pompe à chaleur Air-Air'
                row.tauxTva = 5.5
                row.ht = data.airAmountHt
                row.ttc = data.airAmountTtc

                tableContent.push(row)
            }
            if(!!data.eauAmountHt){
                const row = {}
                row.qty = 1
                row.descr = 'Pack Pompe à chaleur Air-Eau'
                row.tauxTva = 5.5
                row.ht = data.eauAmountHt
                row.ttc = data.eauAmountTtc

                tableContent.push(row)
            }

            data.amountTTCHT = 0  
            data.amountTTCTTC = 0
            tableContent.forEach((row,index)=>{
                data['line'+(index+1)+'Qty'] = row.qty
                data['line'+(index+1)+'Descr'] = row.descr
                data['line'+(index+1)+'TauxTva'] = row.tauxTva
                data['line'+(index+1)+'HT'] = row.ht
                data['line'+(index+1)+'TTC'] = row.ttc
                data.amountTTCTTC += row.ttc
                data.amountTTCHT +=row.ht
            })

            // data.amountTTCHT = numFormater.format(data.amountTTCHT)
            // data.amountTTCTTC = numFormater.format(data.amountTTCTTC)
        }

        makeIsolationData()
        makePanelsData()
        makeThermodynamiqueData()
        makePACData()
        makeAcessoriesData()
        makeOrderData()
        makeTableData()

        for(const key of Object.keys(data)){
            // console.log(key, typeof(key))
            if(key.toLowerCase().includes('ht') || key.toLowerCase().includes('ttc')){
                data[key] = numFormater.format(data[key])
            }
            else if(typeof(data[key]) ==='number'){
                data[key] = data[key].toString()
            }
        }
        return data
        
    }
 
    api.fillBdc = async (data, signature = false) => {
        const path = signature ? data.path : '/home/ec2-user/files/models/bdc.pdf'
        const existingPdfBytes = fs.readFileSync(path)
        let pdfDoc = await PDFDocument.load(existingPdfBytes)

        const timesRomanFont = await pdfDoc.embedFont(StandardFonts.TimesRoman)

        const model = bdcModel

        const pages = pdfDoc.getPages()
        await Promise.all(model.map(async (pageData, i) => {
          const page = pages[i];
          for (const [k, v] of Object.entries(pageData)) {
            if (!data[k]) continue
            switch (v.type || '') {
              case 'text':
                console.log(k,typeof(data[k]))
                page.drawText(data[k], { font: timesRomanFont, ...v });
                break
              case 'image':
                const image = await pdfDoc.embedPng(data[k])
                const dims = image.scale(0.5);
                const widthRatio = v.width / dims.width;
                const heightRatio = v.height / dims.height;
                const ratio = widthRatio > heightRatio ? heightRatio : widthRatio;
                const width = dims.width * ratio;
                const height = dims.height * ratio;
                page.drawImage(image, { ...v, width, height })
                break
              case 'checkbox':
                if (data[k]) {
                  page.drawText('X', { font: timesRomanFont, size: 14, ...v });
                }
                break
              default:
                console.log('Unknown type', v.type)
            }
          }
        }))
        const pdfBytes = await pdfDoc.save()

        return pdfBytes

    }
    

    api.writeBdc = (bdcId,pdfBytes,signed=false) => {
        const path = `/home/ec2-user/files/generated/bdc/${bdcId}${signed ? '-signé':''}.pdf`
        fs.writeFileSync(path, pdfBytes);
        const status = signed ? 'signé' : 'sauvegardé'
        bdcsData.update({_id:bdcId},{$set:{path:path, status:status}})
    }

    api.sendBdc = (bdcId,signed=false) =>{ 
        console.log(bdcId)
        const { path, clientId, _id } = bdcs.getBdc(bdcId)
        const { name, lastname, email } = clients.getClient(clientId)
        const user = Meteor.user()
        const emailOptions = {}
        const htmlArray = [
            `<h3>Bonjour ${name},</h3>`,
            `<div>Vous trouverez ci-joint un exemplaire du bon de commande ${signed ? 'signé' : ''} établis avec ${user.profile.name} ${user.profile.lastname}.</div><br>`,
            `<div>De plus, vous avez la possibilité de le consulter ${signed ? 'et de le signer en ligne' : ''} à tout moment en cliquant sur le bouton ci dessous :</div><br>`,
            `<div style='width:100%;display:flex'>`,
            `<a href='${Meteor.absoluteUrl()}documents/bdc/${_id}' style='margin:auto'><button style='border:1px solid #F7E13B;background:#F7E13B; border-radius:37px; height:25px'>Accédez à votre bon</button></a>`,
            `</div><br>`,
            `Cordialement,<br> le service commercial heh<br> `
        ]
        let emailHTML = ''
        htmlArray.forEach((line)=>{
            emailHTML += line
        })
        emailOptions.to = `${name} ${lastname} <${email}>`
        emailOptions.from = `${user.profile.name} ${user.profile.lastname} <no-reply@insideall.fr>`
        emailOptions.subject = `Votre bon de commande HEH du ${moment().format('DD/MM/YYYY')}`
        emailOptions.html = emailHTML
        emailOptions.attachments = [
            {   
                filename: `bdc${signed ? '-signé' : ''}.pdf`,
                path:path
            }
        ]

        Email.send(emailOptions)
        if(!signed){
            bdcsData.update({_id:bdcId},{$set:{status:'envoyé'}})
        }
        
    }


    return api;
})();