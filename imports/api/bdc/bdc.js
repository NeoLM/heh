import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';

const bdcsData = new Mongo.Collection('bdcsData')

var bdcsSchema = new SimpleSchema({
    userId:{
        type:String,
        label:'id commercial',
    },
    clientId:{
        type:String,
        label:'id client'
    },
    path:{
        type:String,
        label:"chemin d'accès du bdc",
        optional:true
    },
    status:{
      type:String,
      label:'Status du bdc',
      allowedValues:['créé', 'sauvegardé', 'envoyé', 'sms', 'signé', 'refusé']
  },
  code:{
    type:Number,
    label:'Code sms de vérification',
    optional:true
  },
    data:{
        type:Object,
        label:'Données insérées dans le bdc',
        blackbox:true
    },
    addedDate: {
        type: Date,
        label: "Creation date",
        autoValue: function () {
          if (this.isInsert) {
            return new Date();
          } else {
            this.unset();
          }
        }
      },
      modifiedDate: {
        type: Date,
        label: "Modification date",
        autoValue: function () {
          if (this.isUpdate) {
            return new Date();
          } else {
            this.unset();
          }
        },
        optional: true
      },
})

bdcsData.attachSchema(bdcsSchema);

export {
    bdcsData
}