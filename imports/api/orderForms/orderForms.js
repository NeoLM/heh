import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';

const orderFormsData = new Mongo.Collection('orderFormsData')

var cashTermSchema = new SimpleSchema({
  depositAmount:{
    type:Number,
    label:'Accompte de 30%',
    optional:true
  },
  balance:{
    type:Number,
    label:'Solde à la réception des travaux',
    optional:true
  }
})

var fundingTermSchema = new SimpleSchema({
  financialOrganization:{
    type:String,
    label:'Organisme financier',
    optional:true
  },
  amount:{
    type:Number,
    label:'Montant',
    optional:true
  },
  monthlyPayment:{
    type:Number,
    label:'Mensualité',
    optional:true
  },
  postponement:{
    type:Number,
    label:'Report',
    optional:true
  },
  duration:{
    type:Number,
    label:'Durée en mois',
    optional:true
  },
  financialContribution:{
    type:Number,
    label:'apport personnel',
    optional:true
  },
  taeg:{
    type:Number,
    label:'Taeg',
    optional:true
  },
  lendingRate:{
    type:Number,
    label:'Taux débiteur',
    optional:true
  },
  totalCost:{
    type:Number,
    label:'Coût total du crédit',
    optional:true
  }
})

var orderFormsSchema = new SimpleSchema({
    userId:{
        type:String,
        label:'id commercial',
    },
    clientId:{
        type:String,
        label:'id client'
    },
    deliveryDelay:{
      type:Number,
      label:'Délai de livraison'
    },
    adress:{
      type:String,
      label:'Adresse de livraison (si différente)',
      optional:true
    },
    zipCode:{
      type:String,
      label:'Code postal',
      optional:true
    },
    city:{
      type:String,
      label:'ville',
      optional:true
    },
    paiementTerms:{
      type:String,
      label:'Conditions de paiement',
      allowedValues:['Au comptant', 'Avec financement']
    },
    cashTerm:{
      type:cashTermSchema,
      optional:true
    },
    fundingTerm:{
      type:fundingTermSchema,
      optional:true
    },
    agreement:{
      type:Number,
      label:"Bon pour accord, option d'installation",
      allowedValues:[1,2]
    },
    addedDate: {
        type: Date,
        label: "Creation date",
        autoValue: function () {
          if (this.isInsert) {
            return new Date();
          } else {
            this.unset();
          }
        }
      },
      modifiedDate: {
        type: Date,
        label: "Modification date",
        autoValue: function () {
          if (this.isUpdate) {
            return new Date();
          } else {
            this.unset();
          }
        },
        optional: true
      },
})

orderFormsData.attachSchema(orderFormsSchema);

export {
    orderFormsData
}