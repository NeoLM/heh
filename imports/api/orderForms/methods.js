import { Meteor } from 'meteor/meteor'
import { orderFormsData } from './orderForms'

Meteor.methods({
  saveOrder(data,clientId){
    console.log(data)
    const user = Meteor.user();
    this.unblock();    
    if(user.profile.active){
      const entryTypeNumber = ['deliveryDelay', 'agreement']
      if(data.paiementTerms === 'Au comptant'){
          const cashValues = ['depositAmount', 'balance']
          const entryTypeNumberAdded = [...entryTypeNumber, ...cashValues]
          entryTypeNumberAdded.forEach((entry)=>{
            data[entry] = Number(data[entry])
          })
          const cashTerm = {}
          cashValues.forEach((key)=>{
            cashTerm[key] = data[key]
            delete data[key]
          })
          data.cashTerm = cashTerm
      } else {
          const fundingValues = ['amount', 'monthlyPayment','postponement', 'duration', 'financialContribution', 'taeg', 'lendingRate', 'totalCost']
          const entryTypeNumberAdded = [...entryTypeNumber, ...fundingValues]
          entryTypeNumberAdded.forEach((entry)=>{
            data[entry] = Number(data[entry])
          })
          const fundingTerm = {}
          fundingValues.forEach((key)=>{
            fundingTerm[key] = data[key]
            delete data[key]
          })
          fundingTerm.financialOrganization = data.financialOrganization
          data.fundingTerm = fundingTerm
      }

      const exist = orderForms.getOrderFormByClientId(clientId)
      if(!!exist){
        const id = orderFormsData.update({clientId:clientId}, {$set:{...data, userId:user._id}})
        return exist._id
      } else {
        const id = orderForms.newOrderForm({...data,clientId:clientId,userId:user._id})
        return id
      }
    } else {
      throw new Meteor.Error('Not allowed');
    }
  }
})

// @describ : Server API for orderForms manipulation
orderForms = (function () {

    var api = {};

    // @describ : creating a orderForms document
    // @Params : json object with all required fields
    // @result : new id of the document created
    api.newOrderForm = (data) => {
      try {
        var id = orderFormsData.insert({ ...data });
        return id;
      } catch (e) {
        throw new Meteor.Error('Error new orderForms', e.message);
      }
    };

    // @describ : getting a orderForms document
    // @Params : id of the document 
    // @result : content of the document fetched
    api.getOrderForm = (OrderFormId) => {
      try {
        return orderFormsData.findOne({ _id:OrderFormId });
      } catch (e) {
        throw new Meteor.Error('Error new orderForms', e.message);
      }
    };


    // @describ : getting a devis document
    // @Params : id of the document 
    // @result : content of the document fetched
    api.getOrderFormByClientId = (clientId) => {
      try {
        return orderFormsData.findOne({ clientId:clientId });
      } catch (e) {
        throw new Meteor.Error('Error get devis',e.message);
      }
    };

    // @describ : deleting a orderForms document
    // @Params : id of the orderForms document(string)
    // @result : id of the document deleted
    api.deleteOrderForm = (OrderFormId) => {
        try {
          return orderFormsData.remove({ _id:OrderFormId });
        } catch (e) {
          throw new Meteor.Error('Error delete orderForms');
        }
    };

    return api;
})();