import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';

const housingsData = new Mongo.Collection('housingsData')

var housingsSchema = new SimpleSchema({
    userId:{
        type:String,
        label:'id commercial',
    },
    clientId:{
        type:String,
        label:'id client'
    },
    housingType:{
        type:String,
        label:"Type d'habitation",
        allowedValues:['Maison', 'Appartement', 'Bureau', 'Commerce']
    },
    constructionYear:{
        type:Number,
        label:"Année de construction"
    },
    heaterType:{
        type:String,
        label:'Type de chauffage électrique',
        allowedValues:['Radiateur', 'Chaudière', 'Pompe à chaleur'],
    },
    combustibleHeaterType:{
        type:String,
        label:'Type de chauffage combustible',
        allowedValues:['Électrique','Gaz', 'Propane', 'Fioul', 'Bois'],
    },
    ambientTemperature:{
        type:Number,
        label:'temperature ambiance souhaité',
    },
    livingSpace:{
        type:Number,
        label:'Surface habitable'
    },
    ceilingHeight:{
        type:Number,
        label:'Hauteur sous plafond'
    },
    levelNumber:{
        type:Number,
        label:'Nombe de niveaux'
    },
    buildingSoil:{
        type:String,
        label:'Construction sur sol de type :',
        allowedValues:['Terre plein', 'Vide sanitaire', 'Sous-sol']
    },
    insulationCoeff:{
        type:Number,
        label:"Coefficient d'isolation",
        allowedValues:[1,1.3,1.5,1.8,2]
    },
    lastRenovations:{
        type:String,
        label:"Travaux de rénovation réalisés dans les 10 dernières années",
        optional:true
    },
    combustibleConsumption:{
      type:Number,
      label:"Consommation du ménage de combustible en €",
      optional:true
    },
    currentConsumption:{
      type:Number,
      label:"Consommation du ménage en €"
    },
    roofIsolationQuality:{
      type:String,
      label:"Qualité de l'isolation de la toiture",
      allowedValues:['Bonne', 'Moyenne', 'Mauvaise']
    },
    brickwallIsolationQuality:{
      type:String,
      label:"Qualité de l'isolation des murs",
      allowedValues:['Bonne', 'Moyenne', 'Mauvaise']
    },
    floorIsolationQuality:{
      type:String,
      label:"Qualité de l'isolation du sol",
      allowedValues:['Bonne', 'Moyenne', 'Mauvaise']
    },
    windowType:{
      type:String,
      label:"Type de fenetre",
      allowedValues:['Simple vitrage', 'Double vitrage']
    },
    windowMaterial:{
      type:String,
      label:"Matière des fenetre",
      allowedValues:['PVC', 'Bois', 'Autre']
    },
    addedDate: {
        type: Date,
        label: "Creation date",
        autoValue: function () {
          if (this.isInsert) {
            return new Date();
          } else {
            this.unset();
          }
        }
      },
      modifiedDate: {
        type: Date,
        label: "Modification date",
        autoValue: function () {
          if (this.isUpdate) {
            return new Date();
          } else {
            this.unset();
          }
        },
        optional: true
      },
})

housingsData.attachSchema(housingsSchema);

export {
    housingsData
}