import { Meteor } from 'meteor/meteor'
import { housingsData } from './housings'


Meteor.methods({
  saveHousing(data,clientId){
    const user = Meteor.user();
    this.unblock();    
    if(user.profile.active){
      const entryTypeNumber = ['livingSpace', 'ceilingHeight', 'levelNumber', 'insulationCoeff', 'combustibleConsumption', 'currentConsumption', 'constructionYear', 'ambientTemperature']
      entryTypeNumber.forEach((entry)=>{
        if(!!data[entry]){
          data[entry] = Number(data[entry])
        }
      })
      const exist = housings.getHousingByClientId(clientId)
      if(!!exist){
        const id = housingsData.update({clientId:clientId}, {$set:{...data, userId:user._id}})
        return id
      } else {
        console.log(data)
        const id = housings.newHousing({...data,clientId:clientId,userId:user._id})
        console.log(id)
        return id
      }
    } else {
      throw new Meteor.Error('Not allowed');
    }
  }
})


// @describ : Server API for housing manipulation
housings = (function () {

    var api = {};

    // @describ : creating a housing document
    // @Params : json object with all required fields
    // @result : new id of the document created
    api.newHousing = (data) => {
      try {
        var id = housingsData.insert({ ...data });
        return id;
      } catch (e) {
        throw new Meteor.Error('Error new housing', e.message);
      }
    };

    // @describ : getting a housing document
    // @Params : id of the document 
    // @result : content of the document fetched
    api.getHousing = (housingId) => {
      try {
        return housingsData.findOne({ _id:housingId });
      } catch (e) {
        throw new Meteor.Error('Error new housing');
      }
    };

    // @describ : getting a housing document by clientId
    // @Params : id of the document 
    // @result : content of the document fetched
    api.getHousingByClientId = (clientId) => {
      try {
        return housingsData.findOne({ clientId:clientId });
      } catch (e) {
        throw new Meteor.Error('Error get housing');
      }
    };

    // @describ : deleting a housing document
    // @Params : id of the housing document(string)
    // @result : id of the document deleted
    api.deleteHousing = (housingId) => {
        try {
          return housingsData.remove({ _id:housingId });
        } catch (e) {
          throw new Meteor.Error('Error delete housing');
        }
    };

    return api;
})();