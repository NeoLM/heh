import { Meteor } from 'meteor/meteor'
import { needsData } from './needs'


Meteor.methods({
  saveNeed(data,clientId){
    const user = Meteor.user();
    this.unblock();    
    if(user.profile.active){
      const entryTypeNumber = ['floorRoofSpace', 'floorRoofPrice', 'crawlingRoofSpace', 'crawlingRoofPrice', 'insideWallMeasure', 'insideWallPrice', 'outsideWallMeasure', 'outsideWallPrice','lowFloorMeasure', 'lowFloorPrice','panelsNumber', 'panelsPrice']
      entryTypeNumber.forEach((entry)=>{
        data[entry] = Number(data[entry])
      })

      const exist = needs.getNeedByClientId(clientId)
      if(!!exist){
        const id = needsData.update({clientId:clientId}, {$set:{...data, userId:user._id}})
        return id
      } else {
        const id = needs.newNeed({...data,clientId:clientId,userId:user._id})
        return id
      }
    } else {
      throw new Meteor.Error('Not allowed');
    }
  }
})


// @describ : Server API for needs manipulation
needs = (function () {

    var api = {};

    // @describ : creating a needs document
    // @Params : json object with all required fields
    // @result : new id of the document created
    api.newNeed = (data) => {
      try {
        var id = needsData.insert({ ...data });
        return id;
      } catch (e) {
        throw new Meteor.Error('Error new needs');
      }
    };

    // @describ : getting a needs document
    // @Params : id of the document 
    // @result : content of the document fetched
    api.getNeed = (needId) => {
      try {
        return needsData.findOne({ _id:needId });
      } catch (e) {
        throw new Meteor.Error('Error new needs');
      }
    };

    // @describ : getting a need document by clientId
    // @Params : id of the document 
    // @result : content of the document fetched
    api.getNeedByClientId = (clientId) => {
      try {
        return needsData.findOne({ clientId:clientId });
      } catch (e) {
        throw new Meteor.Error('Error new need');
      }
    };

    // @describ : deleting a needs document
    // @Params : id of the needs document(string)
    // @result : id of the document deleted
    api.deleteNeed = (needId) => {
        try {
          return needsData.remove({ _id:needId });
        } catch (e) {
          throw new Meteor.Error('Error delete needs');
        }
    };

    return api;
})();