import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';

const needsData = new Mongo.Collection('needsData')

var needsSchema = new SimpleSchema({
    userId:{
        type:String,
        label:'id commercial',
    },
    clientId:{
        type:String,
        label:'id client'
    },
    floorRoofSpace:{
      type:Number,
      label:'Surface des combles au sol en m2',
      optional:true
    },
    crawlingRoofSpace:{
      type:Number,
      label:'Surface des combles rampants en m2',
      optional:true
    },
    floorRoofPrice:{
      type:Number,
      label:"Tarif de l'isolation des combles au sol en €",
      optional:true
    },
    crawlingRoofPrice:{
      type:Number,
      label:"Tarif de l'isolation des combles au sol en €",
      optional:true
    },
    insideWallMeasure:{
      type:Number,
      label:'Mesure des murs intérieurs en m2',
      optional:true
    },
    insideWallPrice:{
      type:Number,
      label:"Tarif de l'isolation des murs intérieurs en €",
      optional:true
    },
    outsideWallMeasure:{
      type:Number,
      label:'Mesure des murs extérieurs en m2',
      optional:true
    },
    outsideWallPrice:{
      type:Number,
      label:"Tarif de l'isolation des murs extérieurs en €",
      optional:true
    },
    lowFloorMeasure:{
      type:Number,
      label:'Mesure des planchers bas en m2',
      optional:true
    },
    lowFloorPrice:{
      type:Number,
      label:"Tarif de l'isolation des planchers bas en €",
      optional:true
    },
    photovoltaicPanels:{
      type:Boolean,
      label:'Panneaux photovoltaïques',
      optional:true
    },
    panelsNumber:{
      type:Number,
      label:'Nombre de panneaux photovoltaïques',
      optional:true
    },
    panelsPrice:{
      type:Number,
      label:'Prix des panneaux photovoltaïques',
      optional:true
    },
    panelsOrientation:{
      type:String,
      label:'Orientation des panneaux photovoltaïques',
      optional:true
    },
    panelsInclinaison:{
      type:String,
      label:'Inclinaison des panneaux photovoltaïques',
      optional:true
    },
    batteriePanels:{
      type:String,
      label:'Option de batterie accompagnant les panneaux photovoltaïques',
      optional:true
    },
    airToAirHeatPump:{
      type:Array,
      label:'Pompes à chaleur air air',
      optional:true
    },
    'airToAirHeatPump.$':{type:Object,blackbox:true},    
    airWaterHeatPump:{
      type:Array,
      label:'Pompes à chaleur air eau',
      optional:true
    },
    'airWaterHeatPump.$':{type:Object,blackbox:true}, 
    thermodynamicHeater:{
      type:Array,
      label:'Ballon thermodynamiques',
      optional:true
    },
    'thermodynamicHeater.$':{type:Object,blackbox:true},
    accessories:{
      type:Array,
      label:'Accessoires',
      optional:true
    },
    'accessories.$':{type:Object,blackbox:true},  
    addedDate: {
        type: Date,
        label: "Creation date",
        autoValue: function () {
          if (this.isInsert) {
            return new Date();
          } else {
            this.unset();
          }
        }
      },
      modifiedDate: {
        type: Date,
        label: "Modification date",
        autoValue: function () {
          if (this.isUpdate) {
            return new Date();
          } else {
            this.unset();
          }
        },
        optional: true
      },
})

needsData.attachSchema(needsSchema);

export {
    needsData
}