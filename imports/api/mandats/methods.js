import { Meteor } from 'meteor/meteor'
import { mandatsData } from './mandats'
import fs from 'fs';
import path from 'path';
import PdfLib from 'pdf-lib';
const { PDFDocument, StandardFonts } = PdfLib;
import { moment } from 'meteor/momentjs:moment';
import '../clients/methods'
import '../needs/methods'
import '../orderForms/methods'
import '../simulation/methods'
import { Email } from 'meteor/email'
import mandatsModel from '../pdf/mandat'
require('intl')

import { paneauxPhotovoltaiques,
    ballonsThermodynamiques,
    pompeAirEau,
    paneauxPhotovoltaiquesPrix,
    batteriePhotovolataique,
    isolation
} from '../dbprice'

Meteor.methods({

    generateAndSendMandat(clientId){
        const user = Meteor.user();
        if(user.profile.active){
            const data = mandats.getDataToFillMandat(clientId,user._id)
            return mandats.fillMandat(data).then((pdfBytes)=>{
                const mandatId = mandats.saveMandat(data,clientId,user._id)
                console.log(mandatId)
                mandats.writeMandat(mandatId,pdfBytes)
                mandats.sendMandat(mandatId)
                return mandatId
            })
        } else {
            throw new Meteor.Error('Not allowed');
        }
    },
    getMandat(mandatId){
        console.log(mandatId)
        const {path, status} = mandats.getMandat(mandatId)
        if(!!path){
            const blob = fs.readFileSync(path);
            return [blob, status]
        } else {
            throw new Meteor.Error('No mandats found');
        }
    },
    sendVerficationSMSMandat(mandatId){
        const {clientId} = mandats.getMandat(mandatId)
        const {phone, name} = clients.getClient(clientId)
        const formatedPhone = phone.replace('0', '+33')
        const code = Math.floor(Math.random() * 90000) + 10000
        const message = `Bonjour ${name}, voici votre code de vérification pour la signature : ${code}`
        sms.sendVerfication(formatedPhone, message)
        devis.addVerifCode(mandatId, code)
        return true
    },
    verifySmsMandat(mandatId, codeSms){
        const {code, modifiedDate} = mandats.getMandat(mandatId)
        const startTime = new Date(modifiedDate)
        const actualTime = new Date()
        const delay = 2*60*1000
        if(actualTime - startTime < delay){
            if(codeSms == code.toString()){
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    },
    signAndSendMandat(mandatId, signature){
        const mandatData = mandats.getMandat(mandatId)
        if(!!mandatData){
            const data = {}
            data.signature = signature
            data.path = mandatData.path
            return mandats.fillMandat(data, true).then((pdfBytes)=>{
                console.log(mandatId)
                mandats.writeMandat(mandatId,pdfBytes,true)
                mandats.sendMandat(mandatId,true)
                return mandatId
            })
        } else {
            throw new Meteor.Error('Not allowed');
        }
    }
})

// @describ : Server API for mandat manipulation
mandats = (function () {

    var api = {};

    // @describ : creating a mandat document
    // @Params : json object with all required fields
    // @result : new id of the document created
    api.newMandat = (data) => {
      try {
        var id = mandatsData.insert({...data});
        return id;
      } catch (e) {
        throw new Meteor.Error('Error new mandat',e.message);
      }
    };

    // @describ : getting a mandat document
    // @Params : id of the document 
    // @result : content of the document fetched
    api.getMandat = (mandatId) => {
      try {
        return mandatsData.findOne({ _id:mandatId });
      } catch (e) {
        throw new Meteor.Error('Error get mandat',e.message);
      }
    };

    // @describ : getting a mandat document
    // @Params : id of the document 
    // @result : content of the document fetched
    api.getMandatByClientId = (clientId) => {
        try {
          return mandatsData.findOne({ clientId:clientId });
        } catch (e) {
          throw new Meteor.Error('Error get mandat',e.message);
        }
      };

    // @describ : deleting a mandat document
    // @Params : id of the mandat document(string)
    // @result : id of the document deleted
    api.deleteMandat = (mandatId) => {
        try {
          return devisData.remove({ _id:mandatId });
        } catch (e) {
          throw new Meteor.Error('Error delete mandat');
        }
    };

    api.saveMandat = (mandats,clientId,userId) => {
        const insertData = {
            userId:userId,
            clientId:clientId,
            mandats:mandats,
            status:'créé',
        }
        try{
            const id = mandats.newMandat(insertData)
            return id
        }catch (e) {
            throw new Meteor.Error('Error save bdc',e.message);
        }
    }

    api.selectMandats = (clientId) => {
        const { photovoltaicPanels, airToAirHeatPump, airWaterHeatPump, thermodynamicHeater, outsideWallMeasure } = needs.getNeedByClientId(clientId)
        let pv, ite, pac = false
        if(photovoltaicPanels){
            pv = true
        }
        if(airToAirHeatPump.length > 0 || airWaterHeatPump.length > 0 || thermodynamicHeater.length > 0){
            pac = true
        }
        if(outsideWallMeasure > 0){
            ite == true
        }

        const deduction = simulations.getSimulationByClientId(cliendId).aides.mprReception == 1 ? false : true 

        const mandatsList = []

        if(pv){
            mandatsList.push('mairie', 'enedis')
        }
        if(pac){
            deduction ? mandatsList.push('mairie', 'cee', 'mpr-administratif-financier') : mandatsList.push('mairie', 'cee', 'mpr-administratif')
        }
        if(ite){
            deduction ? mandatsList.push('cee', 'mpr-administratif-financier') : mandatsList.push('cee', 'mpr-administratif', 'cdp')
        }

        return [...new Set(mandatsList)]
    } 


    api.fillMandat = async (data, signature = false, mandatType) => {
        const path = signature ? data.path : `/home/ec2-user/files/models/mandat-${mandatType}.pdf`
        const existingPdfBytes = fs.readFileSync(path)
        let pdfDoc = await PDFDocument.load(existingPdfBytes)

        const timesRomanFont = await pdfDoc.embedFont(StandardFonts.TimesRoman)

        const model = mandatsModel[mandatType]

        const pages = pdfDoc.getPages()
        await Promise.all(model.map(async (pageData, i) => {
          const page = pages[i];
          for (const [k, v] of Object.entries(pageData)) {
            if (!data[k]) continue
            switch (v.type || '') {
              case 'text':
                console.log(k,typeof(data[k]))
                page.drawText(data[k], { font: timesRomanFont, ...v });
                break
              case 'image':
                const image = await pdfDoc.embedPng(data[k])
                const dims = image.scale(0.5);
                const widthRatio = v.width / dims.width;
                const heightRatio = v.height / dims.height;
                const ratio = widthRatio > heightRatio ? heightRatio : widthRatio;
                const width = dims.width * ratio;
                const height = dims.height * ratio;
                page.drawImage(image, { ...v, width, height })
                break
              case 'checkbox':
                if (data[k]) {
                  page.drawText('X', { font: timesRomanFont, size: 14, ...v });
                }
                break
              default:
                console.log('Unknown type', v.type)
            }
          }
        }))
        const pdfBytes = await pdfDoc.save()

        return pdfBytes

    }
    

    api.writeMandat = (mandatId,pdfBytes,signed=false) => {
        const path = `/home/ec2-user/files/generated/mandat/${mandatId}${signed ? '-signé':''}.pdf`
        fs.writeFileSync(path, pdfBytes);
        const status = signed ? 'signé' : 'sauvegardé'
        mandatsData.update({_id:mandatId},{$set:{path:path, status:status}})
    }

    api.sendMandat = (mandatId,signed=false) =>{ 
        console.log(mandatId)
        const { path, clientId, _id } = mandats.getMandat(mandatId)
        const { name, lastname, email } = clients.getClient(clientId)
        const user = Meteor.user()
        const emailOptions = {}
        const htmlArray = [ 
            `<h3>Bonjour ${name},</h3>`,
            `<div>Vous trouverez ci-joint un exemplaire du mandat ${signed ? 'signé' : ''} établis avec ${user.profile.name} ${user.profile.lastname}.</div><br>`,
            `<div>De plus, vous avez la possibilité de le consulter ${signed ? 'et de le signer en ligne' : ''} à tout moment en cliquant sur le bouton ci dessous :</div><br>`,
            `<div style='width:100%;display:flex'>`,
            `<a href='${Meteor.absoluteUrl()}documents/bdc/${_id}' style='margin:auto'><button style='border:1px solid #F7E13B;background:#F7E13B; border-radius:37px; height:25px'>Accédez à votre bon</button></a>`,
            `</div><br>`,
            `Cordialement,<br> le service commercial heh<br> `
        ]
        let emailHTML = ''
        htmlArray.forEach((line)=>{
            emailHTML += line
        })
        emailOptions.to = `${name} ${lastname} <${email}>`
        emailOptions.from = `${user.profile.name} ${user.profile.lastname} <no-reply@insideall.fr>`
        emailOptions.subject = `Votre bon de commande HEH du ${moment().format('DD/MM/YYYY')}`
        emailOptions.html = emailHTML
        emailOptions.attachments = [
            {   
                filename: `bdc${signed ? '-signé' : ''}.pdf`,
                path:path
            }
        ]

        Email.send(emailOptions)
        if(!signed){
            mandatsData.update({_id:mandatId},{$set:{status:'envoyé'}})
        }
        
    }


    return api;
})();