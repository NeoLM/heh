import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';

const mandatsData = new Mongo.Collection('mandatsData')

var mandatSchema = new SimpleSchema({
    path:{
        type:String,
        label:"chemin d'accès du mandat",
        optional:true
    },
    status:{
      type:String,
      label:'Status du mandat',
      allowedValues:['créé', 'sauvegardé', 'envoyé', 'sms', 'signé', 'refusé']
    },
    data:{
        type:Object,
        label:'Données insérées dans le mandat',
        blackbox:true
    },
})

var mandatsSchema = new SimpleSchema({
    userId:{
        type:String,
        label:'id commercial',
    },
    clientId:{
        type:String,
        label:'id client'
    },
    devisId:{
        type:String,
        label:'id devis'
    },
    mandats:{
        type:Array,
        label:'Liste des mandats'
    },
    'mandats.$':mandatSchema,
    code:{
        type:Number,
        label:'Code sms de vérification',
        optional:true
    },
    addedDate: {
        type: Date,
        label: "Creation date",
        autoValue: function () {
          if (this.isInsert) {
            return new Date();
          } else {
            this.unset();
          }
        }
    },
    modifiedDate: {
        type: Date,
        label: "Modification date",
        autoValue: function () {
          if (this.isUpdate) {
            return new Date();
          } else {
            this.unset();
          }
        },
        optional: true
      },
})

mandatsData.attachSchema(mandatsSchema);

export {
    mandatsData
}