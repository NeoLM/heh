import { Meteor } from 'meteor/meteor'

Meteor.methods({

    updateUserInfo(data){
        const user = Meteor.user()
        const { email, phone, location } = data
        if(!!user && user.profile.active){
            Meteor.users.update({_id:user._id},{$set:{
                'emails[0].address':email,
                'profile.phone':phone,
                'profile.location':location
            }})
        }

        return true

    },

    saveUserSignature(signature){
        const user = Meteor.user()
        if(!!user && user.profile.active){
            Meteor.users.update({_id:user._id},{$set:{
                'profile.signature':signature
            }})
        }

        return true
    }
})