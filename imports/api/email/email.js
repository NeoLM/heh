import { Accounts } from 'meteor/accounts-base'

Accounts.emailTemplates.siteName = 'dev-heh.insideall.fr';
Accounts.emailTemplates.from = 'dev heh Admin <accounts@insideall.fr>';

Accounts.emailTemplates.resetPassword = {
  from () {
    // Overrides the value set in `Accounts.emailTemplates.from` when resetting
    // passwords.
    return 'Service technique <no-reply@insideall.fr>';
  },
  subject(user) {
    return `Réinitialisation de votre mot de passe`
  },
  html(user, url) {
    const htmlArray = [
      `<div style="display:flex;flex-direction:column;"><img src="${Meteor.absoluteUrl('src/img/logo.png')}" style="margin:auto"/><h2 style='margin:auto'>RÉINITIALISER MON MOT DE PASSE</h2><img src="${Meteor.absoluteUrl('src/assets/auth.svg')}" style="margin:auto"/></div>`,
      `<div style='width:100%;display:flex;flex-direction:column;'>`,
      `<h3 style='margin:10px auto'>Bonjour ${user.profile.name},</h3>`,
      `<div style='margin:auto'>Ce message fait suite à votre demande de réinitialisation de mot de passe</div><br>`,
      `<div style='margin:auto'>Si il s'agit d'une erreur, ignorez ce message et la demande ne sera pas prise en compte. Pour renouveller votre mot de passe, cliquez sur le bouton ci-dessous : </div><br>`,
      `<a href='${url}' style='margin:auto;cursor:pointer;'><button style='border:1px solid #F7E13B;background:#F7E13B; border-radius:37px; height:30px; font-size:16px; font-weight:800';cursor:pointer;> Réinitialisez votre mot de passe </button></a>`,
      `</div><br>`,
      `Cordialement,<br /> le service technique<br /> `
    ]
    let emailHTML = ''
    htmlArray.forEach((line)=>{
        emailHTML += line
    })
    return emailHTML
  }
}

Accounts.urls.resetPassword = function(token) {
  return Meteor.absoluteUrl() + 'login?resetPassword=' + token;
};

Accounts.emailTemplates.enrollAccount = {
  from () {
    // Overrides the value set in `Accounts.emailTemplates.from` when resetting
    // passwords.
    return 'Service technique <no-reply@insideall.fr>';
  },
  subject(user) {
    return `Création de votre mot de passe`
  },
  html(user, url) {
    const htmlArray = [
      `<div style="display:flex;flex-direction:column;"><img src="${Meteor.absoluteUrl('src/img/logo.png')}" style="margin:auto"/><h2 style='margin:auto'>CRÉER MON MOT DE PASSE</h2><img src="${Meteor.absoluteUrl('src/assets/auth.svg')}" style="margin:auto"/></div>`,
      `<div style='width:100%;display:flex;flex-direction:column;'>`,
      `<h3 style='margin:10px auto'>Bonjour ${user.profile.name},</h3>`,
      `<div style='margin:auto'>Ce message fait suite à la création de votre compte</div><br>`,
      `<div style='margin:auto'>Vous pouvez créer votre mot de passe en cliquant sur le bouton suivant : </div><br>`,
      `<a href='${url}' style='margin:auto;cursor:pointer;'><button style='border:1px solid #F7E13B;background:#F7E13B; border-radius:37px; height:30px;font-size:16px; font-weight:800';cursor:pointer;> Créez votre mot de passe </button></a>`,
      `</div><br>`,
      `Cordialement,<br /> le service technique<br /> `
    ]
    let emailHTML = ''
    htmlArray.forEach((line)=>{
        emailHTML += line
    })
    return emailHTML
  }
}

Accounts.urls.enrollAccount = function(token) {
return Meteor.absoluteUrl() +  'login?initPassword=' + token;
};


