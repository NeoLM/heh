import { Meteor } from 'meteor/meteor'
import { simulationsData } from './simulation'

import { clientsData } from '../clients/clients'
import '../clients/methods'

import { housingsData } from '../housings/housings'
import '../housings/methods'

import { needsData } from '../needs/needs'
import '../needs/methods'

import '../utils/methods'

import {
  ceeBaremIDF,
  ceeBaremAll,
  ceeAll,
  zone,
  chauffageH1,
  chauffageH2,
  chauffageH3
} from '../dbCEE'

import {
  baremeIDF,
  baremeAll,
  bleu,
  jaune,
  rose
} from '../dbMPR'


import { mappingZipCodeToDegree } from '../dbprice'

Meteor.methods({

    computeBilan(clientId){
        const user = Meteor.user();
        this.unblock();    
        if(user.profile.active){
            const bilan = simulations.computeBilan(clientId)
            const newSimulationData = {
              bilan:bilan,
            }
            const isSimulationData = simulations.getSimulationByClientId(clientId)
            let id
            if(!!isSimulationData){
                simulationsData.update({clientId:clientId},{$set:newSimulationData})
                id = isSimulationData._id
                return {
                    simulationData:newSimulationData,
                    id:id
                }
            } else {
                id = simulations.newSimulation({...newSimulationData,userId:user._id,clientId:clientId})
                return {
                    simulationData:{...newSimulationData, _id:id, userId:user._id,clientId:clientId},
                    id:id
                }
            }  
        }
    },

    computeEco(clientId){
        const user = Meteor.user();
        this.unblock();    
        if(user.profile.active){
            const economies = simulations.computeEco(clientId)
            const newSimulationData = {
                economies:economies,
            }
            const isSimulationData = simulations.getSimulationByClientId(clientId)
            let id
            if(!!isSimulationData){
                simulationsData.update({clientId:clientId},{$set:newSimulationData})
                id = isSimulationData._id
                return {
                    simulationData:{...isSimulationData, ...newSimulationData},
                    id:id
                }
            } else {
                id = simulations.newSimulation({...newSimulationData,userId:user._id,clientId:clientId})
                return {
                    simulationData:{...newSimulationData, _id:id, userId:user._id,clientId:clientId},
                    id:id
                }
            }  
        }
    },

    computeHelp(clientId){
        const user = Meteor.user();
        this.unblock();    
        if(user.profile.active){
            const cee = simulations.computeCEE(clientId)
            const mpr = simulations.computeMPR(clientId)
            const newAides = {
                cee:cee,
                mpr:mpr
            }
            console.log(newAides)
            const isSimulationData = simulations.getSimulationByClientId(clientId)
            let id
            if(!!isSimulationData){
                simulationsData.update({clientId:clientId},{$set:{aides:{...isSimulationData.aides, ...newAides}}})
                id = isSimulationData._id
                return {
                    simulationData:{...isSimulationData, aides:{...isSimulationData.aides, ...newAides}},
                    id:id
                }
            } else {
                id = simulations.newSimulation({...newSimulationData,userId:user._id,clientId:clientId})
                return {
                    simulationData:{...newSimulationData, _id:id, userId:user._id,clientId:clientId},
                    id:id
                }
            }  
        }
    },



    updateSimulation(clientId, data){
      const user = Meteor.user();
      this.unblock();    
      if(user.profile.active){
        const needData = simulations.getSimulationByClientId(clientId)
        const newAides = { ...needData.aides, ...data }
        simulationsData.update({clientId:clientId},{$set:{aides:newAides}})
        return newAides
      }
    }
})

// @describ : Server API for clients manipulation
simulations = (function () {

    var api = {};

    // @describ : creating a simulations document
    // @Params : json object with all required fields
    // @result : new id of the document created
    api.newSimulation = (data) => {
      try {
        var id = simulationsData.insert({...data});
        return id;
      } catch (e) {
        throw new Meteor.Error('Error new simulations',e.message);
      }
    };

    // @describ : getting a simulations document
    // @Params : id of the document 
    // @result : content of the document fetched
    api.getSimulation = (SimulationId) => {
      try {
        return simulationsData.findOne({ _id:simulationId });
      } catch (e) {
        throw new Meteor.Error('Error new simulations');
      }
    };

    // @describ : getting a simulations document
    // @Params : id of the document 
    // @result : content of the document fetched
    api.getSimulationByClientId = (clientId) => {
        try {
          return simulationsData.findOne({ clientId:clientId });
        } catch (e) {
          throw new Meteor.Error('Error new simulations');
        }
      };

    // @describ : deleting a simulations document
    // @Params : id of the simulations document(string)
    // @result : id of the document deleted
    api.deleteSimulation = (SimulationId) => {
        try {
          return simulationsData.remove({ _id:simulationId });
        } catch (e) {
          throw new Meteor.Error('Error delete simulations');
        }
    };

    api.computeCEE = (clientId) => {
        const clientData = clients.getClient(clientId)
        const housingData = housings.getHousingByClientId(clientId)
        const needData = needs.getNeedByClientId(clientId)
        const housingType = housingData.housingType.toLowerCase()
        
        if(housingType === 'bureau' || housingType === 'commerce') {
           return 'Non admissible'
        }
        const computeRevenueType = () => {
            const { zipCode, residentsNumber, lastYearHouseholdIncome } = clientData
            const idfRegions = ["75", "77", "78", "91", "92", "93", "94", "95"]
            const useBarem = idfRegions.includes(zipCode.substring(0,2)) ? ceeBaremIDF : ceeBaremAll
            const revenueTypeList = ['tmo', 'mo', 'st']

            let revenueType 

            if(residentsNumber < 6){
                const ressourceSlot = useBarem[residentsNumber]
                ressourceSlot.forEach((threshold, index)=>{
                    if(lastYearHouseholdIncome < threshold){
                        revenueType = revenueTypeList[index]
                    }else{
                        if(index == 1){
                            revenueType = revenueTypeList[index + 1]
                        }
                    }
                })
            } else {
                const ressourceSlot = useBarem[5]
                const personnesSup = residentsNumber - 5
                for(let i = 0; i < personnesSup; i ++){
                    ressourceSlot[0] = ressourceSlot[0] + useBarem.added[0]
                    ressourceSlot[1] = ressourceSlot[1] + useBarem.added[1]
                }
                ressourceSlot.forEach((threshold, index)=>{
                    if(lastYearHouseholdIncome < threshold){
                        revenueType = revenueTypeList[index]
                    }else{
                        if(index == 1){
                            revenueType = revenueTypeList[index + 1]
                        }
                    }
                })

            }

            return revenueType
        }

        const computeZone = () => {
            const { zipCode } = clientData
            const region = Number(zipCode.substring(0,2))
            let zoneH
            for(const [key,value] of Object.entries(zone)){
                if(value.includes(region)){
                    zoneH = key
                    break
                }
            }
            return zoneH
        }

        const computeIsolation = (revenueType) => {
            const { isolation } = ceeAll
            const { floorRoofSpace, crawlingRoofSpace, lowFloorMeasure } = needData

            return {
                Comble:isolation.comble[revenueType] * floorRoofSpace + isolation.comble[revenueType] * crawlingRoofSpace,
                Plancher:isolation.sol_planchers[revenueType] * lowFloorMeasure
            }
        }

        const computePAC = (revenueType) => {
            const { photovoltaicPanels, airWaterHeatPump } = needData
            const { pac } = ceeAll
            let help = 0
            let result = {}
            if(photovoltaicPanels.length > 0){
                help += pac.solaire[revenueType]
                result['Solaire'] = pac.solaire[revenueType]
            }
            if(airWaterHeatPump.length > 0){
                help += pac.air_eau[revenueType]
                result['Air/eau'] = pac.air_eau[revenueType]
            }

            return result
        }
        

        const computeInstallation = (revenueType,zoneH,housingType) => {
            const chauffage = zoneH === 'h1' ? chauffageH1 : zoneH === 'h2' ? chauffageH2 : chauffageH3
            const { livingSpace } = housingData
            const {photovoltaicPanels, airWaterHeatPump, airToAirHeatPump, thermodynamicHeater} = needData

            let help = 0
            const result = {}

            if(thermodynamicHeater.length > 0){
                const { chauffe_eau } = chauffage
                for(let i=0; i < thermodynamicHeater.length ; i++){
                    help += chauffe_eau.thermodynamique[housingType][revenueType]
                    result['Chauffe-eau thermodynamique'] = chauffe_eau.thermodynamique[housingType][revenueType]
                }
            }

            if(!!photovoltaicPanels){
                const { solaire } = chauffage
                help += solaire[revenueType]
                result['Solaire'] = solaire[revenueType]
            }

            if(airWaterHeatPump.length > 0){
                const { air_eau } = chauffage.PAC
                const threshold = 'threshold_3'
                const threshold_surface = housingType === 'maison' ? [70,90,110,130] : [35,60,70,90,110,130]
                let helpPAC = 0
                for (let i = 0; i < threshold_surface.length; i++) {
                    const thres = threshold_surface[i]
                    if(livingSpace > thres){
                        if(thres == 130){
                            helpPAC += air_eau[revenueType][housingType][threshold]['top']
                            break
                        } else {
                            continue
                        }
                    } else {
                        if(thres == 130){
                            helpPAC += air_eau[revenueType][housingType][threshold]['top']

                            break
                        } else {
                            helpPAC += air_eau[revenueType][housingType][threshold][thres]
                            break
                        }
                    }  
                }
                help += helpPAC * airWaterHeatPump.length
                result['Air/eau'] = helpPAC * airWaterHeatPump.length

            }

            if(airToAirHeatPump.length > 0){
                const { air_air } = chauffage.PAC
                const threshold = housingType === 'maison' ? 'threshold_2' : 'threshold_1'
                const threshold_surface = [35,60,70,90,110,130]

                let helpPAC = 0
                for (let i = 0; i < threshold_surface.length; i++) {
                    const thres = threshold_surface[i]
                    if(livingSpace > thres){
                        if(thres == 130){
                            helpPAC += air_air[revenueType][housingType][threshold]['top']
                            break
                        } else {
                            continue
                        }
                    } else {
                        if(thres == 130){
                            helpPAC += air_air[revenueType][housingType][threshold]['top']
                            break
                        } else {
                            helpPAC += air_air[revenueType][housingType][threshold][thres]
                            break
                        }
                    }  
                }

                help += helpPAC * airToAirHeatPump.length
                result['Air/air'] = helpPAC * airWaterHeatPump.length
            }

            return result
        }

        const revenueType = computeRevenueType()
        const zoneH = computeZone()
        const isolationHelp = computeIsolation(revenueType)
        const PACHelp = computePAC(revenueType)
        const installationHelp = computeInstallation(revenueType,zoneH,housingType)
        const detail = {...isolationHelp, ...PACHelp}
        for (const [key,value] of Object.entries(installationHelp)){
            if(!!detail[key]){
                detail[key] += value
            } else {
                detail[key] = value
            }
        }
        let total = 0
        Object.keys(detail).forEach((key)=>{
            detail[key] = utils.toFixedNumber(detail[key],2,10)
            total += detail[key]
        })

        const finalResult = {detail:detail, total:utils.toFixedNumber(total,2,10)}

        return finalResult
    }

    api.computeBilan = (clientId) => {
        const client = clients.getClient(clientId)
        const housing = housings.getHousingByClientId(clientId)

        const deltaT = (() =>{
            const departement = client.zipCode.substring(0, 2) 
            var degree
            for (const [key, value] of Object.entries(mappingZipCodeToDegree)) {
                if(value.includes(Number(departement))){
                    console.log(key)
                    degree = Number(key)
                }
            }
            return Math.abs(degree+housing.ambientTemperature)
        })()
        console.log(deltaT)
        const bilan = Math.round(housing.livingSpace * housing.ceilingHeight * deltaT * housing.insulationCoeff * 1.2)

        return bilan
    } 

    api.computeEco = (clientId)=>{
        const housing = housings.getHousingByClientId(clientId)
        const need = needs.getNeedByClientId(clientId)
        const { airWaterHeatPump, airToAirHeatPump, thermodynamicHeater, floorRoofSpace, crawlingRoofSpace, insideWallMeasure, outsideWallMeasure, lowFloorMeasure, photovoltaicPanels } = need
        const dailyUsage = 3.6

        const firstCase = ['Électrique','Gaz']
        
        const percentageSun = {
            '0°':{
                'ouest':0.93,
                'sud-ouest':0.93,
                'sud':0.93,
                'sud-est':0.93,
                'est':0.93
            },
            '30°':{
                'ouest':0.90,
                'sud-ouest':0.96,
                'sud':1,
                'sud-est':0.96,
                'est':0.90
            },
            '45°':{
                'ouest':0.84,
                'sud-ouest':0.92,
                'sud':0.96,
                'sud-est':0.92,
                'est':0.84
            },
            '60°':{
                'ouest':0.78,
                'sud-ouest':0.88,
                'sud':0.91,
                'sud-est':0.88,
                'est':0.78
            },
            '90°':{
                'ouest':0.55,
                'sud-ouest':0.66,
                'sud':0.68,
                'sud-est':0.66,
                'est':0.55
            },
        }

        const sunPowerByDep = {
            850:[62,59],
            900:[67,68,57,54,55,88,60,80,02],
            950:[08,51,52,10,75,91,92,93,94,95,77,78,76,27],
            1000:[14,50,61,89,71,58,21,29,22,56,35,28,45,41,18,36,37,70,90,25,39],
            1050:[44,49,53,72,85],
            1100:[33,24,47,40,64,03,63,15,43,87,23,19,17,79,86,16,42,69,01,74,73,38,26,07],
            1150:[65,32,82,46,12,81,31,09],
            1300:[20,66,11,34,30,48],
            1350:[04,05,06,13,83,84]
        }

        if(firstCase.includes(housing.combustibleHeaterType)){
            const totalConsumption = housing.currentConsumption
            const heaterConsumption = (totalConsumption - 220) * 0.65
            const hotWaterConsumption = (totalConsumption - 220) * 0.15
            const lightingConsumption = (totalConsumption - 220) * 0.10
            const domConsumption = (totalConsumption - 220) * 0.10


            let postHeaterConsumption = heaterConsumption
            let postHotWaterConsumption = hotWaterConsumption

            if(airWaterHeatPump.length > 0 || airToAirHeatPump.length > 0){
                postHeaterConsumption = utils.toFixedNumber(heaterConsumption/2.2,2,10)
            }
            if(thermodynamicHeater.length > 0){
                postHotWaterConsumption = utils.toFixedNumber(hotWaterConsumption/4,2,10)
            }

            let combleEco = 0
            let iteEco = 0
            let plancherEco = 0
            let panelsProd = 0
            
            if(floorRoofSpace > 0 || crawlingRoofSpace > 0){
                combleEco = (postHeaterConsumption * 0.20)
                postHeaterConsumption = utils.toFixedNumber(postHeaterConsumption - combleEco,2,10)
            } 
            if(outsideWallMeasure > 0 || insideWallMeasure > 0){
                iteEco = (postHeaterConsumption * 0.30)
                postHeaterConsumption = utils.toFixedNumber(postHeaterConsumption - iteEco,2,10)
            }

            if(lowFloorMeasure > 0 ){
                plancherEco = (postHeaterConsumption * 0.15)
                postHeaterConsumption = utils.toFixedNumber(postHeaterConsumption - iteEco,2,10)
            }

            let postTotalConsumption = postHeaterConsumption + postHotWaterConsumption + lightingConsumption + domConsumption + 220

            if(!!photovoltaicPanels){
                const { panelsNumber, panelsOrientation, panelsInclinaison } = need

                const selectedPercentage = percentageSun[panelsInclinaison][panelsOrientation]
                let selectedSunPower 
                const client = clients.getClient(clientId)
                const departement = client.zipCode.substring(0, 2)
                for (const [key, value] of Object.entries(sunPowerByDep)) {
                    if(value.includes(Number(departement))){
                        selectedSunPower = Number(key)
                    }
                }

                panelsProd = utils.toFixedNumber(panelsNumber * 0.33 * (selectedSunPower * selectedPercentage) * 0.15,2,10)
            }

            const result = {
                before:{
                    total:utils.toFixedNumber(totalConsumption,2,10),
                    heater:utils.toFixedNumber(heaterConsumption,2,10),
                    hotWater:utils.toFixedNumber(hotWaterConsumption,2,10),
                    lighting:utils.toFixedNumber(lightingConsumption,2,10),
                    domestical:utils.toFixedNumber(domConsumption,2,10),
                    subscription:220
                },
                after:{
                    total:utils.toFixedNumber(postTotalConsumption,2,10),
                    heater:utils.toFixedNumber(postHeaterConsumption,2,10),
                    hotWater:utils.toFixedNumber(postHotWaterConsumption,2,10),
                    lighting:utils.toFixedNumber(lightingConsumption,2,10),
                    domestical:utils.toFixedNumber(domConsumption,2,10),
                    subscription:220,

                }
            }
            if(combleEco > 0){
                result.after.combleEco = utils.toFixedNumber(combleEco, 2, 10)
            }
            if(iteEco > 0){
                result.after.iteEco = utils.toFixedNumber(iteEco, 2, 10)
            }
            if(plancherEco > 0){
                result.after.plancherEco = utils.toFixedNumber(plancherEco, 2, 10)
            }
            if(panelsProd > 0){
                result.after.panelsProd = utils.toFixedNumber(panelsProd,2,10)
                result.after.total = result.after.total - panelsProd

            }

            return result
            
        } else {
            const totalConsumption = housing.currentConsumption + housing.combustibleConsumption
            const electricConsumption = housing.currentConsumption
            const heaterConsumption = utils.toFixedNumber(housing.combustibleConsumption * 0.85,2,10) 
            const hotWaterConsumption = utils.toFixedNumber(housing.combustibleConsumption * 0.15,2,10) 
            const lightingConsumption = (electricConsumption - 220) / 2
            const domConsumption = (electricConsumption - 220) / 2

            let postHeaterConsumption = heaterConsumption
            let postHotWaterConsumption = hotWaterConsumption

            if(airWaterHeatPump.length > 0 || airToAirHeatPump.length > 0){
                let divider
                if(['Propane','Fioul'].includes(housing.combustibleHeaterType)){
                    divider = 2.2
                } else {
                    divider = 2
                }
                postHeaterConsumption = utils.toFixedNumber(heaterConsumption/divider,2,10)
            }
            if(thermodynamicHeater.length > 0){
                postHotWaterConsumption = utils.toFixedNumber(hotWaterConsumption/4,2,10)
            } 

            let combleEco = 0
            let iteEco = 0
            let plancherEco = 0
            let panelsProd = 0
            
            if(floorRoofSpace > 0 || crawlingRoofSpace > 0){
                combleEco = (postHeaterConsumption * 0.20)
                postHeaterConsumption = utils.toFixedNumber(postHeaterConsumption - combleEco,2,10)
            } 
            if(outsideWallMeasure > 0 || insideWallMeasure > 0){
                iteEco = (postHeaterConsumption * 0.30)
                postHeaterConsumption = utils.toFixedNumber(postHeaterConsumption - iteEco,2,10)
            }

            if(lowFloorMeasure > 0 ){
                plancherEco = (postHeaterConsumption * 0.15)
                postHeaterConsumption = utils.toFixedNumber(postHeaterConsumption - plancherEco,2,10)
            }

            let postTotalConsumption = utils.toFixedNumber(postHeaterConsumption + postHotWaterConsumption + lightingConsumption + domConsumption + 220,2,10)

            let postElectricConsumption 
            if(airWaterHeatPump.length > 0 || airToAirHeatPump.length > 0 || thermodynamicHeater.length > 0){
                postElectricConsumption = postTotalConsumption
            }else{
                postElectricConsumption = utils.toFixedNumber(lightingConsumption + domConsumption + 220,2,10)
            }

            if(!!photovoltaicPanels){
                const { panelsNumber, panelsOrientation, panelsInclinaison } = need

                const selectedPercentage = percentageSun[panelsInclinaison][panelsOrientation]
                let selectedSunPower 
                const client = clients.getClient(clientId)
                const departement = client.zipCode.substring(0, 2)
                for (const [key, value] of Object.entries(sunPowerByDep)) {
                    if(value.includes(Number(departement))){
                        selectedSunPower = Number(key)
                    }
                }

                panelsProd = utils.toFixedNumber(panelsNumber * 0.33 * (selectedSunPower * selectedPercentage) * 0.15,2,10)
            }

            const result = {
                before:{
                    total:totalConsumption,
                    electric:electricConsumption,
                    heater:heaterConsumption,
                    hotWater:hotWaterConsumption,
                    lighting:lightingConsumption,
                    domestical:domConsumption,
                    subscription:220
                },
                after:{
                    total:postTotalConsumption,
                    electric:postElectricConsumption,
                    heater:postHeaterConsumption,
                    hotWater:postHotWaterConsumption,
                    lighting:lightingConsumption,
                    domestical:domConsumption,
                    subscription:220,

                }
            }
            if(combleEco > 0){
                result.after.combleEco = utils.toFixedNumber(combleEco, 2, 10)
            }
            if(iteEco > 0){
                result.after.iteEco = utils.toFixedNumber(iteEco, 2, 10)
            }
            if(plancherEco > 0){
                result.after.plancherEco = utils.toFixedNumber(plancherEco, 2, 10)
            }
            if(panelsProd > 0){
                result.after.panelsProd = utils.toFixedNumber(panelsProd,2,10)
                result.after.total = utils.toFixedNumber((result.after.total - panelsProd),2,10)
                result.after.electric = utils.toFixedNumber((result.after.electric - panelsProd),2,10)
            }

            return result
        }
    }

    api.computeMPR = (clientId) => {
      const clientData = clients.getClient(clientId)
      const housingData = housings.getHousingByClientId(clientId)
      const needData = needs.getNeedByClientId(clientId)
      const housingType = housingData.housingType.toLowerCase()
      
      if(housingType === 'bureau' || housingType === 'commerce') {
         return 'Non admissible'
      }

      const computeRevenueType = () => {
        const { zipCode, residentsNumber, lastYearHouseholdIncome } = clientData
        const idfRegions = ["75", "77", "78", "91", "92", "93", "94", "95"]
        const useBarem = idfRegions.includes(zipCode.substring(0,2)) ? ceeBaremIDF : ceeBaremAll
        const revenueTypeList = ['bleu', 'jaune', 'violet','rose']

        let revenueType 

        if(residentsNumber < 6){
            const ressourceSlot = useBarem[residentsNumber]
            ressourceSlot.forEach((threshold, index)=>{
                if(lastYearHouseholdIncome < threshold){
                    revenueType = revenueTypeList[index]
                }else{
                    if(index == 2){
                        revenueType = revenueTypeList[index]
                    }
                }
            })
        } else {
            const ressourceSlot = useBarem[5]
            const personnesSup = residentsNumber - 5
            for(let i = 0; i < personnesSup; i ++){
                ressourceSlot[0] = ressourceSlot[0] + useBarem.added[0]
                ressourceSlot[1] = ressourceSlot[1] + useBarem.added[1]
            }
            ressourceSlot.forEach((threshold, index)=>{
                if(lastYearHouseholdIncome < threshold){
                    revenueType = revenueTypeList[index]
                }else{
                    if(index == 2){
                        revenueType = revenueTypeList[index]
                    }
                }
            })

        }

        return utils.toFixedNumber(revenueType * (115/100) ,2,10) 
      }

      const computeMPRHelp = (revenueType) => {
        const helpBase = revenueType === 'bleu' ? bleu : revenueType === 'jaune' ? jaune : revenueType === 'violet' ? violet : rose


        const { insideWallMeasure, outsideWallMeasure, crawlingRoofSpace, airWaterHeatPump, thermodynamicHeater } = needData

        let help = 0

        if(!!crawlingRoofSpace){
            help += crawlingRoofSpace * helpBase.isolation.rampants_combles
          }

        if(!!outsideWallMeasure){
            const threshold = 100
            let measure
            if (outsideWallMeasure > threshold) {
                measure = 100
            } else {
                mesure = outsideWallMeasure
            }
          help += outsideWallMeasure * helpBase.isolation.murs_exterieurs
        }

        if(!!insideWallMeasure){
          help += insideWallMeasure * helpBase.isolation.murs_interieurs
        }

        if(!!thermodynamicHeater && thermodynamicHeater.length > 0){
          help += helpBase.chauffe_eau_thermodynamique
        }

        if(!!airWaterHeatPump && airWaterHeatPump.length > 0){
          help += helpBase.PAC_air_eau
        }

        return help
      }

      const revenueType = computeRevenueType()
      
      return computeMPRHelp(revenueType)
      
    }

    return api;
})();
