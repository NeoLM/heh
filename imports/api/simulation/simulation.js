import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';

const simulationsData = new Mongo.Collection('simulationsData')

var simulationsSchema = new SimpleSchema({
    userId:{
        type:String,
        label:'id commercial',
    },
    clientId:{
        type:String,
        label:'id client'
    },
    bilan:{
        type:String,
        label:'Bilan thermique du client'
    },
    economies:{
        type:Object,
        label:'Détail des économies réalisés',
        blackbox:true,
        optional:true
    },
    aides:{
        type:Object,
        label:"Différents types d'aides auquel le client à le droit",
        blackbox:true,
        optional:true
    },
    addedDate: {
        type: Date,
        label: "Creation date",
        autoValue: function () {
          if (this.isInsert) {
            return new Date();
          } else {
            this.unset();
          }
        }
      },
      modifiedDate: {
        type: Date,
        label: "Modification date",
        autoValue: function () {
          if (this.isUpdate) {
            return new Date();
          } else {
            this.unset();
          }
        },
        optional: true
      },
})

simulationsData.attachSchema(simulationsSchema);

export {
    simulationsData
}
