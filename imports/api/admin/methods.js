import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base';

import { clientsData } from '../clients/clients'
import '../clients/methods'

import { housingsData } from '../housings/housings'
import '../housings/methods'

import { needsData } from '../needs/needs'
import '../needs/methods'

import { simulationsData } from '../simulation/simulation'
import '../simulation/methods'

import { devisData } from '../devis/devis'
import '../devis/methods'

import { orderFormsData } from '../orderForms/orderForms'
import '../orderForms/methods'

import { bdcsData } from '../bdc/bdc'
import '../bdc/methods'

import '../utils/methods'

import { paneauxPhotovoltaiques,
    ballonsThermodynamiques,
    pompeAirEau,
    paneauxPhotovoltaiquesPrix,
    isolation
} from '../dbprice'

import { moment } from 'meteor/momentjs:moment';
import fs from 'fs';

Meteor.methods({

    commercialList(){
        const user = Meteor.user();
        if(user.profile.level === 'superadmin' || user.profile.level === 'admin'){
            const usersData = Meteor.users.find()
            
            return usersData.map((user)=>{
                const userData = {
                    id:user._id,
                    name:user.profile.name,
                    lastname:user.profile.lastname,
                    email:user.emails[0].address,
                    phone:user.profile.phone,
                    location:user.profile.location,
                    active:user.profile.active,
                    level:user.profile.level

                }
                return userData
            })

        }
    },

    createAccount(userData){
        const user = Meteor.user();
        if(user.profile.level === 'superadmin' || user.profile.level === 'admin'){
            const query = {
                email:userData.email,
                profile:{
                    level:userData.level,
                    name:userData.name,
                    lastname:userData.lastname,
                    active:true,
                    phone:userData.phone,
                    location:userData.location
                }
            }
            console.log('inside accounts creation')
            const userId = Accounts.createUser(query)
            Accounts.sendEnrollmentEmail(userId)
            return true
        } 
    }, 

    modifyAccount(userData, userId){
        const user = Meteor.user();
        if(user.profile.level === 'superadmin' || user.profile.level === 'admin'){
            const query = {
                emails:[{
                    address:userData.email,
                    verified:false
                }],
                profile:{
                    level:userData.level,
                    name:userData.name,
                    lastname:userData.lastname,
                    active:userData.active,
                    phone:userData.phone,
                    location:userData.location}
            }

            Meteor.users.update({_id:userId}, {$set:query})
            
            return true
        } 
    },

    deactivateAccount(userId){
        const user = Meteor.user();
        if(user.profile.level === 'superadmin' || user.profile.level === 'admin'){

            Meteor.users.update({_id:userId}, {$set:{'profile.active':false}})
            
            return true
        } 
    },

    clientList(){
        const user = Meteor.user();
        if(user.profile.level === 'superadmin' || user.profile.level === 'admin'){
            return admin.getAllClientData()
        }
    },

    async clientExtract(){
        const user = Meteor.user();
        if(user.profile.level === 'superadmin' || user.profile.level === 'admin'){
            const toCSVData = admin.getAllClientDataToCSV()
            const headerClient = ['Nom', 'Email', 'Téléphone', 'Adresse', 'Situation Familiale', 'Nombre de résidents', 'Professions', "Revenus de l'année n-1", 'Revenus du foyer', 'Dates de naissances']
            const headerHousing = ['Type de maison', 'Année de construction', 'Type de chauffage', 'Consommation actuelle', 'Consommation combustible', 'Type de fenêtre', 'Surface habitable', 'Hauteur sous plafond', 'Niveaux', 'Construction sur', 'Isolation', "Coefficient d'isolation", 'Travaux de rénovations'] 
            const headerNeed = ['Isolation combles', 'Isolations murs', 'Isolations plancher', 'Panneaux photovoltaiques', 'Pompe à chaleur Air Air', 'Pompe à chaleur Air Eau', 'Ballon thermodynamiques', 'Accessoires']
            const headerSimu = ['Bilan', 'Économies', "Ma Prime Renov'", 'Cee', 'Ecobonus', 'Aide régionale']
            const headerOrder = ['Délai de livraison', 'Accord de paiement', 'Détails', 'Options']
            const headerDoc = ['Date du devis', 'Montant', 'Devis signé', 'Date de signature', 'Date du bon de commande', 'Bon de commande signé', 'Date de signature']

            const headers = headerClient.concat(headerHousing).concat(headerNeed).concat(headerSimu).concat(headerOrder).concat(headerDoc)

            const CSVData = [headers].concat(toCSVData)

            return admin.writeArrayAsCSV('/home/ec2-user/files/extract/', CSVData, `clients-${moment().format('DD-MM-YYYY')}.csv`).then(()=>{
                const blob = fs.readFileSync(`/home/ec2-user/files/extract/clients-${moment().format('DD-MM-YYYY')}.csv`);
                return blob 

            })

        } 
    },

    documentList(){
        const user = Meteor.user();
        if(user.profile.level === 'superadmin'){
            const clientsVal = clientsData.find({},{sort:{addedDate:-1}})
            let devisListData = []
            let bdcsListData = []
            
            clientsVal.forEach((client)=>{
                const devisFetchedData = devis.getDevisByClientId(client._id)
                const bdcsFetchedData = bdcs.getBdcByClientId(client._id)
                const commercialData = Meteor.users.findOne({_id:client.userId})

                if(!!devisFetchedData){

                    devisListData.push({
                        clientId:client._id,
                        clientEmail:client.email,
                        clientName:`${client.name} ${client.lastname}`,
                        commercialName:`${commercialData.profile.name} ${commercialData.profile.lastname}`,
                        commercialId:commercialData._id,
                        date:devisFetchedData.data.date,
                        amount:devisFetchedData.data.toPay + ' €',
                        status:devisFetchedData.status,
                        devisId:devisFetchedData._id
                    })
                }

                if(!!bdcsFetchedData){
                    bdcsListData.push({
                        clientId:client._id,
                        clientName:`${client.name} ${client.lastname}`,
                        clientEmail:client.email,
                        commercialName:`${commercialData.profile.name} ${commercialData.profile.lastname}`,
                        commercialId:commercialData._id,
                        date:moment(bdcsFetchedData.addedDate).format('DD/MM/YYYY'),
                        status:bdcsFetchedData.status,
                        bdcId:bdcsFetchedData._id
                    })
                }
            })

            return {
                devis:devisListData,
                bdcs:bdcsListData
            }
            
        }
    },

    getClientDataPDF(clientId){
        const user = Meteor.user();
        if(user.profile.level === 'superadmin' || user.profile.level === 'admin'){
            const clientData = admin.getClientDataPDF(clientId)
            console.log(clientData)
            const dd = {
                pageSize: 'A4',                
                pageOrientation: 'landscape',                
                pageMargins: [ 40, 60, 40, 30 ],                
                footer: function(currentPage, pageCount) { return {
                    text:currentPage.toString() + ' / ' + pageCount,
                    alignment:'right',
                    marginRight:10
                }},
                content: []
            }

            const generatePDFContent = (clientData)=>{
                const content = [{ text: 'Extract de données client', fontSize: 25, bold:true, marginBottom:10, alignment:'center' }]

                const headerClient = ['Nom', 'Email', 'Téléphone', 'Adresse', 'Situation Familiale', 'Nombre de résidents', 'Professions', "Revenus de l'année n-1", 'Revenus du foyer', 'Dates de naissances']
                const headerHousing = ['Type de maison', 'Année de construction', 'Type de chauffage', 'Consommation actuelle', 'Consommation combustible', 'Type de fenêtre', 'Surface habitable', 'Hauteur sous plafond', 'Niveaux', 'Construction sur', 'Isolation', "Coefficient d'isolation", 'Travaux de rénovations'] 
                const headerNeed = ['Isolation combles', 'Isolations murs', 'Isolations plancher', 'Panneaux photovoltaiques', 'Pompe à chaleur Air Air', 'Pompe à chaleur Air Eau', 'Ballon thermodynamiques', 'Accessoires']
                const headerSimu = ['Bilan', 'Économies', "Ma Prime Renov'", 'Cee', 'Ecobonus', 'Aide régionale']
                const headerOrder = ['Délai de livraison', 'Accord de paiement', 'Détails', 'Options']
                const headerDoc = ['Date du devis', 'Montant', 'Devis signé', 'Date de signature', 'Date du bon de commande', 'Bon de commande signé', 'Date de signature']
    
                const headerList = [headerClient, headerHousing, headerNeed, headerSimu, headerOrder, headerDoc]
                const titleList = ['Informations client', 'Découverte habitation', 'Analyse des besoins', 'Simulation', 'Bon pour accord', 'Devis et Bon de commande']
    
                clientData.forEach((data, index)=>{
                    const title = { text: titleList[index], fontSize: 15, bold:true, marginBottom:10 }
                    if(index !== 0){
                        title.marginTop = 30
                    }
                    let table = {
                        table:{
                            headerRows: 1,
                            widths: [],
                        }
                    }
                    const threshold = {
                        0:4,
                        1:7,
                        2:3 
                    }

                    let headers = []
                    let values = []
                    content.push(title)
                    headerList[index].forEach((elem,ind)=>{
                        if(!!threshold[index] && ind == threshold[index]){
                            table.table.body = [headers,values]
                            content.push(table)
                            table = {
                                table:{
                                    headerRows: 1,
                                    widths: [],
                                },
                                margin:[0,8,0,0]
                            }
                            headers = []
                            values = []
                        }
                        table.table.widths.push('*')
                        headers.push({
                            text:elem,
                            bold:true
                        })
                        values.push(data[ind])
                    })
                    table.table.body = [headers,values]
                    content.push(table)
                })
                return content
            }

            dd.content = generatePDFContent(clientData)

            return dd

        }
    }

})

admin = (function(){
    let api = {}

    api.getAllClientData = function(){
        const getClientData = (clientId) =>{
            const methodsList = {
                clientData(){
                    const clientsVal = clients.getClient(clientId)
                    if(!!clientsVal){
                        const formatedClientsVal = {...clientsVal}
                        formatedClientsVal['profession1'] = clientsVal['professions'][0]
                        formatedClientsVal['profession2'] = clientsVal['professions'][1]
                        formatedClientsVal['birthDate1'] = clientsVal['birthDate'][0]
                        formatedClientsVal['birthDate2'] = clientsVal['birthDate'][1]
                        formatedClientsVal['houseHoldIncome1'] = clientsVal['houseHoldIncome'][0] + ' €'
                        formatedClientsVal['houseHoldIncome2'] = clientsVal['houseHoldIncome'][1] + ' €'
    
                        delete formatedClientsVal['professions']
                        delete formatedClientsVal['birthDate']
                        delete formatedClientsVal['houseHoldIncome']
    
                        return formatedClientsVal
                    }else{
                        return false
                    }
                },
                housingData(){
                    const housingVal = housings.getHousingByClientId(clientId)
                    if(!!housingVal){
                        const formatedHousingVal = {...housingVal} 
                        formatedHousingVal.insulationCoeff = formatedHousingVal.insulationCoeff.toString()
                        formatedHousingVal.currentConsumption = formatedHousingVal.currentConsumption + ' €'
                        let heaterType = ''
                        if(formatedHousingVal?.heaterType){
                            heaterType += formatedHousingVal.heaterType + ' '
                            delete formatedHousingVal.heaterType
                        }
                        if(formatedHousingVal?.combustibleHeaterType){
                            heaterType += formatedHousingVal.combustibleHeaterType + ' '
                            delete formatedHousingVal.combustibleHeaterType
                        }
                        formatedHousingVal.heaterType = heaterType

                        return formatedHousingVal
                    }else{
                        return false
                    }
                },
                needData(){

                    const toFixedNumber = (num, digits, base) =>{
                        var pow = Math.pow(base||10, digits);
                        return Math.round(num*pow) / pow;
                    }
                    const needsVal = needs.getNeedByClientId(clientId)
                    if(!!needsVal){
                        const formatedNeedsVal = {...needsVal}

                        const makeItemPhotovoltaicPanels = (photovoltaicPanels, panelsNumber, batteriePanels, panelsPrice, panelsOrientation, panelsInclinaison)=>{
                            if(photovoltaicPanels){
                                const newItem = {inclinaison:panelsInclinaison, orientation:panelsOrientation}
                                const tauxTva = panelsNumber > 10 ? 20 : 10
                                newItem.quantity = panelsNumber
                                newItem.totalTTC = panelsPrice
                                newItem.ref = paneauxPhotovoltaiques.ref + ' €'
                                newItem.totalHT = toFixedNumber(panelsPrice / (1 + (tauxTva/100)),2,10) + ' €'
                            
                                if(!!batteriePanels){
                                    newItem.batterie = formatedNeedsVal.batteriePanels
                                }
                                console.log(newItem)
                                return newItem
                            } else {
                                return false
                            }
                        }

                        formatedNeedsVal.photovoltaicPanelsData = makeItemPhotovoltaicPanels(formatedNeedsVal.photovoltaicPanels, formatedNeedsVal.panelsNumber, formatedNeedsVal.batteriePanels, formatedNeedsVal.panelsPrice, formatedNeedsVal.panelsOrientation, formatedNeedsVal.panelsInclinaison)
                        delete formatedNeedsVal.photovoltaicPanels
                        delete formatedNeedsVal.panelsNumber
                        delete formatedNeedsVal.batteriePanels
                        delete formatedNeedsVal.panelsOrientation
                        delete formatedNeedsVal.panelsInclinaison

                        const makeItems = (values) => {
                            if(values.length>0){
                                return values.map((val)=>{
                                    const newItem = {}
                                    newItem.ref = val.ref
                                    newItem.quantity = 1
                                    newItem.totalTTC = val.prixTTC + ' €'
                                    newItem.totalHT = val.prixHT + ' €'
                                    return newItem
                                })
                            } else {
                                return []
                            }
                        }

                        formatedNeedsVal.airToAirHeatPump = makeItems(needsVal.airToAirHeatPump)
                        formatedNeedsVal.airWaterHeatPump = makeItems(needsVal.airWaterHeatPump)
                        formatedNeedsVal.thermodynamicHeater = makeItems(needsVal.thermodynamicHeater)
                        formatedNeedsVal.accessories = makeItems(needsVal.accessories)

                        return formatedNeedsVal
                    }else{
                        return false
                    }
                },
                simulationData(){
                    const simulationsVal = simulations.getSimulationByClientId(clientId)
                    if(!!simulationsVal){
                        const formatedSimuVal = {}
                        formatedSimuVal.bilan = simulationsVal.bilan + (simulationsVal.bilan === 'Pas disponible' ? '' : ' KW/H')
                        formatedSimuVal.economies = (simulationsVal.economies.before.total - simulationsVal.economies.after.total) + ' €'
                        formatedSimuVal.aides = {}
                        formatedSimuVal.aides.mpr = simulationsVal.aides.mpr + ((!!simulationsVal.aides.mpr || simulationsVal.aides.mpr == 0 )? ' €' : '')
                        formatedSimuVal.aides.cee = simulationsVal.aides.cee.total + ((!!simulationsVal.aides.cee.total || simulationsVal.aides.cee.total == 0) ? ' €' : '')
                        formatedSimuVal.aides.ecobonus = simulationsVal.aides.ecobonus + ((!!simulationsVal.aides.ecobonus && simulationsVal.aides.ecobonus == 0)  ? ' €' : '')
                        formatedSimuVal.aides.region = simulationsVal.aides.region + ((!!simulationsVal.aides.region && simulationsVal.aides.region == 0)  ? ' €' : '')

                        return formatedSimuVal
                    }else{
                        return false
                    }
                },
                devisData(){
                    const devisVal = devis.getDevisByClientId(clientId)
                    if(!!devisVal){
                        const formatedDevisVal = {}
                        formatedDevisVal.date = devisVal.data.date
                        formatedDevisVal.totalTTC = devisVal.data.toPay + ' €'
                        formatedDevisVal.signature = !!devisVal.data.signature
                        formatedDevisVal.signatureDate = !!devisVal.data.signature ? moment(devisVal.modifiedDate).format('DD/MM/yyyy') : ''

                        return formatedDevisVal
                    }else{
                        return false
                    }
                },
                orderData(){
                    const orderVal = orderForms.getOrderFormByClientId(clientId)
                    if(!!orderVal){
                        const formatedOrderVal = {}
                        formatedOrderVal.deliveryDelay = orderVal.deliveryDelay + ' jours'
                        formatedOrderVal.agreement = orderVal.agreement
                        formatedOrderVal.paiementTerms = orderVal.paiementTerms
                        if(orderVal.paiementTerms === 'Au comptant'){
                            formatedOrderVal.details = [`Accompte : ${orderVal.cashTerm.depositAmount} €`,`Solde : ${orderVal.cashTerm.balance} €`]
                        } else {
                            formatedOrderVal.details = [
                                `Organisme : ${orderVal.fundingTerm.financialOrganization}`, 
                                `Montant : ${orderVal.fundingTerm.amount} €`,
                                `Apport : ${orderVal.fundingTerm.financialContribution} €`,
                                `Mensualité : ${orderVal.fundingTerm.monthlyPayment} €`,
                                `Report : ${orderVal.fundingTerm.postponement}`,
                                `Durée : ${orderVal.fundingTerm.duration}`,
                                `TAEG : ${orderVal.fundingTerm.taeg}%`,
                                `Taux Débiteur : ${orderVal.fundingTerm.lendingRate}%`,
                                `Coût total : ${orderVal.fundingTerm.totalCost} €`,
                            ]
                        }

                        return formatedOrderVal
                    }else{
                        return false
                    }
                },
                bdcData(){
                    const bdcVal = bdcs.getBdcByClientId(clientId)
                    if(!!bdcVal){
                        const formatedBdcVal = {}
                        formatedBdcVal.date = moment(bdcVal.addedDate).format('DD/MM/yyyy')
                        formatedBdcVal.signature = !!bdcVal.data.signature
                        formatedBdcVal.signatureDate = !!bdcVal.data.signature ? moment(bdcVal.modifiedDate).format('DD/MM/yyyy') : ''
                        return formatedBdcVal
                    }else{
                        return false
                    }
                }
            }
            let clientData = {}
            for (const [key, method] of Object.entries(methodsList)){
                const valueFetched = method()
                if(!!valueFetched){
                    clientData[key] = valueFetched
                } else{
                    break
                }
            }

            return clientData
        }

        const clientsList = clientsData.find().fetch()

        return clientsList.map((client)=>{
            return getClientData(client._id)
        })
        
    }

    api.writeArrayAsCSV = async function (path, arrayData, fileName) {    
        var mkdirp = Npm.require('mkdirp');
        return new Promise((resolve,reject)=>{
            mkdirp(path).then(async function (err,res) {
              if (err) {
                reject(err);
              }
        
              var file = fs.createWriteStream(path + fileName);
              arrayData.forEach(function (line) {
                file.write(line.join(';') + '\n', 'utf8');
              })
            file.end();
            file.on('finish',resolve) 
            })

        })
         
    }

    api.getAllClientDataToCSV = function(){
        const getClientData = (clientId) =>{
            const methodsList = {
                clientData(){
                    const clientsVal = clients.getClient(clientId)
                    if(!!clientsVal){
                        const formatedClientsVal = {...clientsVal}
                        formatedClientsVal['profession'] = clientsVal['professions'].join(' / ')
                        formatedClientsVal['birthDates'] = clientsVal['birthDate'].join(' / ')
                        formatedClientsVal['houseHoldIncomes'] = clientsVal['houseHoldIncome'].join(' - ')
    
                        delete formatedClientsVal['professions']
                        delete formatedClientsVal['birthDate']
                        delete formatedClientsVal['houseHoldIncome']
                        
                        const result = [
                            formatedClientsVal.name + ' ' + formatedClientsVal.lastname,
                            formatedClientsVal.email, 
                            formatedClientsVal.phone,
                             `${formatedClientsVal.adress}, ${formatedClientsVal.zipCode} ${formatedClientsVal.city}`, 
                            formatedClientsVal.familySituation, 
                            formatedClientsVal.residentsNumber, 
                            formatedClientsVal.profession, 
                            formatedClientsVal.lastYearHouseholdIncome, 
                            formatedClientsVal.houseHoldIncomes, 
                            formatedClientsVal.birthDates
                        ]

                        return result
                    }else{
                        return false
                    }
                },
                housingData(){
                    const housingVal = housings.getHousingByClientId(clientId)
                    if(!!housingVal){
                        const formatedHousingVal = {...housingVal} 
                        formatedHousingVal.insulationCoeff = formatedHousingVal.insulationCoeff.toString()
                        let heaterType = ''
                        if(formatedHousingVal?.heaterType){
                            heaterType += formatedHousingVal.heaterType + ' '
                            delete formatedHousingVal.heaterType
                        }
                        if(formatedHousingVal?.combustibleHeaterType){
                            heaterType += formatedHousingVal.combustibleHeaterType + ' '
                            delete formatedHousingVal.combustibleHeaterType
                        }
                        formatedHousingVal.heaterType = heaterType
                        
                        if(!formatedHousingVal?.combustibleConsumption){
                            formatedHousingVal.combustibleConsumption = ''
                        }
                        
                        const result = [
                            formatedHousingVal.housingType,
                            formatedHousingVal.constructionYear,
                            formatedHousingVal.heaterType,
                            formatedHousingVal.currentConsumption,
                            formatedHousingVal.combustibleConsumption,
                            formatedHousingVal.windowType + ' ' + formatedHousingVal.windowMaterial,
                            formatedHousingVal.livingSpace,
                            formatedHousingVal.ceilingHeight,
                            formatedHousingVal.levelNumber,
                            formatedHousingVal.buildingSoil,
                            `Toiture : ${formatedHousingVal.roofIsolationQuality} / Murs : ${formatedHousingVal.brickwallIsolationQuality} / Sols : ${formatedHousingVal.floorIsolationQuality}`,
                            formatedHousingVal.insulationCoeff,
                            formatedHousingVal.lastRenovations
                        ]
                        return result
                    }else{
                        return false
                    }
                },
                needData(){

                    const toFixedNumber = (num, digits, base) =>{
                        var pow = Math.pow(base||10, digits);
                        return Math.round(num*pow) / pow;
                    }
                    const needsVal = needs.getNeedByClientId(clientId)
                    if(!!needsVal){
                        const formatedNeedsVal = {...needsVal}
                        const makeItemPhotovoltaicPanels = (photovoltaicPanels, panelsNumber, batteriePanels, panelsPrice, panelsOrientation, panelsInclinaison)=>{
                            if(photovoltaicPanels){
                                const newItem = {inclinaison:panelsInclinaison, orientation:panelsOrientation}
                                const tauxTva = panelsNumber > 10 ? 20 : 10
                                newItem.quantity = panelsNumber
                                newItem.totalTTC = panelsPrice
                                newItem.ref = paneauxPhotovoltaiques.ref
                                newItem.totalHT = toFixedNumber(panelsPrice / (1 + (tauxTva/100)),2,10) + ' €'
                            
                                if(!!batteriePanels){
                                    newItem.batterie = formatedNeedsVal.batteriePanels
                                }
                                console.log(newItem)
                                return newItem
                            } else {
                                return false
                            }
                        }

                        formatedNeedsVal.photovoltaicPanelsData = makeItemPhotovoltaicPanels(formatedNeedsVal.photovoltaicPanels, formatedNeedsVal.panelsNumber, formatedNeedsVal.batteriePanels, formatedNeedsVal.panelsPrice, formatedNeedsVal.panelsOrientation, formatedNeedsVal.panelsInclinaison)
                        delete formatedNeedsVal.photovoltaicPanels
                        delete formatedNeedsVal.panelsNumber
                        delete formatedNeedsVal.batteriePanels
                        delete formatedNeedsVal.panelsOrientation
                        delete formatedNeedsVal.panelsInclinaison

                        const makeItems = (values) => {
                            if(values.length>0){
                                return values.map((val)=>{
                                    let newItem = ''
                                    newItem += `Ref : ${val.ref}`
                                    newItem += ` Quantité : 1 `
                                    newItem += ` prixTTC ${val.prixTTC} €`
                                    newItem += `  prixHT ${val.prixHT} €`
                                    return newItem
                                })
                            } else {
                                return []
                            }
                        }

                        formatedNeedsVal.airToAirHeatPump = makeItems(needsVal.airToAirHeatPump)
                        formatedNeedsVal.airWaterHeatPump = makeItems(needsVal.airWaterHeatPump)
                        formatedNeedsVal.thermodynamicHeater = makeItems(needsVal.thermodynamicHeater)
                        formatedNeedsVal.accessories = makeItems(needsVal.accessories)


                        const photovoltaicPanelsVal = !!formatedNeedsVal.photovoltaicPanelsData ? `Ref: ${formatedNeedsVal.photovoltaicPanelsData.ref} Quantité: ${formatedNeedsVal.photovoltaicPanelsData.quantity} Prix TTC: ${formatedNeedsVal.photovoltaicPanelsData.totalTTC} Prix HT: ${formatedNeedsVal.photovoltaicPanelsData.totalHT}${!!formatedNeedsVal.photovoltaicPanelsData.batterie ? ' Batterie : ' + formatedNeedsVal.photovoltaicPanelsData.batterie : ''} Orientation: ${formatedNeedsVal.photovoltaicPanelsData.orientation} Inclinaison: ${formatedNeedsVal.photovoltaicPanelsData.inclinaison}` : ''
                        const result = [
                            `Sols : ${formatedNeedsVal.floorRoofSpace}m2, ${formatedNeedsVal.floorRoofPrice}€ Rampants : ${formatedNeedsVal.crawlingRoofSpace}m2, ${formatedNeedsVal.crawlingRoofPrice}€`,
                            `Intérieurs : ${formatedNeedsVal.insideWallMeasure}m2, ${formatedNeedsVal.insideWallPrice}€ Extérieurs : ${formatedNeedsVal.outsideWallMeasure}m2, ${formatedNeedsVal.outsideWallPrice}€`,
                            `${formatedNeedsVal.lowFloorMeasure}m2, ${formatedNeedsVal.lowFloorPrice}€`,
                            photovoltaicPanelsVal,
                            formatedNeedsVal.airToAirHeatPump.join(' / '),
                            formatedNeedsVal.airWaterHeatPump.join(' / '),
                            formatedNeedsVal.thermodynamicHeater.join(' / '),
                            formatedNeedsVal.accessories.join(' / ')
                        ]
                        return result
                    }else{
                        return false
                    }
                },
                simulationData(){
                    const simulationsVal = simulations.getSimulationByClientId(clientId)
                    if(!!simulationsVal){
                        
                        const result = [
                            `${simulationsVal.bilan}KW/H`,
                            `${simulationsVal.economies.before.total - simulationsVal.economies.after.total}€`,
                            `${simulationsVal.aides.mpr}€`,
                            `${simulationsVal.aides.cee.total}€`,
                            `${simulationsVal.aides.ecobonus}€`,
                            `${simulationsVal.aides.region}€`
                        ]

                        return result
                    }else{
                        return false
                    }
                },
                orderData(){
                    const orderVal = orderForms.getOrderFormByClientId(clientId)
                    if(!!orderVal){
                        const formatedOrderVal = {}
                        formatedOrderVal.deliveryDelay = orderVal.deliveryDelay + ' jours'
                        formatedOrderVal.agreement = orderVal.agreement
                        formatedOrderVal.paiementTerms = orderVal.paiementTerms
                        if(orderVal.paiementTerms === 'Au comptant'){
                            formatedOrderVal.details = [`Accompte : ${orderVal.cashTerm.depositAmount}€`,`Solde : ${orderVal.cashTerm.balance}€`]
                        } else {
                            formatedOrderVal.details = [
                                `Organisme : ${orderVal.fundingTerm.financialOrganization}`, 
                                `Montant : ${orderVal.fundingTerm.amount}€`,
                                `Apport : ${orderVal.fundingTerm.financialContribution}€`,
                                `Mensualité : ${orderVal.fundingTerm.monthlyPayment}€`,
                                `Report : ${orderVal.fundingTerm.postponement}`,
                                `Durée : ${orderVal.fundingTerm.duration}`,
                                `TAEG : ${orderVal.fundingTerm.taeg}%`,
                                `Taux Débiteur : ${orderVal.fundingTerm.lendingRate}%`,
                                `Coût total : ${orderVal.fundingTerm.totalCost}€`,
                            ]
                        }

                        const result = [
                            `${formatedOrderVal.deliveryDelay}`,
                            `${formatedOrderVal.paiementTerms}`,
                            formatedOrderVal.details.join(' '),
                            formatedOrderVal.agreement = orderVal.agreement
                        ]

                        return result
                    }else{
                        return false
                    }
                },
                devisData(){
                    const devisVal = devis.getDevisByClientId(clientId)
                    if(!!devisVal){
                        const formatedDevisVal = {}
                        formatedDevisVal.date = devisVal.data.date
                        formatedDevisVal.totalTTC = devisVal.data.toPay
                        formatedDevisVal.signature = !!devisVal.data.signature ? 'oui' : 'non'
                        formatedDevisVal.signatureDate = !!devisVal.data.signature ? moment(devisVal.modifiedDate).format('DD/MM/yyyy') : ''

                        const result = [
                            formatedDevisVal.date,
                            formatedDevisVal.totalTTC,
                            formatedDevisVal.signature,
                            formatedDevisVal.signatureDate
                        ]
                        return result
                    }else{
                        return false
                    }
                },
                bdcData(){
                    const bdcVal = bdcs.getBdcByClientId(clientId)
                    if(!!bdcVal){
                        const formatedBdcVal = {}
                        formatedBdcVal.date = moment(bdcVal.addedDate).format('DD/MM/yyyy')
                        formatedBdcVal.signature = !!bdcVal.data.signature ? 'oui' : 'non'
                        formatedBdcVal.signatureDate = !!bdcVal.data.signature ? moment(bdcVal.modifiedDate).format('DD/MM/yyyy') : ''
                        
                        const result = [
                            formatedBdcVal.date ,
                            formatedBdcVal.signature,
                            formatedBdcVal.signatureDate 
                        ]
                        return result
                    }else{
                        return false
                    }
                }
            }
            let clientDataLine = []
            for (const [key, method] of Object.entries(methodsList)){
                const valueFetched = method()
                if(!!valueFetched){
                    valueFetched.forEach((elem)=>{
                        clientDataLine.push(elem)
                    })
                }
            }

            return clientDataLine
        }

        const clientsList = clientsData.find().fetch()

        return clientsList.map((client)=>{
            return getClientData(client._id)
        })
    }

    api.getClientDataPDF = (clientId) =>{
        const methodsList = {
            clientData(){
                const clientsVal = clients.getClient(clientId)
                if(!!clientsVal){
                    const formatedClientsVal = {...clientsVal}
                    formatedClientsVal['profession'] = clientsVal['professions'].join('\n')
                    formatedClientsVal['birthDates'] = clientsVal['birthDate'].join('\n')
                    formatedClientsVal['houseHoldIncomes'] = clientsVal['houseHoldIncome'].join('\n')

                    delete formatedClientsVal['professions']
                    delete formatedClientsVal['birthDate']
                    delete formatedClientsVal['houseHoldIncome']
                    
                    const result = [
                        formatedClientsVal.name + ' ' + formatedClientsVal.lastname,
                        formatedClientsVal.email, 
                        formatedClientsVal.phone,
                         `${formatedClientsVal.adress}, ${formatedClientsVal.zipCode} ${formatedClientsVal.city}`, 
                        formatedClientsVal.familySituation, 
                        formatedClientsVal.residentsNumber, 
                        formatedClientsVal.profession, 
                        formatedClientsVal.lastYearHouseholdIncome, 
                        formatedClientsVal.houseHoldIncomes, 
                        formatedClientsVal.birthDates
                    ]

                    return result
                }else{
                    return false
                }
            },
            housingData(){
                const housingVal = housings.getHousingByClientId(clientId)
                if(!!housingVal){
                    const formatedHousingVal = {...housingVal} 
                    formatedHousingVal.insulationCoeff = formatedHousingVal.insulationCoeff.toString()
                    let heaterType = ''
                    if(formatedHousingVal?.heaterType){
                        heaterType += formatedHousingVal.heaterType + '\n'
                        delete formatedHousingVal.heaterType
                    }
                    if(formatedHousingVal?.combustibleHeaterType){
                        heaterType += formatedHousingVal.combustibleHeaterType + '\n'
                        delete formatedHousingVal.combustibleHeaterType
                    }
                    formatedHousingVal.heaterType = heaterType

                    if(!formatedHousingVal?.combustibleConsumption){
                        formatedHousingVal.combustibleConsumption = ''
                    }

                    const result = [
                        formatedHousingVal.housingType,
                        formatedHousingVal.constructionYear,
                        formatedHousingVal.heaterType,
                        formatedHousingVal.currentConsumption,
                        formatedHousingVal.combustibleConsumption,
                        formatedHousingVal.windowType + '\n' + formatedHousingVal.windowMaterial,
                        formatedHousingVal.livingSpace,
                        formatedHousingVal.ceilingHeight,
                        formatedHousingVal.levelNumber,
                        formatedHousingVal.buildingSoil,
                        `Toiture : ${formatedHousingVal.roofIsolationQuality}\nMurs : ${formatedHousingVal.brickwallIsolationQuality}\nSols : ${formatedHousingVal.floorIsolationQuality}`,
                        formatedHousingVal.insulationCoeff,
                        formatedHousingVal.lastRenovations
                    ]
                    return result
                }else{
                    return false
                }
            },
            needData(){

                const toFixedNumber = (num, digits, base) =>{
                    var pow = Math.pow(base||10, digits);
                    return Math.round(num*pow) / pow;
                }
                const needsVal = needs.getNeedByClientId(clientId)
                if(!!needsVal){
                    const formatedNeedsVal = {...needsVal}
                    const makeItemPhotovoltaicPanels = (photovoltaicPanels, panelsNumber, batteriePanels, panelsPrice, panelsOrientation, panelsInclinaison)=>{
                        if(photovoltaicPanels){
                            const newItem = {inclinaison:panelsInclinaison, orientation:panelsOrientation}
                            const tauxTva = panelsNumber > 10 ? 20 : 10
                            newItem.quantity = panelsNumber
                            newItem.totalTTC = panelsPrice
                            newItem.ref = paneauxPhotovoltaiques.ref
                            newItem.totalHT = toFixedNumber(panelsPrice / (1 + (tauxTva/100)),2,10) + ' €'
                        
                            if(!!batteriePanels){
                                newItem.batterie = formatedNeedsVal.batteriePanels
                            }
                            console.log(newItem)
                            return newItem
                        } else {
                            return false
                        }
                    }

                    formatedNeedsVal.photovoltaicPanelsData = makeItemPhotovoltaicPanels(formatedNeedsVal.photovoltaicPanels, formatedNeedsVal.panelsNumber, formatedNeedsVal.batteriePanels, formatedNeedsVal.panelsPrice, formatedNeedsVal.panelsOrientation, formatedNeedsVal.panelsInclinaison)
                    delete formatedNeedsVal.photovoltaicPanels
                    delete formatedNeedsVal.panelsNumber
                    delete formatedNeedsVal.batteriePanels
                    delete formatedNeedsVal.panelsOrientation
                    delete formatedNeedsVal.panelsInclinaison

                    const makeItems = (values) => {
                        if(values.length>0){
                            return values.map((val)=>{
                                let newItem = ''
                                newItem += `Ref : ${val.ref}\n`
                                newItem += `Quantité : 1\n`
                                newItem += `Prix TTC ${val.prixTTC} €\n`
                                newItem += `PrixHT ${val.prixHT} €\n`
                                return newItem
                            })
                        } else {
                            return []
                        }
                    }

                    formatedNeedsVal.airToAirHeatPump = makeItems(needsVal.airToAirHeatPump)
                    formatedNeedsVal.airWaterHeatPump = makeItems(needsVal.airWaterHeatPump)
                    formatedNeedsVal.thermodynamicHeater = makeItems(needsVal.thermodynamicHeater)

                    const photovoltaicPanelsVal = !!formatedNeedsVal.photovoltaicPanelsData ? `Ref: ${formatedNeedsVal.photovoltaicPanelsData.ref}\nQuantité: ${formatedNeedsVal.photovoltaicPanelsData.quantity}\nPrix TTC: ${formatedNeedsVal.photovoltaicPanelsData.totalTTC}\nPrix HT: ${formatedNeedsVal.photovoltaicPanelsData.totalHT}${!!formatedNeedsVal.photovoltaicPanelsData.batterie ? '\nBatterie : ' + formatedNeedsVal.photovoltaicPanelsData.batterie : ''} \nOrientation: ${formatedNeedsVal.photovoltaicPanelsData.orientation} \nInclinaison: ${formatedNeedsVal.photovoltaicPanelsData.inclinaison}` : ''
                    const result = [
                        `Sols : ${formatedNeedsVal.floorRoofSpace}m2, ${formatedNeedsVal.floorRoofPrice}€ Rampants : ${formatedNeedsVal.crawlingRoofSpace}m2, ${formatedNeedsVal.crawlingRoofPrice}€`,
                        `Intérieurs : ${formatedNeedsVal.insideWallMeasure}m2, ${formatedNeedsVal.insideWallPrice}€ Extérieurs : ${formatedNeedsVal.outsideWallMeasure}m2, ${formatedNeedsVal.outsideWallPrice}€`,
                        `${formatedNeedsVal.lowFloorMeasure}m2, ${formatedNeedsVal.lowFloorPrice}€`,
                        photovoltaicPanelsVal,
                        formatedNeedsVal.airToAirHeatPump.join('\n'),
                        formatedNeedsVal.airWaterHeatPump.join('\n'),
                        formatedNeedsVal.thermodynamicHeater.join('\n')
                    ]
                    return result
                }else{
                    return false
                }
            },
            simulationData(){
                const simulationsVal = simulations.getSimulationByClientId(clientId)
                if(!!simulationsVal){
                    
                    const result = [
                        `${simulationsVal.bilan}KW/H`,
                        `${simulationsVal.economies.before.total - simulationsVal.economies.after.total}€`,
                        `${simulationsVal.aides.mpr}€`,
                        `${simulationsVal.aides.cee.total}€`,
                        `${simulationsVal.aides.ecobonus}€`,
                        `${simulationsVal.aides.region}€`
                    ]

                    return result
                }else{
                    return false
                }
            },
            orderData(){
                const orderVal = orderForms.getOrderFormByClientId(clientId)
                if(!!orderVal){
                    const formatedOrderVal = {}
                    formatedOrderVal.deliveryDelay = orderVal.deliveryDelay
                    formatedOrderVal.agreement = orderVal.agreement
                    formatedOrderVal.paiementTerms = orderVal.paiementTerms
                    if(orderVal.paiementTerms === 'Au comptant'){
                        formatedOrderVal.details = [`Accompte : ${orderVal.cashTerm.depositAmount} €`,`Solde : ${orderVal.cashTerm.balance} €`]
                    } else {
                        formatedOrderVal.details = [
                            `Organisme : ${orderVal.fundingTerm.financialOrganization}`, 
                            `Montant : ${orderVal.fundingTerm.amount} €`,
                            `Apport : ${orderVal.fundingTerm.financialContribution} €`,
                            `Mensualité : ${orderVal.fundingTerm.monthlyPayment} €`,
                            `Report : ${orderVal.fundingTerm.postponement}`,
                            `Durée : ${orderVal.fundingTerm.duration}`,
                            `TAEG : ${orderVal.fundingTerm.taeg}%`,
                            `Taux Débiteur : ${orderVal.fundingTerm.lendingRate}%`,
                            `Coût total : ${orderVal.fundingTerm.totalCost} €`,
                        ]
                    }

                    const result = [
                        `${formatedOrderVal.deliveryDelay}`,
                        `${formatedOrderVal.paiementTerms}`,
                        formatedOrderVal.details.join('\n'),
                        formatedOrderVal.agreement = orderVal.agreement
                    ]

                    return result
                }else{
                    return false
                }
            },
            devisData(){
                const devisVal = devis.getDevisByClientId(clientId)
                if(!!devisVal){
                    const formatedDevisVal = {}
                    formatedDevisVal.date = devisVal.data.date
                    formatedDevisVal.totalTTC = devisVal.data.toPay
                    formatedDevisVal.signature = !!devisVal.data.signature ? 'oui' : 'non'
                    formatedDevisVal.signatureDate = !!devisVal.data.signature ? moment(devisVal.modifiedDate).format('DD/MM/yyyy') : ''

                    const result = [
                        formatedDevisVal.date,
                        formatedDevisVal.totalTTC,
                        formatedDevisVal.signature,
                        formatedDevisVal.signatureDate
                    ]
                    return result
                }else{
                    return false
                }
            },
            bdcData(){
                const bdcVal = bdcs.getBdcByClientId(clientId)
                if(!!bdcVal){
                    const formatedBdcVal = {}
                    formatedBdcVal.date = moment(bdcVal.addedDate).format('DD/MM/yyyy')
                    formatedBdcVal.signature = !!bdcVal.data.signature ? 'oui' : 'non'
                    formatedBdcVal.signatureDate = !!bdcVal.data.signature ? moment(bdcVal.modifiedDate).format('DD/MM/yyyy') : ''
                    
                    const result = [
                        formatedBdcVal.date ,
                        formatedBdcVal.signature,
                        formatedBdcVal.signatureDate 
                    ]
                    return result
                }else{
                    return false
                }
            }
        }
        let clientDataRow = []
        let clientDataRowContent = []
        for (const [key, method] of Object.entries(methodsList)){
            const valueFetched = method()
            if(!!valueFetched){
                valueFetched.forEach((elem)=>{
                    clientDataRowContent.push(elem)
                })
                console.log(key)
                if(key === 'devisData'){
                    continue
                } else if (key === 'bdcData'){
                    clientDataRow.push(clientDataRowContent)
                    clientDataRowContent = []
                } else {
                    clientDataRow.push(clientDataRowContent)
                    clientDataRowContent = []
                }
            }
        }

        return clientDataRow
    }

    return api
})()