const baremeIDF = {
    1:[20593,25068,38184],
    2:[30225,36792,56130],
    3:[36297,44188,67585],
    4:[42381,51597,79041],
    5:[48488,59026,90496],
    added:[6096,7422,11455]
}

const baremeAll = {
    1:[14879,19074,29148],
    2:[21760,27896,42848],
    3:[26170,33547,51592],
    4:[30572,39192,60336],
    5:[34993,44860,69081],
    added:[4412,5651,8744]
}

const bleu = {
    chaudiere_granules:10000,
    PAC_geothermiques_solaires:10000,
    chauffage_solaire:8000,
    chaudiere_buche:8000,
    PAC_air_eau:4000,
    chauffe_eau_solaire:4000,
    poeles_granules:3000,
    poeles_buches:2500,
    solaire_hybride:2500,
    chaudiere_gaz:1200,
    chauffe_eau_thermodynamique:1200,
    isolation:{
        thermique_fenetre:100,
        murs_exterieurs:75,
        toitures:75,
        murs_interieurs:25,
        rampants_combles:25
    }
}

const jaune = {
    chaudiere_granules:8000,
    PAC_geothermiques_solaires:8000,
    chauffage_solaire:6500,
    chaudiere_buche:6500,
    PAC_air_eau:3000,
    chauffe_eau_solaire:3000,
    poeles_granules:2500,
    poeles_buches:2000,
    solaire_hybride:2000,
    chaudiere_gaz:800,
    chauffe_eau_thermodynamique:800,
    isolation:{
        thermique_fenetre:80,
        murs_exterieurs:60,
        toitures:60,
        murs_interieurs:20,
        rampants_combles:20
    }
}

const violet = {
    chaudiere_granules:4000,
    PAC_geothermiques_solaires:4000,
    chauffage_solaire:4000,
    chaudiere_buche:3000,
    PAC_air_eau:2000,
    chauffe_eau_solaire:2000,
    poeles_granules:1500,
    poeles_buches:1000,
    solaire_hybride:1000,
    chaudiere_gaz:0,
    chauffe_eau_thermodynamique:400,
    isolation:{
        thermique_fenetre:40,
        murs_exterieurs:40,
        toitures:40,
        murs_interieurs:15,
        rampants_combles:15
    }
}

const rose = {
    chaudiere_granules:0,
    PAC_geothermiques_solaires:0,
    chauffage_solaire:0,
    chaudiere_buche:0,
    PAC_air_eau:0,
    chauffe_eau_solaire:0,
    poeles_granules:0,
    poeles_buches:0,
    solaire_hybride:0,
    chaudiere_gaz:0,
    chauffe_eau_thermodynamique:0,
    isolation:{
        thermique_fenetre:0,
        murs_exterieurs:15,
        toitures:15,
        murs_interieurs:7,
        rampants_combles:7
    }
}

export {
    baremeIDF,
    baremeAll,
    bleu,
    jaune,
    violet,
    rose
}