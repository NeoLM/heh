import React from "react";
import { Meteor } from "meteor/meteor";
import { useTracker } from "meteor/react-meteor-data";
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";

import useAccount from './hooks/useAccount'

// Page & Route Components
import Login from './ui/layout/Login';
import Loading from "./ui/layout/Loading";
import Dashboard from './ui/layout/Dashboard';
import DevisLayout from './ui/layout/DevisLayout';
import BdcLayout from './ui/layout/BdcLayout';


import Home from './ui/component/Home'
import Decouverte from './ui/component/Decouverte'
import Clients from './ui/component/Clients'

import Admin from './ui/layout/Admin'


// App Container & Routes
const App = () => {
  return(
    <Router>
      
      <Route exact path="/login" component={Login} />

      {/* <Header /> */}
      <Switch>
        <ProtectedRoute path="/" layout={Dashboard} component={Home} exact />
        <ProtectedRoute path="/decouverte-client" layout={Dashboard} component={Decouverte} exact />
        <ProtectedRoute path="/mes-clients" layout={Dashboard} component={Clients} exact />
        <Route path="/documents/devis/:id"  component={DevisLayout} exact/>
        <Route path="/documents/bdc/:id"  component={BdcLayout} exact/>
        <AdminRoute path='/admin' component={Admin} exact/>
      </Switch>
      {/* <Footer /> */}
    </Router>
  );
}


// Must be logged in for this route... Briefly shows '...' while loading account data rather than redirecting...
const ProtectedRoute = ({ layout: Layout, component: Component, ...rest }) => {
  const {isLoggedIn, user, loggingIn} = useAccount()
  
  const isAdmin = user?.profile.level === 'superadmin' ? true : user?.profile.level === 'admin' ? true : false

  return (<Route
      {...rest}
      render={props => {
        return loggingIn ? <Loading /> : isLoggedIn ? isAdmin ? <Redirect to={{ pathname: "/admin" }} /> : <Layout {...props} user={user}><Component {...props} user={user} /></Layout> : <Redirect to={{ pathname: "/login" }} />;
      }}
    />
  );
}

const AdminRoute = ({ layout: Layout, component: Component, ...rest }) => {
  const {isLoggedIn, user, loggingIn} = useAccount()

  const isAdmin = user?.profile.level === 'superadmin' ? true : user?.profile.level === 'admin' ? true : false

  return (<Route
      {...rest}
      render={props => {
        return loggingIn ? <Loading /> : isLoggedIn ? isAdmin ? <Component {...props} user={user} /> : <Redirect to={{ pathname: "/" }} /> : <Redirect to={{ pathname: "/login" }} />;
      }}
    />
  );
}


export default App