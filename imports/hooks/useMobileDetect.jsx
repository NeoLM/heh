import { useState } from "react";

const useMobileDetect = (tablet)=>{

    let hasTouchScreen = false;
    if ("maxTouchPoints" in navigator) {
        hasTouchScreen = navigator.maxTouchPoints > 0;
    } else if ("msMaxTouchPoints" in navigator) {
        hasTouchScreen = navigator['msMaxTouchPoints'] > 0;
    } else {
        let mQ = window.matchMedia && matchMedia("(pointer:coarse)");
        if (mQ && mQ.media === "(pointer:coarse)") {
            hasTouchScreen = !!mQ.matches;
        } else if ('orientation' in window) {
            hasTouchScreen = true; // deprecated, but good fallback
        }
    }

    const md = new MobileDetect(window.navigator.userAgent);
    if(tablet == 'tablet'){
        const isMobileDetected = md.phone() !== null;
        return hasTouchScreen && md.isPhoneSized(600) || isMobileDetected;
    }else{
        const isMobileDetected = md.mobile() !== null || md.phone() !== null || md.tablet() !== null;
        return hasTouchScreen && md.isPhoneSized(600) || isMobileDetected;

    }

}

export default useMobileDetect