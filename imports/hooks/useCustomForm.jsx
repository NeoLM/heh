import { useState, useEffect, useRef } from "react";

const useCustomForm = ({
      initialValues,
      onSubmit,
      requiredValues,
    }) => {
    const [values, setValues] = useState(initialValues || {});
    const [errors, setErrors] = useState({});
    const [required, setRequired] = useState(requiredValues || {})
    const [touched, setTouched] = useState({});
    const [onSubmitting, setOnSubmitting] = useState(false);
    const [onBlur, setOnBlur] = useState(false);
    const [validated, setValidated] = useState(false)

    const formRendered = useRef(true);

    useEffect(() => {
      if (formRendered.current) {
        setValues(initialValues);
        setRequired(requiredValues)
        setErrors({});
        setTouched({});
        setOnSubmitting(false);
        setOnBlur(false);
        checkValidated(initialValues)
      }
      formRendered.current = false;
    }, []);

    const checkRequired = (keyValue, value) =>{
      const req = !!required[keyValue] ? true : false;
      if(typeof(value) === "number"){
        value = value.toString()
      }
      const test =  req ? !!value ? true : false : true
      return test
    }

    const checkValidated = (newValues) => {
      let isValidated = true
      let keyValidated = []
      if(!!required.$or){
        required.$or.forEach((statements)=>{
          let isValid = false
          statements.forEach((key)=>{
            const isKeyValid = checkRequired(key, newValues[key]) 
            isValid = isValid || isKeyValid
          })
          if(!!isValid){
            keyValidated = [...keyValidated, ...statements]
          }
        })
      }
      Object.keys(newValues).forEach((key)=>{
        if(!!required.$or){
          if(keyValidated.includes(key)){
            const isValid = true
            isValidated = isValidated && isValid
          } else {
            const isValid = checkRequired(key, newValues[key])
            isValidated = isValidated && isValid
          }
        } else {
          const isValid = checkRequired(key, newValues[key])
          isValidated = isValidated && isValid
        }
      })
      setValidated(isValidated)
    }

    const addErrorRequired = ()=>{
      const errorsList = {}
      let keyValidated = []
      if(!!required.$or){
        required.$or.forEach((statements)=>{
          let isValid = false
          statements.forEach((key)=>{
            const isKeyValid = checkRequired(key, values[key]) 
            isValid = isValid || isKeyValid
          })
          if(!!isValid){
            keyValidated = [...keyValidated, ...statements]
          }
        })
      }
      Object.keys(values).forEach((key)=>{
        if(!keyValidated.includes(key)){
          const isValid = checkRequired(key, values[key])
          if(!isValid){
            errorsList[key] = true
          }
        }
      })
      setErrors(errorsList)
    }

    const checkErrorRequired = (keyValue, value)=>{
      const newErrors = {...errors}
      if(Object.keys(newErrors).includes(keyValue)){
        if(!!value){
          newErrors[keyValue] = false
        } 
      }
      setErrors(newErrors)
    }

    const handleChange = (event) => {
      const { target } = event;
      const { name, value } = target;
      event.persist();
      const newValues = { ...values, [name]: value }
      setValues(newValues);
      checkValidated(newValues)
      checkErrorRequired(name,value)
    };

      const handleBlur = (event) => {
        const { target } = event;
        const { name } = target;
        setTouched({ ...touched, [name]: true });
        setErrors({ ...errors });
    };

    const handleSelect = (name, value) =>{ 
      const newValues = { ...values, [name]: value }
      setValues(newValues);
      checkValidated(newValues)
      checkErrorRequired(name,value)
    }

    const handleMultiple = (object) => {
      const newValues = {...values}
      Object.keys(object).forEach((key)=>{
        newValues[key] = object[key]
        checkErrorRequired(key,object[key])
      })
      setValues(newValues)
      checkValidated(newValues)
    }

      const handleSubmit = (event) => {
        if (event) event.preventDefault();
        if (validated){
          onSubmit({ values, errors });
        } else {
          addErrorRequired()
        }
      };

      return {
        values,
        errors,
        touched,
        validated,
        handleChange,
        handleSelect,
        handleMultiple,
        handleBlur,
        handleSubmit
      };
};

export default useCustomForm;