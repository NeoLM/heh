import { useState } from "react";
import { useTracker } from 'meteor/react-meteor-data'


const useAccount = () => useTracker(() => {
    const user = Meteor.user()
    const userId = Meteor.userId()
    const loggingIn = Meteor.loggingIn()
    return {
      user,
      userId,
      loggingIn,
      isLoggedIn: !!userId
    }
}, []);

export default useAccount;