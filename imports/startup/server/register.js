//register all methods & api here
//clients
import '../../api/clients/clients.js';
import '../../api/clients/methods.js';

//housings
import '../../api/housings/housings.js';
import '../../api/housings/methods.js';

//needs
import '../../api/needs/needs.js';
import '../../api/needs/methods.js';

//simulations
import '../../api/simulation/simulation.js';
import '../../api/simulation/methods.js';

//orderForms
import '../../api/orderForms/orderForms.js';
import '../../api/orderForms/methods.js';

//devis
import '../../api/devis/devis.js';
import '../../api/devis/methods.js';

//bdc
import '../../api/bdc/bdc.js';
import '../../api/bdc/methods.js';

//users
import '../../api/users/methods.js';

//utils
import '../../api/utils/methods.js';

//admin 
import '../../api/admin/methods.js';

//sms
import '../../api/sms/methods.js';

//mandats
import '../../api/mandats/methods.js';
import '../../api/mandats/mandats.js';

console.log('register api loaded !! ');