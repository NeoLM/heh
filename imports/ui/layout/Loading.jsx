import React, { Component } from 'react'
import { StyleSheet, css } from 'aphrodite';


class Loading extends Component {
    state = {  }
    render() { 
        return ( 
           <div className={css(styles.fullscreen)} dangerouslySetInnerHTML= {{__html:'<iframe src="loading-emoby.html" width="100%" height="100%" frameborder="0" allowfullscreen />'}}></div>
         );
    }
}

const styles = StyleSheet.create({
    fullscreen:{
        width: "100%",
        height: "100vh"
    }
})
 
export default Loading;