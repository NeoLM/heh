import React, {useEffect} from 'react';
import { StyleSheet, css } from 'aphrodite';
import LoginForm from '../component/LoginForm'
import { useHistory } from 'react-router-dom'
import useAccount from '../../hooks/useAccount'

const Picture = ({userInfo}) => {
    return (
        <>
            <div className={css(styles.linearPic)}>
                <img className={css(styles.picture)} src="src/img/default-user.jpg" />
            </div>
            <div className={css(styles.userInfo)}><span className={css(styles.medium)}>{userInfo.name}</span> {userInfo.lastname}</div>
        </>
    )
}

const Login = () => {

    const { isLoggedIn } = useAccount()

    const history = useHistory()
    
    useEffect(()=>{
        if(isLoggedIn){
            history.push('/')
        }
    },[])

    remember = Session.get('userInfo')
    return(
            <div className={ css(styles.bg)}>
                <img className={css(!!remember ? styles.logo : styles.logoNoRemember)} src="src/img/logo.png"></img>
                <div className={ css(!!remember ? styles.greatings : styles.greatingsNoRemember)}>{ 'Bienvenue ' + (!!remember ? (remember.name + ' !') : '!') }</div>
                { !!remember ? <Picture userInfo={remember} /> : null }
                 <LoginForm />        
            </div>
    )
}

const styles = StyleSheet.create({
    bg:{
        background: "url('src/img/bg-login.jpg') center no-repeat padding-box padding-box",
        backgroundSize:"cover",
        border: "1px solid #707070",
        width:'100%',
        height:"100vh",
        opacity: 1,
        display:'flex',
        flexDirection:'column',
    },
    logo:{
        width:'160px',
        height:'auto',
        margin:'1% auto 0 auto',
    },
    greatings:{
        fontSize:'37px',
        font:'Montserrat',
        margin:'0 auto',
        color:'white',
        letterSpacing: '0px',
    },
    linearPic:{
        display:'flex',
        margin:'15px auto 15px',
        border: '1px solid #FFFFFF',
        borderRadius: '50%',        
        width: '174px',
        height: '174px',    
    },
    picture:{
        margin:'auto',
        width: '144px',
        height: '144px',  
    },
    userInfo:{
        margin:'0 auto',
        fontSize: '25px',
        letterSpacing: '0px',
        color: '#FFFFFF',
    },
    logoNoRemember:{
        width:'170px',
        height:'auto',
        margin:'5% auto 0 auto',
    },
    greatingsNoRemember:{
        fontSize:'30px',
        font:'Montserrat',
        margin:'2% auto',
        color:'white',
        letterSpacing: '0px',
    },
    medium:{
        fontWeight:'500'
    }
})

export default Login