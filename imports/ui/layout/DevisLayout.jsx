import React, { useEffect } from 'react';
import { StyleSheet, css } from 'aphrodite';
import { useParams } from "react-router-dom";
import Quote from '../component/newClient/devis'



const DevisLayout = ({children}) =>{

    let { id } = useParams();
    console.log(useParams())
    useEffect(()=>{
        console.log(id)
        Session.set('devisId',id)
    },[id])

    return (
        <div className={ css(styles.bg)}>
            <div className={ css(styles.container)}>
                <div className={ css(styles.absContainer)}>
                    <Quote />
                </div>
            </div>
        </div>
    )
}

const styles = StyleSheet.create({
    bg:{
        background: "url('/src/img/architecte-cote-azur.png') center 80% no-repeat padding-box padding-box transparent",
        border: "1px solid #707070",
        width:'100%',
        height:"100vh",
        opacity: 1,
        display:'flex',
        flexDirection:'column',
    },
    container:{
        background: '#fff',
        margin: 'auto',
        borderRadius: '56px',
        maxWidth: '1000px',
        width: '55%',
        height:'75%',
        maxHeight:'1414px',
        display:'flex',
        boxSizing: 'border-box',
        alignContent:'Center'
    },
    absContainer:{
        position:'absolute',
        width:'100%',
        height:'100%',
        top:'25%',
        left:'0'
    }
})

export default DevisLayout