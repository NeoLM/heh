import React, { useEffect, useState, useRef } from 'react';
import { StyleSheet, css } from 'aphrodite';
import { useHistory } from "react-router-dom";
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session'
import { Accounts } from 'meteor/accounts-base';
import SignatureCanvas from 'react-signature-canvas'
import Modal from '../component/Modal'
import useModal from '../../hooks/useModal'

import { useTracker } from "meteor/react-meteor-data"

const Dashboard = ({user,children}) => {

    const history = useHistory()
    const [ userData, setUserData ] = useState({})

    useEffect(()=>{
        const user = Meteor.user()
        const userInfo = {
            name:user.profile.name,
            lastname:user.profile.lastname,
        }

        const fetchedUserData = {
            email:user.emails[0].address,
            phone:user.profile.phone,
            location:user.profile.location,
            oldPassword:'**********',
            newPassword:'',
            signature:user.profile?.signature
        }

        setUserData(fetchedUserData)
        Session.setPersistent('userInfo',userInfo)
    },[])
    
    const redirectHome = ()=>{
        history.push("/")
    }

    const redirectAdmin = ()=>{
        history.push("/admin")
    }

    const redirectMyClients = () => {
        history.push("/mes-clients")
        setTimeout(()=>{
            handleActiveMenu()
        },900)
    }

    const [activeMenu, setActiveMenu] = useState(false)
    const [activeModProfile, setActiveModProfile ] = useState(false)
    const [errorPass, setErrorPass ] = useState(false)
    const [ isShowingSignature, toggleSignature ] = useModal()
    const canvasRef = useRef(null);


    const handleActiveMenu = () => {
        newStateMenu = !activeMenu
        if(!newStateMenu && activeModProfile){
            handleCancelProfile()
        }
        setActiveMenu(newStateMenu)
    }

    const handleModProfile = () => {
        const newActiveModProfile = !activeModProfile
        setErrorPass(false)
        if(!newActiveModProfile && (!!userData.oldPassword && !!userData.newPassword)){
            Accounts.changePassword(userData.oldPassword, userData.newPassword, (err,res)=>{
                console.log(err,res)
                if(!err){
                    Meteor.call('updateUserInfo', {email:userData.email, phone:userData.phone, location:userData.location},(err,res)=>{
                        console.log(err,res)
                        if(res){
                            setUserData({...userData,oldPassword:''})
                            setActiveModProfile(newActiveModProfile)
                        }
                    })
                } else {
                    setErrorPass(true)
                }
            })
        } else {
            setUserData({...userData,oldPassword:''})
            setActiveModProfile(newActiveModProfile)
        }
    }
    const handleCancelProfile = () => {
        const newActiveModProfile = !activeModProfile
        setActiveModProfile(newActiveModProfile)
        setErrorPass(false)
        const user = Meteor.user()
        const fetchedUserData = {
            email:user.emails[0].address,
            phone:user.profile.phone,
            location:user.profile.location,
            oldPassword:'**********',
            newPassword:'',
            signature:user.profile?.signature
        }
        setUserData(fetchedUserData)
    }

    const handleChange = (e) => {
        const { target } = e;
        const { name, value } = target;
        e.persist();
        const newUserData = { ...userData, [name]: value }
        setUserData(newUserData)
    }

    const logout = () => {
        Meteor.logout()
    }

    const saveSignature = function(){
        const signature = canvasRef.current.getTrimmedCanvas().toDataURL('image/png')
        Meteor.call('saveUserSignature',signature,(err,res)=>{
            if(!!res){
                toggleSignature()
                const newUserData = { ...userData, signature: signature }
                setUserData(newUserData)
            }
        }) 
    }

    const [bg,setBg] = useState(false)
    useTracker(()=>{
        const bg = Session.get('background')
        setBg(bg)
    },[])

    const name = user.profile.name
    const lastname = user.profile.lastname
    const isAdmin = user.profile.level === 'superadmin' || user.profile.level === 'admin'
    return (
        <div className={css(!!bg ? styles[bg] : styles.bg)}>
            <header className={css(styles.header)}>
                <img className={css(styles.logo)} src="src/img/logo.png" onClick={redirectHome}></img>
                <div className={css(activeMenu ? styles.profileContainerActive : styles.profileContainer)}>
                    {activeMenu ?
                    <> 
                        <div onClick={handleActiveMenu} className={css(styles.closeMenu)}>X</div>
                        <div className={css(styles.profileActive)}>
                            <div className={css(styles.userPictureContainer)}>
                                <img className={css(styles.userPictureActive)} src="src/img/default-user.jpg" />
                                <div className={css(styles.userPictureUpdateContainer)}>
                                    <img className={css(styles.userPictureUpdate)} src="src/assets/camera-to-take-photos.svg" />
                                </div>
                            </div>
                            <div className={css(styles.profileNameActive)}>
                                <span style={{fontWeight:'600'}}>{name} </span> 
                                {lastname}
                            </div>
                            <div className={css(styles.profileCity)}>Paris</div>
                            <div className={css(styles.buttonActionContainer)}>
                                {activeModProfile ? 
                                <button onClick={handleCancelProfile} className={css(styles.buttonClients)}>Annuler</button>
                                :                               
                                <button onClick={redirectMyClients} className={css(styles.buttonClients)}>Mes dossiers</button>
                                }
                                <button onClick={handleModProfile} className={css(styles.buttonProfile)}>{activeModProfile ? 'Valider' : 'Éditer'} le profil</button>
                            </div>
                            {(isAdmin && !activeModProfile) && <div className={css(styles.buttonActionContainer)}>
                                <button onClick={redirectAdmin} className={css(styles.buttonClients)}>Administration</button>
                            </div>}
                            <div className={css(styles.divider)}/>
                            <div className={css(styles.profileFormContainer)}>
                                <div className={css(styles.profileFormRow)}>
                                    <div className={css(styles.profileInputContainer, styles.left)}>
                                        <div className={css(styles.profileInputTitle)}>Email</div>
                                        <div className={css(styles.flex)}>
                                            <input onChange={handleChange} className={css(styles.profileInput)} type='email' value={userData.email} name='email' disabled={!activeModProfile}/>
                                            {activeModProfile && <div className={css(styles.formDividerLeft)}/>}
                                        </div>
                                    </div>
                                    <div className={css(styles.profileInputContainer, styles.right)}>
                                        <div className={css(styles.profileInputTitle)}>Téléphone</div>
                                        <div className={css(styles.flex)}>
                                            <input onChange={handleChange} className={css(styles.profileInput)} type='phone' value={userData.phone} name='phone' disabled={!activeModProfile}/>
                                            {activeModProfile && <div className={css(styles.formDividerRight)}/>}
                                        </div>
                                    </div>
                                </div>
                                <div className={css(styles.profileFormRow)}>
                                    <div className={css(styles.profileInputContainer, styles.left)}>
                                        <div className={css(styles.profileInputTitle)}>{activeModProfile && 'Ancien'} Mot de passe</div>
                                        <div className={css(styles.flex)}>
                                            <input onChange={handleChange} className={css(styles.profileInput)} type='password' value={userData.oldPassword} name='oldPassword' disabled={!activeModProfile}/>
                                            {activeModProfile && <div className={css(styles.formDividerLeft, errorPass && styles.dividerError )}/>}
                                        </div>
                                    </div>
                                    <div className={css(styles.profileInputContainer, styles.right)}>
                                        <div className={css(styles.profileInputTitle)}>Localisation</div>
                                        <div className={css(styles.flex)}>
                                            <input onChange={handleChange} className={css(styles.profileInput)} type='place' value={userData.location} name='location' disabled={!activeModProfile}/>
                                            {activeModProfile && <div className={css(styles.formDividerRight)}/>}
                                        </div>
                                    </div>
                                </div>
                                {activeModProfile && (<div className={css(styles.profileFormRow)}>
                                    <div className={css(styles.profileInputContainer, styles.left)}>
                                        <div className={css(styles.profileInputTitle)}>Nouveau Mot de passe</div>
                                        <input onChange={handleChange} className={css(styles.profileInput)} type='password' value={userData.newPassword} name='newPassword' disabled={!activeModProfile}/>
                                        <div className={css(styles.formDividerLeft, errorPass && styles.dividerError)}/>
                                    </div>
                                    <div className={css(styles.profileInputContainer, styles.right)}>
                                        { errorPass && <div className={css(styles.profileInputTitleRed)}>Ancien mot de passe non valide</div>}
                                    </div>
                                </div>)}
                               {activeModProfile && !userData.signature && (<button onClick={toggleSignature} className={css(styles.signatureButton)}>
                                    <div className={css(styles.buttonContent)}> 
                                        <div className={css(styles.buttonSignatureText)}>Créez votre signature</div>
                                        {/* <img className={css(styles.buttonIcon)} src="src/assets/logout.svg" /> */}
                                    </div>
                                </button>)}
                            </div>
                        </div>
                        <button onClick={logout} className={css(styles.logoutButton)}>
                            <div className={css(styles.buttonContent)}> 
                                <div className={css(styles.buttonText)}>Déconnexion</div>
                                <img className={css(styles.buttonIcon)} src="src/assets/logout.svg" />
                            </div>
                        </button>

                    </>
                    :
                        <div onClick={handleActiveMenu} className={css(styles.profile)}>
                            <div className={css(styles.profileName)}>
                                <span style={{fontWeight:'600'}}>{name} </span> 
                                {lastname}<span className={css(styles.chevron)}>&#x2304;</span>
                            </div>
                            <img className={css(styles.userPicture)} src="src/img/default-user.jpg" /> 
                        </div>}
                </div>
            </header>
            {children}
            <Modal isShowing={isShowingSignature} hide={toggleSignature} title={'Signature'}>
                <div className={css(styles.canvasContainer)}>
                    <SignatureCanvas
                        ref={canvasRef}
                        canvasProps={{width: 400, height: 250, className: css(styles.canvasSign)}}
                    />
                </div>
                <div className={css(styles.buttonContainerSign)}>
                    <button
                    type="button"
                    onClick={() => canvasRef.current.clear()}
                    className={css(styles.prevButton)}
                    >
                    Recommencer
                    </button>
                    <button
                    type="button"
                    onClick={saveSignature}
                    className={css(styles.nextButton)}
                    >
                    Valider
                    </button>
                </div>
            </Modal>
        </div>
    )
}

const styles = StyleSheet.create({
    bg:{
        background: "#FBFCFE 0% 0% no-repeat padding-box;",
        width:'100%',
        height:"100vh",
        opacity: 1,
        display:'flex',
        flexDirection:'column',
    },
    Image1:{
        background:`url(/src/img/bg/Image1.png) no-repeat center fixed`,
        backgroundSize:'cover',
        width:'100%',
        height:"auto",
        minHeight:'100vh',
        opacity: 1,
        display:'flex',
        flexDirection:'column',
    },
    Image2:{
        background:`url(/src/img/bg/Image2.png) no-repeat center`,
        width:'100%',
        height:"100vh",
        opacity: 1,
        display:'flex',
        flexDirection:'column',
    },
    Image3:{
        background:`url(/src/img/bg/Image3.png) no-repeat center`,
        width:'100%',
        height:"100vh",
        opacity: 1,
        display:'flex',
        flexDirection:'column',
    },
    Image4:{
        background:`url(/src/img/bg/Image4.png) no-repeat center`,
        width:'100%',
        height:"100vh",
        opacity: 1,
        display:'flex',
        flexDirection:'column',
    },
    Image5:{
        background:`url(/src/img/bg/Image5.png) no-repeat center`,
        width:'100%',
        height:"100vh",
        opacity: 1,
        display:'flex',
        flexDirection:'column',
    },
    Image6:{
        background:`url(/src/img/bg/Image6.png) no-repeat center`,
        width:'100%',
        height:"100vh",
        opacity: 1,
        display:'flex',
        flexDirection:'column',
    },
    Image7:{
        background:`url(/src/img/bg/Image7.png) no-repeat center`,
        width:'100%',
        height:"100vh",
        opacity: 1,
        display:'flex',
        flexDirection:'column',
    },
    header:{
        display:'flex',
        padding:'2px 2%',
        marginBottom:'1px',
        justifyContent:'space-between',
        alignItems:'center',
        '@media (max-width:600px)':{
            minHeight:'12vh',
            height:'auto'
        } 
    },
    logo:{
        width:'120px',
        height:'120px',
        margin:'auto',
        marginLeft:'15px',
        cursor:'pointer',
        '@media (max-width:600px)':{
            marginLeft:'0px',
        } 
    },
    profileContainer:{
        display:'flex',
        width:'20%',
        minWidth:'220px',
        zIndex:'100',
        cursor:'pointer',
        position:'fixed',
        right: '15px',
        top:'30px',
        '@media (max-width:600px)':{
            width:'60%',
        } 
    },
    profileContainerActive:{
        background: '#FFFFFF 0% 0% no-repeat padding-box',
        boxShadow: '0px 15px 30px #1D4F900D',
        borderRadius: '20px',
        display:'flex',
        flexDirection:'column',
        justifyContent:'space-between',
        width:'27%',
        minWidth:'220px',
        height:'92%',
        zIndex:'100',
        position: 'fixed',
        right: '15px',
        top:'30px',
        padding:'10px',
        boxSizing:'border-box',
        '@media (max-width:600px)':{
            width:'calc(100vw - 30px)',
            marginLeft:'15px'
        },
        '@media (min-width:768px) and (max-width:1024px) and (min-height:1024px)':{
            width:'calc(100vw - 30px)',
            marginLeft:'15px'
        } 
    },
    profile:{
        background: '#FFFFFF 0% 0% no-repeat padding-box',
        boxShadow: '0px 15px 30px #1D4F900D',
        borderRadius: '20px',
        width:'100%',
        height:'60px',
        opacity: 1,
        margin:'auto',
        display:'flex',
    },
    profileActive:{
        width:'100%',
        height:'30%',
        opacity: 1,
        display:'flex',
        flexDirection:'column'
    },
    profileName:{
        margin:'auto 10px auto auto',
        textAlign: 'left',
        font: 'normal normal 20px/31px Montserrat',
        letterSpacing: '0px',
        color: '#4D4F5C',
    },
    logoutButton:{
        background: 'rgba(191, 30, 35, 0.17) 0% 0% no-repeat padding-box',
        width:'175px',
        height:'45px',
        border:'none',
        borderRadius:'12px',
        margin:'0 auto 20px',
        cursor:'pointer'
    },
    buttonIcon:{
        width:'20px',
        height:'20px',
        margin:'auto'
    },
    buttonContent:{
        display:'flex',
        justifyContent:'space-between'
    },
    buttonText:{
        textAlign: 'center',
        font: 'normal normal normal 15px Montserrat',
        letterSpacing: '0px',
        color: '#BF1E23',
        textShadow: '0px 3px 6px #00000014',
        margin:'auto'
    },
    signatureButton:{
        background: '#F7E13B 0% 0% no-repeat padding-box',
        width:'175px',
        height:'30px',
        border:'none',
        borderRadius:'18px',
        margin:'0 auto 20px',
        cursor:'pointer',
        '@media (max-width:600px)':{
            width:'90%'
        },
    },
    buttonSignatureText:{
        textAlign: 'center',
        font: 'normal normal normal 15px Montserrat',
        letterSpacing: '0px',
        color: 'black',
        textShadow: '0px 3px 6px #00000014',
        margin:'auto'
    },
    buttonContainerSign:{
        position:'relative',
        margin:'auto',
        marginTop:'20px',
        display:'flex',
        justifyContent:'space-evenly',
        width:'100%'
    },
    prevButton:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'white',
        borderRadius: '37px',
        width:'130px',
        height:'35px',
        cursor:'pointer',
    },
    nextButton:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'#F7E13B',
        borderRadius: '37px',
        width:'130px',
        height:'35px',
        cursor:'pointer'
    },
    canvasContainer:{
        display:'flex',
        width:'100%',
        marginTop:'20px',
    }, 
    canvasSign:{
        border: '1px solid black',
        margin:'auto'
    },
    chevron:{
        color:'#1D4F90',
        width:'11px'
    },
    body:{
        display:'flex',
        height:'100%'
    },
    bodyRow:{
        display:'flex',
        width:'80%',
    },
    buttonContainer:{
        background: '#FBFCFE 0% 0% no-repeat padding-box',
        boxShadow: '15px 15px 30px #00000014',
        border: '1px solid #1D4F90A3',
        borderRadius: '15px',
        opacity: '1',
    },
    closeMenu:{
        position:'fixed',
        right:'25px',
        top:'40px',
        fontFamily:'arial',
        fontSize:'20px',
        cursor:'pointer'
    },
    userPicture:{
        width:'40px',
        height:'40px',
        margin:'auto auto auto 2px'
    },
    chevron:{
        color:'#91BAED',
        verticalAlign: 'text-bottom',
        margin:'0 4px'
    },
    userPictureActive:{
        width:'150px',
        height:'150px',
        '@media (max-width:600px)':{
            width:'120px',
            height:'120px',
        }
    },
    userPictureContainer:{
        width:'150px',
        height:'150px',
        position:'relative',
        margin:'auto auto 10px',
        '@media (max-width:600px)':{
            width:'120px',
            height:'120px',
        }
    },
    userPictureUpdateContainer:{
        display:'flex',
        borderRadius:'50%',
        background:'white',
        cursor:'pointer',
        width:'35px',
        height:'35px',
        position:'absolute',
        top:'0px',
        right:'8px'
    },  
    userPictureUpdate:{
        margin:'auto',
        width:'19px',
        height:'19px'
    },
    profileNameActive:{
        margin:'auto',
        textAlign: 'left',
        font: 'normal normal 20px/31px Montserrat',
        letterSpacing: '0px',
        color: '#4D4F5C',
    },
    profileCity:{
        color: '#A7A7A7',
        margin:'-10px auto',
        font: 'normal normal 14px Montserrat',
    },
    buttonActionContainer:{
        width:'90%',
        display:'flex',
        margin:'20px auto auto'
    },
    buttonClients:{
        border: '2px solid #BF1E23',
        margin:'auto',
        borderRadius: '37px',
        background:'transparent',
        width:'45%',
        height:'40px',
        font: 'normal normal 12px Montserrat',
        color:'#393939',
        cursor:'pointer'
    },
    buttonProfile:{
        margin:'auto',
        border:'2px solid transparent',
        borderRadius: '37px',
        width:'45%',
        height:'40px',
        font: 'normal normal 12px Montserrat',
        color:'#393939',
        background:'linear-gradient(white,white) padding-box,linear-gradient(108deg, #1D4F90 0%, #4D75E2 100%) border-box',
        cursor:'pointer'
    },
    divider:{
        margin:'30px auto 25px auto',
        width:'70%',
        minHeight:'1px',
        backgroundColor:'rgba(29, 79, 144, 0.2)',
        '@media (max-width:600px)':{
            margin:'15px auto 0px auto'
        }
    },
    formDividerLeft:{
        marginTop:'2px',
        width:'45%',
        minHeight:'1px',
        backgroundColor:'rgba(29, 79, 144, 0.2)',
    },
    formDividerRight:{
        marginTop:'2px',
        width:'80%',
        minHeight:'1px',
        backgroundColor:'rgba(29, 79, 144, 0.2)',
    },
    dividerError:{
        backgroundColor:'rgb(191,30,35,0.8)'
    },
    profileInput:{
        background: 'none',
        border: 'none',
        font: 'normal normal 14px Montserrat',
        color:'dark'
    },
    profileFormContainer:{
        display:'flex',
        flexDirection:'column',
        width:'90%',
        margin:'auto'
    },
    profileFormRow:{
        display:'flex',
        flexDirection:'row',
        margin:'12px 0',
        justifyContent: 'space-between',
    },
    profileInputContainer:{
        display:'flex',
        flexDirection:'Column'
    },
    profileInputTitle:{
        font: 'normal normal 12px Montserrat',
        color:'#393939'
    },
    profileInputTitleRed:{
        font: 'normal normal 12px Montserrat',
        color:'rgb(191,30,35)'
    },
    left:{
        width:'65%'
    },
    right:{
        width:'35%'
    },
    flex:{
        display:'flex',
        flexDirection:'column'
    }
})

export default Dashboard