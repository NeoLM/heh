import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";


import { StyleSheet, css } from 'aphrodite';



const PDFModal = ({ isShowing, hide, vertical=false,...props }) => { 
    const hideOutSide = (e) => {
        e.preventDefault()
        if (e.target.id == 'modalOverlay'){
            hide()
        }
    }
   return isShowing
    ?  ReactDOM.createPortal(
        <>
          <div  className={css(styles.modalOverlay)} >
            <div id ={'modalOverlay'} className={css(styles.modalWrapper)} onClick={hideOutSide} >
              <div className={css(vertical ? styles.modalVertical : styles.modal)} id ={'PDFModal'}>
                <div className={css(styles.modalBody)}>{props.children}</div>
              </div>
            </div>
          </div>
        </>,
        document.body
      )
    : null;
}

PDFModal.propTypes = {
    isShowing: PropTypes.bool.isRequired,
    hide: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
    modalOverlay:{
        position:'fixed',
        top: 0,
        left: 0,
        width:'100vw',
        height:'100vh',
        zIndex:1000,
        backgroundColor:'rgba(0,0,0,0.4)'
    },
    modalWrapper:{
        position: 'fixed',
        top: 0,
        left: 0,
        zIndex: 1010,
        width: '100%',
        height: '100%',
        overflowX: 'hidden',
        overflowY: 'auto',
        outline: 0,
        display: 'flex',
        alignItems: 'center',
    },
    modal:{
        zIndex: 1020,
        background: '#fff',
        position: 'relative',
        margin: 'auto',
        borderRadius: '56px',
        maxWidth: '1000px',
        width: '35%',
        height:'95%',
        maxHeight:'1414px',
        padding: '1.5rem',
        display:'flex',
        boxSizing: 'border-box',
        '@media (max-width:768px)':{
          minWidth:'600px'
        },
        '@media (max-width:1024px)':{
          minWidth:'750px',
          minHeight:'960px',
        },
    },
    modalVertical:{
      zIndex: 1020,
      background: '#fff',
      position: 'relative',
      margin: 'auto',
      borderRadius: '56px',
      maxWidth: '1000px',
      width: '70%',
      height:'90%',
      maxHeight:'1414px',
      padding: '1.5rem',
      display:'flex',
      boxSizing: 'border-box',
      '@media (max-width:768px)':{
        minWidth:'700px'
      },
      '@media (max-width:1024px)':{
        minWidth:'950px',
      },
  },
    modalBody:{
        margin:'auto',
        display:'flex',
        flexDirection:'column',
    },
})


export default PDFModal;