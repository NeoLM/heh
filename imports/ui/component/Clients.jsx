import React, { useEffect, useState } from 'react';
import { StyleSheet, css } from 'aphrodite';
import { useHistory } from "react-router-dom";
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session'


const ClientRow = ({client})=>{
    const {name, id, date, amount, devisId, bdcId } = client

    const amountVal = ()=>{
        return !!amount ? `${amount} €` : `-`
    }

    const history = useHistory()
    
    const redirect = (adress)=>{
        history.push(adress)
    }

    const handleClientModif = ()=>{
        const address = `/decouverte-client?id=${id}`
        history.push(address)
    }

    const handleDownloadDevis = () => {

        var saveByteArray = (function () {
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            return function (data, name, callback) {
              // var blob = new Blob(data, {type: "octet/stream"});
              var blob = new Blob(data, { type: "application/pdf" });
              // var url = window.URL.createObjectURL(blob);
              var url = URL.createObjectURL(blob);
  
              var open = false;
  
              if (open) {
                window.open(url);
              } else {
                if (navigator.msSaveOrOpenBlob) {
                  navigator.msSaveOrOpenBlob(blob, fileName + '.pdf');
                  return;
                }
                else if (window.navigator.msSaveBlob) { // for IE browser
                  window.navigator.msSaveBlob(blob, fileName + '.pdf');
                  return;
                }
                a.href = url;
                a.download = name;
                a.target = '_blank';
                a.click();
                setTimeout(function () {
                  window.URL.revokeObjectURL(url);
                }, 100);
              }
  
  
              callback(null, name);
            };
          }());
          Meteor.call('getDevis',devisId,(err,res)=>{
            console.log(res)
            if(!!res){
                saveByteArray([res], 'devis.pdf', function (err, res) {})
            }
        })

         
    }

    const handleDownloadBdc = () => {

        var saveByteArray = (function () {
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            return function (data, name, callback) {
              // var blob = new Blob(data, {type: "octet/stream"});
              var blob = new Blob(data, { type: "application/pdf" });
              // var url = window.URL.createObjectURL(blob);
              var url = URL.createObjectURL(blob);
  
              var open = false;
  
              if (open) {
                window.open(url);
              } else {
                if (navigator.msSaveOrOpenBlob) {
                  navigator.msSaveOrOpenBlob(blob, fileName + '.pdf');
                  return;
                }
                else if (window.navigator.msSaveBlob) { // for IE browser
                  window.navigator.msSaveBlob(blob, fileName + '.pdf');
                  return;
                }
                a.href = url;
                a.download = name;
                a.target = '_blank';
                a.click();
                setTimeout(function () {
                  window.URL.revokeObjectURL(url);
                }, 100);
              }
  
  
              callback(null, name);
            };
          }());
          Meteor.call('getBdc',bdcId,(err,res)=>{
            console.log(res)
            if(!!res){
                saveByteArray([res], 'bdc.pdf', function (err, res) {})
            }
        })

         
    }


    return(
        <div className={css(styles.clientRow)}>
            <div className={css(styles.clientInfo)}>
                <div className={css(styles.modifyIconContainer)} onClick={handleClientModif}>
                    <img className={css(styles.icon)} src='src/assets/stilet.svg'/>
                </div>
                <div className={css(styles.ClientInfoContainer)}>
                    <div className={css(styles.ClientName)}>{name}</div>
                    <div className={css(styles.ClientDate)}>{date}</div>
                </div>
            </div>
            <div className={css(styles.clientContent)}>
                <div className={css(styles.devisContent)}>{amountVal()}</div>
            </div >
            <div className={css(styles.clientContent)}>
                { !!devisId ? 
                    <div className={css(styles.devisIconContainer)} onClick={handleDownloadDevis}>
                        <img className={css(styles.icon)} src='src/assets/download.svg'/>
                    </div> 
                    : <div className={css(styles.empty)}> - </div>
                }
            </div>
            <div className={css(styles.clientContent)}>
                { !!bdcId ?
                <div className={css(styles.bdcIconContainer)} onClick={handleDownloadBdc}>
                    <img className={css(styles.icon)} src='src/assets/download-blue.svg' />
                </div>
                : <div className={css(styles.empty)}> - </div>
                }                
            </div>
        </div>
    )
}

const Clients = () => {
    Session.set('background','Image2')
    const [clientsData, setClientsData] = useState([])
    const [searchedValue, setSearchedValue] = useState('')
    useEffect(()=>{
        Meteor.call('getClientPageData', searchedValue,(err,res)=>{
            console.log(err,res)
            if(!!res){
                setClientsData(res)
            }
        })
    },[])



    return (
        <main className={css(styles.body)}>
            <section className={css(styles.header)}>
                <div className={css(styles.headerItemFirst)}>Nom complet</div>
                <div className={css(styles.headerItem)}>Montant</div>
                <div className={css(styles.headerItem)}>Devis</div>
                <div className={css(styles.headerItem)}>Bon de commande</div>
            </section>
            <section>
                {clientsData.map((item,index)=>{
                    return(<ClientRow key={index} client={item} />)
                })}
            </section>
        </main>
    )
}

const styles = StyleSheet.create({
    body:{
        display:'flex',
        flexDirection:'column',
        height:'60vh',
        width:'95%',
        margin:'40px auto auto '
    },
    header:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-evenly',
        height:'20px',
        '@media (max-width:600px)':{
            marginBottom:'10px'
        }
    },
    headerItem:{
        width:'25%',
        color:'#BF1E23',
        font:'normal normal 14px Montserrat',
        fontWeight:'bold',
        textAlign:'center'
    },
    headerItemFirst:{
        width:'25%',
        color:'#BF1E23',
        font:'normal normal 14px Montserrat',
        fontWeight:'bold',
        textAlign:'left',
        paddingLeft:'10px',
        boxSizing:'border-box'
    },
    clientRow:{
        display:'flex',
        flexDirection:'row',
        background: '#E1ECE0 0% 0% no-repeat padding-box',
        border:'1px solid #007100',
        boxShadow: '0px 6px 30px #00000014',
        borderRadius: '10px',
        justifyContent:'space-evenly',
        height:'60px',
        margin:'15px 0'
    },
    clientRowOld:{
        display:'flex',
        flexDirection:'row',
        background: '#FBFCFE 0% 0% no-repeat padding-box',
        boxShadow: '0px 6px 30px #00000014',
        borderRadius: '10px',
        justifyContent:'space-evenly',
        height:'60px',
        margin:'15px 0'
    },
    clientInfo:{
        display:'flex',
        justifyContent:'row',
        width:'25%',
        margin:'auto'
    },
    modifyIconContainer:{
        width:'23px',
        height:'23px',
        margin:'auto 10px',
        display:'flex',
        cursor:'pointer'
    },
    ClientInfoContainer:{
        display:'flex',
        flexDirection:'column',
        margin:'auto auto auto 0'
    },
    ClientName:{
        font:'normal normal 12px Montserrat',
        fontWeight:'bold',
        color:'#4071AF',
        textAlign:'left'
    },
    ClientDate:{
        font:'normal normal 12px Montserrat',
        textAlign:'left',
        '@media (max-width:600px)':{
            fontSize:'10px'
        }
    },
    clientContent:{
        display:'flex',
        width:'25%',
        justifyContent:'center'
    },
    devisContent:{
        margin:'auto',
        fontWeight:'600',
        color:'#BF1E23',
        '@media (max-width:600px)':{
            fontSize:'12px'
        }
    },
    devisIconContainer:{
        display:'flex',
        borderRadius:'50%',
        width:'36px',
        height:'36px',
        background:'rgba(191, 30, 35, 0.17)',
        margin:'auto',
        cursor:'pointer'
    },
    bdcIconContainer:{
        display:'flex',
        borderRadius:'50%',
        width:'36px',
        height:'36px',
        background:'rgba(29, 79, 144, 0.17)',
        margin:'auto',
        cursor:'pointer'
    },
    icon:{
        width:'15px',
        height:'15px',
        margin:'auto'
    },
    empty:{
        margin:'auto'
    },
})

export default Clients