import React, { useState } from 'react';
import { StyleSheet, css } from 'aphrodite';
import { Meteor } from 'meteor/meteor'
import { useHistory } from "react-router-dom";


const Home = () =>{

    Session.set('background','Image1')
    const history = useHistory()
    
    const redirect = (adress)=>{
        history.push(adress)
    }

    const startSell = ()=>{
        redirect('/decouverte-client')
    }

    const myClients = () => {
        redirect("/mes-clients")
    }

    const myDoc = () =>{
        const link = Meteor.absoluteUrl() + 'BOOK_HOME_EXPERT_HABITAT_V3.pdf'
        const win = window.open(link)
    }

    return (
        <main className={css(styles.body)}>
            <div className={css(styles.bodyContainer)}>
                <div className={css(styles.bodyColumn)}>
                    {/* <div className={css(styles.blocContainer)}> */}
                        <button className={css(styles.buttonContainerYellow)} onClick={myDoc}>
                            <img className={css(styles.buttonImgYellow)} src="src/img/book.svg"></img>
                            <div className={css(styles.buttonBottomContainer)}>
                                <div className={css(styles.buttonTextYellow)}>Book de présentation</div>
                                <div className={css(styles.downloadContainer)}><img className={css(styles.downloadIcon)} src="src/assets/download.svg"></img></div>
                            </div>
                        </button>
                        <button className={css(styles.buttonContainer)} onClick={myClients}>
                            <img className={css(styles.buttonImg)} src="src/img/clients.svg"></img>
                            <div className={css(styles.buttonBottomContainer)}>
                                <div className={css(styles.buttonText)}>Mes dossiers</div>
                            </div>
                        </button>
                    {/* </div> */}
                </div>
                <div className={css(styles.divider)} />
                <div className={css(styles.bodyColumn)}>
                    <div className={css(styles.blocContainer)}>
                        <img className={css(styles.imgSell)} src="src/img/home.png"></img>
                        <button className={css(styles.buttonSell)} onClick={startSell}>
                            <div className={css(styles.buttonSellContent)}>
                                <div className={css(styles.buttonSellText)}>Démarrer l'étude</div> 
                                <img src="src/assets/arrow-bottom-white.svg"></img>
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </main>
    )
}

const styles = StyleSheet.create({
    body:{
        display:'flex',
        height:'calc(100vh - 125px)',
        '@media (max-width:600px)':{
            height:'100%'
        } 
    },
    bodyContainer:{
        display:'flex',
        width:'100%',
        '@media (max-width:600px)':{
            flexDirection:'column-reverse',
            height:'100%'
        } 
    },
    bodyColumn:{
        margin:'0 auto 2%',
        display:'flex',
        flexDirection:'column',
        width:'100%',
        alignContent: 'space-between',
        '@media (max-width:600px)':{
            alignContent:'normal',
            height:'100%'
        } 
    },
    blocContainer:{
        display:'flex',
        height:'100%',
        width:'100%',
        flexDirection:'column',
        margin:'auto'
    },
    divider:{
        margin:'auto',
        height:'85%',
        width:'1px',
        backgroundColor:'rgba(29, 79, 144, 0.2)',
        display:'none',
    },
    buttonContainer:{
        cursor:'pointer',
        margin:'auto auto 0',
        padding:0,
        position:'relative',
        background: '#FBFCFE 0% 0% no-repeat padding-box',
        boxShadow: '15px 15px 30px #00000014',
        border: '1px solid #1D4F90A3',
        borderRadius: '15px',
        opacity: '1',
        width:'320px',
        height:'230px',
        '@media (max-width:600px)':{
            width:'300px',
            height:'180px',
            margin:'50px auto 30px auto'
        } 
    },
    buttonContainerYellow:{
        cursor:'pointer',
        margin:'0 auto auto',
        padding:0,
        position:'relative',
        display:'flex',
        background: '#F8F7E8 0% 0% no-repeat padding-box',
        boxShadow: '15px 15px 30px #00000014',
        border: '1px solid #F7E13B',
        borderRadius: '15px',
        opacity: '1',
        width:'320px',
        height:'230px',
        '@media (max-width:600px)':{
            width:'300px',
            height:'180px',
        } 
    },
    buttonBottomContainer:{
        display: 'flex',
        justifyContent: 'space-between',
        position:'absolute',
        bottom:'0px',
        width:'100%'
    },
    buttonText:{
        font: 'normal normal normal 22px/27px Montserrat',
        letterSpacing:'0px',
        color:'#393939',
        margin:'0 auto 10px auto'
    },
    buttonTextYellow:{
        margin:'auto',
        font: 'normal normal normal 22px/27px Montserrat',
        letterSpacing:'0px',
        color:'#1D4F90',
    },
    buttonImg:{
        width: '70%',
        position:'absolute',
        top:'-20px',
        right:'30px',
        '@media (max-width:600px)':{
            width:'60%',
            top:'-27px',
            right:'52px'
        } 
    },
    buttonImgYellow:{
        width: '90%',
        position:'absolute',
        top:'-20px',
        right:'17px',
        '@media (max-width:600px)':{
            width:'80%',
            top:'-27px',
            right:'30px'
        } 
    },
    downloadContainer:{
        display:'flex',
        background: '#BCCADF 0% 0% no-repeat padding-box',
        borderRadius: '15px',
        width:'50px',
        height:'50px',
        opacity: 1,
    },
    downloadIcon:{
        margin:'auto',
        width:'50%'
    },
    buttonSell:{
        cursor:'pointer',
        display:'flex',
        margin:'0 auto auto',
        padding:0,
        background: '#007100 0% 0% no-repeat padding-box',
        border: '1px solid #007100',
        borderRadius: '40px',
        opacity: 1,
        width:'320px',
        height:'80px',
        '@media (max-width:600px)':{
            width:'300px',
            height:'60px',
            marginBottom:'50px'
        } 
    },
    buttonSellText:{
        font: 'normal normal normal 22px/27px Montserrat',
        letterSpacing:'0px',
        margin:'auto', 
        color:'white',
    },
    buttonSellContent:{
        display:'flex',
        width:'80%',
        margin:'auto'
    },
    imgSell:{
        width:'470px',
        margin:'0 auto 10px',
        border: '1px solid #BF1E23',
        borderRadius: '40px',
        '@media (max-width:600px)':{
            width:'300px',
            height:'180px',
        } 
    }
})

export default Home