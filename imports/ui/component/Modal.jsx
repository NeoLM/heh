import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";


import { StyleSheet, css } from 'aphrodite';



const Modal = ({ isShowing, hide, title, logo=true, ...props }) => { 
    const hideOutSide = (e) => {
        e.preventDefault()
        if (e.target.id == 'modalOverlay'){
            hide()
        }
    }
   return isShowing
    ?  ReactDOM.createPortal(
        <>
          <div  className={css(styles.modalOverlay)} >
            <div id ={'modalOverlay'} className={css(styles.modalWrapper)} onClick={hideOutSide} >
              <div className={css(styles.modal)}>
                <div className={css(styles.modalHeader)}>
                  {logo && <img className={css(styles.logo)} src="/src/img/logo.png"></img>}
                  <div className={css(styles.modalTitle)}>{title}</div>
                </div>
                <div className={css(styles.modalBody)}>{props.children}</div>
              </div>
            </div>
          </div>
        </>,
        document.body
      )
    : null;
}

Modal.propTypes = {
    isShowing: PropTypes.bool.isRequired,
    hide: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired
};

const styles = StyleSheet.create({
    modalOverlay:{
        position:'fixed',
        top: 0,
        left: 0,
        width:'100vw',
        height:'100vh',
        zIndex:1000,
        backgroundColor:'rgba(0,0,0,0.4)'
    },
    modalWrapper:{
        position: 'fixed',
        top: 0,
        left: 0,
        zIndex: 1010,
        width: '100%',
        height: '100%',
        overflowX: 'hidden',
        overflowY: 'auto',
        outline: 0,
        display: 'flex',
        alignItems: 'center',
    },
    modal:{
        zIndex: 1020,
        background: '#fff',
        position: 'relative',
        margin: 'auto',
        borderRadius: '56px',
        maxWidth: '650px',
        width: '80%',
        padding: '1rem',
    },
    modalHeader:{
        display: 'flex',
        flexDirection:'column',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    modalTitle:{
        font: 'normal normal medium 20px/24px Montserrat',
        letterSpacing: '2px',
        color: '#393939',
        textTransform: 'uppercase',
    },
    modalBody:{

    },
    logo:{
        width:'140px',
        height:'auto',
        margin:'0 auto',
    },
})


export default Modal;