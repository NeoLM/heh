import React, { useState } from 'react';
import { Meteor } from 'meteor/meteor'
import { StyleSheet, css } from 'aphrodite';
import { Accounts } from 'meteor/accounts-base'
import { useHistory } from 'react-router-dom'


const ResetPassword = ({token, iSToken, hide}) =>{

    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [isSame, setIsSame] = useState(false)
    const [isOk, setIsOk] = useState(false)
    const [login, setLogin] = useState(false)
    const [message, setMessage] = useState({
        message:"Entrez votre email pour réinitialiser votre mot de passe",
        type:'message'
    })

    const history = useHistory()



    const checkFormatPassword = (value) => {
        setPassword1(value)
        let regexPass = new RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/)
        if (regexPass.test(value)){
            setIsOk(true)
            setMessage({
                message:"Sécurité du mot de passe correcte",
                type:'validationMessage'
            })
        } else {
            setMessage({
                message:"Votre mot de passe doit être composé de 8 caractères minimum et inclure une majuscule, une minuscule et un chiffre",
                type:'errorMessage'
            })
        }
    }

    const checkSamePassword = (value) => {
        setPassword2(value)
        if (password1 == value) {
            setIsSame(true)
            setMessage({
                message:"Les mots de passe sont identiques",
                type:'validationMessage',
            })
        } else {
            setMessage({
                message:"Les mots de passe ne sont pas identiques",
                type:'errorMessage',
            })

        }

    }

    const resetPassword = () => {
        if(isSame && isOk){
            console.log(token)
            Accounts.resetPassword(token, password1, (err)=>{
                setLogin(true)
                setTimeout(()=>{
                    history.push('/')
                },2000)
                if(err){
                    console.log(err)
                    setMessage({
                        message:"Le token a expériré, veuillez relancer la procédure d'oubli de mot de passe",
                        type:'errorMessage',
                    })
                }
            })
        }
    }

    return login ? 
    (
        <div className={ css(styles.resetContainer)}>
                <img className={ css(styles.img)} src="src/assets/auth.svg"/>
                <div className={ css(styles.textContainer)}>
                    Réinitialisation du Mot de passe Validé<br />Vous allez être redirigé dans un instant
                </div>
        </div>
    ) :
    (
        <form className={ css(styles.resetContainer)} onSubmit={resetPassword}>
            <input 
                className={css(styles.inputReset)} 
                type="password" 
                value={password1}
                onChange={e => checkFormatPassword(e.target.value)}
            />
            <input 
                className={css(styles.inputReset)} 
                type="password" 
                value={password2}
                onChange={e => checkSamePassword(e.target.value)}
            />
            <div className={ css(styles[message.type])}>{message.message}</div>
            <button className={ css(isSame && isOk ? styles.buttonEnabled : styles.buttonDisabled)} onClick={resetPassword}>Réinitialiser mon mot de passe&nbsp;&nbsp;&nbsp;<img className={ css(styles.arrow)} src={'src/assets/arrow-black.svg'} disabled={isSame && isOk} /></button>
        </form>
    )

}

const styles = StyleSheet.create({
    message:{
        margin:'25px auto',
        textAlign:'center',
        font: 'normal normal normal 18px/25px Montserrat',
        letterSpacing:'Opx',
        color: '#101010',
        opacity: '0.3',
        lineHeight: 'normal',
    },
    errorMessage:{
        margin:'25px auto',
        textAlign:'center',
        font: 'normal normal normal 18px/25px Montserrat',
        letterSpacing:'Opx',
        color: '#BF1E23',
        lineHeight: 'normal',
    },
    validationMessage:{
        margin:'25px auto',
        textAlign:'center',
        font: 'normal normal normal 18px/25px Montserrat',
        letterSpacing:'Opx',
        color: '#1D931D',
        lineHeight: 'normal',
    },
    resetContainer:{
        margin:'25px auto',
        display:'flex',
        flexDirection:'column',
    },
    inputReset:{
        background: '#FCF3F3 0% 0% no-repeat padding-box;',
        borderRadius: '15px',
        border:'none',
        width:'70%',
        padding:'20px',
        paddingLeft:'35px',
        paddingRight:'35px',
        letterSpacing: '0px',
        color: '#39393967',
        font:'normal normal normal 16px/19px Montserrat',
        margin:'3px auto',
    },
    resendEmail:{
        margin:'4px auto',
        textAlign:'center',
        textDecoration:'underline',
        font: 'normal normal normal 14px/18px Montserrat',
        letterSpacing:'Opx',
        color:'white',
    },
    buttonDisabled:{
        margin:'25px auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'white',
        borderRadius: '37px',
        width:'55%',
        height:'50px',
        cursor:'pointer',
        '@media (max-width:600px)':{
            width:'90%',
            height:'60px'
        } 
    },
    arrow:{
        marginBottom:'3px'
    },
    buttonEnabled:{
        margin:'25px auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'#F7E13B',
        borderRadius: '37px',
        width:'55%',
        height:'50px',
        cursor:'pointer',
        '@media (max-width:600px)':{
            width:'90%',
            height:'60px'
        } 
    },
    img:{
        margin:'auto',
        width:'50%',
    },
    textContainer:{
        margin:'auto',
        font: 'normal normal normal 14px/18px Montserrat, sans serif',
        textTransform:'uppercase',
        letterSpacing:'2px',
        textAlign:'center'
    }
})

export default ResetPassword