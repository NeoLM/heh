import React, { useState, useRef, useImperativeHandle, forwardRef, useEffect } from 'react';
import { StyleSheet, css } from 'aphrodite';
import { Meteor } from 'meteor/meteor';
import { Column, Row, Input, InputRadio, Select, TextArea } from '../designSystem/DesignSystem'
import useCustomForm from '../../../hooks/useCustomForm'


const DiscoverHome = forwardRef((props,ref) => {


    const saveData = ({values,error})=>{
        Session.setPersistent('housingData', values)
        const clientId = Session.get('clientId')
        Session.set('waitFetch',true)
        console.log(values)
        Meteor.call('saveHousing',values,clientId,(err,res)=>{
            Session.setPersistent('housingId', res)
            Meteor.call('computeBilan', clientId, (err,res)=>{
                if(res){
                    Session.setPersistent('simulationId',res.id)
                    Session.setPersistent('simulationData',res.simulationData)
                    Session.set('waitFetch', false)
                }
            })
        })
    }

    const habitationContent = [
        {
            id:0,
            icon:'basement',
            content:'Maison',
            selected:false,
            key:'housingType'
        },
        {
            id:1,
            icon:'flat',
            content:'Appartement',
            selected:false,
            key:'housingType'
        },
        {
            id:2,
            icon:'office',
            content:'Bureau',
            selected:false,
            key:'housingType'
        },
        {
            id:3,
            icon:'shop',
            content:'Commerce',
            selected:false,
            key:'housingType'
        },
    ]

    const chauffage1Content = [
        {
            id:0,
            icon:'radiateur',
            content:'Radiateur',
            selected:false,
            key:'heaterType'
        }, 
        {
            id:1,
            icon:'chaudiere',
            content:'Chaudière',
            selected:false,
            key:'heaterType'
        }, 
        {
            id:2,
            icon:'pompe',
            content:'Pompe à chaleur',
            selected:false,
            key:'heaterType'
        }, 
    ]

    const chauffage2Content = [
        {
            id:0,
            icon:'flash',
            content:'Électrique',
            selected:false,
            key:'combustibleHeaterType'
        }, 
        {
            id:1,
            icon:'gaz',
            content:'Gaz',
            selected:false,
            key:'combustibleHeaterType'
        }, 
        {
            id:2,
            icon:'propane',
            content:'Propane',
            selected:false,
            key:'combustibleHeaterType'
        }, 
        {
            id:3,
            icon:'fioul',
            content:'Fioul',
            selected:false,
            key:'combustibleHeaterType'
        },
        {
            id:4,
            icon:'bois',
            content:'Bois',
            selected:false,
            key:'combustibleHeaterType'
        }, 
    ]

    const windowMaterialContent = [
        {
            id:0,
            icon:'foundation',
            content:'PVC',
            selected:false,
            key:'windowMaterial'
        }, 
        {
            id:1,
            icon:'foundation',
            content:'Bois',
            selected:false,
            key:'windowMaterial'
        }, 
        {
            id:2,
            icon:'foundation',
            content:'Autre',
            selected:false,
            key:'windowMaterial'
        }
    ]

    const roofIsolationQualityContent = [
        {
            id:0,
            icon:'foundation',
            content:'Bonne',
            selected:false,
            key:'roofIsolationQuality'
        }, 
        {
            id:1,
            icon:'foundation',
            content:'Moyenne',
            selected:false,
            key:'roofIsolationQuality'
        }, 
        {
            id:2,
            icon:'foundation',
            content:'Mauvaise',
            selected:false,
            key:'roofIsolationQuality'
        }
    ]

    const wallIsolationQualityContent = [
        {
            id:0,
            icon:'brickwall',
            content:'Bonne',
            selected:false,
            key:'brickwallIsolationQuality'
        }, 
        {
            id:1,
            icon:'brickwall',
            content:'Moyenne',
            selected:false,
            key:'brickwallIsolationQuality'
        }, 
        {
            id:2,
            icon:'brickwall',
            content:'Mauvaise',
            selected:false,
            key:'brickwallIsolationQuality'
        }
    ]

    const floorIsolationQualityContent = [
        {
            id:0,
            icon:'floor',
            content:'Bonne',
            selected:false,
            key:'floorIsolationQuality'
        }, 
        {
            id:1,
            icon:'floor',
            content:'Moyenne',
            selected:false,
            key:'floorIsolationQuality'
        }, 
        {
            id:2,
            icon:'floor',
            content:'Mauvaise',
            selected:false,
            key:'floorIsolationQuality'
        }
    ]

    const radio1 = {
        title:'Construction sur',
        name:'buildingSoil',
        radios:[
            {
                value:'Terre plein',
                label:'Terre plein',
                after:{contentType:'icon',content:'house',position:'left'},
                size:33
            },
            {
                value:'Vide sanitaire',
                label:'Vide sanitaire',
                after:{contentType:'icon',content:'foundation',position:'left'},
                size:33
            },
            {
                value:'Sous-sol',
                label:'Sous-sol',
                size:33
            },
        ]
    }

    const radio2 = {
        title:"Coefficient d'isolation",
        name:'insulationCoeff',
        radios:[
            {
                value:'1',
                label:'1',
                size:20
            },
            {
                value:'1.3',
                label:'1.3',
                size:20
            },
            {
                value:'1.5',
                label:'1.5',
                size:20
            },            
            {
                value:'1.8',
                label:'1.8',
                size:20
            },            
            {
                value:'2',
                label:'2',
                size:20
            },
        ]
    }

    const radio3 = {
        title:'Type de fenêtre',
        name:'windowType',
        radios:[
            {
                value:'Simple vitrage',
                label:'Simple vitrage',
                size:100
            },
            {
                value:'Double vitrage',
                label:'Double vitrage',
                size:100
            },
        ]
    }

    const [habitation, setHabitation] = useState(habitationContent)

    const [chauffage1, setChauffage1] = useState(chauffage1Content)
    const [chauffage2, setChauffage2] = useState(chauffage2Content)

    const [windowMaterial, setWindowMaterial] = useState(windowMaterialContent)

    const [roofIsolationQuality, setRoofIsolationQuality] = useState(roofIsolationQualityContent)
    const [wallIsolationQuality, setWallIsolationQuality] = useState(wallIsolationQualityContent)
    const [floorIsolationQuality, setFloorIsolationQuality] = useState(floorIsolationQualityContent)

    const selectState ={
        housingType:{
            state:habitation,
            setter:setHabitation
        },
        heaterType:{
            state:chauffage1,
            setter:setChauffage1
        },
        combustibleHeaterType:{
            state:chauffage2,
            setter:setChauffage2
        },
        windowMaterial:{
            state:windowMaterial,
            setter:setWindowMaterial
        },
        roofIsolationQuality:{
            state:roofIsolationQuality,
            setter:setRoofIsolationQuality
        },
        brickwallIsolationQuality:{
            state:wallIsolationQuality,
            setter:setWallIsolationQuality
        },
        floorIsolationQuality:{
            state:floorIsolationQuality,
            setter:setFloorIsolationQuality
        },
    }

    const resetThenSet = (id, key) => {
        const temp = [...selectState[key].state];
        temp.forEach((item) => item.selected = false);
        temp[id].selected = true;
        handleSelect(key, temp[id].content)
        selectState[key].setter(temp)
    }

    const initialValues = !!Session.get('housingData') ? Session.get('housingData') : {
        housingType:'',
        constructionYear:'',
        heaterType:'',
        combustibleHeaterType:'',
        ambientTemperature:'',
        livingSpace:'',
        ceilingHeight:'',
        levelNumber:'',
        buildingSoil:'',
        insulationCoeff:'',
        lastRenovations:'',
        currentConsumption:'',
        windowType:'',
        windowMaterial:'',
        roofIsolationQuality:'',
        brickwallIsolationQuality:'',
        floorIsolationQuality:'',
    }

    const requiredValues = {
        housingType:true,
        constructionYear:true,
        heaterType:true,
        combustibleHeaterType:true,
        combustibleConsumption:false,
        ambientTemperature:true,
        livingSpace:true,
        ceilingHeight:true,
        levelNumber:true,
        buildingSoil:true,
        insulationCoeff:true,
        lastRenovations:false,
        currentConsumption:true,
        windowType:true,
        windowMaterial:true,
        roofIsolationQuality:true,
        brickwallIsolationQuality:true,
        floorIsolationQuality:true,
    }

    const {
        values,
        errors,
        touched,
        validated,
        handleChange,
        handleSelect,
        handleBlur,
        handleSubmit
      } = useCustomForm({
        initialValues,
        onSubmit: saveData,
        requiredValues
      });

      useImperativeHandle(ref, () => ({
          submit(){
              return handleSubmit()
          },
          isValid(){
              return validated
          }
      }))
      Session.set('background','Image3')

      const [combustibleConso, setCombustibleConso] = useState('')
      useEffect(()=>{
        if(['Propane','Fioul','Bois'].includes(combustibleConso)){
            if(!['Propane','Fioul','Bois'].includes(values.combustibleHeaterType)){
                handleSelect('combustibleConsumption','')
            }
        }
        setCombustibleConso(values.combustibleHeaterType)
      },[values])

    return (
        <div className={css(styles.container)}>
            <Column>
                <Row>
                    <Select size={50} label={'Habitation'} title={{content:'Choisissez votre habitation',icon:'basement'}} list={habitation} resetThenSet={resetThenSet} value={values.housingType} error={errors?.housingType}/>
                    <Input onChange={handleChange} value={values.constructionYear} error={errors?.constructionYear} size={50} title={'Année de construction'} icon={'calendar'} type={'number'} name={'constructionYear'} placeholder={'1990'}></Input>
                </Row>
                <Row>
                    <Select size={50} label={'Type de chauffage'} title={{content:'Choisissez le type',icon:'radiateur'}} list={chauffage1} resetThenSet={resetThenSet} value={values.heaterType} error={errors?.heaterType}/>
                    <Select size={50} label={''} title={{content:'Combustible',icon:'flash'}} list={chauffage2} resetThenSet={resetThenSet} value={values.combustibleHeaterType} error={errors?.combustibleHeaterType}/>
                </Row>
                {['Propane','Fioul','Bois'].includes(combustibleConso) ? 
                (<Row>
                    <Input onChange={handleChange} value={values.ambientTemperature} error={errors?.ambientTemperature} size={33} title={'Température du foyer'} type={'number'} name={'ambientTemperature'} placeholder={'18'}></Input>
                    <Input onChange={handleChange} value={values.combustibleConsumption} error={errors?.combustibleConsumption} size={33} title={'Conso combustible en €'} icon={'coins'} type={'number'} name={'combustibleConsumption'} placeholder={'500'} after={{contentType:'text',content:'€',position:'left'}}></Input>
                    <Input onChange={handleChange} value={values.currentConsumption} error={errors?.currentConsumption} title={'Conso Électrique en €'} icon={'coins'} type={'number'} name={'currentConsumption'} placeholder={'500'} after={{contentType:'text',content:'€',position:'left'}} size={33}/>
                </Row>)
                :
                (<Row>
                    <Input onChange={handleChange} value={values.ambientTemperature} error={errors?.ambientTemperature} size={50} title={'Température du foyer'} type={'number'} name={'ambientTemperature'} placeholder={'18'}></Input>
                    <Input onChange={handleChange} value={values.currentConsumption} error={errors?.currentConsumption} title={'Conso actuelle en €'} icon={'coins'} type={'number'} name={'currentConsumption'} placeholder={'500'} after={{contentType:'text',content:'€',position:'left'}} size={50}/>
                </Row>)
                }
                <Row>
                    <InputRadio onChange={handleChange} value={values.windowType} error={errors?.windowType} content={radio3}></InputRadio>
                    <Select size={50} label={''} title={{content:'Matière des fenêtres',icon:'foundation'}} list={windowMaterial} resetThenSet={resetThenSet} value={values.windowMaterial} error={errors?.windowMaterial}/>
                </Row>
                <Row>
                    <Input onChange={handleChange} value={values.livingSpace} error={errors?.livingSpace} size={33} title={'Surface Habitable'} icon={'compass'} type={'number'} name={'livingSpace'} placeholder={'120'} after={{contentType:'text',content:'m2',position:'left'}}></Input>
                    <Input onChange={handleChange} value={values.ceilingHeight} error={errors?.ceilingHeight} size={33} title={'Hauteur sous plafond'} icon={'roof'} type={'number'} name={'ceilingHeight'} placeholder={'2,5'} after={{contentType:'text',content:'M',position:'left'}}></Input>
                    <Input onChange={handleChange} value={values.levelNumber} error={errors?.levelNumber} size={33} title={'Nombre de niveaux'} icon={'stairs'} type={'number'} name={'levelNumber'} placeholder={'3'} after={{contentType:'text',content:'niveaux',position:'left'}}></Input>
                </Row>
            </Column>
            <Column>
                <Row>
                    <InputRadio onChange={handleChange} value={values.buildingSoil} error={errors?.buildingSoil} content={radio1}></InputRadio>
                </Row>
                <Row>
                    <Select size={33} label={'Isolation'} title={{content:'Toiture',icon:'foundation'}} list={roofIsolationQuality} resetThenSet={resetThenSet} value={values.roofIsolationQuality} error={errors?.roofIsolationQuality}/>
                    <Select size={33} label={''} title={{content:'Mur',icon:'brickwall'}} list={wallIsolationQuality} resetThenSet={resetThenSet} value={values.brickwallIsolationQuality} error={errors?.brickwallIsolationQuality}/>
                    <Select size={33} label={''} title={{content:'Sol',icon:'floor'}} list={floorIsolationQuality} resetThenSet={resetThenSet} value={values.floorIsolationQuality} error={errors?.floorIsolationQuality}/>
                </Row>
                <Row>
                    <InputRadio onChange={handleChange} value={values.insulationCoeff} error={errors?.insulationCoeff} content={radio2}></InputRadio>
                </Row>
                <Row>
                    <TextArea onChange={handleChange} value={values.lastRenovations} error={errors?.lastRenovations} size={100} title={'Traveaux de rénovation réalisés dans les 10 dernières années :'} name={'lastRenovations'}></TextArea>
                </Row>
            </Column>
        </div>
    )
})

const styles = StyleSheet.create({
    container:{
        margin:'5px 0',
        display:'flex',
        flexDirection:'row',
        height:'100%',
        width:'100%',
        justifyContent:'space-between',
        '@media (max-width:600px)':{
            flexDirection:'column',
        },
        '@media (max-width:768px)':{
            flexDirection:'column',
        },
    },
})

export default DiscoverHome