import React, { useState, useRef, useImperativeHandle, forwardRef, useEffect } from 'react';
import { StyleSheet, css } from 'aphrodite';
import { Meteor } from 'meteor/meteor'
import PDFModal from '../PDFModal'
import Modal from '../Modal'
import useModal from '../../../hooks/useModal'
import { Document, Page, pdfjs } from 'react-pdf';
import SignatureCanvas from 'react-signature-canvas'
import { useTracker } from "meteor/react-meteor-data";
import useMobileDetect from '../../../hooks/useMobileDetect'
import ReactCodeInput from 'react-code-input'



pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;


const OrderForm = forwardRef((props,ref) => {

    const [ isShowingPDF, togglePDF ] = useModal()
    const [ isShowingSignature, toggleSignature ] = useModal()
    const [numPages, setNumPages] = useState(null);
    const [pageNumber, setPageNumber] = useState(1);
    const [pdfFile, setPdfFile] = useState(null)
    const [scale, setScale] = useState(1)
    const [verified, setVerified] = useState(false)
    const [isCodeValid, setIsCodeValid] = useState(true)
    const [signed, setSigned ] = useState(false)
    const canvasRef = useRef(null);

    const [bdcId, setBdcId] = useState() 
    const md = useMobileDetect('tablet')


    useTracker(()=>{
        if(!!Session.get('bdcId') && !bdcId){
            const bdcId = Session.get('bdcId')
            setBdcId(bdcId)
        }
    })
    
    const handleSeeBdc = function(){ 
        togglePDF()
    } 

    useEffect(()=>{
        console.log(bdcId)
        if(!!bdcId){
            Meteor.call('getBdc',bdcId,(err,res)=>{
                console.log(res)
                setPdfFile({data:res[0]})
                if(res[1] === 'signé'){
                    setSigned(true)
                }
            }) 
        }
    },[bdcId])

    useEffect(()=>{
        if(!!bdcId && !!signed){
            Meteor.call('getBdc',bdcId,(err,res)=>{
                setPdfFile({data:res[0]})
                if(res[1] === 'signé'){
                    setSigned(true)
                }
            }) 
        }
    },[signed])

    const saveSignature = function(){
        const bdcId = Session.get('bdcId')
        const signature = canvasRef.current.getTrimmedCanvas().toDataURL('image/png')
        Meteor.call('signAndSendBdc',bdcId,signature,(err,res)=>{
            console.log(res)
            toggleSignature()
            if(!!res){
                setSigned(true)
            }
        }) 
    }

    const startSignature = function(){
        if(verified){
            toggleSignature()
        } else {
            toggleSignature()
            Meteor.call('sendVerficationSMSDevis',bdcId,(err,res)=>{
                
            })
        }
    }
    const resendCode = () =>{
        Meteor.call('sendVerficationSMSDevis',bdcId,(err,res)=>{
                
        })
    }

    const handleChangeCode = function(code){
        setIsCodeValid(true)
        if(code.length == 5){
            Meteor.call('verifySmsDevis',bdcId,code,(err,res)=>{
                if(!!res){
                    setVerified(true)
                }else{
                    setIsCodeValid(false)
                }
            })
        }
    }

    const handleDownloadBdc = () => {
        var saveByteArray = (function () {
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            return function (data, name, callback) {
              // var blob = new Blob(data, {type: "octet/stream"});
              var blob = new Blob(data, { type: "application/pdf" });
              // var url = window.URL.createObjectURL(blob);
              var url = URL.createObjectURL(blob);
  
              var open = false;
  
              if (open) {
                window.open(url);
              } else {
                if (navigator.msSaveOrOpenBlob) {
                  navigator.msSaveOrOpenBlob(blob, fileName + '.pdf');
                  return;
                }
                else if (window.navigator.msSaveBlob) { // for IE browser
                  window.navigator.msSaveBlob(blob, fileName + '.pdf');
                  return;
                }
                a.href = url;
                a.download = name;
                a.target = '_blank';
                a.click();
                setTimeout(function () {
                  window.URL.revokeObjectURL(url);
                }, 100);
              }
  
  
              callback(null, name);
            };
          }());

          saveByteArray([pdfFile.data], 'bdc.pdf', function (err, res) {
            
            });
    }

    function cleanHide(){
        setPageNumber(1)
        togglePDF()
    }

    function onDocumentLoadSuccess({ numPages }) {
        setNumPages(numPages);
        setPageNumber(1);
    }

    function onPageLoadSuccess(page){
        const parentDiv = document.querySelector('#PDFModal')
        let pageScale = (parentDiv.clientWidth - 0.25*parentDiv.clientWidth) / page.originalWidth
        setScale(pageScale)
    }
    
    function changePage(offset) {
        let prevPageNumber = pageNumber 
        setPageNumber(prevPageNumber + offset);
    }
    
    function previousPage() {
        changePage(-1);
    }
    
    function nextPage() {
        changePage(1);
    }

    useImperativeHandle(ref, () => ({
        submit(){
            return true
        },
        isValid(){
            return true
        }
    }))
    
    Session.set('background',false)

    return (
        <>
        <div className={css(styles.container)}>
            <div className={css(styles.boxContainer)}>
                <div className={css(styles.buttonsContainer)}>
                    {!md && (<button className={css(styles.button)} onClick={handleSeeBdc}>
                        <div className={css(styles.buttonText)}>Visualiser le bon</div>
                        <div className={css(styles.iconContainer)}><img className={css(styles.icon)} src="/src/assets/loupe.svg"></img></div>
                    </button>)}
                    <button className={css(styles.button)}>
                        <div className={css(styles.buttonText)} onClick={handleDownloadBdc}>Télécharger le bon</div>
                        <div className={css(styles.iconContainer)}><img className={css(styles.icon)} src="/src/assets/download.svg"></img></div>
                    </button>
                    {!signed && <button className={css(styles.button)} onClick={toggleSignature}>
                        <div className={css(styles.buttonText)}>Signer le bon</div>
                        <div className={css(styles.iconContainer)}><img className={css(styles.icon)} src="/src/assets/signature-red.svg"></img></div>
                    </button>}
                </div>
            </div>
        </div>
        <PDFModal isShowing={isShowingPDF} hide={cleanHide} vertical={true}>
            <Document
                file={pdfFile}
                onLoadSuccess={onDocumentLoadSuccess}
            >
                <Page 
                    onLoadSuccess={onPageLoadSuccess}
                    scale={scale} 
                    pageNumber={pageNumber} 
                    key={`${pageNumber}_${scale}`}
                />
            </Document>
            <div className={css(styles.buttonContainer)}>
                <button
                type="button"
                disabled={pageNumber <= 1}
                onClick={previousPage}
                className={css(styles.prevButton)}
                >
                Précédente
                </button>
                <p className={css(styles.textPage)}>
                Page {pageNumber || (numPages ? 1 : '--')} sur {numPages || '--'}
                </p>
                <button
                type="button"
                disabled={pageNumber >= numPages}
                onClick={nextPage}
                className={css(styles.nextButton)}
                >
                Suivante
                </button>
            </div>
        </PDFModal>
        <Modal isShowing={isShowingSignature} hide={toggleSignature} title={'Signature'}>
            {verified ? 
            (<>
            <div className={css(styles.canvasContainer)}>
                <SignatureCanvas
                    ref={canvasRef}
                    canvasProps={{width: 400, height: 250, className: css(styles.canvasSign)}}
                />
            </div>
            <div className={css(styles.buttonContainerSign)}>
                <button
                type="button"
                onClick={() => canvasRef.current.clear()}
                className={css(styles.prevButton)}
                >
                Recommencer
                </button>
                <button
                type="button"
                onClick={saveSignature}
                className={css(styles.nextButton)}
                >
                Valider
                </button>
            </div>
            </>)
            :
            (<div className={css(styles.verifiedContainer)}>
                <div className={css(styles.verifiedText)}>Un code sms va être reçu dans quelques instants</div>
                <ReactCodeInput type='number' fields={5}  onChange={handleChangeCode} isValid={isCodeValid} inputStyleInvalid={codeStyleUnvalidated}/>
                <button
                    type="button"
                    onClick={resendCode}
                    className={css(styles.codeButton)}
                    >
                    Renvoyer un autre code 
                    </button>
            </div>)}
        </Modal>
        </>
    )
})

const styles = StyleSheet.create({
    container:{
        margin:'5px 0',
        display:'flex',
        flexDirection:'row',
        height:'47vh',
        width:'100%',
        justifyContent:'space-between',
        background: "url('/src/assets/documents.svg') center no-repeat padding-box padding-box",
        backgroundSize:'63vh'
    },
    boxContainer:{
        background: '#FBFCFE 0% 0% no-repeat padding-box',
        boxShadow: '15px 15px 30px #00000014',
        border: '1px solid #1D4F9046',
        borderRadius: '15px',
        opacity: 1,
        width:'25%',
        height:'70%',
        maxWidth:'320px',
        minWidth:'260px',
        maxHeight:'248px',
        minHeight:'238px',
        margin:'auto',
        padding:'15px',
        display:'flex'
    },
    buttonsContainer:{
        display:'flex',
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        width:'80%',
        margin:'auto'
    },
    verifiedContainer:{
        display:'flex',
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        width:'100%',
        margin:'auto'
    },
    verifiedText:{
        margin:'10px auto'
    },
    button:{
        display:'flex',
        background: 'rgba(191, 30, 35, 0.17) 0% 0% no-repeat padding-box',
        width:'100%',
        height:'50px',
        border:'none',
        borderRadius:'12px',
        margin:'10px',
        cursor:'pointer',
        padding:'0'
    },
    buttonText:{
        color:'#1D4F90',
        margin:'auto',
        fontFamily: "'Montserrat', sans-serif",
        fontSize: '14px',
    },
    iconContainer:{
        display:'flex',
        background: 'rgba(191, 30, 35, 0.23) 0% 0% no-repeat padding-box',
        borderRadius: '15px',
        width:'50px',
        height:'50px',
    },
    icon:{
        margin:'auto',
        width:'50%'
    },
    buttonContainer:{
        position:'relative',
        margin:'auto',
        bottom:'12px',
        display:'flex',
        justifyContent:'space-evenly',
        width:'100%'
    },
    buttonContainerSign:{
        position:'relative',
        margin:'auto',
        marginTop:'20px',
        display:'flex',
        justifyContent:'space-evenly',
        width:'100%'
    },
    prevButton:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'white',
        borderRadius: '37px',
        width:'130px',
        height:'35px',
        cursor:'pointer',
    },
    nextButton:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'#F7E13B',
        borderRadius: '37px',
        width:'130px',
        height:'35px',
        cursor:'pointer'
    },
    codeButton:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'white',
        borderRadius: '37px',
        width:'270px',
        height:'35px',
        cursor:'pointer',
    },
    textPage:{
        font:'Montserrat',
        fontSize:'12px',
        margin:'auto'
    },
    canvasContainer:{
        display:'flex',
        width:'100%',
    }, 
    canvasSign:{
        border: '1px solid black',
        margin:'auto'
    }
})

export default OrderForm