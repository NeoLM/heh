import React, { useState, useRef, useImperativeHandle, forwardRef, isValidElement } from 'react';
import { StyleSheet, css } from 'aphrodite';
import { Meteor } from 'meteor/meteor';
import { Column, Row, Title, Input, InputRadio } from '../designSystem/DesignSystem'
import useCustomForm from '../../../hooks/useCustomForm'
import useMobileDetect from '../../../hooks/useMobileDetect'
import { useTracker } from 'meteor/react-meteor-data'



const DiscoverClient = forwardRef((props,ref) => {

    const saveData = ({values,error})=>{
        Session.setPersistent('clientData', values)
        const clientId = Session.get('clientId')
        Meteor.call('saveClient',values,clientId,(err,res)=>{
            if(!!res){
                Session.setPersistent('clientId',res)
            }
        })
    }

    const radio1 = {
        title:'Situation familiale',
        name:'familySituation',
        radios:[
            {
                value:'Mariés',
                label:'Mariés / pacsés',
                size:33
            },
            {
                value:'Concubinage',
                label:'Concubinage',
                size:33
            },
            {
                value:'Célibataire',
                label:'Célibataire',
                size:33
            },
        ]
    }

    const initialValues = !!Session.get('clientData') ? Session.get('clientData') : {
            civilite:'',
            name:'',
            lastname:'',
            email:'',
            phone:'',
            adress:'',
            zipCode:'',
            city:'',
            familySituation:'',
            residentsNumber:'',
            profession1:'',
            profession2:'',
            birthDate1:'',
            birthDate2:'',
            lastYearHouseholdIncome:'',
            houseHoldIncome1:'',
            houseHoldIncome2:'',
        }


    const requiredValues = {
        civilite:true,
        name:true,
        lastname:true,
        email:true,
        phone:true,
        adress:true,
        zipCode:true,
        city:true,
        familySituation:true,
        residentsNumber:true,
        profession1:true,
        profession2:false,
        birthDate1:true,
        birthDate2:false,
        lastYearHouseholdIncome:true,
        houseHoldIncome1:true,
        houseHoldIncome2:false,
    }

    const {
        values,
        errors,
        touched,
        validated,
        handleChange,
        handleBlur,
        handleSubmit
      } = useCustomForm({
        initialValues,
        onSubmit: saveData,
        requiredValues
      });

      useImperativeHandle(ref, () => ({
          submit(){
              return handleSubmit()
          },
          isValid(){
              return validated
          }
      }))
      Session.set('background','Image2')

      const md = useMobileDetect('tablet')



    return (
        <div className={css(styles.container)}>
            <Column>
                <Row>
                    <div className={css(styles.input, !!md ? styles[100] : styles[50])}>
                        <div className={css(styles.inputTitle)}>Civilité</div>
                        <form className={css(styles.inputContentRadio)} value={values.civilite}>
                            <div className={css(styles.inputStyle, styles[50])}>
                                <label className={css(styles.labelRadio)}>
                                    <input type='radio' id='homme' name='civilite' value='Mr' className={css(styles.inputRadio)} defaultChecked={values.civilite === 'Mr'} onChange={handleChange}/>
                                    <div className={css(styles.svgContainer)}>
                                        <img className={css(styles.userRadioImg)} src="src/assets/man.svg"/>
                                    </div>
                                </label>
                            </div>
                            <div className={css(styles.inputStyle, styles[50])}>
                                <label className={css(styles.labelRadio)}>
                                    <input type='radio' id='femme' name='civilite' value='Mme' className={css(styles.inputRadio)} defaultChecked={values.civilite === 'Mme'} onChange={handleChange}/>
                                    <div className={css(styles.svgContainer)}>
                                        <img className={css(styles.userRadioImg)} src="src/assets/women.svg"/>
                                    </div>
                                </label>
                            </div>
                        </form>
                    </div>
                    <Input onChange={handleChange} value={values.name} error={errors?.name} title={'Prénom'} icon={'user'} type={'text'} name={'name'} placeholder={'Prénom' } size={25}/>
                    <Input onChange={handleChange} value={values.lastname} error={errors?.lastname}  title={'Nom'} icon={'user'} type={'text'} name={'lastname'} placeholder={'Nom'} size={25}/>
                </Row>
                <Row>
                    <Input onChange={handleChange} value={values.email} error={errors?.email}  title={'Email du client'} icon={'mail'} type={'email'} name={'email'} placeholder={'Email'} size={50}/>
                    <Input onChange={handleChange} value={values.phone} error={errors?.phone}  title={'Téléphone du client'} icon={'smartphone'} type={'number'} name={'phone'} placeholder={'0619048737'} size={50} />
                </Row>
                <Title text={'Adresse du client'}></Title>
                <Row>
                    <Input onChange={handleChange} value={values.adress} error={errors?.adress}  title={'Numéro et nom de la voie'} icon={'place'} type={'text'} name={'adress'} placeholder={'31 rue de Paris'} size={100}/>
                </Row>
                <Row>
                    <Input onChange={handleChange} value={values.city} error={errors?.city} title={'Ville'} icon={'place'} type={'text'} name={'city'} placeholder={'Paris'} size={50}/>
                    <Input onChange={handleChange} value={values.zipCode} error={errors?.zipCode} title={'Code postal'} icon={'place'} type={'number'} name={'zipCode'} placeholder={'75016'} size={50}/>
                </Row>
                <Title text={'Informations sur le foyer'}></Title>
                <Row>
                    <InputRadio content={radio1} onChange={handleChange} value={values.familySituation} error={errors?.familySituation} ></InputRadio>
                </Row>
            </Column>
            <Column>
                <Row>
                    <Input onChange={handleChange} value={values.residentsNumber} error={errors?.residentsNumber} title={'Habitant dans le foyer :'} type={'number'} name={'residentsNumber'} placeholder={'5'} after={{contentType:'text',content:'personnes',position:'left'}} size={100}/>
                </Row>
                <Title text={'Informations personnelles'}></Title>
                <Row>
                    <Input onChange={handleChange} value={values.profession1} error={errors?.profession1} title={'Profession conjoint 1'} icon={'suitcase'} type={'text'} name={'profession1'} placeholder={'Profession'} size={50}/>
                    <Input onChange={handleChange} value={values.profession2} error={errors?.profession2} title={'Profession conjoint 2'} icon={'suitcase'} type={'text'} name={'profession2'} placeholder={'Profession'} size={50}/>
                </Row>
                <Row>
                    <Input onChange={handleChange} value={values.birthDate1} error={errors?.birthDate1} title={'Date de naissance conjoint 1'} icon={'cake'} type={'date'} name={'birthDate1'} placeholder={'10/10/1990'} size={50}/>
                    <Input onChange={handleChange} value={values.birthDate2} error={errors?.birthDate2} title={'Date de naissance conjoint 2'} icon={'cake'} type={'date'} name={'birthDate2'} placeholder={'10/10/1990'} size={50}/>
                </Row>
                <Row>
                    <Input onChange={handleChange} value={values.lastYearHouseholdIncome} error={errors?.lastYearHouseholdIncome} title={'Revenus fiscal du foyer année N-1'} icon={'coins'} type={'number'} name={'lastYearHouseholdIncome'} placeholder={'90000'} after={{contentType:'text',content:'€',position:'left'}} size={50}/>
                    {(values.familySituation === 'Célibataire' || values.familySituation === 'Concubinage') && <Input onChange={handleChange} value={values.houseHoldIncome1} error={errors?.houseHoldIncome1} title={'Revenus fiscal conjoint 1'} icon={'coins'} type={'number'} name={'houseHoldIncome1'} placeholder={'45000'} after={{contentType:'text',content:'€',position:'left'}} size={50}/>}
                </Row>
                { values.familySituation === 'Concubinage' && <Row>
                    <Input onChange={handleChange} value={values.houseHoldIncome2} error={errors?.houseHoldIncome2} title={'Revenus fiscal conjoint 2'} icon={'coins'} type={'number'} name={'houseHoldIncome2'} placeholder={'45000'} after={{contentType:'text',content:'€',position:'left'}} size={50}/>
                </Row>}
            </Column>
        </div>
    )
})

const styles = StyleSheet.create({
    container:{
        margin:'5px 0',
        display:'flex',
        flexDirection:'row',
        height:'100%',
        width:'100%',
        justifyContent:'space-between',
        '@media (max-width:600px)':{
            flexDirection:'column',
        },
        '@media (max-width:768px)':{
            flexDirection:'column',
        },
    },
    rowTitle:{
        display:'flex',
        flexDirection:'column',
        margin:'5px 0'
    },
    specificRow:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        margin:'0 0 10px 0'
    },
    specificRowTitle:{
        display:'flex',
        flexDirection:'column',
        margin:'0 0 5px 0'
    },
    input:{
        display:'flex',
        flexDirection:'column',
        paddingRight:'5px'
    },
    inputTitle:{
        margin:'3px 0',
        textAlign: 'left',
        font: 'normal normal normal 16px Montserrat',
        letterSpacing: '0px',
        color: '#393939',
        opacity: 1,
    }, 
    inputContentRadio:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        height:'70px'
    },
    inputContent:{
        paddingLeft:'10px',
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        marginRight:'5%',
        height:'70px'
    },
    inputStyle:{
        display:'flex',
        background: '#E1ECE0 0% 0% no-repeat padding-box',
        border:'1px solid #007100',
        borderRadius: '15px',
        marginRight:'5px',
    },   
    inputStyleOld:{
        display:'flex',
        background: 'rgba(191,30,35,0.05) 0% 0% no-repeat padding-box',
        borderRadius: '15px',
        marginRight:'5px'
    },
    inputRadio:{
        margin:'auto 10px',
        background: '#FFFFFF 0% 0% no-repeat padding-box',
        border: '1px solid #4D75E279',
        opacity: 1,
        paddingLeft:'5px'
    },
    labelRadio:{
        display:'flex',
        width:'100%'
    },
    labelRadioTexte:{
        font: 'normal normal normal 14px Montserrat',
        letterSpacing: '0px',
        color: '#393939',
        margin:'auto 10px'
    },
    svgContainer:{
        margin:'auto',
        marginTop:'4px',
        height:'64px',
        overflow:'hidden'
    },
    userRadioImg:{
        width:'45px'
    },
    inputText:{
        background: 'none',
        border:'none',
        letterSpacing: '0px',
        color: '#39393967',
        font:'normal normal normal 14 Montserrat',
        margin:'auto auto auto 0',
        width:'70%',
        ':focus':{
            outline:'none'
        },
    },
    inputTextSuffix:{
        background: 'none',
        border:'none',
        letterSpacing: '0px',
        color: '#39393967',
        font:'normal normal normal 14 Montserrat',
        margin:'auto 10px',
        width:'40px',
        ':focus':{
            outline:'none'
        },
    },
    inputTextSuffixLeft:{
        background: 'none',
        border:'none',
        letterSpacing: '0px',
        color: '#39393967',
        font:'normal normal normal 14 Montserrat',
        margin:'auto 0',
        width:'25%',
        ':focus':{
            outline:'none'
        },
    },
    inputTextSuffixRight:{
        background: 'none',
        border:'none',
        letterSpacing: '0px',
        color: '#39393967',
        font:'normal normal normal 14 Montserrat',
        margin:'auto 0',
        width:'25%',
        ':focus':{
            outline:'none'
        },
    },
    inputIcon:{
        margin:'auto 15px auto 10px',
        maringLeft:'10px',
        width:'12px'
    },
    title:{
        margin:'3px 0',
        textAlign: 'left',
        font: 'normal normal normal 600 16px Montserrat',
        letterSpacing: '0px',
        color: '#BF1E23',
        opacity: 1,
    }, 
    suffix:{
        margin:'auto auto auto 0',
        font: 'normal normal normal 14px Montserrat',
        letterSpacing: '0px',
        color: '#393939',
    },
    specificText:{
        margin:'3px 0',
        textAlign: 'left',
        font: 'normal normal normal 16px Montserrat',
        letterSpacing: '0px',
        color: '#393939',
        opacity: 1,
    },
    100:{
        width:'100%'
    },
    75:{
        width:'75%'
    },
    50:{
        width:'50%'
    },
    25:{
        width:'25%' 
    },
    33:{
        width:'calc(100%/3)'
    }
})

export default DiscoverClient