import React, { useState, useRef, useEffect, useImperativeHandle, forwardRef } from 'react';
import { StyleSheet, css } from 'aphrodite';
import { Meteor } from 'meteor/meteor'
import { Column, Row, Input, Title, InputRadioSelect, InputRadio, InputRadioSelectMobile } from '../designSystem/DesignSystem'
import useCustomForm from '../../../hooks/useCustomForm'
import useModal from '../../../hooks/useModal'
import Modal from '../Modal'
import useMobileDetect from '../../../hooks/useMobileDetect'



import Carousel from 'react-elastic-carousel'


const GFA = forwardRef((props,ref) => {

    const md = useMobileDetect('tablet')

    const [ isShowing, toggle ] = useModal()
    useEffect(()=>{
        Session.set('computeBdc', true)
    },[])

    const saveData = ({values,error})=>{
        Session.set('waitFetch', true)
        if(!!Session.get('bdcId') || !!Session.get('bdcData')){
            toggle()
            Session.set('waitModal',true)
        } else {
            Session.set('waitModal',false)
        }
        Session.setPersistent('orderData', values)
        const clientId = Session.get('clientId')
        const waitForModal = setInterval(()=>{
            if(!!Session.get('waitModal')){
                return
            }else{
                Meteor.call('saveOrder',values,clientId,(err,res)=>{
                    console.log(err,res)
                    if(!!res){ 
                        Session.setPersistent('orderId',res)
                        if(!!Session.get('computeBdc')){
                            Meteor.call('generateAndSendBdc',clientId,(err,res)=>{
                                Session.setPersistent('bdcId',res)
                                Session.set('waitFetch', false)
                            })
                        }else {
                            Session.set('waitFetch', false)
                        }
                    }
                })
                clearInterval(waitForModal)
                return
            }
        })
    }

    const handleModal = (choice) =>{
        Session.set('computeBdc', choice)
        Session.set('waitModal',false)
        toggle()
    }

    const radioArrayGenerate = (radioNumber)=>{
        var result = []
        var i
        for (i = 0;i < radioNumber;i++){
            toInsert = {
                id:i,
                selected:false,
                validated2:false
            }
            result.push(toInsert)
        }
        return result
    }

    const [current, setCurrent] = useState(0)
    const [current2, setCurrent2] = useState(0)

    const handleCurrent = (id)=>{
        const values = ['Au comptant', 'Avec financement']
        handleSelect('paiementTerms', values[id])
        setCurrent2(id)
    }


    const radioArrayState = radioArrayGenerate(3)
    const radio2ArrayState = radioArrayGenerate(2)


    const [radios,setRadios] = useState([])
    const [radios2,setRadios2] = useState([])


    const [validated2,setValidated2] = useState([])

    const updateRadio = (radioId, radioArray, validated2) =>{

        const newRadios = [...radioArray]

        let radioCounter = 0

        while(radioCounter < newRadios.length){
            if(radioCounter === radioId){
                newRadios[radioCounter] = {
                    ...newRadios[radioCounter],
                    selected:true,
                }
                radioCounter++
            } else if (radioCounter < radioId){
                newRadios[radioCounter] = {
                    ...newRadios[radioCounter],
                    selected:false,
                }
                radioCounter++
            } else {
                newRadios[radioCounter] = {
                    ...newRadios[radioCounter],
                    selected:false, 
                }
                radioCounter++
            }
            if(validated2.includes(radioCounter)){
                newRadios[radioCounter] = {
                    ...newRadios[radioCounter],
                    validated2:true, 
                }
            }
        }
        return newRadios
    }

    const currentRadios = updateRadio(current, radioArrayState, validated2)
    const currentRadios2 = updateRadio(current2, radio2ArrayState, [])

    const addValidated2 = (radioId) => {
        newRadios = [...radios]
        newRadios[radioId].validated2 = true
        setRadios(newRadios)
        newValidated2 = [...validated2]
        !newValidated2.includes(radioId) && newValidated2.push(radioId)
        setValidated2(newValidated2)
    }

    const removeValidated2 = (radioId) => {
        newRadios = [...radios]
        newRadios[radioId].validated2 = false
        newValidated2 = validated2.filter((elem)=> elem !== radioId)
        setRadios(newRadios)
        setValidated2(newValidated2)
    }

    const slider = useRef();
    const slider2 = useRef();

    
    useEffect(()=>{
        setRadios(currentRadios)
        !md && slider.current.goTo(current)
    },[current,validated2])

    useEffect(()=>{
        setRadios2(currentRadios2)
        !md && slider2.current.goTo(current2)
    },[current2])

    const radio1 = {
        title:'',
        name:'agreement',
        radios:[
            {
                value:1,
                label:'Option 1',
                size:50
            },
            {
                value:2,
                label:'Option 2',
                size:50
            }
        ]
    }

    const initialValues = !!Session.get('orderData') ? Session.get('orderData') : {
        deliveryDelay:'',
        adress2:'',
        zipCode2:'',
        city2:'',
        paiementTerms:'',
        depositAmount:'',
        balance:'',
        financialOrganization:'',
        financialContribution:'',
        amount:'',
        monthlyPayment:'',
        postponement:'',
        duration:'',
        taeg:'',
        lendingRate:'',
        totalCost:'',
        agreement:'',
    }

    const requiredValues = {
        deliveryDelay:true,
        adress2:false,
        zipCode2:false,
        city2:false,
        paiementTerms:true,
        depositAmount:false,
        balance:false,
        financialOrganization:false,
        financialContribution:false,
        amount:false,
        monthlyPayment:false,
        postponement:false,
        duration:false,
        taeg:false,
        lendingRate:false,
        totalCost:false,
        agreement:true,
    }

    const {
        values,
        errors,
        touched,
        validated,
        handleChange,
        handleSelect,
        handleMultiple,
        handleBlur,
        handleSubmit
      } = useCustomForm({
        initialValues,
        onSubmit: saveData,
        requiredValues
      });


      useImperativeHandle(ref, () => ({
        submit(){
            return handleSubmit()
        },
        isValid(){
            return validated
        }
    }))

    Session.set('background','Image7')

    if(!!md){
        return (
            <div className={css(styles.container)}>
                <Column>
                    <Row>
                        <InputRadioSelectMobile size={100} icon={'coins'} setCurrent={setCurrent} title={'Livraison'} id={0} selected={radios[0]?.selected} validated2={radios[0]?.validated2}>
                            <div style={{width:'100%'}}>
                                <Row>
                                    <Input onChange={handleChange} value={values.deliveryDelay} error={errors?.deliveryDelay} title={'Delai de livraison'} icon={'place'} type={'number'} name={'deliveryDelay'} placeholder={'15'} after={{contentType:'text',content:'jours',position:'left'}} size={100}/>
                                </Row>
                                <Row>
                                    <Input onChange={handleChange} value={values.adress2} error={errors?.adress2} title={'Adresse de livraison (si différente)'} icon={'place'} type={'text'} name={'adress2'} placeholder={'31 rue de Paris'} size={100}/>
                                </Row>
                                <Row>
                                    <Input onChange={handleChange} value={values.city2} error={errors?.city2} title={'Ville'} icon={'place'} type={'text'} name={'city2'} placeholder={'Paris'} size={50}/>
                                    <Input onChange={handleChange} value={values.zipCode2} error={errors?.zipCode2} title={'Code postal'} icon={'place'} type={'number'} name={'zipCode2'} placeholder={'75016'} size={50}/>
                                </Row>
                            </div>
                        </InputRadioSelectMobile>
                    </Row>
                    <Row>
                        <InputRadioSelectMobile size={100} icon={'coins'} setCurrent={setCurrent} title={'Condition de paiement'} id={1} selected={radios[1]?.selected} validated2={radios[1]?.validated2}>
                            <div style={{width:'100%'}}>
                                <Title text={'Conditions de paiement'}></Title>
                                <Row>
                                    <InputRadioSelectMobile size={100} error={errors?.paiementTerms} icon={'coins'} setCurrent={handleCurrent} title={'Au comptant'} id={0} selected={radios2[0]?.selected} validated2={false}>
                                        <div style={{width:'100%'}}>
                                            <Row>
                                                <Input onChange={handleChange} value={values.depositAmount} error={errors?.depositAmount} title={'Acompte de 30% à percevoir à compter du 15ème jour de la signature :'} icon={'coins'} type={'number'} name={'depositAmount'} placeholder={'15'} after={{contentType:'text',content:'€',position:'left'}} size={100}/>
                                            </Row>
                                            <Row>
                                                <Input onChange={handleChange} value={values.balance} error={errors?.balance} title={'Solde à la réception des travaux'} icon={'coins'} type={'number'} name={'balance'} placeholder={'15'} after={{contentType:'text',content:'€',position:'left'}} size={100}/>
                                            </Row>
                                        </div>
                                    </InputRadioSelectMobile>
                                </Row>
                                <Row>
                                    <InputRadioSelectMobile size={100} error={errors?.paiementTerms} icon={'coins'} setCurrent={handleCurrent} title={'Avec financement'} id={1} selected={radios2[1]?.selected} validated2={false}>
                                        <div style={{width:'100%'}}>
                                            <Row>
                                                <Input onChange={handleChange} value={values.financialOrganization} error={errors?.financialOrganization} title={'Organisme financier'} icon={'place'} type={'text'} name={'financialOrganization'} placeholder={''} size={50}/>
                                                <Input onChange={handleChange} value={values.financialContribution} error={errors?.financialContribution} title={'Apport personnel'} icon={'coins'} type={'number'} name={'financialContribution'} placeholder={'0'} after={{contentType:'text',content:'€',position:'left'}} size={50}/>
                                            </Row>
                                            <Row>
                                                <Input onChange={handleChange} value={values.amount} error={errors?.amount} title={'Montant'} icon={'coins'} type={'number'} name={'amount'} placeholder={'15'} after={{contentType:'text',content:'€',position:'left'}} size={33}/>
                                                <Input onChange={handleChange} value={values.monthlyPayment} error={errors?.monthlyPayment} title={'Mensualité'} icon={'coins'} type={'number'} name={'monthlyPayment'} placeholder={'15'} after={{contentType:'text',content:'€',position:'left'}} size={33}/>
                                                <Input onChange={handleChange} value={values.postponement} error={errors?.postponement} title={'Report'} icon={'calendar'} type={'number'} name={'postponement'} placeholder={''}  size={33}/>
                                            </Row>
                                            <Row>
                                                <Input onChange={handleChange} value={values.duration} error={errors?.duration} title={'Durée'} icon={'calendar'} type={'number'} name={'duration'} placeholder={''} after={{contentType:'text',content:'mois',position:'left'}} size={33}/>
                                                <Input onChange={handleChange} value={values.taeg} error={errors?.taeg} title={'TAEG'} icon={'calc-mini'} type={'number'} name={'taeg'} placeholder={'15'} after={{contentType:'text',content:'%',position:'left'}} size={33}/>
                                                <Input onChange={handleChange} value={values.lendingRate} error={errors?.lendingRate} title={'Taux débiteur'} icon={'calc-mini'} type={'number'} name={'lendingRate'} placeholder={'15'} after={{contentType:'text',content:'%',position:'left'}} size={33}/>
                                            </Row>
                                            <Row>
                                                <Input onChange={handleChange} value={values.totalCost} error={errors?.totalCost} title={'Cout total du crédit'} icon={'coins'} type={'number'} name={'totalCost'} placeholder={'15'} after={{contentType:'text',content:'€',position:'left'}} size={100}/>
                                            </Row>
                                        </div>
                                    </InputRadioSelectMobile>
                                </Row>
                            </div>
                        </InputRadioSelectMobile>
                    </Row>
                    <Row>
                        <InputRadioSelectMobile size={100} icon={'coins'} setCurrent={setCurrent} title={'Bon pour accord'} id={2} selected={radios[2]?.selected} validated2={radios[2]?.validated2}>
                            <div style={{width:'100%'}}>
                                <Title text={"L'installation des produits sera réalisée :"}></Title>
                                <Row>
                                    <InputRadio onChange={handleChange} value={values.agreement} error={errors?.agreement} content={radio1} />
                                </Row>
                                <Row>
                                    <div className={css(styles.mentions)}>Entre le 15ème et le 30ème jour suivant la livraison des produits (stockage des produits et transfert des risques chez le client)</div>
                                    <div className={css(styles.mentions)}>Le jour de la la livraison des produits (cf: article 14 des conditions générales de vente)</div>
                                </Row>
                            </div>
                        </InputRadioSelectMobile>
                    </Row>
                </Column>
                <Modal isShowing={isShowing} hide={toggle} logo={false} title={''}>
                    <div className={css(styles.modalContent)}>Souhaitez vous regénérer un bon de commande ?</div>
                    <div className={css(styles.buttonContainer)}>
                        <button
                        type="button"
                        onClick={()=>handleModal(false)}
                        className={css(styles.prevButton)}
                        >
                        Non
                        </button>
                        <button
                        type="button"
                        onClick={()=>handleModal(true)}
                        className={css(styles.nextButton)}
                        >
                        Oui
                        </button>
                    </div>
                </Modal>
            </div>
        )
    } else {
        return (
            <div className={css(styles.container)}>
                <Column>
                    <Row>
                        <InputRadioSelect size={100} icon={'coins'} setCurrent={setCurrent} title={'Livraison'} id={0} selected={radios[0]?.selected} validated2={radios[0]?.validated2}/>
                    </Row>
                    <Row>
                        <InputRadioSelect size={100} icon={'coins'} setCurrent={setCurrent} title={'Condition de paiement'} id={1} selected={radios[1]?.selected} validated2={radios[1]?.validated2}/>
                    </Row>
                    <Row>
                        <InputRadioSelect size={100} icon={'coins'} setCurrent={setCurrent} title={'Bon pour accord'} id={2} selected={radios[2]?.selected} validated2={radios[2]?.validated2}/>
                    </Row>
                </Column>
                <Column>
                    <section>
                        <Carousel
                            showArrows={false}
                            enableMouseSwipe={false}
                            enableSwipe={false}
                            renderPagination={()=>{ return (<></>)}}
                            ref={ref=>{
                                slider.current = ref
                            }}
                        >
                            <div style={{width:'100%'}}>
                                <Row>
                                    <Input onChange={handleChange} value={values.deliveryDelay} error={errors?.deliveryDelay} title={'Delai de livraison'} icon={'place'} type={'number'} name={'deliveryDelay'} placeholder={'15'} after={{contentType:'text',content:'jours',position:'left'}} size={100}/>
                                </Row>
                                <Row>
                                    <Input onChange={handleChange} value={values.adress2} error={errors?.adress2} title={'Adresse de livraison (si différente)'} icon={'place'} type={'text'} name={'adress2'} placeholder={'31 rue de Paris'} size={100}/>
                                </Row>
                                <Row>
                                    <Input onChange={handleChange} value={values.city2} error={errors?.city2} title={'Ville'} icon={'place'} type={'text'} name={'city2'} placeholder={'Paris'} size={50}/>
                                    <Input onChange={handleChange} value={values.zipCode2} error={errors?.zipCode2} title={'Code postal'} icon={'place'} type={'number'} name={'zipCode2'} placeholder={'75016'} size={50}/>
                                </Row>
                            </div>
                            <div style={{width:'100%'}}>
                                <Title text={'Conditions de paiement'}></Title>
                                <Row>
                                    <InputRadioSelect size={50} error={errors?.paiementTerms} icon={'coins'} setCurrent={handleCurrent} title={'Au comptant'} id={0} selected={radios2[0]?.selected} validated2={false}/>
                                    <InputRadioSelect size={50} error={errors?.paiementTerms} icon={'coins'} setCurrent={handleCurrent} title={'Avec financement'} id={1} selected={radios2[1]?.selected} validated2={false}/>
                                </Row>
                                <Carousel
                                    showArrows={false}
                                    enableMouseSwipe={false}
                                    enableSwipe={false}
                                    renderPagination={()=>{ return (<></>)}}
                                    ref={ref=>{
                                        slider2.current = ref
                                    }}
                                >
                                    <div style={{width:'100%'}}>
                                        <Row>
                                            <Input onChange={handleChange} value={values.depositAmount} error={errors?.depositAmount} title={'Acompte de 30% à percevoir à compter du 15ème jour de la signature :'} icon={'coins'} type={'number'} name={'depositAmount'} placeholder={'15'} after={{contentType:'text',content:'€',position:'left'}} size={100}/>
                                        </Row>
                                        <Row>
                                            <Input onChange={handleChange} value={values.balance} error={errors?.balance} title={'Solde à la réception des travaux'} icon={'coins'} type={'number'} name={'balance'} placeholder={'15'} after={{contentType:'text',content:'€',position:'left'}} size={100}/>
                                        </Row>
                                    </div>
                                    <div style={{width:'100%'}}>
                                        <Row>
                                            <Input onChange={handleChange} value={values.financialOrganization} error={errors?.financialOrganization} title={'Organisme financier'} icon={'place'} type={'text'} name={'financialOrganization'} placeholder={''} size={50}/>
                                            <Input onChange={handleChange} value={values.financialContribution} error={errors?.financialContribution} title={'Apport personnel'} icon={'coins'} type={'number'} name={'financialContribution'} placeholder={'0'} after={{contentType:'text',content:'€',position:'left'}} size={50}/>
                                        </Row>
                                        <Row>
                                            <Input onChange={handleChange} value={values.amount} error={errors?.amount} title={'Montant'} icon={'coins'} type={'number'} name={'amount'} placeholder={'15'} after={{contentType:'text',content:'€',position:'left'}} size={33}/>
                                            <Input onChange={handleChange} value={values.monthlyPayment} error={errors?.monthlyPayment} title={'Mensualité'} icon={'coins'} type={'number'} name={'monthlyPayment'} placeholder={'15'} after={{contentType:'text',content:'€',position:'left'}} size={33}/>
                                            <Input onChange={handleChange} value={values.postponement} error={errors?.postponement} title={'Report'} icon={'calendar'} type={'number'} name={'postponement'} placeholder={''}  size={33}/>
                                        </Row>
                                        <Row>
                                            <Input onChange={handleChange} value={values.duration} error={errors?.duration} title={'Durée'} icon={'calendar'} type={'number'} name={'duration'} placeholder={''} after={{contentType:'text',content:'mois',position:'left'}} size={33}/>
                                            <Input onChange={handleChange} value={values.taeg} error={errors?.taeg} title={'TAEG'} icon={'calc-mini'} type={'number'} name={'taeg'} placeholder={'15'} after={{contentType:'text',content:'%',position:'left'}} size={33}/>
                                            <Input onChange={handleChange} value={values.lendingRate} error={errors?.lendingRate} title={'Taux débiteur'} icon={'calc-mini'} type={'number'} name={'lendingRate'} placeholder={'15'} after={{contentType:'text',content:'%',position:'left'}} size={33}/>
                                        </Row>
                                        <Row>
                                            <Input onChange={handleChange} value={values.totalCost} error={errors?.totalCost} title={'Cout total du crédit'} icon={'coins'} type={'number'} name={'totalCost'} placeholder={'15'} after={{contentType:'text',content:'€',position:'left'}} size={100}/>
                                        </Row>
                                    </div>
                                </Carousel>
                            </div>
                            <div style={{width:'100%'}}>
                                <Title text={"L'installation des produits sera réalisée :"}></Title>
                                <Row>
                                    <InputRadio onChange={handleChange} value={values.agreement} error={errors?.agreement} content={radio1} />
                                </Row>
                                <Row>
                                    <div className={css(styles.mentions)}>Entre le 15ème et le 30ème jour suivant la livraison des produits (stockage des produits et transfert des risques chez le client)</div>
                                    <div className={css(styles.mentions)}>Le jour de la la livraison des produits (cf: article 14 des conditions générales de vente)</div>
                                </Row>
                            </div>
                        </Carousel>
                    </section>
                </Column>
                <Modal isShowing={isShowing} hide={toggle} logo={false} title={''}>
                    <div className={css(styles.modalContent)}>Souhaitez vous regénérer un bon de commande ?</div>
                    <div className={css(styles.buttonContainer)}>
                        <button
                        type="button"
                        onClick={()=>handleModal(false)}
                        className={css(styles.prevButton)}
                        >
                        Non
                        </button>
                        <button
                        type="button"
                        onClick={()=>handleModal(true)}
                        className={css(styles.nextButton)}
                        >
                        Oui
                        </button>
                    </div>
                </Modal>
            </div>
        )
    }
})

const styles = StyleSheet.create({
    container:{
        margin:'5px 0',
        display:'flex',
        flexDirection:'row',
        height:'100%',
        width:'100%',
        justifyContent:'space-between',
    },
    imgDefault:{
        margin:'5% auto auto auto',
        width:'90%'
    },
    mentions:{
        width:'50%',
        font: 'normal normal normal 11px Montserrat',
        color: '#393939',
        marginRight:'5px',
    }, 
    prevButton:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'white',
        borderRadius: '37px',
        width:'130px',
        height:'35px',
        cursor:'pointer',
    },
    nextButton:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'#F7E13B',
        borderRadius: '37px',
        width:'130px',
        height:'35px',
        cursor:'pointer'
    },
    buttonContainer:{
        position:'relative',
        margin:'auto',
        marginTop:'20px',
        display:'flex',
        justifyContent:'space-evenly',
        width:'100%'
    },
    modalContent:{
        width:'90%',
        margin:'50px auto',
        font: 'Montserrat, sans serif',
        fontSize:'16px',
        letterSpacing: '0px',
        textAlign:'center',
        color: '#393939',
    },
})

export default GFA