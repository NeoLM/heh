import React, { useState, useRef, useEffect, useImperativeHandle, forwardRef } from 'react';
import { StyleSheet, css } from 'aphrodite';
import { Meteor } from 'meteor/meteor';
import useCustomForm from '../../../hooks/useCustomForm'
import Modal from '../Modal'
import useModal from '../../../hooks/useModal'
import useMobileDetect from '../../../hooks/useMobileDetect'


import { Column, Row, Title, Case, Input, Select, InputRadio } from '../designSystem/DesignSystem'


const SimulationEco = forwardRef((props,ref) => {

    const md = useMobileDetect('tablet')

    const [economies, setEconomies] = useState({...Session.get('simulationData').economies})

    const [ isOnlyPanels, setIsOnlyPanels ] = useState(false)

    const toFixedNumber = (num, digits, base) =>{
        var pow = Math.pow(base||10, digits);
        return Math.round(num*pow) / pow;
    }

    useEffect(()=>{
        Session.set('computeDevis', true)
        setEconomies({...Session.get('simulationData').economies})
        const eco = Session.get('simulationData').economies
        const testPanelsOnly = (eco.before.heater - eco.after.heater) == 0 && (eco.before.hotWater - eco.after.hotWater) == 0 ? true : false
        setIsOnlyPanels(testPanelsOnly)
    },[])

    const saveData = ()=>{
        Session.set('waitFetch', true)
        const clientId = Session.get('clientId')
       
        Meteor.call('computeHelp',clientId,(err,res)=>{
            console.log(res)
            Session.setPersistent('simulationData',res.simulationData)
            Session.set('waitFetch', false)
        })
    }


    const estimationList = [
        {
            id:0,
            icon:'calculator',
            content:'10 ans',
            selected:false,
            key:'estimation'
        }, 
        {
            id:1,
            icon:'calculator',
            content:'20 ans',
            selected:false,
            key:'estimation'
        }, 
        {
            id:2,
            icon:'calculator',
            content:'25 ans',
            selected:false,
            key:'estimation'
        }, 
    ]

    const [estimation, setEstimation] = useState(estimationList)
    const [estimationValue, setEstimationValue] = useState('-')

    const selectState ={
        estimation:{
            state:estimation,
            setter:setEstimation
        },
    }

    const resetThenSet = (id, key) => {
        const temp = [...selectState[key].state];
      
        temp.forEach((item) => item.selected = false);
        temp[id].selected = true;
        
        selectState[key].setter(temp)

        const years = id == 0 ? 10 : id == 1 ? 20 : 25
        var totalEco = economies.before.total - economies.after.total
        var currentEconomies = totalEco
        for(let i = 0; i< years; i++){
            currentEconomies = currentEconomies * 1.05
            totalEco = totalEco + currentEconomies
        }
        setEstimationValue(Math.round(totalEco))  

    }
 


    useImperativeHandle(ref, () => ({
        submit(){
            return saveData()
        },
        isValid(){
            return true
        }
    }))
    Session.set('background','Image5')


    return (
        <div className={css(styles.container)}>
            <Column>
                <Row>
                    <Case content={`Consommation totale`} title={' '} size={33}/>
                    <Case content={`${economies.before.total} €`} title={'Avant travaux'} size={33}/>
                    <Case content={`${economies.after.total} €`} title={'Après travaux'} size={33}/>
                </Row>
                {!!economies.before.electric && (<Row>
                    <Case content={`Consommation électrique totale`} size={33}/>
                    <Case content={`${economies.before.electric} €`} title={!!md ? 'Avant travaux' : undefined} size={33}/>
                    <Case content={`${economies.after.electric} €`} title={!!md ? 'Après travaux' : undefined} size={33}/>
                </Row>  )}
                <Row>
                    <Case content={`Chauffage`} disabled={isOnlyPanels} size={33}/>
                    <Case content={`${economies.before.heater} €`} disabled={isOnlyPanels} title={!!md ? 'Avant travaux' : undefined} size={33}/>
                    <Case content={`${economies.after.heater} €`} disabled={isOnlyPanels} title={!!md ? 'Après travaux' : undefined} size={33}/>
                </Row>                
                <Row>
                    <Case content={`Eau chaude`} disabled={isOnlyPanels} size={33}/>
                    <Case content={`${economies.before.hotWater} €`} disabled={isOnlyPanels} title={!!md ? 'Avant travaux' : undefined} size={33}/>
                    <Case content={`${economies.after.hotWater} €`} disabled={isOnlyPanels} title={!!md ? 'Après travaux' : undefined} size={33}/>
                </Row> 
                <Row>
                    <Case content={`Éclairage`} disabled={isOnlyPanels} size={33}/>
                    <Case content={`${economies.before.lighting} €`} disabled={isOnlyPanels} title={!!md ? 'Avant travaux' : undefined} size={33}/>
                    <Case content={`${economies.after.lighting} €`} disabled={isOnlyPanels} title={!!md ? 'Après travaux' : undefined} size={33}/>
                </Row> 
                <Row>
                    <Case content={`Domestique`} disabled={isOnlyPanels} size={33}/>
                    <Case content={`${economies.before.domestical} €`} disabled={isOnlyPanels} title={!!md ? 'Avant travaux' : undefined} size={33}/>
                    <Case content={`${economies.after.domestical} €`} disabled={isOnlyPanels} title={!!md ? 'Après travaux' : undefined} size={33}/>
                </Row> 
                <Row>
                    <Case content={`Abonnement`} disabled={isOnlyPanels} size={33}/>
                    <Case content={`${economies.before.subscription} €`} disabled={isOnlyPanels} title={!!md ? 'Avant travaux' : undefined} size={33}/>
                    <Case content={`${economies.after.subscription} €`} disabled={isOnlyPanels} title={!!md ? 'Après travaux' : undefined} size={33}/>
                </Row> 
            </Column>
            <Column>
                <Title text={'Éconnomies réalisées'}></Title>
                <Row>
                    <Case icon={'coins'} title={''} content={`${toFixedNumber(economies.before.total - economies.after.total, 2, 10)} €`} size={50} iconSize={'normal'}/>
                    <Case icon={'calc-mini'} title={''} content={`${ Math.abs(Math.round((economies.before.total - economies.after.total) / economies.before.total * 100)) } %`} size={50} iconSize={'normal'}/>
                </Row>
                {!!(economies.after.combleEco || economies.after.iteEco || economies.after.plancherEco) && (
                    <>
                        <Title text={`Impact des travaux d'isolation`}></Title>
                        <Row>
                            {!!economies.after.combleEco && <Case icon={'coins'} title={''} content={`- ${economies.after.combleEco} €`} size={33} title={'Combles'} iconSize={'normal'}/>}
                            {!!economies.after.iteEco && <Case icon={'coins'} title={''} content={`- ${economies.after.iteEco} €`} size={33} title={'Murs intérieurs / extérieurs'} iconSize={'normal'}/>}
                            {!!economies.after.plancherEco && <Case icon={'coins'} title={''} content={`- ${economies.after.plancherEco} €`} size={33} title={'Planchers'} iconSize={'normal'}/>}

                        </Row>
                    </>
                )}
                {!!economies.after.panelsProd && (
                    <>
                        <Title text={`Production d'électricité des panneaux`}></Title>
                        <Row>
                            <Case icon={'coins'} title={''} content={`${economies.after.panelsProd} €`} size={50} iconSize={'normal'}/>
                        </Row>
                    </>
                )}
                <Title text={`Simulation d'économies sur la durée`}></Title>
                <Row style={styles.customRow}>
                    <Select title={{icon:'calculator', iconSize:'normal',content:'Sélectionnez une durée'}} list={estimation} resetThenSet={resetThenSet} size={50}/>
                    <div className={css(styles.specificText)}>Resultats :</div>
                    <Case icon={'eco-house'} content={`${estimationValue} €`} size={33} iconSize={'normal'}/>
                </Row>
            </Column>
        </div>
    )
})

const styles = StyleSheet.create({
    container:{
        margin:'5px 0',
        display:'flex',
        flexDirection:'row',
        height:'100%',
        width:'100%',
        justifyContent:'space-between',
        '@media (max-width:600px)':{
            flexDirection:'column',
        },
        '@media (max-width:768px)':{
            flexDirection:'column',
        },
    },
    centerContainer:{
        margin:'auto 0'
    },
    customRow:{
        marginBottom:'55px !important'
    },
    specificText:{
        width:'25%',
        margin:'auto',
        marginRight:'5px',
        textAlign:'end',
        paddingRight:'15px', 
        font: 'normal normal normal 14px Montserrat',
        letterSpacing: '0px',
        color: '#393939',
        '@media (max-width:600px)':{
            margin:'3px auto 3px 3px'
        },
    },
    prevButton:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'white',
        borderRadius: '37px',
        width:'130px',
        height:'35px',
        cursor:'pointer',
    },
    nextButton:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'#F7E13B',
        borderRadius: '37px',
        width:'130px',
        height:'35px',
        cursor:'pointer'
    },
    buttonContainer:{
        position:'relative',
        margin:'auto',
        marginTop:'20px',
        display:'flex',
        justifyContent:'space-evenly',
        width:'100%'
    },
    modalContent:{
        width:'90%',
        margin:'50px auto',
        font: 'Montserrat, sans serif',
        fontSize:'16px',
        letterSpacing: '0px',
        textAlign:'center',
        color: '#393939',
    },
})

export default SimulationEco