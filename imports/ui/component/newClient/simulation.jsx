import React, { useState, useRef, useEffect, useImperativeHandle, forwardRef } from 'react';
import { StyleSheet, css } from 'aphrodite';
import { Meteor } from 'meteor/meteor';
import useCustomForm from '../../../hooks/useCustomForm'
import Modal from '../Modal'
import useModal from '../../../hooks/useModal'


import { Column, Row, Title, Case, Input, Select, InputRadio } from '../designSystem/DesignSystem'


const Simulation = forwardRef((props,ref) => {

    const [ isShowing, toggle ] = useModal()

    useEffect(()=>{
        Session.set('computeDevis', true)
        // const { aides } = Session.get('simulationData')
        // if(!!aides.delegataire && aides.delegataire !== 'Premium Energy' && aides.delegataire !== 'Leyton'){
        //     initialValues.delegataire = 'Autre'
        //     initialValues.delegataireOther = Session.get('simulationData').aides.delegataire
        // }
    },[])

    const saveData = ({values,error})=>{
        Session.set('waitFetch', true)
        const clientId = Session.get('clientId')
        const data = values
        if(data.delegataire == 'Autre'){
            data.delegataire = data.delegataireOther
            delete data['delegataireOther']
        }
        if(!!Session.get('devisId') || !!Session.get('devisData')){
            toggle()
            Session.set('waitModal',true)
        } else {
            Session.set('waitModal',false)
        }
        const waitForModal = setInterval(()=>{
            if(!!Session.get('waitModal')){
                return
            }else{
                Meteor.call('updateSimulation',clientId,data,(err,res)=>{
                    if(!!res){
                        console.log({...Session.get('simulationData'), aides:{...res}}, res)
                        Session.set('simulationData', {...Session.get('simulationData'), aides:{...res}})
                    }
                    if(!!Session.get('computeDevis')){
                        Meteor.call('generateAndSendDevis',clientId,(err,res)=>{
                            console.log('compute')
                            Session.setPersistent('devisId',res)
                            Session.set('waitFetch', false)
                        })
                    } else {
                        Session.set('waitFetch', false)
                    }
                })
                clearInterval(waitForModal)
                return
            }
        })
    }

    const handleModal = (choice) =>{
        Session.set('computeDevis', choice)
        Session.set('waitModal',false)
        toggle()
    }

    const radio1 = {
        title:' ',
        name:'ceeReception',
        radios:[
            {
                value:'1',
                label:'Réception par le client',
                size:50
            },
            {
                value:'2',
                label:'Déduction du devis',
                size:50
            },
        ]
    }

    const radio2 = {
        title:' ',
        name:'mprReception',
        radios:[
            {
                value:'1',
                label:'Réception par le client',
                size:50
            },
            {
                value:'2',
                label:'Déduction du devis',
                size:50
            },
        ]
    }

    const radio3 = {
        title:' ',
        name:'regionReception',
        radios:[
            {
                value:'1',
                label:'Réception par le client',
                size:50
            },
            {
                value:'2',
                label:'Déduction du devis',
                size:50
            },
        ]
    }

    const radio4 = {
        title:' ',
        name:'ecobonusReception',
        radios:[
            {
                value:'1',
                label:'Réception par le client',
                size:50
            },
            {
                value:'2',
                label:'Déduction du devis',
                size:50
            },
        ]
    }


    const delegatairesContent = [
        {
            id:0,
            content:'Premium Energy',
            selected:false,
            key:'delegataire'
        }, 
        {
            id:1,
            content:'Leyton',
            selected:false,
            key:'delegataire'
        }, 
        {
            id:2,
            content:'Autre',
            selected:false,
            key:'delegataire'
        },
    ]

    const [delegataires, setDelegataires] = useState(delegatairesContent)


    const resetThenSet = (id, key) => {
        const temp = [...delegataires];
        temp.forEach((item) => item.selected = false);
        temp[id].selected = true;
        handleSelect(key, temp[id].content)
        setDelegataires(temp)
    }



    let initialValues = {}
    if(!!Session.get('simulationData').aides.delegataire && Session.get('simulationData').aides.delegataire !== 'Premium Energy' && Session.get('simulationData').aides.delegataire !== 'Leyton'){
        initialValues = {...Session.get('simulationData').aides}
        initialValues.delegataire = 'Autre'
        initialValues.delegataireOther = Session.get('simulationData').aides.delegataire
    } else {
        initialValues = {...Session.get('simulationData').aides}
    }

    console.log(Session.get('simulationData').aides, initialValues)

    const requiredValues = {
        ecobonus:false,
        delegataire:true,
        ceeReception:true,
        mprReception:true,
        ecobonusReception:true,
        region:false,
        regionReception:true
    }

    const {
        values,
        errors,
        touched,
        validated,
        handleChange,
        handleSelect,
        handleBlur,
        handleMultiple,
        handleSubmit,
      } = useCustomForm({
        initialValues,
        onSubmit: saveData,
        requiredValues,
      });

      useImperativeHandle(ref, () => ({
          submit(){
              return handleSubmit()
          },
          isValid(){
              return validated
          }
      }))
      Session.set('background','Image5')


    return (
        <div className={css(styles.container)}>
                    <Title text={'Simulation des aides disponibles'}></Title>
                    <Row>
                        <Input onChange={handleChange} icon={'problem-solving'} title={'Prime énergie C.E.E.'} type={'number'} name={'cee'}  value={values.cee.total} error={errors?.cee} size={25} iconSize={'normal'}/>
                        <Select size={25} label={''} title={{content:'Délégataire'}} list={delegataires} resetThenSet={resetThenSet} value={values.delegataire} error={errors?.delegataire}/>
                        {(values.delegataire !== '' && values.delegataire !== 'Premium Energy' && values.delegataire !== 'Leyton') && <Input onChange={handleChange} title={''} type={'text'} name={'delegataireOther'}  value={values.delegataireOther} error={errors?.delegataireOther} size={25}/>}
                        <InputRadio content={radio1} onChange={handleChange} value={values.ceeReception} error={errors?.ceeReception} ></InputRadio>
                    </Row>
                    <Row>
                        <Input onChange={handleChange} icon={'problem-solving'} title={`Ma prime Rénov'`} type={'number'} name={'mpr'} value={values.mpr} error={errors?.mpr} size={50} iconSize={'normal'}/>
                        <InputRadio content={radio2} onChange={handleChange} value={values.mprReception} error={errors?.mprReception} ></InputRadio>
                    </Row>
                    <Row>
                        <Input icon={'problem-solving'} onChange={handleChange} type={'number'} name={'region'} title={`Aide régionale`} value={values.region} error={errors?.region} size={50} placeholder={'Entrez un montant'} iconSize={'normal'}/>
                        <InputRadio content={radio3} onChange={handleChange} value={values.regionReception} error={errors?.regionReception} ></InputRadio>
                    </Row>
                    <Row>
                        <Input size={50} onChange={handleChange} icon={'plant'} type={'number'} title={'Ecobonus'} name={'ecobonus'} value={values.ecobonus} placeholder={'Entrez un montant'} iconSize={'normal'}/>
                        <InputRadio content={radio4} onChange={handleChange} value={values.ecobonusReception} error={errors?.ecobonusReception} ></InputRadio>
                    </Row>
            <Modal isShowing={isShowing} hide={toggle} logo={false} title={''}>
                <div className={css(styles.modalContent)}>Souhaitez vous regénérer un devis ?</div>
                <div className={css(styles.buttonContainer)}>
                    <button
                    type="button"
                    onClick={()=>handleModal(false)}
                    className={css(styles.prevButton)}
                    >
                    Non
                    </button>
                    <button
                    type="button"
                    onClick={()=>handleModal(true)}
                    className={css(styles.nextButton)}
                    >
                    Oui
                    </button>
                </div>
            </Modal>
        </div>
    )
})

const styles = StyleSheet.create({
    container:{
        margin:'5px 0',
        display:'flex',
        flexDirection:'column',
        height:'100%',
        width:'100%',
        justifyContent:'space-between',
    },
    centerContainer:{
        margin:'auto 0'
    },
    customRow:{
        marginBottom:'55px !important'
    },
    specificText:{
        width:'33%',
        margin:'auto',
        marginRight:'5px',
        textAlign:'end',
        paddingRight:'15px', 
        font: 'normal normal normal 14px Montserrat',
        letterSpacing: '0px',
        color: '#393939',
    },
    prevButton:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'white',
        borderRadius: '37px',
        width:'130px',
        height:'35px',
        cursor:'pointer',
    },
    nextButton:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'#F7E13B',
        borderRadius: '37px',
        width:'130px',
        height:'35px',
        cursor:'pointer'
    },
    buttonContainer:{
        position:'relative',
        margin:'auto',
        marginTop:'20px',
        display:'flex',
        justifyContent:'space-evenly',
        width:'100%'
    },
    modalContent:{
        width:'90%',
        margin:'50px auto',
        font: 'Montserrat, sans serif',
        fontSize:'16px',
        letterSpacing: '0px',
        textAlign:'center',
        color: '#393939',
    },
})

export default Simulation