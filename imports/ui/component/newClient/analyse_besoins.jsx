import React, { useState, useRef, useEffect, useImperativeHandle, forwardRef } from 'react';
import { StyleSheet, css } from 'aphrodite';
import { Meteor } from 'meteor/meteor'
import { Column, Row, InputRadioSelect, Select, TextArea, Input, Title, Case, InputRadioSelectMobile } from '../designSystem/DesignSystem'
import useCustomForm from '../../../hooks/useCustomForm'
import useModal from '../../../hooks/useModal'
import Modal from '../Modal'
import { paneauxPhotovoltaiques,
    ballonsThermodynamiques,
    pompeAirEau,
    batteriePhotovolataique,
    } from '../../../api/dbprice'

import Carousel from 'react-elastic-carousel'

import useMobileDetect from '../../../hooks/useMobileDetect'


const ModalContent = ({type, toggle, mode, syncMethod, productList, productSetter, product})=>{
    const toFixedNumber = (num, digits, base) =>{
        var pow = Math.pow(base||10, digits);
        return Math.round(num*pow) / pow;
    }
    const initialValues = mode === 'product' ? {
        ref:'',
        modèle:'',
        description:'',
        puissance:0,
        prixHT:0,
        prixTTC:0,
        tauxTva:5.5,
        quantity:1
    } : {
        tauxTva:product.tauxTva,
        prixHT:product.prixHT,
        prixTTC:product.prixTTC
    }

    const requiredValues = mode === 'product' ? {
        ref:true,
        modèle:true,
        description:true,
        puissance:true,
        prixHT:true,
        prixTTC:true,
        tauxTva:true,
        quantity:true
    } : {
        prixHT:true,
        prixTTC:true,
    }


    if(type === 'ballon' && mode === 'product'){
        initialValues.contenance = ''
        requiredValues.contenance = true
    } 
    if(type === 'PACRO' || type === 'PACRR'  && mode === 'product'){
        initialValues.type = ''
        requiredValues.type = true
    } 
    if(type === 'ACCESS' && mode === 'product'){
        delete initialValues.puissance
        delete requiredValues.puissance
        initialValues.categorie = '',
        requiredValues.categorie = true
    }
    const categorieList = ['Isolation', 'Panneaux photo','Chauffe eau','Radiateurs','Pompe Air-Air','Pompe Air-eau']
    const categoriesContent = categorieList.map((item,index)=>{
        return {
            id:index,
            content:item,
            selected:false,
            key:'categorie'
        }
    })

    const [categorie, setCategorie] = useState(categoriesContent)

    const typeList = ['console', 'murales']
    const typesContent = typeList.map((item,index)=>{
        return {
            id:index,
            content:item,
            selected:false,
            key:'type'
        }
    })

    const [typePACRR, setTypesPACRR] = useState(typesContent)

    const resetThenSet = (id, key) => {
        const temp = type == 'PACRR' ? [...typePACRR] : [...categorie];
        temp.forEach((item) => item.selected = false);
        temp[id].selected = true;
        handleSelect(key, temp[id].content)
        setCategorie(temp)
    }

    const addProduct = () => {
        if(validated){
            const numberKeyType = ['puissance', 'prixHT', 'prixTTC']
            let typedValues = {}
            for (const [key, val] of Object.entries(values)){
                if(numberKeyType.includes(key)){
                    typedValues[key] = Number(val)
                } else {
                    typedValues[key] = val
                }
            }
            const newList = [typedValues, ...productList]
            productSetter(newList)
            syncMethod(typedValues, mode)
            toggle()
        }
    }
    const addPrice = () => {
        if(validated){
            let newProduct = {}
            const newProductList = productList.map((productElem)=>{
                if(productElem.ref === product.ref){
                    newProduct = { ...productElem}
                    newProduct.prixHT = Number(values.prixHT)
                    newProduct.prixTTC = Number(values.prixTTC)
                    return newProduct
                } else {
                    return productElem
                }
            })
            productSetter(newProductList)
            syncMethod(newProduct, mode)
            toggle()
        }
    }

    const handlePrice = (e)=>{
        const { name, value } = e.target
        console.log(name, value)
        if(name=== 'prixHT'){
            const valueTTC = toFixedNumber(Number(value) * (1 + (values.tauxTva/100)),2,10)
            handleMultiple({'prixHT': value, 'prixTTC':valueTTC})
        }
        if(name=== 'prixTTC'){
            const valueHT = toFixedNumber(Number(value) / (1 + (values.tauxTva/100)),2,10)
            handleMultiple({'prixHT': valueHT, 'prixTTC':value})
        }
    }

    const submitFunc = mode === 'product' ? addProduct : addPrice

    const {
        values,
        errors,
        touched,
        validated,
        handleChange,
        handleSelect,
        handleMultiple,
        handleBlur,
        handleSubmit
      } = useCustomForm({
        initialValues,
        onSubmit: submitFunc,
        requiredValues
      });


    return ( 
        <> 
        <div className={css(styles.modalContentContainer)}>            
            <div className={css(styles.modalColumn)}>
                { mode === 'product' ? (
                <>
                    <Row>
                        <Input onChange={handleChange} value={values.ref} error={errors.ref} title={'Référence'} type={'text'} name={'ref'} size={100}/>
                    </Row>
                    <Row>
                        <Input onChange={handleChange} value={values.modèle} error={errors.modèle} title={'Modèle'} type={'text'} name={'modèle'} size={100}/>
                    </Row>
                    { type === 'ACCESS' && (
                        <Row>
                            <Select size={100} label={'Catégorie'} title={{content:'Choisissez votre catégorie'}} list={categorie} resetThenSet={resetThenSet} value={values.categorie} error={errors?.categorie}/>
                        </Row>
                    )}
                    { type != 'ACCESS' && ( <Row>
                        <Input onChange={handleChange} value={values.puissance} error={errors.puissance} title={'Puissance'} type={'number'} name={'puissance'} size={100}/>
                    </Row>)}
                    {type != 'PACRR' && type != 'ACCESS' && (<Row>
                        <Input onChange={handleChange} value={type === 'ballon' ? values.contenance : values.type} error={type === 'ballon' ? errors.contenance : errors.type} title={type === 'ballon' ? 'Contenance' : 'Type'} type={type === 'ballon' ? 'number' : 'text'} name={type === 'ballon' ? 'contenance' : 'type'} size={100}/>
                    </Row>)}
                    { type === 'PACRR' && (
                        <Row>
                            <Select size={100} label={'Type'} title={{content:'Choisissez votre type'}} list={typePACRR} resetThenSet={resetThenSet} value={values.type} error={errors?.type}/>
                        </Row>
                    )}
                </>
                ) : (
                <Row>
                    <Input onChange={handlePrice} value={values.prixHT} error={errors.prixHT} title={'Prix hors taxe'} type={'number'} name={'prixHT'} size={100}/>
                </Row>
                )}
            </div>
            <div className={css(styles.modalColumn)}>
                { mode === 'product' ? (
                <>
                    <Row>
                        <Input onChange={handleChange} value={values.tauxTva} error={errors.tauxTva} title={'Taux de tva'} type={'number'} name={'tauxTva'} size={100}/>
                    </Row>
                    <Row>
                        <Input onChange={handlePrice} value={values.prixHT} error={errors.prixHT} title={'Prix hors taxe'} type={'number'} name={'prixHT'} size={100}/>
                    </Row>
                    <Row>
                        <Input onChange={handlePrice} value={values.prixTTC} error={errors.prixTTC} title={'Prix TTC'} type={'number'} name={'prixTTC'} size={100}/>
                    </Row>
                </> 
                ) : (
                    <Row>
                        <Input onChange={handlePrice} value={values.prixTTC} error={errors.prixTTC} title={'Prix TTC'} type={'number'} name={'prixTTC'} size={100}/>
                    </Row>
                )}
            </div>
        </div>
        { mode === 'product' && (<Row style={styles.rowDesc}>
                <TextArea onChange={handleChange} value={values.description} error={errors.description} title={'Description'} type={'text'} name={'description'} size={100}/>
            </Row>)}
        <div className={css(styles.buttonContainer)}>
            <button
            type="button"
            onClick={toggle}
            className={css(styles.prevButton)}
            >
            Annuler
            </button>
            <button
            type="button"
            onClick={handleSubmit}
            className={css(styles.nextButton)}
            >
            Sauvegarder
            </button>
        </div>
        </>
    )

}

const NeedAnalysis = forwardRef((props,ref) => {

    const md = useMobileDetect('tablet')

    const saveData = ({values,error})=>{
        console.log(values)
        Session.set('waitFetch',true)
        const valuesToUplaod = values
        if(valuesToUplaod?.batteriePanels === "Sans Batterie"){
            valuesToUplaod.batteriePanels = ''
        }
        if(valuesToUplaod.photovoltaicPanels == true){
            if(!(valuesToUplaod.panelsNumber > 0 && valuesToUplaod.panelsPrice > 0 && !!valuesToUplaod.panelsOrientation && !!valuesToUplaod.panelsInclinaison)){
                valuesToUplaod.photovoltaicPanels = false
                valuesToUplaod.panelsNumber = 0
                valuesToUplaod.panelsPrice = 0
                valuesToUplaod.panelsOrientation = ''
                valuesToUplaod.panelsInclinaison = ''
                valuesToUplaod.batteriePanels = ''
            }
        }
        Session.setPersistent('needData', valuesToUplaod)
        const clientId = Session.get('clientId')
        Meteor.call('saveNeed',valuesToUplaod,clientId,(err,res)=>{
            if(!!res){ 
                Session.setPersistent('needId',res)
                Meteor.call('computeEco', clientId,(err,res)=>{
                    console.log(res)
                    if(res){
                        Session.setPersistent('simulationId',res.id)
                        Session.setPersistent('simulationData',res.simulationData)
                        Session.set('waitFetch', false)
                    }
                })
            }
        })
    }

    const radioArrayGenerate = (radioNumber)=>{
        var result = []
        var i
        for (i = 0;i < radioNumber;i++){
            toInsert = {
                id:i,
                selected:false,
                validated2:false
            }
            result.push(toInsert)
        }
        return result
    }

    const addQuantity = (productArray) =>{
        return productArray.map((product)=>{
            product.quantity = 1
            return product
        })
    }

    const [pompeAirEauData, setPompeAirEauData ] = useState(addQuantity(pompeAirEau))
    const [pompeAirAirData, setPompeAirAirData ] = useState([])
    const [ballonsThermoData, setBallonsThermoData ] = useState(addQuantity(ballonsThermodynamiques))
    const [accessoiresData, setAccessoiresData ] = useState([])

    useEffect(()=>{
        if(!!Session.get('needData')){
            let pompeAirEauToAdd = []
            let pompeAirEauToFilter = []
            let pompeAirAirToAdd = []
            let pompeAirAirToFilter = []
            let ballonsThermoToAdd = []
            let ballonsThermoToFilter = []
            let accessoriesToAdd = []
            const { airToAirHeatPump, airWaterHeatPump, thermodynamicHeater, accessories } = Session.get('needData')

            airToAirHeatPump.forEach((product)=>{
                if(!pompeAirAirData.some((elem)=>elem.ref === product.ref)){
                    pompeAirAirToAdd.push(product)
                }else{
                    pompeAirAirToFilter.push(product)
                }
            })
            airWaterHeatPump.forEach((product)=>{
                if(!pompeAirEauData.some((elem)=>elem.ref === product.ref)){
                    pompeAirEauToAdd.push(product)
                }else{
                    pompeAirEauToFilter.push(product)
                }
            })
            thermodynamicHeater.forEach((product)=>{
                if(!ballonsThermoData.some((elem)=>elem.ref === product.ref)){
                    ballonsThermoToAdd.push(product)
                }else{
                    ballonsThermoToFilter.push(product)
                }
            })
            accessories.forEach((product)=>{
                if(!accessoiresData.some((elem)=>elem.ref === product.ref)){
                    accessoriesToAdd.push(product)
                }
            })

            const filterDefaultData = (arrayValues, toFilter)=>{
                return arrayValues.map((item)=>{
                    if(toFilter.some((elem)=>elem.ref === item.ref)){
                        let result
                        toFilter.forEach((elem)=>{
                            if(elem.ref === item.ref){
                                console.log(elem)
                                result = elem
                            }
                        })
                        console.log(result)
                        return result
                    }else{
                        return item
                    }
                })
            }            
            setPompeAirEauData([...pompeAirEauToAdd, ...filterDefaultData(pompeAirEauData,pompeAirEauToFilter)])
            setPompeAirAirData([...pompeAirAirToAdd, ...filterDefaultData(pompeAirAirData,pompeAirAirToFilter)])
            setBallonsThermoData([...ballonsThermoToAdd, ...filterDefaultData(ballonsThermoData,ballonsThermoToFilter)])
            setAccessoiresData([...accessoriesToAdd, ...accessoiresData])
        }
    },[])



    const [current, setCurrent] = useState()

    const radioArrayState = radioArrayGenerate(9)

    const [radios,setRadios] = useState([])

    const [validated2,setValidated2] = useState([])

    const [ isShowing, toggle ] = useModal()
    const [ modalType, setModalType ] = useState('PACRR')
    const [ modalMode, setModalMode ] = useState('product')
    const [ productModified, setProductModified] = useState({})
    const [ bilan, setBilan ] = useState(Session.get('simulationData').bilan)

    const addToggle = (type, mode)=>{
        setModalMode(mode)
        setModalType(type)
        toggle()
    }

    const selectStateProduct ={
        PACRO:{
            state:pompeAirEauData,
            setter:setPompeAirEauData,
            keyName:'airWaterHeatPump'
        },
        PACRR:{
            state:pompeAirAirData,
            setter:setPompeAirAirData,
            keyName:'airToAirHeatPump'
        },
        ballon:{
            state:ballonsThermoData,
            setter:setBallonsThermoData,
            keyName:'thermodynamicHeater'
        }, 
        ACCESS:{
            state:accessoiresData,
            setter:setAccessoiresData,
            keyName:'accessories'
        }
    }

    const updateRadio = (radioId, radioArray) =>{

        const newRadios = [...radioArray]
        const validated2Radio = [...validated2]
        let radioCounter = 0

        while(radioCounter < newRadios.length){
            if(validated2Radio.includes(radioCounter)){
                newRadios[radioCounter] = {
                    ...newRadios[radioCounter],
                    validated2:true, 
                }
            }
            if(radioCounter === radioId){
                newRadios[radioCounter] = {
                    ...newRadios[radioCounter],
                    selected:true,
                }
                radioCounter++
            } else if (radioCounter < radioId){
                newRadios[radioCounter] = {
                    ...newRadios[radioCounter],
                    selected:false,
                }
                radioCounter++
            } else {
                newRadios[radioCounter] = {
                    ...newRadios[radioCounter],
                    selected:false, 
                }
                radioCounter++
            }
        }
        return newRadios
    }

    const addValidated2 = (radioId) => {
        newRadios = [...radios]
        newRadios[radioId].validated2 = true
        setRadios(newRadios)
        newValidated2 = [...validated2]
        !newValidated2.includes(radioId) && newValidated2.push(radioId)
        setValidated2(newValidated2)
    }

    const removeValidated2 = (radioId) => {
        newRadios = [...radios]
        newRadios[radioId].validated2 = false
        newValidated2 = validated2.filter((elem)=> elem !== radioId)
        setRadios(newRadios)
        setValidated2(newValidated2)
    }

    const currentRadios = updateRadio(current, radioArrayState)
    const slider = useRef();

    const radioKeyValue = [['floorRoofSpace','crawlingRoofSpace'],'insideWallMeasure', 'outsideWallMeasure', 'lowFloorMeasure', 'photovoltaicPanels', 'airToAirHeatPump', 'airWaterHeatPump', 'thermodynamicHeater', 'accessories']

    useEffect(()=>{
        if(!!Session.get('needData')){
            const needData = Session.get('needData')
            const toValidate = []
            radioKeyValue.forEach((key,index)=>{
                if(index == 0){
                    let toPush = false
                    key.forEach((k,i)=>{
                        if(!!needData[k]){
                            toPush = true
                        }
                    })
                    if(toPush){
                        toValidate.push(0)
                    }   
                } else if (index == 4){
                    let toPush = false
                    if(needData.photovoltaicPanels && !!needData.panelsNumber && !!needData.panelsPrice){
                        toPush = true
                    }
                    if(toPush){
                        toValidate.push(4)
                    }   
                } else {
                    if(!!needData[key]){
                        switch(typeof(needData[key])){
                            case 'number':
                                toValidate.push(index)
                                break
                            case 'object':
                                console.log('array')
                                if(needData[key].length > 0){
                                    toValidate.push(index)
                                }
                                break
                            case 'boolean':
                                console.log('bool')
                                toValidate.push(index)
                                break
                        }
                    }
                }
            })
            setValidated2(toValidate) 
            setCurrent(0)
        }
    },[])

    useEffect(()=>{
        setRadios(currentRadios)
        if(typeof current !== 'undefined'){
            if(current === 5){
                setModalType('PACRR')
            }
            if(current === 6){
                setModalType('PACRO')
            }
            if(current === 7){
                setModalType('ballon')
            }
            if(current === 8){
                setModalType('ACCESS')
            }
            !md && slider.current.goTo(current+1)
        }
    },[current])

    const initialValues = !!Session.get('needData') ? Session.get('needData') : {
        floorRoofSpace:'',
        floorRoofPrice:'',
        crawlingRoofSpace:'',
        crawlingRoofPrice:'',
        insideWallMeasure:'',
        insideWallPrice:'',
        outsideWallMeasure:'',
        outsideWallPrice:'',
        lowFloorMeasure:'',
        lowFloorPrice:'',
        photovoltaicPanels:false,
        panelsNumber:'',
        panelsPrice:'',
        panelsOrientation:'',
        panelsInclinaison:'',
        batteriePanels:'',
        airToAirHeatPump:[],
        airWaterHeatPump:[],
        thermodynamicHeater:[],
        accessories:[],
        insideWallPrice:0,
        outsideWallPrice:0,
        lowFloorPrice:0
    }



    const requiredValues = {
        floorRoofSpace:true,
        floorRoofPrice:false,
        crawlingRoofSpace:true,
        crawlingRoofPrice:false,
        insideWallMeasure:true,
        insideWallPrice:false,
        outsideWallMeasure:true,
        outsideWallPrice:false,
        lowFloorMeasure:true,
        lowFloorPrice:false,
        photovoltaicPanels:true,
        panelsNumber:false,
        panelsPrice:false,
        panelsOrientation:false,
        panelsInclinaison:false,
        batteriePanels:false,
        airToAirHeatPump:true,
        airWaterHeatPump:true,
        thermodynamicHeater:true,
        roofPrice:false,
        insideWallPrice:false,
        outsideWallPrice:false,
        lowFloorPrice:false,
        accessories:false,
        $or:[
            ['floorRoofSpace', 'crawlingRoofSpace', 'insideWallMeasure', 'outsideWallMeasure', 'lowFloorMeasure', 'photovoltaicPanels', 'airToAirHeatPump', 'thermodynamicHeater']
        ]
    }

    const {
        values,
        errors,
        touched,
        validated,
        handleChange,
        handleSelect,
        handleMultiple,
        handleBlur,
        handleSubmit
      } = useCustomForm({
        initialValues,
        onSubmit: saveData,
        requiredValues
      });


      useImperativeHandle(ref, () => ({
        submit(){
            return handleSubmit()
        },
        isValid(){
            return validated
        }
    }))

    const handleSelectArray = (product, keyName, selectedIndex) =>{
        var newValues = values[keyName]
        newValues.some(elem => elem.ref == product.ref) ? newValues = newValues.filter((elem) => elem.ref !== product.ref) : newValues.push(product)
        handleSelect(keyName, newValues)
        newValues.length > 0 ? addValidated2(selectedIndex) : removeValidated2(selectedIndex)
    }


    const ProductCase = ({product, selected, onClick , custom,  productType}) => {
        const {ref, modèle, puissance, type, categorie, quantity} = product

        const handleDelProduct = (e)=>{
            e.cancelBubble = true,
            e.stopPropagation()
            const productList = selectStateProduct[modalType].state
            const productSetter = selectStateProduct[modalType].setter
            const newProductList = productList.filter((elem)=>elem.ref !== ref)
            productSetter(newProductList)
            onClick()
        }

        const handlePriceProduct = (e)=>{
            e.cancelBubble = true,
            e.stopPropagation()
            setModalMode('price')
            setProductModified(product)
            addToggle(productType ,'price')
        }

        const handleQuantityProduct = (e, operation)=>{
            e.cancelBubble = true,
            e.stopPropagation()
            let newProduct = {}
            const methodList={
                '-':(productElem)=>{
                    newProduct = { ...productElem}
                    newProduct.quantity = newProduct.quantity - 1
                    return newProduct  
                },
                '+':(productElem)=>{
                    newProduct = { ...productElem}
                    newProduct.quantity = newProduct.quantity + 1
                    return newProduct
                }
            }
            const productList = selectStateProduct[modalType].state
            const productSetter = selectStateProduct[modalType].setter
            const newProductList = productList.map((productElem)=>{
                if(productElem.ref === product.ref){
                    const result = methodList[operation](productElem)
                    if(result.quantity > 0){
                        return result
                    } else {
                        return productElem
                    }
                } else {
                    return productElem
                }
            })
            console.log(newProductList, newProduct)
            productSetter(newProductList)
            syncValselectedlist(newProduct, 'price')
        }

        return ( 
            <>
                <div onClick={onClick} className={css(styles.redBoxContainer, selected && styles.redBoxContainerSelected)}>
                    {custom && <div id='del' onClick={handleDelProduct} className={css(styles.deleteBoxContainer)}>
                        <div className={css(styles.deleteBox)}>x</div>
                    </div>}
                    {selected && (<>
                    <div id='price' onClick={handlePriceProduct} className={css(custom ? styles.priceBoxContainerCustom : styles.priceBoxContainer)}>
                        <div className={css(styles.priceBox)}>€</div>
                    </div>
                    <div onClick={(e)=>handleQuantityProduct(e,'+')} className={css(custom ? styles.addBoxContainerCustom : styles.addBoxContainer)}>
                        <div className={css(styles.priceBox)}>+</div>
                    </div>
                    <div onClick={(e)=>handleQuantityProduct(e,'-')} className={css(custom ? styles.delBoxContainerCustom : styles.delBoxContainer)}>
                        <div className={css(styles.priceBox)}>-</div>
                    </div>
                    </>)}
                    <div className={css(styles.redBoxItem)}>
                        <img src='src/assets/price-tags.svg' className={css(styles.redBoxIcon)}/>
                        <div className={css(styles.redBoxItemContent)}>
                            <div className={css(styles.redBoxItemTitle)}>Modèle :</div>
                            <div className={css(styles.redBoxItemText)}>{modèle}</div>
                        </div>
                    </div>
                    {productType === 'ACCESS' ? 
                    <div className={css(styles.redBoxItem)}>
                        <img src='src/assets/flash-red.svg' className={css(styles.redBoxIcon)}/>
                        <div className={css(styles.redBoxItemContent)}>
                            <div className={css(styles.redBoxItemTitle)}>Catégorie</div>
                            <div className={css(styles.redBoxItemText)}>{categorie}</div>
                        </div>
                    </div>
                    :
                    <div className={css(styles.redBoxItem)}>
                        <img src='src/assets/flash-red.svg' className={css(styles.redBoxIcon)}/>
                        <div className={css(styles.redBoxItemContent)}>
                            <div className={css(styles.redBoxItemTitle)}>Puissance nominale :</div>
                            <div className={css(styles.redBoxItemText)}>{puissance} kw</div>
                        </div>
                    </div>}
                    {type && (<div className={css(styles.redBoxItem)}>
                        <img src='src/assets/electric-meter.svg' className={css(styles.redBoxIcon)}/>
                        <div className={css(styles.redBoxItemContent)}>
                            <div className={css(styles.redBoxItemTitle)}>Type :</div>
                            <div className={css(styles.redBoxItemText)}>{type}</div>
                        </div>
                    </div>)}
                    {selected && (<div className={css(styles.redBoxItem)}>
                        <img src='src/assets/electric-meter.svg' className={css(styles.redBoxIcon)}/>
                        <div className={css(styles.redBoxItemContent)}>
                            <div className={css(styles.redBoxItemTitle)}>Quantité :</div>
                            <div className={css(styles.redBoxItemText)}>{quantity}</div>
                        </div>
                    </div>)}
                </div>
            </>
        )
    }

    const AddCase = ({onClick}) => {
        return ( 
            <>
                <div onClick={onClick} className={css(styles.blueBoxAddContainer)}>
                    <div className={css(styles.redBoxAdd)}>+</div>
                </div>
            </>
        )
    }


    const resetThenSet = (id, key) => {
        const temp = [...selectState[key].state];
        temp.forEach((item) => item.selected = false);
        temp[id].selected = true;
        handleSelect(key, temp[id].content)
        selectState[key].setter(temp)
    }

    const newBatteriePhoto = batteriePhotovolataique.map((elem,index)=>{
        newElem = {}
        newElem.content = elem.type
        newElem.selected=!!Session.get('needData')?.batteriePanels ? Session.get('needData').batteriePanels === elem.type : false
        newElem.key = 'batteriePanels'
        newElem.id = index
        return newElem
    })

    const orientationContent = ['ouest', 'sud-ouest', 'sud', 'sud-est', 'est']
    const inclinaisonContent = [ '0°', '30°', '45°', '60°', '90°']

    const newOrientationContent = orientationContent.map((elem,index)=>{
        newElem = {}
        newElem.content = elem
        newElem.selected=!!Session.get('needData')?.panelsOrientation ? Session.get('needData').panelsOrientation === elem : false
        newElem.key = 'panelsOrientation'
        newElem.id = index
        return newElem
    })
    const newInclinaisonContent = inclinaisonContent.map((elem,index)=>{
        newElem = {}
        newElem.content = elem
        newElem.selected=!!Session.get('needData')?.panelsInclinaison ? Session.get('needData').panelsInclinaison === elem : false
        newElem.key = 'panelsInclinaison'
        newElem.id = index
        return newElem
    })

    const [batteriePhoto, setBatteriePhoto]  = useState(newBatteriePhoto)
    const [orientation, setOrienation]  = useState(newOrientationContent)
    const [inclinaison, setInclinaison]  = useState(newInclinaisonContent)


    const selectState ={
        batteriePanels:{
            state:batteriePhoto,
            setter:setBatteriePhoto
        },
        panelsOrientation:{
            state:orientation,
            setter:setOrienation
        },
        panelsInclinaison:{
            state:inclinaison,
            setter:setInclinaison
        },
    }

    const selectPanels = () => {
        var active = !values.photovoltaicPanels
        if(active == false){
            handleMultiple({
                photovoltaicPanels:active,
                panelsNumber:'',
                panelsPrice:'',
                panelsOrientation:'',
                panelsInclinaison:'',
                batteriePanels:''
            })
            const newBatteriePhoto = batteriePhotovolataique.map((elem,index)=>{
                newElem = {}
                newElem.content = elem.type
                newElem.selected=false
                newElem.key = 'batteriePanels'
                newElem.id = index
                return newElem
            })
            const newOrientationContent = orientationContent.map((elem,index)=>{
                newElem = {}
                newElem.content = elem
                newElem.selected=false
                newElem.key = 'panelsOrientation'
                newElem.id = index
                return newElem
            })
            const newInclinaisonContent = inclinaisonContent.map((elem,index)=>{
                newElem = {}
                newElem.content = elem
                newElem.selected=false
                newElem.key = 'panelsInclinaison'
                newElem.id = index
                return newElem
            })
            setBatteriePhoto(newBatteriePhoto)
            setOrienation(newOrientationContent)
            setInclinaison(newInclinaisonContent)
        }else{
            handleSelect('photovoltaicPanels',active)
        }
        removeValidated2(4)
    }

    const testListValidated2 = (()=>{
        const test4 = ()=>{
            if(!!values.panelsPrice && !!values.panelsNumber){
                addValidated2(4)
            } else {
                removeValidated2(4)
            }
        }
        const test0 = () => {
            if((!!values.floorRoofSpace && !!values.floorRoofPrice) || (!!values.crawlingRoofSpace && !!values.crawlingRoofPrice)){
                addValidated2(0)
            }else{
                removeValidated2(0)
            }
        }
        const test1 = ()=> {
            if(!!values.insideWallMeasure && !!values.insideWallPrice){
                addValidated2(1)
            }else{
                removeValidated2(1)
            }
        }
        const test2 = ()=>{
            if(!!values.outsideWallMeasure && !!values.outsideWallPrice){
                addValidated2(2)
            }else{
                removeValidated2(2)
            }
        }
        const test3 = ()=>{
            if(!!values.lowFloorMeasure && !!values.lowFloorPrice){
                addValidated2(3)
            }else{
                removeValidated2(3)
            }
        }
        const listTest = [test0, test1, test2, test3, test4]
        return listTest
    })()

    useEffect(()=>{
        if(radios.length > 0 && current < 5){
            testListValidated2[current]()
        }
    },[values])

    const syncValselectedlist = (product, mode) => {
        const keyName = selectStateProduct[modalType].keyName
        let newProductList
        if(mode === 'price'){
            newProductList = values[keyName].map((productItem)=>{
                if(productItem.ref === product.ref){
                    return product
                } else {
                    return productItem
                }
            })
        } 
        if (mode === 'product'){
            newProductList = [product, ...values[keyName]]
        }
        handleSelect(keyName, newProductList)
    }

    Session.set('background','Image4')

    if(!!md){ 
        return (
            <div className={css(styles.container)}>
                <div className={css(styles.column)}>
                    <Row>
                        <Case icon={'invoice'} content={`Bilan thermique : ${bilan}${bilan === 'Pas disponible' ? '' : ' Kwh'}` } size={100} iconSize={'normal'}/>
                    </Row>
                    <Row>
                        <InputRadioSelectMobile size={100} error={errors?.roofSpace} icon={'foundation'} setCurrent={setCurrent} title={'Combles'} id={0} selected={radios[0]?.selected} validated={radios[0]?.validated2}>
                            <div className={css(styles.carouselItemContainerIso)}>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Surface des combles<br/>Au sol | Rempants
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/foundation.svg' />
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.floorRoofSpace} type={'number'} name={'floorRoofSpace'} placeholder={'0'} after={{contentType:'text',content:'m2',position:'left'}} size={50} contentSize={50}/>
                                        <Input onChange={handleChange} value={values.crawlingRoofSpace} type={'number'} name={'crawlingRoofSpace'} placeholder={'0'} after={{contentType:'text',content:'m2',position:'left'}} size={50} contentSize={50}/>
                                    </div>
                                </div>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Résultat (en €)<br/>Au sol | Rempants
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/foundation.svg'/>
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.floorRoofPrice} type={'number'} name={'floorRoofPrice'} placeholder={'0'} size={50} contentSize={50} after={{contentType:'text',content:'€',position:'left'}}/> 
                                        <Input onChange={handleChange} value={values.crawlingRoofPrice} type={'number'} name={'crawlingRoofPrice'} size={50} placeholder={'0'} contentSize={50} after={{contentType:'text',content:'€',position:'left'}}/>   
                                    </div>
                                </div>
                            </div>
                        </InputRadioSelectMobile>
                    </Row>
                    <Row>
                        <InputRadioSelectMobile size={100} error={errors?.insideWallMeasure} icon={'brickwall'} setCurrent={setCurrent} title={'Mur intérieur'} id={1} selected={radios[1]?.selected} validated={radios[1]?.validated2}>
                            <div className={css(styles.carouselItemContainerIso)}>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Mesure mur intérieur
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/brickwall.svg' />
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.insideWallMeasure} type={'number'} name={'insideWallMeasure'} placeholder={'0'} after={{contentType:'text',content:'m2',position:'left'}} size={75} contentSize={50}/>
                                    </div>
                                </div>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Résultat (en €)
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/brickwall.svg'/>
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.insideWallPrice} type={'number'} name={'insideWallPrice'} placeholder={'0'} size={75} contentSize={50} after={{contentType:'text',content:'€',position:'left'}}/>  
                                    </div>
                                </div>
                            </div>
                        </InputRadioSelectMobile>
                    </Row>
                    <Row>
                        <InputRadioSelectMobile size={100} error={errors?.outsideWallMeasure} icon={'brickwall'} setCurrent={setCurrent} title={'Mur extérieur'} id={2} selected={radios[2]?.selected} validated={radios[2]?.validated2}>
                            <div className={css(styles.carouselItemContainerIso)}>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Mesure mur extérieur
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/brickwall.svg' />
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.outsideWallMeasure} type={'number'} name={'outsideWallMeasure'} placeholder={'0'} after={{contentType:'text',content:'m2',position:'left'}} size={75} contentSize={50}/>
                                    </div>
                                </div>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Résultat (en €)
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/brickwall.svg'/>
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.outsideWallPrice} type={'number'} name={'outsideWallPrice'} placeholder={'0'} size={75} contentSize={50} after={{contentType:'text',content:'€',position:'left'}}/>  
                                    </div>
                                </div>
                            </div>
                        </InputRadioSelectMobile>
                    </Row>
                    <Row>
                        <InputRadioSelectMobile size={100} error={errors?.lowFloorMeasure} icon={'floor'} setCurrent={setCurrent} title={'Plancher bas'} id={3} selected={radios[3]?.selected} validated={radios[3]?.validated2}>
                            <div className={css(styles.carouselItemContainerIso)}>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Mesure plancher bas
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/floor.svg' />
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.lowFloorMeasure} type={'number'} name={'lowFloorMeasure'} placeholder={'0'} after={{contentType:'text',content:'m2',position:'left'}} size={75} contentSize={50}/>
                                    </div>
                                </div>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Résultat (en €)
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/floor.svg'/>
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.lowFloorPrice} type={'number'} name={'lowFloorPrice'} placeholder={'0'} size={75} contentSize={50} after={{contentType:'text',content:'€',position:'left'}}/>  
                                    </div>
                                </div>
                            </div>
                        </InputRadioSelectMobile>
                    </Row>
                    <Row>
                        <InputRadioSelectMobile size={100} error={errors?.photovoltaicPanels} icon={'solar-panel'} setCurrent={setCurrent} title={'Panneaux photovoltaïques'} id={4} selected={radios[4]?.selected} validated={radios[4]?.validated2}>
                            <div className={css(styles.carouselItemPanelContainer)}>
                                <div className={css(styles.specificTitleContainer)}>
                                    <Title text={'Sélectionner un nombre de panneaux :'}/>
                                </div>
                                <div className={css(styles.panelsContainer, values.photovoltaicPanels && styles.fullPanelContainer)}>
                                    <div className={css(styles.redBoxContainerSpecific, values.photovoltaicPanels && styles.redBoxContainerSelected)} onClick={selectPanels} >
                                        <div className={css(styles.redBoxItem)}>
                                            <img src='src/assets/price-tags.svg' className={css(styles.redBoxIcon)}/>
                                            <div className={css(styles.redBoxItemContent)}>
                                                <div className={css(styles.redBoxItemTitle)}>Modèle :</div>
                                                <div className={css(styles.redBoxItemText)}>{paneauxPhotovoltaiques.modele}</div>
                                            </div>
                                        </div>
                                        <div className={css(styles.redBoxItem)}>
                                            <img src='src/assets/flash-red.svg' className={css(styles.redBoxIcon)}/>
                                            <div className={css(styles.redBoxItemContent)}>
                                                <div className={css(styles.redBoxItemTitle)}>Puissance nominale :</div>
                                                <div className={css(styles.redBoxItemText)}>{paneauxPhotovoltaiques.puissance} w</div>
                                            </div>
                                        </div>
                                        <div className={css(styles.redBoxItem)}>
                                            <img src='src/assets/electric-meter.svg' className={css(styles.redBoxIcon)}/>
                                            <div className={css(styles.redBoxItemContent)}>
                                                <div className={css(styles.redBoxItemTitle)}>Nombre :</div>
                                                <div className={css(styles.redBoxItemText)}>{values.panelsNumber}</div>
                                            </div>
                                        </div>
                                    </div>
                                    {values.photovoltaicPanels && (<>
                                        <Input onChange={handleChange} value={values.panelsNumber} title={'Nombre de panneaux'} type={'number'} name={'panelsNumber'} placeholder={'9'} size={100}/>
                                        <Input onChange={handleChange} value={values.panelsPrice} title={'Prix total'} type={'number'} name={'panelsPrice'} placeholder={'3500'} size={100} after={{contentType:'text',content:'€',position:'left'}}/>
                                        <Select size={100} label={'Option batterie'} title={{content:'Choisissez votre option'}} list={batteriePhoto} resetThenSet={resetThenSet} value={values.batteriePanels} error={errors?.batteriePanels}/>
                                        <Select size={100} label={'Orientation'} title={{content:`Choisissez l'orientation`}} list={orientation} resetThenSet={resetThenSet} value={values.panelsOrientation} error={errors?.panelsOrientation}/>
                                        <Select size={100} label={'Inclinaison'} title={{content:`Choisissez l'inclinaison`}} list={inclinaison} resetThenSet={resetThenSet} value={values.panelsInclinaison} error={errors?.panelsInclinaison}/>
                                    </>)}
                                </div>
                            </div>
                        </InputRadioSelectMobile>
                    </Row>
                    <Row>
                        <InputRadioSelectMobile size={100} error={errors?.airToAirHeatPump} icon={'windy'} setCurrent={setCurrent} title={'Pompe à chaleur Air Air'} id={5} selected={radios[5]?.selected} validated={radios[5]?.validated2}>
                            <div className={css(styles.carouselItemContainer)}>
                                <div className={css(styles.titleContainer)}>
                                    <Title text={'Sélectionner un modèle :'}/>
                                </div>
                                <div className={css(styles.redBoxWrapper)}>
                                    <AddCase onClick={()=>addToggle('PACRR','product')}/>
                                    {pompeAirAirData.map((pompe,index)=>{
                                        return <ProductCase custom={true}  productType={'PACRR'}  key={index} product={pompe} selected={values.airToAirHeatPump.some(elem => elem.ref == pompe.ref)} onClick={()=>handleSelectArray(pompe, 'airToAirHeatPump',5)}/>
                                    })}
                                </div>
                            </div>        
                        </InputRadioSelectMobile>
                    </Row>
                    <Row>
                        <InputRadioSelectMobile size={100} error={errors?.airWaterHeatPump} icon={'water'} setCurrent={setCurrent} title={'Pompe à chaleur Air Eau'} id={6} selected={radios[6]?.selected} validated={radios[6]?.validated2}>
                            <div className={css(styles.carouselItemContainer)}>
                                <div className={css(styles.titleContainer)}>
                                    <Title text={'Sélectionner un modèle :'}/>
                                </div>
                                <div className={css(styles.redBoxWrapper)}>
                                    <AddCase onClick={()=>addToggle('PACRO','product')}/>
                                    {pompeAirEauData.map((pompe,index)=>{
                                        return <ProductCase custom={!pompeAirEau.some((elem)=>elem.ref === pompe.ref)} productType={'PACRO'} key={index} product={pompe} selected={values.airWaterHeatPump.some(elem => elem.ref == pompe.ref)} onClick={()=>handleSelectArray(pompe, 'airWaterHeatPump',6)}/>
                                    })}
                                </div>
                            </div>
                        </InputRadioSelectMobile>
                    </Row>
                    <Row>
                        <InputRadioSelectMobile size={100} error={errors?.thermodynamicHeater} icon={'baloon'} setCurrent={setCurrent} title={'Ballon Thermodynamique'} id={7} selected={radios[7]?.selected} validated={radios[7]?.validated2}>
                            <div className={css(styles.carouselItemContainer)}>
                                <div className={css(styles.titleContainer)}>
                                    <Title text={'Sélectionner un modèle :'}/>
                                </div>
                                <div className={css(styles.redBoxWrapper)}>
                                    <AddCase onClick={()=>addToggle('ballon','product')}/>
                                    {ballonsThermoData.map((ballon,index)=>{
                                        return <ProductCase custom={!ballonsThermodynamiques.some((elem)=>elem.ref === ballon.ref)} productType={'ballon'} key={index} product={ballon} selected={values.thermodynamicHeater.some(elem => elem.ref == ballon.ref)} onClick={()=>handleSelectArray(ballon, 'thermodynamicHeater',7)}/>
                                    })}
                                </div>
                            </div>
                        </InputRadioSelectMobile>
                    </Row>
                    <Row>
                        <InputRadioSelectMobile size={100} error={errors?.accessories} icon={'accessories'} setCurrent={setCurrent} title={'Accessoires'} id={8} selected={radios[8]?.selected} validated={radios[8]?.validated2}>
                            <div className={css(styles.carouselItemContainer)}>
                                <div className={css(styles.titleContainer)}>
                                    <Title text={'Ajouter des accessoires :'}/>
                                </div>
                                <div className={css(styles.redBoxWrapper)}>
                                    <AddCase onClick={()=>addToggle('ACCESS','product')}/>
                                    {accessoiresData.map((product,index)=>{
                                        return <ProductCase custom={true} productType={'ACCESS'} key={index} product={product} selected={values.accessories.some(elem => elem.ref == product.ref)} onClick={()=>handleSelectArray(product, 'accessories',8)}/>
                                    })}
                                </div>
                            </div>
                        </InputRadioSelectMobile>
                    </Row>
                </div>
                <Modal isShowing={isShowing} hide={toggle} title={modalMode === 'product' ? "Ajout d'un produit" : 'Changement du prix'} logo={false}>
                    <ModalContent type={modalType} mode={modalMode} product={productModified} toggle={toggle} syncMethod={syncValselectedlist} productList={selectStateProduct[modalType].state} productSetter={selectStateProduct[modalType].setter} />
                </Modal>
            </div>
        )
    } else {
        return (
            <div className={css(styles.container)}>
                <div className={css(styles.column)}>
                    <Row>
                        <Case icon={'invoice'} content={`Bilan thermique : ${bilan}${bilan === 'Pas disponible' ? '' : ' Kwh'}` } size={100} iconSize={'normal'}/>
                    </Row>
                    <Row>
                        <InputRadioSelect size={100} error={errors?.roofSpace} icon={'foundation'} setCurrent={setCurrent} title={'Combles'} id={0} selected={radios[0]?.selected} validated={radios[0]?.validated2}/>
                    </Row>
                    <Row>
                        <InputRadioSelect size={50} error={errors?.insideWallMeasure} icon={'brickwall'} setCurrent={setCurrent} title={'Mur intérieur'} id={1} selected={radios[1]?.selected} validated={radios[1]?.validated2}/>
                        <InputRadioSelect size={50} error={errors?.outsideWallMeasure} icon={'brickwall'} setCurrent={setCurrent} title={'Mur extérieur'} id={2} selected={radios[2]?.selected} validated={radios[2]?.validated2}/>
                    </Row>
                    <Row>
                        <InputRadioSelect size={100} error={errors?.lowFloorMeasure} icon={'floor'} setCurrent={setCurrent} title={'Plancher bas'} id={3} selected={radios[3]?.selected} validated={radios[3]?.validated2}/>
                    </Row>
                    <Row>
                        <InputRadioSelect size={100} error={errors?.photovoltaicPanels} icon={'solar-panel'} setCurrent={setCurrent} title={'Panneaux photovoltaïques'} id={4} selected={radios[4]?.selected} validated={radios[4]?.validated2}/>
                    </Row>
                    <Row>
                        <InputRadioSelect size={100} error={errors?.airToAirHeatPump} icon={'windy'} setCurrent={setCurrent} title={'Pompe à chaleur Air Air'} id={5} selected={radios[5]?.selected} validated={radios[5]?.validated2}/>
                    </Row>
                    <Row>
                        <InputRadioSelect size={100} error={errors?.airWaterHeatPump} icon={'water'} setCurrent={setCurrent} title={'Pompe à chaleur Air Eau'} id={6} selected={radios[6]?.selected} validated={radios[6]?.validated2}/>
                    </Row>
                    <Row>
                        <InputRadioSelect size={100} error={errors?.thermodynamicHeater} icon={'baloon'} setCurrent={setCurrent} title={'Ballon Thermodynamique'} id={7} selected={radios[7]?.selected} validated={radios[7]?.validated2}/>
                    </Row>
                    <Row>
                        <InputRadioSelect size={100} error={errors?.accessories} icon={'accessories'} setCurrent={setCurrent} title={'Accessoires'} id={8} selected={radios[8]?.selected} validated={radios[8]?.validated2}/>
                    </Row>
                </div>
                <Column>
                    <section>
                        <Carousel
                            showArrows={false}
                            enableMouseSwipe={false}
                            enableSwipe={false}
                            renderPagination={()=>{ return (<></>)}}
                            ref={ref=>{
                                slider.current = ref
                            }}
                        >
                            <img className={css(styles.imgDefault)} src="src/img/teacher.svg" />
                            <div className={css(styles.carouselItemContainerIso)}>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Surface des combles<br/>Au sol | Rempants
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/foundation.svg' />
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.floorRoofSpace} type={'number'} name={'floorRoofSpace'} placeholder={'20'} after={{contentType:'text',content:'m2',position:'left'}} size={50} contentSize={50}/>
                                        <Input onChange={handleChange} value={values.crawlingRoofSpace} type={'number'} name={'crawlingRoofSpace'} placeholder={'20'} after={{contentType:'text',content:'m2',position:'left'}} size={50} contentSize={50}/>
                                    </div>
                                </div>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Résultat (en €)<br/>Au sol | Rempants
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/foundation.svg'/>
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.floorRoofPrice} type={'number'} name={'floorRoofPrice'} placeholder={'20'} size={50} contentSize={50} after={{contentType:'text',content:'€',position:'left'}}/> 
                                        <Input onChange={handleChange} value={values.crawlingRoofPrice} type={'number'} name={'crawlingRoofPrice'} size={50} placeholder={'1500'} contentSize={50} after={{contentType:'text',content:'€',position:'left'}}/>   
                                    </div>
                                </div>
                            </div>
                            <div className={css(styles.carouselItemContainerIso)}>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Mesure mur intérieur
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/brickwall.svg' />
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.insideWallMeasure} type={'number'} name={'insideWallMeasure'} placeholder={'20'} after={{contentType:'text',content:'m2',position:'left'}} size={75} contentSize={50}/>
                                    </div>
                                </div>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Résultat (en €)
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/brickwall.svg'/>
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.insideWallPrice} type={'number'} name={'insideWallPrice'} placeholder={'1500'} size={75} contentSize={50} after={{contentType:'text',content:'€',position:'left'}}/>  
                                    </div>
                                </div>
                            </div>
                            <div className={css(styles.carouselItemContainerIso)}>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Mesure mur extérieur
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/brickwall.svg' />
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.outsideWallMeasure} type={'number'} name={'outsideWallMeasure'} placeholder={'20'} after={{contentType:'text',content:'m2',position:'left'}} size={75} contentSize={50}/>
                                    </div>
                                </div>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Résultat (en €)
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/brickwall.svg'/>
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.outsideWallPrice} type={'number'} name={'outsideWallPrice'} placeholder={'1500'} size={75} contentSize={50} after={{contentType:'text',content:'€',position:'left'}}/>  
                                    </div>
                                </div>
                            </div>
                            <div className={css(styles.carouselItemContainerIso)}>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Mesure plancher bas
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/floor.svg' />
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.lowFloorMeasure} type={'number'} name={'lowFloorMeasure'} placeholder={'90'} after={{contentType:'text',content:'m2',position:'left'}} size={75} contentSize={50}/>
                                    </div>
                                </div>
                                <div className={css(styles.boxContainer)}>
                                    <div className={css(styles.boxTitle)}>
                                        Résultat (en €)
                                    </div>
                                    <img className={css(styles.boxIcon)} src='src/assets/floor.svg'/>
                                    <div className={css(styles.boxItemContainer)}>
                                        <Input onChange={handleChange} value={values.lowFloorPrice} type={'number'} name={'lowFloorPrice'} placeholder={'2000'} size={75} contentSize={50} after={{contentType:'text',content:'€',position:'left'}}/>  
                                    </div>
                                </div>
                            </div>
                            <div className={css(styles.carouselItemPanelContainer)}>
                                <div className={css(styles.specificTitleContainer)}>
                                    <Title text={'Sélectionner un nombre de panneaux :'}/>
                                </div>
                                <div className={css(styles.panelsContainer, values.photovoltaicPanels && styles.fullPanelContainer)}>
                                    <div className={css(styles.redBoxContainerSpecific, values.photovoltaicPanels && styles.redBoxContainerSelected)} onClick={selectPanels} >
                                        <div className={css(styles.redBoxItem)}>
                                            <img src='src/assets/price-tags.svg' className={css(styles.redBoxIcon)}/>
                                            <div className={css(styles.redBoxItemContent)}>
                                                <div className={css(styles.redBoxItemTitle)}>Modèle :</div>
                                                <div className={css(styles.redBoxItemText)}>{paneauxPhotovoltaiques.modele}</div>
                                            </div>
                                        </div>
                                        <div className={css(styles.redBoxItem)}>
                                            <img src='src/assets/flash-red.svg' className={css(styles.redBoxIcon)}/>
                                            <div className={css(styles.redBoxItemContent)}>
                                                <div className={css(styles.redBoxItemTitle)}>Puissance nominale :</div>
                                                <div className={css(styles.redBoxItemText)}>{paneauxPhotovoltaiques.puissance} w</div>
                                            </div>
                                        </div>
                                        <div className={css(styles.redBoxItem)}>
                                            <img src='src/assets/electric-meter.svg' className={css(styles.redBoxIcon)}/>
                                            <div className={css(styles.redBoxItemContent)}>
                                                <div className={css(styles.redBoxItemTitle)}>Nombre :</div>
                                                <div className={css(styles.redBoxItemText)}>{values.panelsNumber}</div>
                                            </div>
                                        </div>
                                    </div>
                                    {values.photovoltaicPanels && (<>
                                        <Input onChange={handleChange} value={values.panelsNumber} title={'Nombre de panneaux'} type={'number'} name={'panelsNumber'} placeholder={'9'} size={100}/>
                                        <Input onChange={handleChange} value={values.panelsPrice} title={'Prix total'} type={'number'} name={'panelsPrice'} placeholder={'3500'} size={100} after={{contentType:'text',content:'€',position:'left'}}/>
                                        <Select size={100} label={'Option batterie'} title={{content:'Choisissez votre option'}} list={batteriePhoto} resetThenSet={resetThenSet} value={values.batteriePanels} error={errors?.batteriePanels}/>
                                        <Select size={100} label={'Orientation'} title={{content:`Choisissez l'orientation`}} list={orientation} resetThenSet={resetThenSet} value={values.panelsOrientation} error={errors?.panelsOrientation}/>
                                        <Select size={100} label={'Inclinaison'} title={{content:`Choisissez l'inclinaison`}} list={inclinaison} resetThenSet={resetThenSet} value={values.panelsInclinaison} error={errors?.panelsInclinaison}/>
                                    </>)}
                                </div>
                            </div>
                            <div className={css(styles.carouselItemContainer)}>
                                <div className={css(styles.titleContainer)}>
                                    <Title text={'Sélectionner un modèle :'}/>
                                </div>
                                <div className={css(styles.redBoxWrapper)}>
                                    <AddCase onClick={()=>addToggle('PACRR','product')}/>
                                    {pompeAirAirData.map((pompe,index)=>{
                                        return <ProductCase custom={true}  productType={'PACRR'}  key={index} product={pompe} selected={values.airToAirHeatPump.some(elem => elem.ref == pompe.ref)} onClick={()=>handleSelectArray(pompe, 'airToAirHeatPump',5)}/>
                                    })}
                                </div>
                            </div>
                            <div className={css(styles.carouselItemContainer)}>
                                <div className={css(styles.titleContainer)}>
                                    <Title text={'Sélectionner un modèle :'}/>
                                </div>
                                <div className={css(styles.redBoxWrapper)}>
                                    <AddCase onClick={()=>addToggle('PACRO','product')}/>
                                    {pompeAirEauData.map((pompe,index)=>{
                                        return <ProductCase custom={!pompeAirEau.some((elem)=>elem.ref === pompe.ref)} productType={'PACRO'} key={index} product={pompe} selected={values.airWaterHeatPump.some(elem => elem.ref == pompe.ref)} onClick={()=>handleSelectArray(pompe, 'airWaterHeatPump',6)}/>
                                    })}
                                </div>
                            </div>
                            <div className={css(styles.carouselItemContainer)}>
                                <div className={css(styles.titleContainer)}>
                                    <Title text={'Sélectionner un modèle :'}/>
                                </div>
                                <div className={css(styles.redBoxWrapper)}>
                                    <AddCase onClick={()=>addToggle('ballon','product')}/>
                                    {ballonsThermoData.map((ballon,index)=>{
                                        return <ProductCase custom={!ballonsThermodynamiques.some((elem)=>elem.ref === ballon.ref)} productType={'ballon'} key={index} product={ballon} selected={values.thermodynamicHeater.some(elem => elem.ref == ballon.ref)} onClick={()=>handleSelectArray(ballon, 'thermodynamicHeater',7)}/>
                                    })}
                                </div>
                            </div>
                            <div className={css(styles.carouselItemContainer)}>
                                <div className={css(styles.titleContainer)}>
                                    <Title text={'Ajouter des accessoires :'}/>
                                </div>
                                <div className={css(styles.redBoxWrapper)}>
                                    <AddCase onClick={()=>addToggle('ACCESS','product')}/>
                                    {accessoiresData.map((product,index)=>{
                                        return <ProductCase custom={true} productType={'ACCESS'} key={index} product={product} selected={values.accessories.some(elem => elem.ref == product.ref)} onClick={()=>handleSelectArray(product, 'accessories',8)}/>
                                    })}
                                </div>
                            </div>
                        </Carousel>
                    </section>
                </Column>
                <Modal isShowing={isShowing} hide={toggle} title={modalMode === 'product' ? "Ajout d'un produit" : 'Changement du prix'} logo={false}>
                    <ModalContent type={modalType} mode={modalMode} product={productModified} toggle={toggle} syncMethod={syncValselectedlist} productList={selectStateProduct[modalType].state} productSetter={selectStateProduct[modalType].setter} />
                </Modal>
            </div>
        )
    }
})

const styles = StyleSheet.create({
    column:{
        display:'flex',
        flexDirection:'column',
        width:'48%',
        height:'calc(100vh - 320px)',
        overflow:'scroll',
        '@media (max-width:600px)':{
            width:'100%'
        },
        '@media (max-width:768px)':{
            width:'100%'
        },

    },
    container:{
        display:'flex',
        flexDirection:'row',
        height:'calc(100vh - 320px)',
        width:'100%',
        justifyContent:'space-between',
        '@media (max-width:600px)':{
            flexDirection:'column',
        },
    },
    imgDefault:{
        margin:'5% auto auto auto',
        width:'90%'
    },
    modalContentContainer:{
        display:'flex',
        width:'90%',
        margin:'20px auto auto auto',
        '@media (max-width:600px)':{
            flexDirection:'column',
        },
    },
    modalColumn:{
        display:'flex',
        flexDirection:'column',
        width:'50%',
        '@media (max-width:600px)':{
            width:'100%'
        },
    },
    carouselItemContainer:{
        width:'100%',
        display:'flex',
        flexDirection:'column',
        height:'calc(100vh - 320px)'
    },
    carouselItemContainerIso:{
        width:'100%',
        display:'flex',
        flexDirection:'row',
        height:'calc(100vh - 320px)',
        '@media (max-width:600px)':{
            flexDirection:'column',
            height:'500px'
        },
        '@media (max-width:1024px)':{
            flexDirection:'column',
        },
    },
    carouselItemPanelContainer:{
        width:'100%',
        display:'flex',
        flexDirection:'column',
        height:'900px' 
    },
    boxContainer:{
        display:'flex',
        flexDirection:'column',
        background: '#FBFCFE 0% 0% no-repeat padding-box',
        boxShadow: '15px 15px 20px #00000014',
        border: '1px solid #1D4F9046',
        borderRadius: '15px',
        opacity: 1,
        width:'45%',
        height:'55%',
        maxWidth:'350px',
        minWidth:'260px',
        minHeight:'190px',
        maxHeight:'210px',
        margin:'auto',
        padding:'15px',
    },
    boxTitle:{
        font: 'normal normal normal 14px Montserrat',
        margin:'auto',
        textAlign:'center'
    },
    boxIcon:{
        width:'25px',
        margin:'auto'
    },
    boxItemContainer:{
        margin:'auto',
        width:'100%',
        display:'flex',
        justifyContent:'center'
    },
    titleContainer:{
        display:'flex',
        justifyContent:'center',
        paddingLeft:'-3px',
        margin:'auto'
    },
    specificTitleContainer:{
        display:'flex',
        justifyContent:'center',
        paddingLeft:'-3px',
        margin:'15px auto'
    },
    specificSelectContainer:{
        display: 'flex',
        justifyContent: 'center',
    },
    redBoxWrapper:{
        display:'flex',
        flexDirection:'row',
        width:'100%',
        flexWrap:'wrap',
        overflowY:'scroll',
        height:'100%'
    },
    redBoxContainer:{
        display:'flex',
        flexDirection:'column',
        background: 'transparent 0% 0% no-repeat padding-box',
        border: '1px solid #007100',
        borderRadius: '15px',
        opacity: 1,
        width:'27%',
        height:'25%',
        maxWidth:'220px',
        maxHeight:'170px',
        minWidth:'140px',
        minHeight:'145px',
        margin:'15px auto',
        padding:'15px',
        boxSizing:'border-box',
        cursor:'pointer',
        position:'relative'
    },
    redBoxContainerSpecific:{
        display:'flex',
        flexDirection:'column',
        background: 'transparent 0% 0% no-repeat padding-box',
        border: '1px solid #007100',
        borderRadius: '15px',
        opacity: 1,
        width:'27%',
        height:'25%',
        maxWidth:'220px',
        maxHeight:'170px',
        minWidth:'140px',
        minHeight:'145px',
        margin:'15px auto',
        padding:'15px',
        boxSizing:'border-box',
        cursor:'pointer'
    },
    redBoxContainerSelected:{
        background: '#E1ECE0 0% 0% no-repeat padding-box',
    },
    redBoxContainerOld:{
        display:'flex',
        flexDirection:'column',
        background: '#FBFCFE 0% 0% no-repeat padding-box',
        border: '1px solid #BF1E23',
        borderRadius: '15px',
        opacity: 1,
        width:'27%',
        height:'25%',
        maxWidth:'220px',
        maxHeight:'170px',
        minWidth:'140px',
        minHeight:'145px',
        margin:'15px auto',
        padding:'15px',
        boxSizing:'border-box',
        cursor:'pointer',
        position:'relative'
    },
    redBoxContainerSpecificOld:{
        display:'flex',
        flexDirection:'column',
        background: '#FBFCFE 0% 0% no-repeat padding-box',
        border: '1px solid #BF1E23',
        borderRadius: '15px',
        opacity: 1,
        width:'27%',
        height:'25%',
        maxWidth:'220px',
        maxHeight:'170px',
        minWidth:'140px',
        minHeight:'145px',
        margin:'15px auto',
        padding:'15px',
        boxSizing:'border-box',
        cursor:'pointer'
    },
    redBoxContainerSelectedOld:{
        background: 'rgba(191,30,35,0.05) 0% 0% no-repeat padding-box',
    },
    blueBoxAddContainer:{
        display:'flex',
        flexDirection:'column',
        background: '#E1ECE0 0% 0% no-repeat padding-box',
        borderRadius: '15px',
        border: '1px solid #007100',
        opacity: 1,
        width:'27%',
        height:'25%',
        maxWidth:'220px',
        maxHeight:'170px',
        minWidth:'140px',
        minHeight:'145px',
        margin:'15px auto',
        padding:'15px',
        boxSizing:'border-box',
        cursor:'pointer'
    },
    blueBoxAddContainerOld:{
        display:'flex',
        flexDirection:'column',
        background: 'rgba(29, 79, 144,0.05) 0% 0% no-repeat padding-box',
        borderRadius: '15px',
        border: '1px solid #BF1E23',
        opacity: 1,
        width:'27%',
        height:'25%',
        maxWidth:'220px',
        maxHeight:'170px',
        minWidth:'140px',
        minHeight:'145px',
        margin:'15px auto',
        padding:'15px',
        boxSizing:'border-box',
        cursor:'pointer'
    },
    redBoxAdd:{
        color:'#007100',
        fontSize:'45px',
        margin:'auto'
    },    
    redBoxAddOld:{
        color:'rgb(191,30,35)',
        fontSize:'45px',
        margin:'auto'
    },
    redBoxItem:{
        margin:'auto auto auto 0',
        display:'flex',
        flexDirection:'row',
    },
    redBoxIcon:{
        width:'15px',
        margin:'auto 8px auto 0'
    },
    redBoxItemContent:{
        display:'flex',
        flexDirection:'column'
    },
    redBoxItemTitle:{
        font: 'normal normal normal 10px Montserrat',
        fontWeight:'600',
        color:'#1D4F90'
    },
    redBoxItemText:{
        font: 'normal normal normal 10px Montserrat',
        color:'#393939'
    },
    panelsContainer:{
        display:'flex',
        flexDirection:'row',
        flexWrap:'wrap',
    },
    fullPanelContainer:{
        // height:'800px'
    },
    buttonContainer:{
        position:'relative',
        margin:'auto',
        display:'flex',
        justifyContent:'space-evenly',
        width:'100%'
    },
    prevButton:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'white',
        borderRadius: '37px',
        width:'130px',
        height:'35px',
        cursor:'pointer',
    },
    nextButton:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'#F7E13B',
        borderRadius: '37px',
        width:'130px',
        height:'35px',
        cursor:'pointer'
    },
    deleteBoxContainer:{
        position:'absolute',
        display:'flex',
        borderRadius:'50%',
        border:'1px solid black',
        width:'21px',
        zIndex:'5',
        right:'10px',
        top:'10px'
    },
    deleteBox:{
        margin:'auto',
        color:'black'
    },
    priceBoxContainer:{
        position:'absolute',
        display:'flex',
        borderRadius:'50%',
        border:'1px solid rgb(29, 79, 144)',
        width:'21px',
        zIndex:'5',
        right:'10px',
        top:'10px'
    },
    priceBoxContainerCustom:{
        position:'absolute',
        display:'flex',
        borderRadius:'50%',
        border:'1px solid rgb(29, 79, 144)',
        width:'21px',
        zIndex:'5',
        right:'10px',
        top:'41px'
    },
    priceBox:{
        margin:'auto',
        color:'rgb(29, 79, 144)'
    },
    rowDesc:{
        width:'90%',
        margin:'auto auto 20px'
    },
    addBoxContainer:{
        position:'absolute',
        display:'flex',
        borderRadius:'50%',
        border:'1px solid rgb(29, 79, 144)',
        width:'21px',
        zIndex:'5',
        right:'10px',
        top:'41px'
    },
    delBoxContainer:{
        position:'absolute',
        display:'flex',
        borderRadius:'50%',
        border:'1px solid rgb(29, 79, 144)',
        width:'21px',
        zIndex:'5',
        right:'10px',
        top:'71px'
    },
    addBoxContainerCustom:{
        position:'absolute',
        display:'flex',
        borderRadius:'50%',
        border:'1px solid rgb(29, 79, 144)',
        width:'21px',
        zIndex:'5',
        right:'10px',
        top:'71px'
    },
    delBoxContainerCustom:{
        position:'absolute',
        display:'flex',
        borderRadius:'50%',
        border:'1px solid rgb(29, 79, 144)',
        width:'21px',
        zIndex:'5',
        right:'10px',
        top:'101px'
    },
})

export default NeedAnalysis