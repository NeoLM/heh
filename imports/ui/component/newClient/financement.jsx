import React, { useState, useRef } from 'react';
import { StyleSheet, css } from 'aphrodite';
import { Meteor } from 'meteor/meteor'

const Funding = () => {

    const simulatorList = [
        {
            id:0,
            title:'DOBOX',
            icon:'dobox.png',
            link:'https://play.google.com/store/apps/details?id=fr.snapp.android.cetelem&hl=fr'
        },
        {
            id:1,
            title:'PROJEXIO',
            icon:'cofidis.png',
            link:'https://www.moncofidispro.fr/fr/identification/msg_deconnexion.html',
            user:'0197074g',
            mdp:'123456'
        },
        {
            id:2,
            title:'ADHEFI',
            icon:'sofinco.svg',
            link:'https://www.adhefi.com/InitApplication.asp?ParamSte='
        }
    ]

    const handleLinkLogin = function(idList){ 
        const {link} = simulatorList[idList]
        console.log(link)
        const win = window.open(link)
    } 

    Session.set('background','Image6')


    return (
        <div className={css(styles.container)}>
            {simulatorList.map((simulator)=>{
                return(
                    <div key={simulator.id} className={css(styles.boxContainer)}>
                        <div className={css(styles.title)}>{simulator.title}</div>
                        <div className={css(styles.semiDashlane)} />
                        <div className={css(styles.logoContainer)}><img className={css(styles.logo)} src={`src/img/${simulator.icon}`}/></div>
                        <button onClick={() => handleLinkLogin(simulator.id)}className={css(styles.button)}>Simuler&nbsp;&nbsp;&nbsp;<img className={ css(styles.arrow)} src={'src/assets/arrow-bottom-black.svg'}/></button>
                    </div>
                )
            })}
        </div>
    )
}

const styles = StyleSheet.create({
    container:{
        margin:'5px 0',
        display:'flex',
        flexDirection:'row',
        height:'calc(100vh - 320px)',
        width:'100%',
        justifyContent:'space-between',
        '@media (max-width:600px)':{
            flexDirection:'column',
            overflow:'scroll'
        },
    },
    title:{
        margin:'auto',
        font:'Montserrat',
        fontSize:'16px',
    },
    semiDashlane:{
        height:'1px',
        backgroundColor:'#BF1E23',
        width:'30%',
        margin:'0 20% 0 auto'
    },
    logoContainer:{
        display:'flex',
        width:'70%',
        height:'50%',
        margin:'auto'
    },
    logo:{
        width:'100%',
        margin:'auto'
    },
    boxContainer:{
        display:'flex',
        flexDirection:'column',
        background: '#FBFCFE 0% 0% no-repeat padding-box',
        boxShadow: '15px 15px 20px #00000014',
        border: '1px solid #1D4F9046',
        borderRadius: '15px',
        opacity: 1,
        width:'20%',
        height:'80%',
        maxWidth:'270px',
        minWidth:'230px',
        maxHeight:'370px',
        minHeight:'265px',
        margin:'auto',
        padding:'15px',
        '@media (max-width:600px)':{
            margin:'10px auto'
        },
    },
    button:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'14px',
        background:'#F7E13B',
        borderRadius: '37px',
        width:'65%',
        height:'50px',
        cursor:'pointer'
    },
    arrow:{
        marginBottom:'0'
    },
})

export default Funding