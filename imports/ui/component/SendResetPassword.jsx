import React, { useState } from 'react';
import { Meteor } from 'meteor/meteor'
import { StyleSheet, css } from 'aphrodite';
import { Accounts } from 'meteor/accounts-base'


const sendResetPassword = () =>{

    const [email, setEmail] = useState("")
    const [message, setMessage] = useState("Entrez votre email pour réinitialiser votre mot de passe")
    const [resetSend, setResetSend] = useState(false)

    const reset = (e) => {
        e.preventDefault()
        Accounts.forgotPassword({email:email}, (err)=>{
            console.log(email, err)
            if(err){
                setMessage('Adresse email inconnue, entrez une adresse valide')
            } else {
                setResetSend(true)
                setMessage('Consultez votre boite mail, vous allez bientôt recevoir un mail vous permettant de réinitialiser votre mot de passe')
            }
        })
    }

    const resendEmail = (e) => {
        e.preventDefault()
        console.log('resendEmail')
    }

    return (
        <form className={ css(styles.resetContainer)} onSubmit={reset}>
            <div className={ css(styles.message)}>{message}</div>
            <input 
                className={css(styles.inputReset)} 
                type="email" 
                value={email}
                onChange={e => setEmail(e.target.value)}
            />
            { !resetSend ? 
            <button className={ css(resetSend ? styles.buttonEnabled : styles.buttonDisabled)} onClick={reset}>Réinitialiser mon mot de passe&nbsp;&nbsp;&nbsp;<img className={ css(styles.arrow)} src={'src/assets/arrow-black.svg'} /></button> :
            <button className={ css(styles.buttonEnabled)} onClick={resendEmail}>Renvoyer l'email&nbsp;&nbsp;&nbsp;<img className={ css(styles.arrow)} src={'src/assets/arrow-black.svg'} /></button> }
        </form>
    )

}

const styles = StyleSheet.create({
    message:{
        margin:'25px auto',
        textAlign:'center',
        font: 'normal normal normal 18px/25px Montserrat',
        letterSpacing:'Opx',
        color: '#101010',
        opacity: '0.3',
        lineHeight: 'normal',
    },
    resetContainer:{
        margin:'auto',
        display:'flex',
        flexDirection:'column',
    },
    inputReset:{
        background: '#FCF3F3 0% 0% no-repeat padding-box;',
        borderRadius: '15px',
        border:'none',
        width:'70%',
        padding:'20px',
        paddingLeft:'35px',
        paddingRight:'35px',
        letterSpacing: '0px',
        color: '#39393967',
        font:'normal normal normal 16px/19px Montserrat',
        margin:'auto',
    },
    resendEmail:{
        margin:'4px auto',
        textAlign:'center',
        textDecoration:'underline',
        font: 'normal normal normal 14px/18px Montserrat',
        letterSpacing:'Opx',
        color:'white',
    },
    buttonDisabled:{
        margin:'25px auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'white',
        borderRadius: '37px',
        width:'55%',
        height:'50px',
        cursor:'pointer',
        '@media (max-width:600px)':{
            width:'90%',
            height:'60px'
        } 
    },
    arrow:{
        marginBottom:'3px'
    },
    buttonEnabled:{
        margin:'25px auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'#F7E13B',
        borderRadius: '37px',
        width:'55%',
        height:'50px',
        cursor:'pointer',
        '@media (max-width:600px)':{
            width:'90%',
            height:'60px'
        } 
    },
})

export default sendResetPassword