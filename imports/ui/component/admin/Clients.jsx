import React, { useEffect, useState } from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';


import { Input } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';


import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';

import IconButton from '@material-ui/core/IconButton'
import SettingsIcon from '@material-ui/icons/Settings';
import DeleteIcon from '@material-ui/icons/Delete';
import GetAppIcon from '@material-ui/icons/GetApp';
import CachedIcon from '@material-ui/icons/Cached';

import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';

import useCustomForm from '../../../hooks/useCustomForm'

import { Meteor } from "meteor/meteor";


const useRowStyles = makeStyles({
    root: {
      '& > *': {
        borderBottom: 'unset',
      },
    },
  });


function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  width100: {
    width:'100%'
  },
  flexColumnContainer:{
      display:'flex',
      flexDirection:'row'
  },
  button: {
    margin: theme.spacing(1),
  },
  shortCell:{
      width:150,
  },
  longCell:{
      width:300,
  },
  primary:{
      color:theme.palette.primary.dark
  },
  mainTable:{
    maxHeight:400,
    overflow:'auto',
    '&::-webkit-scrollbar':{
        height:8,
        width:8  
    },
    '&::-webkit-scrollbar-track':{
        background:'#f1f1f1',
        borderRadius:'4px'
    },
    '&::-webkit-scrollbar-thumb':{
        background:'#3F51B5',
        borderRadius:'4px'
    },
    '&::-webkit-scrollbar-thumb:hover':{
        background:'#555'
    },
  }
}));

const Row = (props) => {
    const { clientData, housingData, needData, simulationData, orderData, devisData, bdcData } = props.client;
    console.log(!!bdcData)
    const [open, setOpen] = React.useState(false);
    const classes = useRowStyles();

    const generateClientPDF = (clientData) =>{
        Meteor.call('getClientDataPDF', clientData._id, (err,res)=>{
            if(!!res){
                const filename = `${clientData.name} ${clientData.lastname}.pdf`
                pdfMake.createPdf(res).download(filename)
            }
        })
    }

    return (
        <>
            <TableRow key={props.key} className={classes.root}>
                <TableCell>
                    <IconButton color="primary" size="small" onClick={()=>generateClientPDF(clientData)} aria-label="download">
                        <GetAppIcon />
                    </IconButton>
                    {!!housingData && (<IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                    {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>)}
                </TableCell>
                <TableCell key={props.key} component="th" scope="row">{`${clientData.name} ${clientData.lastname}`}</TableCell>
                <TableCell className={classes.primary}><a style={{textDecoration:'none', color:'#303f9f'}} href={`mailto:${clientData.email}`}>{clientData.email}</a></TableCell>
                <TableCell className={classes.primary}>{clientData.phone}</TableCell>
                <TableCell>{`${clientData.adress},`}<br />{`${clientData.zipCode} ${clientData.city}`}</TableCell>
                <TableCell>{clientData.familySituation}</TableCell>
                <TableCell>{clientData.residentsNumber}</TableCell>
                <TableCell>{`${clientData.profession1}${!!clientData.profession2 ? ' / ' + clientData.profession2 : ''}`}</TableCell>
                <TableCell>{clientData.lastYearHouseholdIncome}</TableCell>
                <TableCell>{`${clientData.houseHoldIncome1}${!!clientData.houseHoldIncome2 ? ' / ' + clientData.houseHoldIncome2 : ''}`}</TableCell>
                <TableCell>{`${clientData.birthDate1}${!!clientData.birthDate2 ? ' / ' + clientData.birthDate2 : ''}`}</TableCell>
            </TableRow>
            <TableRow key={props.key}>
            <TableCell key={props.key} style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={11}>
                <Collapse in={open} timeout="auto" unmountOnExit>
                    {!!housingData && (<Box margin={1}>
                        <Typography variant="h6" gutterBottom component="div">
                        Découverte habitation
                        </Typography>
                        <Table size="small" aria-label="purchases">
                        <TableHead>
                            <TableRow>
                            <TableCell>Type de maison</TableCell>
                            <TableCell>Année de construction</TableCell>
                            <TableCell>Type de chauffage</TableCell>
                            <TableCell>Consommation actuelle</TableCell>
                            <TableCell>Consommation combustible</TableCell>
                            <TableCell>Type de fenêtre</TableCell>
                            <TableCell>Surface habitable</TableCell>
                            <TableCell>Hauteur sous plafond</TableCell>
                            <TableCell>Niveaux</TableCell>
                            <TableCell>Construction sur</TableCell>
                            <TableCell>Isolation</TableCell>
                            <TableCell>Coefficient d'isolation</TableCell>
                            <TableCell>Travaux de rénovations</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow className={classes.root}>
                            <TableCell>{housingData.housingType}</TableCell>
                            <TableCell>{housingData.constructionYear}</TableCell>
                            <TableCell>{`${housingData.heaterType}`}</TableCell>
                            <TableCell>{housingData.currentConsumption}</TableCell>
                            <TableCell>{housingData.combustibleConsumption}</TableCell>
                            <TableCell>{housingData.windowType} {housingData.windowMaterial}</TableCell>
                            <TableCell>{housingData.livingSpace}</TableCell>
                            <TableCell>{housingData.ceilingHeight}</TableCell>
                            <TableCell>{housingData.levelNumber}</TableCell>
                            <TableCell>{housingData.buildingSoil}</TableCell>
                            <TableCell>Toiture : {housingData.roofIsolationQuality}<br/>Murs : {housingData.brickwallIsolationQuality}<br/>Sols : {housingData.floorIsolationQuality}</TableCell>
                            <TableCell>{housingData.insulationCoeff}</TableCell>
                            <TableCell>{housingData.lastRenovations}</TableCell>
                            </TableRow>
                        </TableBody>
                        </Table>
                    </Box>)}
                    {!!needData && (<Box margin={1}>
                        <Typography variant="h6" gutterBottom component="div">
                        Analyse des besoins
                        </Typography>
                        <Table size="small" aria-label="purchases">
                        <TableHead>
                            <TableRow>
                            <TableCell>Isolation combles</TableCell>
                            <TableCell>Isolations murs</TableCell>
                            <TableCell>Isolations plancher</TableCell>
                            <TableCell>Panneaux photovoltaÏques</TableCell>
                            <TableCell>Pompe à chaleur Air Air</TableCell>
                            <TableCell>Pompe à chaleur Air Eau</TableCell>
                            <TableCell>Ballon thermodynamiques</TableCell>
                            <TableCell>Accessoires</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow className={classes.root}>
                            <TableCell>{!!needData.floorRoofSpace && `Sols : ${needData.floorRoofSpace}m2, ${needData.floorRoofPrice}€`}{(!!needData.floorRoofSpace && !!needData.crawlingRoofSpace) && <br/>}{!!needData.crawlingRoofSpace && `Sols : ${needData.crawlingRoofSpace}m2, ${needData.crawlingRoofPrice}€`}</TableCell>
                            <TableCell>{!!needData.insideWallMeasure && `Intérieurs : ${needData.insideWallMeasure}m2, ${needData.insideWallPrice}€`}{(!!needData.insideWallMeasure && !!needData.outsideWallMeasure) && <br />}{!!needData.outsideWallMeasure && `Sols : ${needData.outsideWallMeasure}m2, ${needData.outsideWallPrice}€`}</TableCell>
                            <TableCell>{!!needData.lowFloorMeasure && `${needData.lowFloorMeasure}m2, ${needData.lowFloorPrice}€`}</TableCell>
                            <TableCell>
                                {!!needData.photovoltaicPanelsData && (
                                    <>
                                        <div>Ref: {needData.photovoltaicPanelsData.ref} quantité: {needData.photovoltaicPanelsData.quantity} prixTTC: {needData.photovoltaicPanelsData.totalTTC} prixHT: {needData.photovoltaicPanelsData.totalHT}</div>
                                        <div>{!!needData.photovoltaicPanelsData.batterie ? ' batterie : ' + needData.photovoltaicPanelsData.batterie + ' ' : ''}orientation: {needData.photovoltaicPanelsData.orientation} inclinaison: {needData.photovoltaicPanelsData.inclinaison}</div>
                                    </>
                                    )}
                            </TableCell>
                            <TableCell>{!!needData.airToAirHeatPump && needData.airToAirHeatPump.map((elem,index)=>{
                                return (
                                    <div key={index}>Ref: {elem.ref} quantité: {elem.quantity} prixTTC: {elem.totalTTC} prixHT: {elem.totalHT}</div> 
                                )
                            })}</TableCell>
                            <TableCell>{!!needData.airWaterHeatPump && needData.airWaterHeatPump.map((elem,index)=>{
                                return (
                                    <div key={index}>Ref: {elem.ref} quantité: {elem.quantity} prixTTC: {elem.totalTTC} prixHT: {elem.totalHT}</div> 
                                )
                            })}</TableCell>
                            <TableCell>{!!needData.thermodynamicHeater && needData.thermodynamicHeater.map((elem,index)=>{
                                return (
                                    <div key={index}>Ref: {elem.ref} quantité: {elem.quantity} prixTTC: {elem.totalTTC} prixHT: {elem.totalHT}</div> 
                                )
                            })}</TableCell>
                            <TableCell>{!!needData.accessories && needData.accessories.map((elem,index)=>{
                                return (
                                    <div key={index}>Ref: {elem.ref} quantité: {elem.quantity} prixTTC: {elem.totalTTC} prixHT: {elem.totalHT}</div> 
                                )
                            })}</TableCell>
                            </TableRow>
                        </TableBody>
                        </Table>
                    </Box>)}
                    {!!simulationData && (<Box margin={1}>
                        <Typography variant="h6" gutterBottom component="div">
                        Simulation
                        </Typography>
                        <Table size="small" aria-label="purchases">
                        <TableHead>
                            <TableRow>
                            <TableCell>Bilan</TableCell>
                            <TableCell>Économies</TableCell>
                            <TableCell>Ma Prime Renov'</TableCell>
                            <TableCell>Cee</TableCell>
                            <TableCell>Ecobonus</TableCell>
                            <TableCell>Aide régionale</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow className={classes.root}>
                            <TableCell>{`${simulationData.bilan}`}</TableCell>
                            <TableCell>{`${simulationData.economies}`}</TableCell>
                            <TableCell>{`${simulationData.aides.mpr}`}</TableCell>
                            <TableCell>{`${simulationData.aides.cee}`}</TableCell>
                            <TableCell>{`${simulationData.aides.ecobonus}`}</TableCell>
                            <TableCell>{`${simulationData.aides.region}`}</TableCell>
                            </TableRow>
                        </TableBody>
                        </Table>
                    </Box>)}
                    {!!orderData && (<Box margin={1}>
                        <Typography variant="h6" gutterBottom component="div">
                        Bon pour accord
                        </Typography>
                        <Table size="small" aria-label="purchases">
                        <TableHead>
                            <TableRow>
                            <TableCell>Délai de livraison</TableCell>
                            <TableCell>Accord de paiement</TableCell>
                            <TableCell>Détails</TableCell>
                            <TableCell>Options</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow className={classes.root}>
                            <TableCell>{`${orderData.deliveryDelay}`}</TableCell>
                            <TableCell>{`${orderData.paiementTerms}`}</TableCell>
                            <TableCell>{orderData.details.map((elem,index)=>{
                                return <div key={index}>{elem}</div>
                            })}</TableCell>
                            <TableCell>{`${orderData.agreement}`}</TableCell>
                            </TableRow>
                        </TableBody>
                        </Table>
                    </Box>)}
                    {!!devisData && (<Box margin={1}>
                        <Typography variant="h6" gutterBottom component="div">
                        Devis et Bon de commande
                        </Typography>
                        <Table size="small" aria-label="purchases">
                        <TableHead>
                            <TableRow>
                            <TableCell>Date du devis</TableCell>
                            <TableCell>Montant</TableCell>
                            <TableCell>Devis signé</TableCell>
                            <TableCell>Date de signature</TableCell>
                            <TableCell>Date du BDC</TableCell>
                            <TableCell>Bon de commande signé</TableCell>
                            <TableCell>Date de signature</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow className={classes.root}>
                            <TableCell>{`${devisData.date}`}</TableCell>
                            <TableCell>{`${devisData.totalTTC}`}</TableCell>
                            <TableCell>{`${devisData.signature ? 'oui': 'non'}`}</TableCell>
                            <TableCell>{`${devisData.signatureDate}`}</TableCell>
                            {!!bdcData && (<><TableCell>{`${bdcData.date}`}</TableCell>
                            <TableCell>{`${bdcData.signature ? 'oui': 'non'}`}</TableCell>
                            <TableCell>{`${bdcData.signatureDate}`}</TableCell></>)}
                            </TableRow>
                        </TableBody>
                        </Table>
                    </Box>)}
                </Collapse>
            </TableCell>
            </TableRow>
        </>
        )
}

export default function Clients() {

    // FetchData
    const [clientData, setClientData] = useState([])
    useEffect(()=>{
        Meteor.call('clientList',(err,res)=>{
            if(!!res){
                console.log(res)
                setClientData(res)
            }
        })
    },[])


    const [searchedValue, setSearchedValue] = useState('')

    const handleSearchChange = (e) => {
        const val = e.target.value
        setSearchedValue(val) 
    }
    const classes = useStyles();

    const [loading, setLoading] = useState(false)

    const handleDownload = () =>{
        setLoading(true)
        Meteor.call('clientExtract',(err,res)=>{
            if(res){
                var saveByteArray = (function () {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    a.style = "display: none";
                    return function (data, name, callback) {
                      var blob = new Blob(data, { type: "octet/stream" }),
                        url = window.URL.createObjectURL(blob);
                      a.href = url;
                      a.download = name;
                      a.click();
                      window.URL.revokeObjectURL(url);
                      callback(null, 'Liste des clients.csv');
                    };
                  }());
        
                saveByteArray([res], 'Liste des clients.csv', function (err, res) {
                    setLoading(false)
                });
            }
            
        })
    }

    return (
        <React.Fragment>
            <Grid item xs={12} md={6} lg={6}>
                <Input

                onChange={handleSearchChange}
                id="input-with-icon-adornment"
                startAdornment={
                    <InputAdornment position="start">
                    <SearchIcon />
                    </InputAdornment>
                }
                />
                
            </Grid>
            <Grid item xs={12} md={6} lg={6}>
                <Grid container justify="flex-end">
                    <Grid item>
                        <Button
                            variant="outlined"
                            color="primary"
                            className={classes.button}
                            startIcon={loading ? <CachedIcon /> : <GetAppIcon />}
                            onClick={handleDownload}
                        >
                            { loading ? 'Génération en cours' : 'Télécharger la liste des clients' }
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Paper className={classes.paper}>
                    <Typography component="h2" variant="h6" color="primary" gutterBottom>
                    Liste des Clients
                    </Typography>
                    <TableContainer component={Paper} className={classes.mainTable}>    
                        <Table stickyHeader aria-label="sticky table" size="medium" style={{width:'200%'}}>
                            <TableHead>
                                <TableRow>
                                    <TableCell />
                                    <TableCell className={classes.longCell}>Nom</TableCell>
                                    <TableCell className={classes.longCell}>Email</TableCell>
                                    <TableCell className={classes.shortCell}>Téléphone</TableCell>
                                    <TableCell className={classes.longCell}>Adresse</TableCell>
                                    <TableCell className={classes.longCell}>Situation familiale </TableCell>
                                    <TableCell className={classes.shortCell}>Nombre de résidents</TableCell>
                                    <TableCell className={classes.longCell}>Professions</TableCell>
                                    <TableCell className={classes.longCell}>Revenus de l'année n-1</TableCell>
                                    <TableCell className={classes.longCell}>Revenus du foyer</TableCell>
                                    <TableCell className={classes.longCell}>Dates de naissances</TableCell>
                                </TableRow>
                            </TableHead>
                        <TableBody>
                        {clientData.filter((client)=>{return client.clientData.name.toLowerCase().includes(searchedValue.toLowerCase()) || client.clientData.lastname.toLowerCase().includes(searchedValue.toLowerCase())}).map((client) => {return (
                            <Row key={client._id} client={client}></Row>
                        )})}
                        </TableBody>
                        </Table>
                    </TableContainer>  
                </Paper>
            </Grid>
        </React.Fragment>
    );
}