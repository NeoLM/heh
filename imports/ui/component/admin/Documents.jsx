import React, { useEffect, useState } from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';

import { Input } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
  } from '@material-ui/pickers';


import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';


import IconButton from '@material-ui/core/IconButton'
import GetAppIcon from '@material-ui/icons/GetApp';



import { Meteor } from "meteor/meteor";
import { moment } from 'meteor/momentjs:moment';
import DateFnsUtils from '@date-io/date-fns';
import { isWithinInterval, isBefore, isEqual} from 'date-fns'
const frenchLocale = require('date-fns/locale/fr');







function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  width100: {
    width:'100%'
  },
  flexColumnContainer:{
      display:'flex',
      flexDirection:'row'
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  input:{
      marginTop:theme.spacing(2)
  }
}));

export default function Documents() {

    // FetchData
    const [devisData, setDevisData] = useState([])
    const [bdcsData, setBdcsData] = useState([])
    const [commercials, setCommercials] = useState([])

    useEffect(()=>{
        Meteor.call('documentList',(err,res)=>{
            if(!!res){
                setDevisData(res.devis)
                setBdcsData(res.bdcs)
            }
        })
    },[])

    useEffect(()=>{
        Meteor.call('commercialList',(err,res)=>{
            if(!!res){
                setCommercials(res)
            }
        })
    },[])


    const [searchedValue, setSearchedValue] = useState('')

    const handleSearchChange = (e) => {
        const val = e.target.value
        setSearchedValue(val) 
    }

    const [selectedCommercial, setSelectedCommercial] = useState('')

    const handleCommercial = (e)=>{
        setSelectedCommercial(e.target.value)
    }

    const [selectedStartDate, setSelectedStartDate] = useState()
    const [selectedEndDate, setSelectedEndDate] = useState()

    const handleStartDate = (date)=>{
        setSelectedStartDate(date)
    }

    const handleEndDate = (date)=>{
        setSelectedEndDate(date)
    }

    const filterDate = (elemDate)=>{
        if(!!selectedStartDate){
            const testedDate = new Date(new Date(elemDate).toLocaleDateString('fr-fr'))
            const startDate = selectedStartDate
            const endDate = !!selectedEndDate ? selectedEndDate : new Date()
            if(isBefore(startDate,endDate) || isEqual(startDate,endDate)){
                const test = isWithinInterval(testedDate, {start:startDate, end:endDate})
                return test
            } else {
                return true
            }
        } else {
            return true
        }
    }

    const classes = useStyles();

    const handleDownloadDevis = (clientName,devisId)=>{
        Meteor.call('getDevis', devisId, (err,res)=>{
            if(!!res){
                var saveByteArray = (function () {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    a.style = "display: none";
                    return function (data, name, callback) {
                      // var blob = new Blob(data, {type: "octet/stream"});
                      var blob = new Blob(data, { type: "application/pdf" });
                      // var url = window.URL.createObjectURL(blob);
                      var url = URL.createObjectURL(blob);
          
                      var open = false;
          
                      if (open) {
                        window.open(url);
                      } else {
                        if (navigator.msSaveOrOpenBlob) {
                          navigator.msSaveOrOpenBlob(blob, fileName + '.pdf');
                          return;
                        }
                        else if (window.navigator.msSaveBlob) { // for IE browser
                          window.navigator.msSaveBlob(blob, fileName + '.pdf');
                          return;
                        }
                        a.href = url;
                        a.download = name;
                        a.target = '_blank';
                        a.click();
                        setTimeout(function () {
                          window.URL.revokeObjectURL(url);
                        }, 100);
                      }
          
          
                      callback(null, name);
                    };
                  }());
        
                  saveByteArray([res], clientName + '-devis.pdf', function (err, res) {
                    
                    });
            }
        })
    }

    const handleDownloadBdc = (clientName,devisId)=>{
        Meteor.call('getBdc', devisId, (err,res)=>{
            if(!!res){
                var saveByteArray = (function () {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    a.style = "display: none";
                    return function (data, name, callback) {
                      // var blob = new Blob(data, {type: "octet/stream"});
                      var blob = new Blob(data, { type: "application/pdf" });
                      // var url = window.URL.createObjectURL(blob);
                      var url = URL.createObjectURL(blob);
          
                      var open = false;
          
                      if (open) {
                        window.open(url);
                      } else {
                        if (navigator.msSaveOrOpenBlob) {
                          navigator.msSaveOrOpenBlob(blob, fileName + '.pdf');
                          return;
                        }
                        else if (window.navigator.msSaveBlob) { // for IE browser
                          window.navigator.msSaveBlob(blob, fileName + '.pdf');
                          return;
                        }
                        a.href = url;
                        a.download = name;
                        a.target = '_blank';
                        a.click();
                        setTimeout(function () {
                          window.URL.revokeObjectURL(url);
                        }, 100);
                      }
          
          
                      callback(null, name);
                    };
                  }());
        
                  saveByteArray([res], clientName + '-bdc.pdf', function (err, res) {
                    
                    });
            }
        })
    }

    return (
        <React.Fragment>
            <Grid item xs={12} md={6} lg={3}>
                <Input
                className={classes.input}
                onChange={handleSearchChange}
                id="input-with-icon-adornment"
                startAdornment={
                    <InputAdornment position="start">
                    <SearchIcon />
                    </InputAdornment>
                }
                />
                
            </Grid>
            <Grid item xs={12} md={6} lg={3}>
            <FormControl className={classes.formControl}>
                <InputLabel id="commercials">Commerciaux</InputLabel>
                <Select
                labelId="commercials"
                id="commercials-list"
                value={selectedCommercial}
                onChange={handleCommercial}
                >
                    <MenuItem value="">
                        <em>Aucun</em>
                    </MenuItem>
                    {commercials.map((elem,index)=>{
                        // console.log(elem)
                        return (<MenuItem key={index} value={elem.id}>{`${elem.name} ${elem.lastname}`}</MenuItem>)
                    })}
                </Select>
            </FormControl>
            </Grid>
            <Grid item xs={12} md={6} lg={3}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                disableToolbar
                variant="inline"
                format="dd/MM/yyyy"
                margin="dense"
                id="date-picker-inline-start"
                label="Date de début"
                value={selectedStartDate}
                onChange={handleStartDate}
                KeyboardButtonProps={{
                    'aria-label': 'change date',
                }}
                />
            </MuiPickersUtilsProvider>
            </Grid>
            <Grid item xs={12} md={6} lg={3}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                disableToolbar
                variant="inline"
                format="dd/MM/yyyy"
                margin="dense"
                id="date-picker-inline-end"
                label="Date de fin"
                value={selectedEndDate}
                onChange={handleEndDate}
                KeyboardButtonProps={{
                    'aria-label': 'change date',
                }}
                />
            </MuiPickersUtilsProvider>
            </Grid>
            <Grid item xs={12}>
                <Paper className={classes.paper}>
                    <Typography component="h2" variant="h6" color="primary" gutterBottom>
                    Liste des devis
                    </Typography>      
                    <Table stickyHeader aria-label="sticky table" size="small">
                    <TableHead>
                    <TableRow>
                        <TableCell>Nom </TableCell>
                        <TableCell>Email</TableCell>
                        <TableCell>Commercial</TableCell>
                        <TableCell>Date du devis</TableCell>
                        <TableCell>Montant</TableCell>
                        <TableCell>Statut</TableCell>
                        <TableCell align="right">Devis PDF</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {devisData.filter((devis)=>{return devis.clientName.toLowerCase().includes(searchedValue.toLowerCase()) && devis.commercialId.includes(selectedCommercial) && filterDate(devis.date) }).map((devis) => (
                        <TableRow key={devis.clientId}>
                            <TableCell>{devis.clientName}</TableCell>
                            <TableCell>{devis.clientEmail}</TableCell>
                            <TableCell>{devis.commercialName}</TableCell>
                            <TableCell>{devis.date}</TableCell>
                            <TableCell>{devis.amount}</TableCell>
                            <TableCell>{devis.status}</TableCell>
                            <TableCell align="right" className={classes.flexRowContainer}>
                                <IconButton color="primary" onClick={()=>handleDownloadDevis(devis.clientName, devis.devisId)} aria-label="modify">
                                    <GetAppIcon />
                                </IconButton>
                            </TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                    </Table>
                </Paper>
            </Grid>
            <Grid item xs={12}>
                <Paper className={classes.paper}>
                    <Typography component="h2" variant="h6" color="primary" gutterBottom>
                    Liste des bons de commande
                    </Typography>      
                    <Table stickyHeader aria-label="sticky table" size="small">
                    <TableHead>
                    <TableRow>
                        <TableCell>Nom </TableCell>
                        <TableCell>Email</TableCell>
                        <TableCell>Commercial</TableCell>
                        <TableCell>Date du BDC</TableCell>
                        <TableCell>Statut</TableCell>
                        <TableCell align="right">BDC PDF</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {bdcsData.filter((bdc)=>{return bdc.clientName.toLowerCase().includes(searchedValue.toLowerCase()) && bdc.commercialId.includes(selectedCommercial) }).map((bdc) => (
                        <TableRow key={bdc.clientId}>
                            <TableCell>{bdc.clientName}</TableCell>
                            <TableCell>{bdc.clientEmail}</TableCell>
                            <TableCell>{bdc.commercialName}</TableCell>
                            <TableCell>{bdc.date}</TableCell>
                            <TableCell>{bdc.status}</TableCell>
                            <TableCell align="right" className={classes.flexRowContainer}>
                                <IconButton color="primary" onClick={()=>handleDownloadBdc(bdc.clientName, bdc.bdcId)} aria-label="modify">
                                    <GetAppIcon />
                                </IconButton>
                            </TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                    </Table>
                </Paper>
            </Grid>
        </React.Fragment>
    );
}