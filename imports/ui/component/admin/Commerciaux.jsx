import React, { useEffect, useState } from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';

import { Input } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';


import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';

import IconButton from '@material-ui/core/IconButton'
import SettingsIcon from '@material-ui/icons/Settings';
import DeleteIcon from '@material-ui/icons/Delete';


import useCustomForm from '../../../hooks/useCustomForm'


import { Meteor } from "meteor/meteor";





function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  width100: {
    width:'100%'
  },
  flexColumnContainer:{
      display:'flex',
      flexDirection:'row'
  }
}));

export default function Commerciaux() {

    // FetchData
    const [userData, setUserData] = useState([])
    useEffect(()=>{
        Meteor.call('commercialList',(err,res)=>{
            if(!!res){
                setUserData(res)
            }
        })
    },[])

    const saveData = () => {
        dialogMethods[dialogMode]()
    }

    const dialogMethods = {
        create(){
            Meteor.call('createAccount', values,(err,res)=>{
                if(!!res){
                    Meteor.call('commercialList',(err,res)=>{
                        if(!!res){
                            setUserData(res)
                            handleMultiple(initialValues)
                            setDialogMode('')
                            setOpen(false);
                        }
                    })
                }
            })
        },
        modify(){
            Meteor.call('modifyAccount', values, currentUserId,(err,res)=>{
                if(!!res){
                    Meteor.call('commercialList',(err,res)=>{
                        if(!!res){
                            setUserData(res)
                            handleMultiple(initialValues)
                            setDialogMode('')
                            setOpen(false);
                        }
                    })
                }
            })
        },
        deactivate(){
            Meteor.call('deactivateAccount', currentUserId,(err,res)=>{
                if(!!res){
                    Meteor.call('commercialList',(err,res)=>{
                        if(!!res){
                            setUserData(res)
                            setDialogMode('')
                            setOpenAlert(false);
                        }
                    })
                }
            })
        }
    }

    const [searchedValue, setSearchedValue] = useState('')

    const [open, setOpen] = useState(false);
    const [openAlert, setOpenAlert] = useState(false);


    const [dialogMode, setDialogMode] = useState()
    const [currentUserId, setCurrentUserId] = useState()


    const handleClickCreate = () => {
        setDialogMode('create')
        setOpen(true);
    };

    const handleClickModify = (user) => {
        setDialogMode('modify')
        setCurrentUserId(user.id)
        const userData = {...user}
        delete userData.id
        handleMultiple(userData)
        setOpen(true);
    }

    const handleClickDeactivate = (user) => {
        setDialogMode('deactivate')
        setCurrentUserId(user.id)
        setOpenAlert(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleCloseAlert = () => {
        setOpenAlert(false);
    };

    const handleSearchChange = (e) => {
        const val = e.target.value
        setSearchedValue(val) 
    }
    const classes = useStyles();

    const initialValues = {
        email:'',
        level:'',
        phone:'',
        name:'',
        lastname:'',
        location:''
    }

    const requiredValues = {
        email:true,
        level:true,
        phone:true,
        name:true,
        lastname:true,
        location:true
    }

    const {
        values,
        errors,
        touched,
        validated,
        handleChange,
        handleSelect,
        handleMultiple,
        handleBlur,
        handleSubmit
      } = useCustomForm({
        initialValues,
        onSubmit: saveData,
        requiredValues
      });

    return (
        <React.Fragment>
            <Grid item xs={12} md={6} lg={8}>
                <Input

                onChange={handleSearchChange}
                id="input-with-icon-adornment"
                startAdornment={
                    <InputAdornment position="start">
                    <SearchIcon />
                    </InputAdornment>
                }
                />
            </Grid>
            <Grid item xs={12} md={6} lg={4}>
                <Grid container justify="flex-end">
                    <Grid item>
                        <Button onClick={handleClickCreate} variant="outlined" color="primary">
                            Créer un compte
                        </Button>
                        <Dialog open={open} onClose={handleClose} aria-labelledby="Créez un nouvel utilisateur">
                            <DialogTitle id="form-dialog-title">{dialogMode === 'create' ? 'Créez un nouvel utilisateur' : 'modifiez un utilisateur'}</DialogTitle>
                            <DialogContent>
                                <TextField
                                    autoFocus
                                    margin="dense"
                                    label="Prenom"
                                    type="text"
                                    name='name'
                                    value={values.name}
                                    onChange={handleChange}
                                    fullWidth
                                    error={errors?.name}
                                />
                                <TextField
                                    autoFocus
                                    margin="dense"
                                    label="Nom"
                                    type="text"
                                    name='lastname'
                                    value={values.lastname}
                                    onChange={handleChange}
                                    fullWidth
                                    error={errors?.lastname}
                                />
                                <TextField
                                    margin="dense"
                                    label="Adresse email"
                                    type="email"
                                    name='email'
                                    value={values.email}
                                    onChange={handleChange}
                                    fullWidth
                                    error={errors?.email}
                                />
                                <TextField
                                    margin="dense"
                                    label="Téléphone"
                                    type="number"
                                    name='phone'
                                    value={values.phone}
                                    onChange={handleChange}
                                    fullWidth
                                    error={errors?.phone}
                                />
                                <TextField
                                    margin="dense"
                                    label="Localisation"
                                    type="text"
                                    name='location'
                                    value={values.location}
                                    onChange={handleChange}
                                    fullWidth
                                    error={errors?.location}
                                />
                                <FormControl fullWidth error={errors?.level}>
                                    <InputLabel id="select-label">Type de compte</InputLabel>
                                    <Select
                                        labelId="select-label"
                                        id="select"
                                        margin="dense"
                                        label="Niveau de droit"
                                        value={values.level}
                                        onChange={(e)=>handleSelect('level',e.target.value)}
                                        fullWidth
                                        >
                                        <MenuItem value={'superadmin'}>Super administrateur</MenuItem>
                                        <MenuItem value={'admin'}>Administrateur</MenuItem>
                                        <MenuItem value={'user'}>Commercial</MenuItem>
                                    </Select>
                                </FormControl>
                            </DialogContent>
                            <DialogActions>
                            <Button onClick={handleClose} color="primary">
                                Annuler
                            </Button>
                            <Button onClick={handleSubmit} color="primary">
                                {dialogMode === 'create' ? 'Créer' : 'Modifier'}
                            </Button>
                            </DialogActions>
                        </Dialog>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Paper className={classes.paper}>
                    <Typography component="h2" variant="h6" color="primary" gutterBottom>
                    Liste des commerciaux
                    </Typography>      
                    <Table stickyHeader aria-label="sticky table" size="small">
                    <TableHead>
                    <TableRow>
                        <TableCell>Nom </TableCell>
                        <TableCell>Email</TableCell>
                        <TableCell>Téléphone</TableCell>
                        <TableCell>Statut du compte</TableCell>
                        <TableCell>Localisation</TableCell>
                        <TableCell align="right">Actions</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {userData.filter((user)=>{return user.name.toLowerCase().includes(searchedValue.toLowerCase()) || user.lastname.toLowerCase().includes(searchedValue.toLowerCase())}).map((user) => (
                        <TableRow key={user.id}>
                            <TableCell>{`${user.name} ${user.lastname}`}</TableCell>
                            <TableCell><a style={{textDecoration:'none', color:'#303f9f'}} href={`mailto:${user.email}`}>{user.email}</a></TableCell>
                            <TableCell>{user.phone}</TableCell>
                            <TableCell>{user.active ? 'actif' : 'non actif'}</TableCell>
                            <TableCell>{user.location}</TableCell>
                            <TableCell align="right" className={classes.flexRowContainer}>
                                <IconButton color="primary" onClick={()=>handleClickModify(user)} aria-label="modify">
                                    <SettingsIcon />
                                </IconButton>
                                <IconButton color="secondary" onClick={()=>handleClickDeactivate(user)} aria-label="deactivate">
                                    <DeleteIcon />
                                </IconButton>
                                <Dialog
                                    open={openAlert}
                                    onClose={handleCloseAlert}
                                    aria-labelledby="deactivate"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="deactivate">{`Désactivez ${user.name} ${user.lastname} ?`}</DialogTitle>
                                    <DialogContent>
                                    <DialogContentText id="alert-dialog-description">
                                        Souhaitez vous désactivez le compte de {user.name} {user.lastname} ?
                                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                    <Button onClick={handleCloseAlert} color="primary">
                                        Non
                                    </Button>
                                    <Button onClick={saveData} color="primary" autoFocus>
                                        Oui
                                    </Button>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                    </Table>
                </Paper>
            </Grid>
        </React.Fragment>
    );
}