import React, { useState, useRef, useEffect } from 'react';
import { StyleSheet, css } from 'aphrodite';
import { Meteor } from 'meteor/meteor'

import DiscoverClient from './newClient/decouverte_client'
import DiscoverHome from './newClient/decouverte_habitation'
import NeedAnalysis from './newClient/analyse_besoins'
import Simulation from './newClient/simulation'
import SimulationEco from './newClient/simulation_eco'
import Quote from './newClient/devis'
import Funding from './newClient/financement'
import GFA from './newClient/bon_accord'
import OrderForm from './newClient/bon_commande'

import { useLocation, useHistory } from 'react-router-dom'

import Steps from './Steps'

import Carousel from 'react-elastic-carousel'
import Loader from "react-loader-spinner";


 
const Decouverte = () => {

    function useQuery() {
        return new URLSearchParams(useLocation().search);
    }

    const clientId = useQuery().get('id')

    const [loading, setLoadging] = useState(true)

    useEffect(()=>{
        Session.set('waitFetch',false)
        if(!!clientId){
            Session.setPersistent('clientId', clientId)
            Meteor.call('getClientFormData', clientId, (err,res)=>{
                if(!!res){
                    console.log(res)
                    for (const [key, value] of Object.entries(res)){
                        Session.setPersistent(key, value)
                        Session.setPersistent(key.replace('Data','Id'), value._id)
                    }
                }
                setLoadging(false)
            })
        } else {
            setLoadging(false)
        }
    },[])

    const history = useHistory()
    
    const redirect = (adress)=>{
        history.push(adress)
    }

    useEffect(()=>{
        return history.listen((location)=>{
            const sessionKeys = ['clientData', 'clientId', 'housingData', 'housingId', 'needData', 'needId', 'devisData', 'devisId', 'simulationData', 'simulationId','orderData', 'orderId', 'bdcData', 'bdcId']
            sessionKeys.forEach((sessionKey)=>{
                Session.clear(sessionKey)
            })
        })
    })

    const goHome = ()=>{
        redirect('/')
    }
    const [current, setCurrent] = useState(0)    
    const slider = useRef();


    const prev = ()=>{
        if(current > 0){
            newCurrent = current-1
            setCurrent(newCurrent)
            slider.current.goTo(newCurrent)
        } else {
            goHome()
        }
    }

    const next = ()=>{
        if(current < stepsArray.length -1){
            !!stepsRef[current] && stepsRef[current].current.submit()
            const waitFetch = setInterval(()=>{
                if(Session.get('waitFetch')){
                    return
                }else{
                    clearInterval(waitFetch)
                    if(!stepsRef[current] || stepsRef[current].current.isValid()){
                        newCurrent = current+1
                        setCurrent(newCurrent)
                        slider.current.goTo(newCurrent)
                    }
                    return
                }
            }) 
        } else {
            redirect('/mes-clients')
        }
    }

    const goTo = (newCurrent) => {
        setCurrent(newCurrent)
        slider.current.goTo(newCurrent)
    }

    const stepsArray = [
        {
            title:"Découverte client",
            icon:'user',
        },
        {
            title:"Découverte Habitat",
            icon:'home',
        },
        {
            title:"Analyse des besoins",
            icon:'search',
        },
        {
            title:"Économies",
            icon:'financement',
        }, 
        {
            title:"Aides",
            icon:'calculator',
        },
        {
            title:"Devis",
            icon:'devis',
        },
        // {
        //     title:"Financement",
        //     icon:'financement',
        // },        
        {
            title:"Bon pour accord",
            icon:'bdc',
        }, 
        {
            title:"Bon de commande",
            icon:'signature',
        }
    ]

    const clientRef = useRef()
    const homeRef = useRef()
    const needRef = useRef()
    const simulationEcoRef = useRef()
    const simulationHelpRef = useRef()
    const gfaRef = useRef()
    const orderFormRef = useRef()

    const stepsRef ={
        0:clientRef,
        1:homeRef,
        2:needRef,
        3:simulationEcoRef,
        4:simulationHelpRef,
        6:gfaRef,
        7:orderFormRef
    }

    return (
        <>
            <main className={css(styles.body)}>
                <section className={css(styles.stepContainer)}>
                    <Steps stepsArray={stepsArray} current={current} goTo={goTo}/>
                </section>
                <section className={css(styles.sectionContainer)}>                            
                    { loading ? 
                        <div className={css(styles.loaderContainer)}>
                            <Loader type="TailSpin" color="rgb(191, 30, 35)" height={120} width={120} />
                        </div>
                        
                        :
                        <Carousel
                            showArrows={false}
                            enableMouseSwipe={false}
                            enableSwipe={false}
                            renderPagination={()=>{ return (<></>)}}
                            ref={ref=>{
                                slider.current = ref
                            }}
                        >
                                <div className={css(styles.pageContainer)}>{(current == 0) && <DiscoverClient ref={clientRef}></DiscoverClient>}</div>
                                <div className={css(styles.pageContainer)}>{(current == 1) && <DiscoverHome ref={homeRef}></DiscoverHome>}</div>
                                <div className={css(styles.pageContainer)}>{(current == 2) && <NeedAnalysis ref={needRef}></NeedAnalysis>}</div>
                                <div className={css(styles.pageContainer)}>{(current == 3) && <SimulationEco ref={simulationEcoRef}></SimulationEco>}</div>
                                <div className={css(styles.pageContainer)}>{(current == 4) && <Simulation ref={simulationHelpRef}></Simulation>}</div>
                                <div className={css(styles.pageContainer)}>{(current == 5) && <Quote></Quote>}</div>
                                {/* <div className={css(styles.pageContainer)}>{(current == 6) && <Funding></Funding>}</div> */}
                                <div className={css(styles.pageContainer)}>{(current == 6) && <GFA ref={gfaRef}></GFA>}</div>
                                <div className={css(styles.pageContainer)}>{(current == 7) && <OrderForm ref={orderFormRef}></OrderForm>}</div>
                        </Carousel>
                    }
                </section>
            </main>
            <footer className={css(styles.footer)}>
                <div className={css(styles.footerContentContainer)}>
                    <button className={css(styles.prevButton)} onClick={prev}><img className={ css(styles.arrow)} src={'src/assets/arrow-reverse-black.svg'}/>&nbsp;&nbsp;&nbsp;Retour</button>
                    <button className={css(styles.nextButton)} onClick={next}>{current === 7 ? 'terminer' : 'Suivant'}&nbsp;&nbsp;&nbsp;<img className={ css(styles.arrow)} src={'src/assets/arrow-bottom-white.svg'}/></button>
                </div>
            </footer>
        </>
    ) 
}

const styles = StyleSheet.create({
    body:{
        display:'flex',
        flexDirection:'column',
        height:'calc(100vh - 190px)',
        width:'100%',
        margin:'0 auto auto auto'
    },
    footer:{
        display:'flex',
        flexDirection:'row',
        width:'100%',
        height:'60px',
        position:'fixed',
        bottom:'0',
        left:'0',
        margin:'0 auto 0',
        zIndex:2,
        '@media (max-width:600px)':{
            width:'100%',
            height:'60px',
        },
    },
    footerContentContainer:{
        display:'flex',
        width:'450px',
        margin:'auto'
    },
    sectionContainer:{
        display:'flex',
        width:'95%',
        margin:'auto'
    },
    stepContainer:{
        width:'100%',
        maxWidth:'1200px',
        margin:'0 auto'
    },
    prevButton:{
        margin:'auto',
        border: '1px solid #007100',
        font:'Montserrat',
        fontSize:'16px',
        background:'white',
        borderRadius: '37px',
        width:'210px',
        height:'50px',
        cursor:'pointer',
        '@media (max-width:600px)':{
            width:'150px',
        },
    },
    nextButton:{
        margin:'auto',
        border: '1px solid #007100',
        font:'Montserrat',
        color:'white',
        fontSize:'16px',
        background:'#007100',
        borderRadius: '37px',
        width:'210px',
        height:'50px',
        cursor:'pointer',
        '@media (max-width:600px)':{
            width:'150px',
        },
    },
    prevButtonOld:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'white',
        borderRadius: '37px',
        width:'210px',
        height:'50px',
        cursor:'pointer',
    },
    nextButtonOld:{
        margin:'auto',
        border: '1px solid #F7E13B',
        font:'Montserrat',
        fontSize:'16px',
        background:'#F7E13B',
        borderRadius: '37px',
        width:'210px',
        height:'50px',
        cursor:'pointer'
    },
    arrow:{
        marginBottom:'3px'
    },
    pageContainer:{
        width:'100%'
    }, 
    loaderContainer:{
        margin:'auto'
    },
    background:{
        background:'url(/src/img/background.png) no-repeat center',
        opacity:'0.40',
        width:'100%',
        height:'100%',
        position:'absolute',
        top:0,
        left:0
    },
})

export default Decouverte