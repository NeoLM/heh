import React, { useEffect, useState } from 'react';
import { Meteor } from 'meteor/meteor'
import { StyleSheet, css } from 'aphrodite';
import useModal from '../../hooks/useModal'
import Modal from './Modal'
import SendResetPassword from './SendResetPassword'
import ResetPassword from './ResetPassword'
import { useLocation, useHistory } from 'react-router-dom'
import { ReactiveVar } from 'meteor/reactive-var'
import { Session } from 'meteor/session'


function useQuery() {
    return new URLSearchParams(useLocation().search);
}




const LoginForm = () =>{

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [errMessage, setErrMessage] = useState("")
    const [ isShowing, toggle ] = useModal()
    const [toko,setToko] = useState(false)
    const [token,setToken] = useState()

    const resetToken = useQuery().get('resetPassword')
    const initToken = useQuery().get('initPassword')

    let history = useHistory();

    useEffect(()=>{
        if(!!resetToken || !!initToken){
            setToko(true)
            setToken(!!resetToken ? resetToken : initToken)
            setTimeout(()=>{
                toggle()
            },200)
        }     
    },[])

    const login = (e) => {
        e.preventDefault()
        setErrMessage('')
        Meteor.loginWithPassword(email, password, (err,res)=>{
            if(err){
                setErrMessage('Identifiant ou mot de passe incorrect')
            } else {
                user = Meteor.user()
                if(!user.profile.active){
                    Meteor.logout()
                    setErrMessage('Compte inactif')
                } else {
                    userInfo = {
                        name:user.profile.name,
                        lastname:user.profile.lastname,
                    }
                    Session.setPersistent('userInfo',userInfo)
                    const isAdmin = user.profile.level === 'superadmin' || user.profile.level === 'admin'
                    history.push(isAdmin ? '/admin' : '/')                    
                }
            }
        })
    }

    return (
        <form className={ css(styles.loginContainer)} onSubmit={login}>
            <div className={ css(styles.errMessage)}>{errMessage}</div>
            <div>
                <input 
                    className={css(styles.inputLogin)} 
                    type="email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    placeholder={'Entrez votre email'}
                />
            </div>
            <div className={css(styles.inputButtonContainer)}>
                <input 
                    className={css(styles.inputLogin)} 
                    type="password" 
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    placeholder={'Entrez votre mot de passe'}
                />
                <button type="submit" className={ css(styles.loginButton)}>
                    <img src={'src/assets/arrow.svg'}></img>
                </button>
            </div>
            <div className={css(styles.passforgotten)} onClick={toggle}>Mot de passe oublié ?</div>
            {!toko ? 
                <Modal isShowing={isShowing} hide={toggle} title={'Mot de passe perdu'}>
                    <SendResetPassword />
                </Modal> : 
                <Modal isShowing={isShowing} hide={toggle} title={`${!!resetToken ? 'Re' : ''}initialisation mot de passe`}>
                    <ResetPassword token={token} iSToken={setToko} hide={toggle} />
                </Modal>}
        </form>
    )

}

const styles = StyleSheet.create({
    errMessage:{
        margin:'4px auto',
        textAlign:'center',
        font: 'normal normal normal 14px/18px Montserrat',
        letterSpacing:'Opx',
        color:'#BF1E23',
        height:'15px',
    },
    loginContainer:{
        margin:'1% auto',
    },
    inputLogin:{
        background: '#FFFFFF 0% 0% no-repeat padding-box',
        boxShadow: '0px 10px 20px #BF1E2319',
        border: '1px solid #0000001A',
        borderRadius: '37px',
        width:'370px',
        padding:'16px',
        paddingLeft:'36px',
        paddingRight:'36px',
        letterSpacing: '0px',
        color: '#979797',
        font:'normal normal normal 17px/20px Montserrat',
        margin:'4px auto',
        '@media (max-width:600px)':{
            width:'250px',
        } 
    },
    passforgotten:{
        margin:'4px auto',
        textAlign:'center',
        textDecoration:'underline',
        font: 'normal normal normal 14px/18px Montserrat',
        letterSpacing:'Opx',
        color:'white',
        cursor:'pointer'
    },
    loginButton:{
        background: '#AB2330 0% 0% no-repeat padding-box',
        boxShadow: '0px 10px 20px #BF1E2319',
        border: '1px solid #8B959A1A',
        borderRadius: '37px',
        width:'49px',
        height:'49px',
        position:'absolute',
        right:4,
        top:6,
        margin:'auto 0',
        cursor:'pointer',
    },
    inputButtonContainer:{
        display:'flex',
        position:'relative'
    }
})

export default LoginForm