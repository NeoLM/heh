import React, { useState, useEffect } from 'react';
import { StyleSheet, css } from 'aphrodite';
import useMobileDetect from '../../hooks/useMobileDetect'
import useWindowDimensions from '../../hooks/useWindowDimensions'

const Steps = ({stepsArray, current, goTo}) =>{

    const { height, width } = useWindowDimensions();

    const stepsState = stepsArray.map((step,index)=>{
        const stepObj = {}
        stepObj.id = index
        stepObj.title = step.title
        stepObj.icon = step.icon 
        stepObj.current = index === 0 ? true : false
        return stepObj
    })

    const [steps, setStep ] = useState([]) 

    const updateStep = (stepNumber, stepsArray) => {
        const newSteps = [...stepsArray]
        
        let stepCounter = 0

        while(stepCounter < newSteps.length){
            if(stepCounter === stepNumber){
                newSteps[stepCounter] = {
                    ...newSteps[stepCounter],
                    current:true,
                }
                stepCounter++
            } else if (stepCounter < stepNumber){
                newSteps[stepCounter] = {
                    ...newSteps[stepCounter],
                    current:false,
                }
                stepCounter++
            } else {
                newSteps[stepCounter] = {
                    ...newSteps[stepCounter],
                    current:false, 
                }
                stepCounter++
            }
        }

        return newSteps
    }        
    
    const currentSteps = updateStep(current, stepsState)

    useEffect(()=>{
        setStep(currentSteps)
    },[])

    useEffect(()=>{
        setStep(currentSteps)
    },[current])

    const md = useMobileDetect('tablet')

    const setCurrentPrev = (step) => {
        if(step.id < current){
            goTo(step.id)
        }
    }


    const stepsDisplay = steps.map((step, index) => {
        return (
            <div key={index} className={css(styles.stepWrapper)}>
                <div className={css(step.current ? styles.stepContentActive : styles.stepContent, step.id < current && styles.pointer)} onClick={() => setCurrentPrev(step)}>
                    <div className={css(step.current ? styles.stepImgContainerActive : styles.stepImgContainer)}><img src={`src/assets/steps/${step.icon}${step.current ? '-white' : ''}.svg`} className={css(step.current ? styles.stepImgActive : styles.stepImg)}/></div>
                    <div className={css(step.current ? styles.stepTitleActive : styles.stepTitle)}>{step.title}</div>
                </div>
                <div className={css(index !== steps.length - 1 && styles.stepDivider)} />
            </div>
        )
    })

    const StepsMobileDisplay = ()=>{


        const [ posX, setPosX ] = useState()
        const [ prevPosX, setPrevPosX ] = useState()

        useEffect(()=>{
            setPrevPosX(posX)
            let newPosX = width/2 - 67 + (current * (-134))
            console.log(newPosX, current)
            setPosX(newPosX)
        },[])
        return (
                <div className={css(styles.stepMobileWrapper)} style={{ left: `${!!posX ? posX : prevPosX}px`}}>
                    {steps.map((step, index) => {
                        return (
                            <div key={index} className={css(styles.stepWrapper)}>
                                <div className={css(step.current ? styles.stepContentActive : styles.stepContent)}>
                                    <div className={css(step.current ? styles.stepImgContainerActive : styles.stepImgContainer)}><img src={`src/assets/steps/${step.icon}${step.current ? '-white' : ''}.svg`} className={css(step.current ? styles.stepImgActive : styles.stepImg)}/></div>
                                    <div className={css(step.current ? styles.stepTitleActive : styles.stepTitle)}>{step.title}</div>
                                </div>
                                <div className={css(index !== steps.length - 1 && styles.stepDivider)} />
                            </div>
                        )
                    })}
                </div>
        )
    }
    
   

    return (
        <div className={css(styles.stepperContainer)}>
            {!!md ? <StepsMobileDisplay /> : width <= 768 ? <StepsMobileDisplay /> : stepsDisplay}
        </div>
    )

}

const styles = StyleSheet.create({
    stepperContainer:{
        display:'flex',
        justifyContent:'space-between',
        width:'100%',
        position:'relative',
        '@media (max-width:600px)':{
            width:'100vw',
            display:'block',
            overflow:'hidden'
        },
        '@media (max-width:768px)':{
            width:'100vw',
            display:'block',
            overflow:'hidden'
        },
    },
    stepWrapper:{
        display:'flex',
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        width:'23%',
        position:'relative',
        marginBottom:'15px',
        '@media (max-width:600px)':{
            marginBottom:'10px',
        },
    },
    stepMobileWrapper:{
        display:'flex',
        justifyContent:'space-between',
        width:'1200px',
        transitionProperty:'left',
        transitionDuration:'0.7s',
        position:'relative'
    },
    stepImgContainer:{
        display:'flex',
        borderRadius:'80%',
        border:'2px solid #4D75E2',
        width:'42px',
        height:'42px',
        marginBottom:'1rem'
    },
    stepImgContainerOld:{
        display:'flex',
        borderRadius:'80%',
        border:'1px solid #1D4F90',
        width:'42px',
        height:'42px',
        marginBottom:'1rem'
    },
    stepImg:{
        margin:'auto',
        width:'18px',
        height:'auto',
    },
    stepTitle:{
        textAlign:'center'
    },
    stepTitleActive:{
        textAlign:'center',
        marginTop:'-10px'
    },
    stepDivider:{
        height:'2px',
        backgroundColor:'#4D75E2',
        position:'absolute',
        width:'68%',
        top:'31%',
        left:'66%'
    },
    stepDividerOld:{
        height:'1px',
        backgroundColor:'#bdbdbd',
        position:'absolute',
        width:'71%',
        top:'31%',
        left:'65%'
    },
    stepImgContainerActive:{
        display:'flex',
        borderRadius:'80%',
        width:'60px',
        height:'60px',
        marginBottom:'1.2rem',
        backgroundColor:'#BF1E23',
        zIndex:'10'
    },
    stepImgActive:{
        margin:'auto',
        width:'24px',
        height:'auto',
    },
    stepContent:{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: '14px auto auto',
    },
    stepContentActive:{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: '5px auto auto',
    },
    pointer:{
        cursor:'pointer'
    },
})

export default Steps