import React, { useState, useRef } from 'react';
import { StyleSheet, css } from 'aphrodite';

const Title = ({text, specific=false}) => {

    return (
        <div className={css(specific ? styles.specificRowTtitle : styles.rowTitle)}>
            <div className={css(styles.title)}>{text}</div>
        </div>
    )
}

const styles = StyleSheet.create({
    specificRowTitle:{
        display:'flex',
        flexDirection:'column',
        margin:'0 0 5px 0'
    },
    rowTitle:{
        display:'flex',
        flexDirection:'column',
        margin:'5px 0'
    },
    title:{
        margin:'3px 0',
        textAlign: 'left',
        font: 'normal normal normal 600 16px Montserrat',
        letterSpacing: '0px',
        color: '#BF1E23',
        opacity: 1,
    }, 
})

export { Title};