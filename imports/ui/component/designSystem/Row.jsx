import React, { useState, useRef } from 'react';
import { StyleSheet, css } from 'aphrodite';

const Row = ({style, children}) => {
    return (
        <div className={css(styles.row, style)}>
            {children}
        </div>
    )
}

const styles = StyleSheet.create({
    row:{
        display:'flex',
        flexDirection:'row',
        margin:'0 0 20px 0',
        '@media (max-width:600px)':{
            flexDirection:'column',
        } 
    },
})

export {Row};