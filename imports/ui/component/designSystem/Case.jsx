import React, { useState, useRef } from 'react';
import { StyleSheet, css } from 'aphrodite';
import useMobileDetect from '../../../hooks/useMobileDetect'


const Case = ({title, icon, iconSize, content, size, after=false, contentSize, disabled}) => {

    const md = useMobileDetect('tablet')


    const AfterContent = () => {

        if(!!after){
            if(after.contentType == 'text'){
                return (
                    <div className={css(styles.inputAfter)}>{after.content}</div>
                )
            }
            if(after.contentType == 'icon'){
                return (
                    <img src={`src/assets/${after.content}.svg`}className={css(styles.inputIconAfter)}/>
                )
            }
        }else{
            return null
        }
    }

    return (
        <div className={css(styles.input, !!md ? styles[100] : styles[size])}>
            {title != undefined && (<label className={css(styles.inputTitle)}>{title}</label>)}
            <div className={css(styles.inputContent,styles.inputStyle, !!disabled && styles.disabled)}>                           
                {!!icon && (<img className={css(styles.inputIcon, styles[iconSize])} src={`src/assets/${icon}.svg`}/>)}
                <div className={css(!!after ? after.position == 'left' ? styles.inputTextSuffixLeft : styles.inputTextSuffixRight : styles.inputText, !icon && styles.inputSolo, !!contentSize && styles[contentSize])}>
                    {content}
                </div>
                <AfterContent />
            </div>
        </div>
    )
}

const styles = StyleSheet.create({
    inputAfter:{
        margin:'auto auto auto 0',
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        letterSpacing: '0px',
        color: '#393939',
    },
    inputIconAfter:{
        margin:'auto 15px auto 10px',
        maringRight:'10px',
        width:'12px'
    },
    input:{
        display:'flex',
        flexDirection:'column',
        paddingRight:'5px'
    },
    inputTitle:{
        margin:'3px 0',
        textAlign: 'left',
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        letterSpacing: '0px',
        color: '#393939',
        opacity: 1,
        height:'19px'
    }, 
    inputContent:{
        paddingLeft:'10px',
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        marginRight:'5%',
        height:'70px'
    },
    inputStyle:{
        display:'flex',
        background: '#E1ECE0 0% 0% no-repeat padding-box',
        border:'1px solid #007100',
        borderRadius: '15px',
        marginRight:'5px',
    },
    disabled:{
        background:'#ddd 0% 0% no-repeat padding-box',
        border:'1px solid grey',
    },  
    inputStyleOld:{
        display:'flex',
        background: 'rgba(191,30,35,0.05) 0% 0% no-repeat padding-box',
        borderRadius: '15px',
        marginRight:'5px'
    },
    inputIcon:{
        margin:'auto 15px auto 10px',
        maringLeft:'10px',
        width:'16px'
    },
    small:{
        width:'12px'
    },
    normal:{
        width:'17px'
    },
    big:{
        width:'25px'
    },
    inputTextSuffixLeft:{
        background: 'none',
        border:'none',
        letterSpacing: '0px',
        color: '#393939',
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        margin:'auto 0',
        width:'25%',
        ':focus':{
            outline:'none'
        },
    },
    inputTextSuffixRight:{
        background: 'none',
        border:'none',
        letterSpacing: '0px',
        color: '#393939',
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        margin:'auto 0',
        width:'25%',
        ':focus':{
            outline:'none'
        },
    },
    inputText:{
        background: 'none',
        border:'none',
        letterSpacing: '0px',
        color: '#393939',
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        margin:'auto auto auto 0',
        width:'70%',
        ':focus':{
            outline:'none'
        },
    },
    inputSolo:{
        marginLeft:'10px'
    },
    100:{
        width:'100%'
    },
    75:{
        width:'75%'
    },
    50:{
        width:'50%'
    },
    25:{
        width:'25%' 
    },
    33:{
        width:'calc(100%/3)'
    },
    20:{
        width:'20%'
    }

})

export {Case};