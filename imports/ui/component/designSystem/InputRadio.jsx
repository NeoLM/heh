import React, { useState, useRef } from 'react';
import { StyleSheet, css } from 'aphrodite';

const InputRadio = ({onChange, value, content, error}) => {

    const AfterContent = ({after}) => {

        if(!!after){
            if(after.contentType == 'text'){
                return (
                    <div className={css(styles.inputAfter)}>{after.content}</div>
                )
            }
            if(after.contentType == 'icon'){
                return (
                    <img src={`src/assets/${after.content}.svg`}className={css(styles.inputIconAfter)}/>
                )
            }
        }else{
            return null
        }
    }




    return (
        <form className={css(styles.input, styles[100])}>
            <div className={css(styles.inputTitle)}>{content.title}</div>
            <div className={css(styles.inputContentRadio)}>
                {content.radios.map((input, index)=>{
                    return (
                        <div key={index}  className={css(styles.inputStyle, styles[input.size], error && styles.error)}>
                            <label className={css(styles.labelRadio)}>
                                <input type='radio' name={content.name} value={input.value} className={css(styles.inputRadio)} defaultChecked={value === input.value} onChange={onChange}/>
                                <div className={css(styles.labelRadioTexte)}>{input.label}</div>
                                <AfterContent after={input.after}/>
                            </label>
                        </div>
                    )
                })}
            </div>
        </form>
    )
}

const styles = StyleSheet.create({
    inputAfter:{
        margin:'auto auto auto 0',
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        letterSpacing: '0px',
        color: '#393939',
    },
    inputIconAfter:{
        margin:'auto 15px auto 10px',
        maringRight:'10px',
        width:'12px'
    },
    input:{
        display:'flex',
        flexDirection:'column',
        paddingRight:'5px'
    },
    inputTitle:{
        margin:'3px 0',
        textAlign: 'left',
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        letterSpacing: '0px',
        color: '#393939',
        opacity: 1,
        height:'19px'
    }, 
    inputContent:{
        paddingLeft:'10px',
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        marginRight:'5%',
        height:'70px'
    },
    inputStyle:{
        display:'flex',
        background: '#E1ECE0 0% 0% no-repeat padding-box',
        border:'1px solid #007100',
        borderRadius: '15px',
        marginRight:'5px',
    },
    inputStyleOld:{
        display:'flex',
        background: 'rgba(191,30,35,0.05) 0% 0% no-repeat padding-box',
        borderRadius: '15px',
        marginRight:'5px'
    },
    inputIcon:{
        margin:'auto 15px auto 10px',
        maringLeft:'10px',
        width:'16px'
    },
    inputTextSuffixLeft:{
        background: 'none',
        border:'none',
        letterSpacing: '0px',
        color: '#39393967',
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        margin:'auto 0',
        width:'25%',
        ':focus':{
            outline:'none'
        },
    },
    inputTextSuffixRight:{
        background: 'none',
        border:'none',
        letterSpacing: '0px',
        color: '#39393967',
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        margin:'auto 0',
        width:'25%',
        ':focus':{
            outline:'none'
        },
    },
    inputText:{
        background: 'none',
        border:'none',
        letterSpacing: '0px',
        color: '#39393967',
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        margin:'auto auto auto 0',
        width:'70%',
        ':focus':{
            outline:'none'
        },
    },
    inputContentRadio:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        height:'70px'
    },
    labelRadio:{
        display:'flex',
        width:'100%'
    },
    inputRadio:{
        margin:'auto 10px',
        background: '#FFFFFF 0% 0% no-repeat padding-box',
        border: '1px solid #4D75E279',
        opacity: 1,
        paddingLeft:'5px',
        '@media (max-width:600px)':{
            margin:'auto 0 auto 5px'
        },
    },
    labelRadioTexte:{
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        letterSpacing: '0px',
        color: '#393939',
        margin:'auto 10px',
        '@media (max-width:600px)':{
            margin:'auto'
        },
    },
    100:{
        width:'100%'
    },
    75:{
        width:'75%'
    },
    50:{
        width:'50%'
    },
    25:{
        width:'25%' 
    },
    33:{
        width:'calc(100%/3)'
    },
    20:{
        width:'20%' 
    },
    error:{
        border:'1px solid rgb(191,30,35)'
    }
})

export {InputRadio};