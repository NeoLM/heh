import React, { useState, useRef } from 'react';
import { StyleSheet, css } from 'aphrodite';

const TextArea = ({onChange, value, name, size, title, error}) => {

    return (
        <div className={css(styles.input, styles[size])}>
            <label className={css(styles.inputTitle)}>{title}</label>
            <textarea onChange={onChange} value={value} name={name} className={css(styles.inputContent,styles.inputStyle, styles.inputText, error && styles.error)}>                           
            </textarea>
        </div>
    )
}

const styles = StyleSheet.create({
    input:{
        display:'flex',
        flexDirection:'column',
        paddingRight:'5px'
    },
    inputTitle:{
        margin:'3px 0',
        textAlign: 'left',
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        letterSpacing: '0px',
        color: '#393939',
        opacity: 1,
    }, 
    inputContent:{
        padding:'10px',
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        marginRight:'5%',
        height:'186px',
        boxSizing: 'border-box',
    },
    inputStyle:{
        display:'flex',
        background: '#E1ECE0 0% 0% no-repeat padding-box',
        border:'1px solid #007100',
        borderRadius: '15px',
        marginRight:'5px',
    },
    inputStyleOld:{
        display:'flex',
        background: 'rgba(191,30,35,0.05) 0% 0% no-repeat padding-box',
        borderRadius: '15px',
        marginRight:'5px',
    },
    inputText:{
        // border:'none',
        letterSpacing: '0px',
        color: '#393939',
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        margin:'auto auto auto 0',
        width:'100%',
        ':focus':{
            outline:'none'
        },
    },
    100:{
        width:'100%'
    },
    75:{
        width:'75%'
    },
    50:{
        width:'50%'
    },
    25:{
        width:'25%' 
    },
    33:{
        width:'calc(100%/3)'
    },
    20:{
        width:'20%'
    },
    error:{
        border:'1px solid rgb(191,30,35)'
    }

})
export {TextArea}