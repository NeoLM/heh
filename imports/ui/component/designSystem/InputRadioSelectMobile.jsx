import React, { useState, useRef } from 'react';
import { StyleSheet, css } from 'aphrodite';

const InputRadioSelectMobile = ({size, icon, title, id, selected, validated, setCurrent, error, children}) => {

    // const [isCurrent, setIsCurrent] = useState(selected)
    // const [isValidated, setIsValidated] = useState(validated)
    
    const [ isActive, setIsActive ] = useState(false)

    const handleClick = ()=>{
        const newActiveState = !isActive
        setCurrent(id)
        setIsActive(newActiveState)
    }


    return (
        <>
            <div onClick={handleClick} className={css(styles.input, styles[size])}>
                <div className={css(styles.inputContent,styles.inputStyle, error && styles.error)}>                           
                    <img className={css(styles.inputIcon)} src={`src/assets/${icon}.svg`}/>
                    <div className={css(styles.inputTextSuffixleft)}>{title}</div>
                    <div className={css(isActive ? styles.inputIconAfter : validated ? styles.inputIconAfterValidated : styles.inputIconAfter)}>
                        {isActive 
                        ? (<div className={css(styles.selected)}/>) 
                        :  validated && (<div className={css(styles.validated)}>&#10003;</div>)}
                    </div>
                </div>
            </div>
            <div className={css(styles.contentSliderContainer, !!isActive && styles.contentActive)}>
                {!!isActive && children}
            </div>
        </>
    )
}

const styles = StyleSheet.create({
    contentSliderContainer:{
        display:'flex',
        maxHeight:'0%',
        transition:'1s',
        overflow: 'hidden',
    },  
    contentActive:{
        maxHeight:'100%'
    },
    input:{
        display:'flex',
        flexDirection:'column',
        paddingRight:'5px',
        cursor:'pointer'
    },
    inputIcon:{
        margin:'auto 15px auto 10px',
        maringLeft:'10px',
        width:'25px'
    },
    inputContent:{
        paddingLeft:'10px',
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        marginRight:'5%',
        height:'70px'
    },
    inputStyle:{
        display:'flex',
        background: '#E1ECE0 0% 0% no-repeat padding-box',
        border:'1px solid #007100',
        borderRadius: '15px',
        marginRight:'5px',
    },
    inputStyleOld:{
        display:'flex',
        background: 'rgba(191,30,35,0.05) 0% 0% no-repeat padding-box',
        borderRadius: '15px',
        marginRight:'5px'
    },
    inputIconAfter:{
        display:'flex',
        margin:'auto 15px auto 10px',
        maringRight:'10px',
        width:'20px',
        height:'20px',
        background: '#FFFFFF 0% 0% no-repeat padding-box',
        border: '1px solid #4D75E279',
        borderRadius:'50%',
        opacity: 1,
    },
    inputIconAfterValidated:{
        display:'flex',
        margin:'auto 15px auto 10px',
        maringRight:'10px',
        width:'20px',
        height:'20px',
        background: '#4D75E279 0% 0% no-repeat padding-box',
        border: '1px solid #4D75E279',
        borderRadius:'50%',
        opacity: 1,
    },
    selected:{
        margin:'auto',
        width:'10px',
        height:'10px',
        background:'#AABDF1 0% 0% no-repeat padding-box',
        borderRadius:'50%'
    },
    validated:{
        margin:'auto',
        color:'white',
        fontSize:'15px',
    },
    inputTextSuffixleft:{
        background: 'none',
        border:'none',
        letterSpacing: '0px',
        color: '#393939',
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        margin:'auto auto auto 0',
        width:'55%',
        ':focus':{
            outline:'none'
        },
    },
    100:{
        width:'100%'
    },
    50:{
        width:'50%'
    },
    error:{
        border:'1px solid rgb(191,30,35)'
    }
})

export {InputRadioSelectMobile} 