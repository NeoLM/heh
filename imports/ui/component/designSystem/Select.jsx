import React, { useState, useRef, useEffect } from 'react';
import { StyleSheet, css } from 'aphrodite';
import useMobileDetect from '../../../hooks/useMobileDetect'

const Select = ({label, title, list, resetThenSet, size, value, error}) => {


    const md = useMobileDetect('tablet')

    const [isOpen, setIsOpen] = useState(false)
    const [headerTitle, setHeaderTitle] = useState(title)

    const toggleList = () =>{
        prevState = isOpen
        setIsOpen(!isOpen)
        console.log()
    }

    const selectItem = (item) =>{
        
        const {icon, iconSize=title?.iconSize || 'small', content, id, key} = item
        
        const newTitle = {
            icon:icon,
            iconSize:iconSize,
            content:content
        }
        setHeaderTitle(newTitle)
        setIsOpen(false)
        resetThenSet(id, key)
    }

    useEffect(()=>{
        if(!!value){
            let id, key, newTitle
            list.forEach((elem)=>{
                if(elem.content === value){
                    id = elem.id
                    key = elem.key
                    newTitle ={
                        icon:elem.icon,
                        iconSize:title?.iconSize || 'small',
                        content:elem.content
                    }
                }
            })
            setHeaderTitle(newTitle)
            resetThenSet(id,key)
        }
    },[])

    const ButtonList = ({listElement}) => {
        return (listElement.map((item)=>{
            return(<button key={item.id} className={css(item.selected ? styles.dropdownListItemSelected : styles.dropdownListItem, listElement.length == item.id +1 && styles.dropdownListItemLast)} onClick={()=>selectItem(item)}>
                {!!item.icon && (<img src={`src/assets/${item.icon}.svg`}className={css(styles.inputIcon, styles[item.iconSize])}/>)}
                <div className={css(item.selected ? styles.dropdownListItemTextSelected : styles.dropdownListItemText)}>{item.content}</div>
            </button>)
        }))
    }

    return (
        <div className={css(styles.input, !!md ? styles[100] : styles[size])}>
            {label != undefined && (<label className={css(styles.inputTitle)}>{label}</label>)}
            <div className={css(styles.dropdownWrapper, styles.inputContent, (isOpen ? styles.inputStyleOpen : styles.inputStyle), error && styles.error)}>
                <button className={css(styles.dropdownHeader)} onClick={toggleList}>
                    {!!headerTitle.icon && (<img src={`src/assets/${headerTitle.icon}.svg`}className={css(styles.inputIcon, styles[headerTitle.iconSize])}/>) }
                    <div className={css(styles.dropdownHeaderTitle)}>{headerTitle.content}</div>
                    <span className={css(isOpen ? styles.chevronRotate : styles.chevron)}>&#x2304;</span>
                </button>
                {isOpen && 
                    (<div className={css(styles.dropdownList, error && styles.errorDropdown)}>
                        <ButtonList listElement={list}/>
                    </div>
                )}
            </div>
        </div>
    )
}

const styles = StyleSheet.create({
    dropdownWrapper:{

    },
    dropdownHeader:{
        border:'none',
        background:'none',
        display:'flex',
        minHeight:'100%',
        cursor:'pointer'
    },
    dropdownHeaderTitle:{
        margin:'auto auto auto 0',
        font:`'Montserrat',sans serif`,
        fontSize:'15px',    
    },
    dropdownList:{
        display: 'flex',
        flexDirection: 'column',
        background: '#E1ECE0',
        borderLeft:'1px solid #007100',
        borderRight:'1px solid #007100',
        borderBottom:'1px solid #007100',
        zIndex:'100',
        cursor:'pointer',
        borderRadius:'0 0 15px 15px'
    },
    dropdownListOld:{
        display: 'flex',
        flexDirection: 'column',
        background: 'white',
        zIndex:'100',
        cursor:'pointer',
        borderRadius:'0 0 15px 15px'
    },
    dropdownListItem:{
        border:'none',
        background:'0% 0% no-repeat padding-box padding-box #E1ECE0',
        height:'30px',
        display:'flex',
        ':hover':{
            background:'#AEC0A3 0% 0% no-repeat padding-box',
        },
        cursor:'pointer'
    },
    dropdownListItemOld:{
        border:'none',
        background:'0% 0% no-repeat padding-box padding-box rgba(191, 30, 35, 0.05)',
        height:'30px',
        display:'flex',
        ':hover':{
            background:'rgba(191,30,35,0.15) 0% 0% no-repeat padding-box',
        },
        cursor:'pointer'
    },
    dropdownListItemLast:{
        borderRadius: '0 0 15px 15px',
    },
    dropdownListItemSelected:{
        border:'none',
        background:'#AEC0A3 0% 0% no-repeat padding-box',
        height:'30px',
        display:'flex',
        cursor:'pointer'
    },
    dropdownListItemSelectedOld:{
        border:'none',
        background:'rgba(191,30,35,0.15) 0% 0% no-repeat padding-box',
        height:'30px',
        display:'flex',
        cursor:'pointer'
    },
    dropdownListItemText:{
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        width:'70%',
        margin:'auto auto auto 0',
        textAlign: 'left',
    },
    dropdownListItemTextSelected:{
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        color:'#007100',
        width:'70%',
        margin:'auto auto auto 0',
        textAlign: 'left',
    },
    dropdownListItemTextSelectedOld:{
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        color:'#BF1E23',
        width:'70%',
        margin:'auto auto auto 0',
        textAlign: 'left',
    },
    chevron:{
        color:'#BF1E23',
        width:'11px',
        margin:'auto 10px auto auto'
    },
    chevronRotate:{
        color:'#BF1E23',
        width:'11px',
        transform:'rotate(180deg)',
        margin:'auto 10px auto auto',
        paddingBottom: '15px',
    },
    inputStyle:{
        display:'flex',
        background: '#E1ECE0 0% 0% no-repeat padding-box',
        border:'1px solid #007100',
        borderRadius: '15px',
    },
    inputStyleOld:{
        display:'flex',
        background: 'rgba(191,30,35,0.05) 0% 0% no-repeat padding-box',
        borderRadius: '15px',
    },
    inputStyleOpen:{
        display:'flex',
        background: '#E1ECE0 0% 0% no-repeat padding-box',
        border:'1px solid #007100',
        borderRadius: '15px 15px 0 0',
    },
    inputStyleOpenOld:{
        display:'flex',
        background: 'rgba(191,30,35,0.05) 0% 0% no-repeat padding-box',
        borderRadius: '15px 15px 0 0',
    },
    inputContent:{
        display:'flex',
        flexDirection:'column',
        justifyContent:'space-between',
        marginRight:'5px',
        height:'70px'
    },
    inputTitle:{
        margin:'3px 0',
        textAlign: 'left',
        font:`'Montserrat',sans serif`,
        fontSize:'15px',
        letterSpacing: '0px',
        color: '#393939',
        opacity: 1,
        height:'19px'
    }, 
    input:{
        display:'flex',
        flexDirection:'column',
        paddingRight:'5px'
    },
    inputIcon:{
        margin:'auto 15px auto 10px',
        maringLeft:'10px',
        width:'12px'
    },
    small:{
        width:'12px'
    },
    normal:{
        width:'17px'
    },
    big:{
        width:'25px'
    },
    100:{
        width:'100%'
    },
    75:{
        width:'75%'
    },
    50:{
        width:'50%'
    },
    25:{
        width:'25%' 
    },
    33:{
        width:'calc(100%/3)'
    },
    20:{
        width:'20%'
    }, 
    error:{
        border:'1px solid rgb(191,30,35)'
    },
    errorDropdown:{
        borderLeft:'1px solid rgb(191,30,35)',
        borderRight:'1px solid rgb(191,30,35)',
        borderBottom:'1px solid rgb(191,30,35)'
    }
})

export {Select}