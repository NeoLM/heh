import React, { useState, useRef } from 'react';
import { StyleSheet, css } from 'aphrodite';

const Column = ({children}) => {
    return (
        <div className={css(styles.column)}>
            {children}
        </div>
    )
}

const styles = StyleSheet.create({
    column:{
        display:'flex',
        flexDirection:'column',
        width:'48%',
        '@media (max-width:600px)':{
            width:'100%'
        },
        '@media (max-width:768px)':{
            width:'100%'
        },

    },
})

export {Column};